#For more information on configuration, see:

#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user  nginx;
worker_processes  1;


pid        /run/nginx.pid;


events {
    worker_connections  1024;
}


http {

    map $remote_addr $log_ua {
        "192.168.12.55" 0;
        default 1;
    }

    include       /etc/nginx/mime.types;
    #include blockips.conf;
    default_type  application/octet-stream;

    log_format upstreamlog '[$time_local] $remote_addr - $remote_user - $server_name to: $upstream_addr: $request upstream_response_time $upstream_response_time msec  request_time $request_time';

    access_log  /home/enterobase/access.log  upstreamlog;
    error_log  /home/enterobase/error.log;
    proxy_read_timeout 600s;
    proxy_connect_timeout 600s;
    limit_req_zone $binary_remote_addr zone=apilimit:10m rate=2r/s;
    limit_req_status 429;
    limit_conn_status 429;
   
    sendfile        on;
    sendfile_max_chunk 1m;
    tcp_nopush     on;
    client_max_body_size 3000M;
    keepalive_timeout  65;

    gzip  on;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript application/gunzip on;
    gunzip on;

    index   index.html index.htm;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;
  upstream backend_hosts {
#       server enterobase.dsmz.de max_fails=0 fail_timeout=30s;
        server enterobase.dsmz.de:8000 max_fails=10 fail_timeout=30s;
#        server tomva.lnx.warwick.ac.uk:8000 max_fails=10 fail_timeout=30s;
#        server olkep.lnx.warwick.ac.uk:8000 max_fails=10 fail_timeout=30s;
  }


#  server{
# 	server_name mlst.warwick.ac.uk;
#    	location /mlst/dbs/Ecoli {
# 		return 301 https://enterobase.dsmz.de/species/ecoli/allele_st_search;
#    	}
# 	location /mlst/dbs/Senterica{
# 		return 301 https://enterobase.dsmz.de/species/senterica/allele_st_search;
# 	}
# 	location /mlst/dbs/Ypseudotuberculosis {
# 		return 301 https://enterobase.dsmz.de/species/yersinia/allele_st_search;
# 	}
#    	location /mlst/dbs/Mcatarrhalis {
#                 return 301 https://enterobase.dsmz.de/species/mcatarrhalis/allele_st_search;
#         }
#   	location / {
# 		return 301 https://enterobase.dsmz.de/warwick_mlst_legacy;
#     }
#   }


  server {
        listen       80;
        listen       443 http2 ssl;
        server_name  enterobase.dsmz.de;
        root         /usr/share/nginx/html;
        ssl_certificate /etc/ssl/enterobase_dsmz_de_issuer_after.pem;
        ssl_certificate_key /etc/ssl/private/enterobase.key;
        
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
        ssl_dhparam /etc/pki/tls/certs/dhparam.pem;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
             proxy_pass http://backend_hosts; 

            proxy_set_header X-Real-IP  $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Forwarded-Port 443;
            proxy_set_header Host $host;
            rewrite http://enterobase.dsmz.de/ /warwick_mlst_legacy;
            add_header X-Proxy-Upstream $upstream_addr;                  
            add_header X-Proxy-Tag $cookie_session;                  
        }

        location /view_jbrowse {
             rewrite ^/(view_jbrowse.*) /$1 break;
	         proxy_pass http://backend_hosts;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }

      

        location /upload {
             rewrite ^/(upload.*) /$1 break;
             proxy_pass http://backend_hosts;
    #  	     proxy_pass http://enterobase.dsmz.de:8000;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;                  
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
        location /api/v2.0/ {
             rewrite ^/(api.*) /$1 break;
             proxy_pass http://backend_hosts;
#	        proxy_pass http://enterobase.dsmz.de:8000;
             proxy_set_header Host $host;
             add_header X-Proxy-Upstream $upstream_addr;                  
             proxy_set_header X-Real-IP  $remote_addr;
             proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	     limit_req zone=apilimit;
   	    }
   	#######Added #######
   	location /api/v2.0/swagger-ui {
        rewrite ^/(api.*) /$1 break;
        proxy_pass http://backend_hosts;
        proxy_set_header Host $host;
        add_header X-Proxy-Upstream $upstream_addr;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
    location ~ grapetree_remote\/https:\/(.*)$ {
        proxy_buffer_size 128k;
        proxy_buffers 4 256k;
        proxy_busy_buffers_size 256k;
        if ($request_method = 'OPTIONS') {
        add_header Access-Control-Allow-Origin $http_origin;
        add_header Access-Control-Allow-Credentials true;
        add_header Access-Control-Allow-Headers Origin,Content-Type,Accept,X-Requested-With;
        add_header Access-Control-Allow-Methods GET,POST,OPTIONS,PUT,DELETE;
        add_header Content-Length 0;
        add_header Content-Type text/plain;}
        return 204;
    }
   	##################

# 	location ~ hierBG.*{
#              deny all;
#              return 403;
#         }

# 	location ~ /schemes/(hierBG.*|.*rMLST|Listeria.*|Bacillus.*|Helicobacter.*|Mycobacterium.*|Neirsseria.*|Photorhabdus.*|x.*|Vibrio.*|klebsiella.*|daily.*){
#              deny all;
#              return 403;
#         }
        location /as {
            deny all;
            alias /share_space/interact/outputs/;
            location ~ (.fasta|.fastq|.gbk.gz|.gff.gz){
                allow all;
            }
        }
        location /old_as {
            deny all;
            alias /share_space/assembly/;
            location ~ (.fasta|.fastq){
                allow all;
            }
        }
# 	location /schemes {
# 	    autoindex on;
#             alias /data/NServ_dump/;
# 	}
#         location ~ /(ipfs.*|ipns.*) {
#              rewrite ^/(ipfs.*|ipns.*) /$1 break;
# 	     proxy_pass http://enterobase.dsmz.de:8020;
#              add_header X-Proxy-Upstream $upstream_addr;
# 	}
# 	location /sparse {
# 	     alias /share_space/metagenome/database/SPARSE/;
# 	     autoindex on;
# 	}
       location /static {
	     root /var/www/entero/entero;
           add_header X-Proxy-Cache $upstream_cache_status;                  

        }
#         location /anvi_public {
#             rewrite ^/anvi_public/(.*)$ https://enterobase.dsmz.de/anvio/public/zhemin/$1  permanent;
#             add_header X-Proxy-Upstream $upstream_addr;
#         }
#         location /anvio {
#                 rewrite ^/(anvio.*) /$1 break;
#                 # proxy_pass http://enterobase.dsmz.de;
# 	        proxy_pass http://enterobase.dsmz.de:8000;
#                 add_header X-Proxy-Upstream $upstream_addr;
# 	}

       location /entero_jbrowse {
                rewrite ^/(entero_jbrowse.*) /$1 break;
                # proxy_pass http://enterobase.dsmz.de;
	        proxy_pass http://enterobase.dsmz.de:8000;
                 proxy_set_header Host $host;
                add_header X-Proxy-Upstream $upstream_addr;
                proxy_set_header X-Real-IP  $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        }
        location ~(search_strains|upload_reads|register|login) {
             proxy_pass http://backend_hosts; 
            proxy_set_header Host             $host;
            proxy_set_header X-Real-IP        $remote_addr;
            proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
            proxy_set_header X-Accel-Internal /internal-nginx-static-location;
            ###Added###
            proxy_set_header X-Forwarded-Proto https;
            proxy_set_header X-Forwarded-Port 443;
            ########
#           if ($scheme = https) {
#             return 301 http://$server_name$request_uri;
#          }
         }
        location /robots.txt {
            alias /var/www/entero/entero/static/robots.txt;
        }
    }
}
