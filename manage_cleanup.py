import logging
import os, sys, shutil, signal
from pathlib import Path
import itertools
import glob
import json
import datetime, time
import subprocess
from csv import writer, DictWriter
from flask_script import Manager
from entero import create_app, db, app, get_database
from entero.databases.system.models import UserUploads, User, UserJobs, UserPreferences
from collections import OrderedDict
import sqlalchemy

# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)
gb_ = 1024 * 1024 * 1024


def output_reads_to_be_deleted(users_uploads_sizes, mode, start):
    print("Number of users to delete their reads: ", len(users_uploads_sizes))
    users_uploads_sizes_sorted_by_value = OrderedDict(
        sorted(users_uploads_sizes.items(), key=lambda x: x[1], reverse=True))
    # print(users_uploads_sizes_sorted_by_value.keys()[0] ,users_uploads_sizes_sorted_by_value[users_uploads_sizes_sorted_by_value.keys()[0]])
    lines = [b'Id, Username, First name,LastName,Institution, Read files size (GB)']
    for user_id, files_size in users_uploads_sizes_sorted_by_value.items():
        if files_size > 0:
            user = db.session.query(User).filter(User.id == user_id).one()
            lines.append((str(user.id) + "," + user.username + "," + user.firstname + "," + user.lastname + ",\"" +
                          user.institution + "\"," + str(float(files_size) / gb_)).encode('utf-8').strip())

    files_string = b'\n'.join(lines)
    output_file_ = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'reads_to_be_deleted_%s_%s.csv' % (mode,str(start)))
    f = open(output_file_, 'wb')
    f.write(files_string)
    f.close()

def get_exempt_users(shared_ebi_folder):
    exempt_users_file = os.path.join(shared_ebi_folder, 'users_EBI.txt')

    if os.path.isfile(exempt_users_file):
        with open(exempt_users_file) as f:
            usernames = f.read().splitlines()
    else:
        print(("file %s is not exist" % exempt_users_file))
        return None
    # sql="select * from users where username in %s"%','.join(usernames)
    for i in range(0, len(usernames)):
        usernames[i] = usernames[i].strip()

    users = db.session.query(User).filter(User.username.in_(usernames)).all()
    counter = 1
    users_ids = []
    user_ids_ = []
    for user in users:
        #print(counter, user.firstname, user.lastname, ', ', user.institution)
        users_ids.append(str(user.id))
        # user_ids_.append(user.id)
        counter += 1

    return usernames, users_ids

@manager.command
@manager.option('-m', '--mode', help='test or delete mode')
def delete_user_reads(mode='test'):
    '''
    It deletes the user read files to free the storage
    If exempts user reads who their names provided inside exempt_users_file, it will read the file from the cache folder,
    the file in cash folder contains user names of our research group in addition to the user who permitted us to
    upload their reads to EBI
    :param mode: run mode, test or delete
    :return:
    '''
    start = datetime.datetime.now()
    shared_ebi_folder = app.config["EBI_SHARED_FOLDER"]

    usernames, users_ids = get_exempt_users(shared_ebi_folder)
    if usernames is None:
        return

    status_file = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'delete_user_reads_%s_%s.txt' % (mode,str(start)))
    current_time = datetime.datetime.utcnow()
    eleven_days_ago = current_time - datetime.timedelta(days=11)

    #    reads_files = db.session.query(UserUploads).filter(UserUploads.species.in_(dbass))
    reads_files = db.session.query(UserUploads). \
        filter(UserUploads.user_id.notin_(users_ids)).filter(UserUploads.status == 'Assembled'). \
        filter(UserUploads.date_uploaded < eleven_days_ago).all()
    # total_s=0
    # for database_name, dbase in dbhandle.items():
    #    sql="select * from strains where owner not in (%s)"%",".join(users_ids)
    #    results=dbase.execute_query(sql)
    #    for res in results:
    #        sql_='select * from taces wehere strain_id=%s'%res['id']
    #        trscs=dbase.execute_query(sql_)
    #        #for trc in tr
    #    print(len(results))

    # return

    total_size = 0
    no_record = 0
    not_exist = 0
    users_uploads_sizes = {}
    # print(','.join(users_ids))
    # sys.exit()
    print("Removing read files, please wait ......")
    nt_included = 0
    con = 0
    for read_file in reads_files:
        con = con + 1
        # print("%s of %s"%(con, len(reads_files)))
        if str(read_file.user_id) in users_ids:
            print ("Error: user id is in ", read_file.user_id)
            sys.exit()
        if read_file.file_name and read_file.file_location:
            p_file = os.path.join(read_file.file_location, read_file.file_name)
            if os.path.isfile(p_file):
                # if p_file.is_file():
                data = json.loads(read_file.data)
                # if data["seq_platform"] =="Complete Genome":
                #    continue
                if data["seq_platform"].strip().lower() != "ILLUMINA,ILLUMINA".lower() \
                        or read_file.status.strip().lower() != 'Assembled'.lower() or \
                        data['seq_library'].strip().lower() != 'Paired,Paired'.lower() or \
                        data['seq_library'].strip().lower() == 'Single'.lower():
                    # print("not included ..........", p_file)
                    nt_included += 1
                    # print(data)
                    # print(data.keys())
                    continue  # print(data["seq_platform"])


                file_size = os.path.getsize(p_file)
                total_size += file_size
                if read_file.user_id in users_uploads_sizes:
                    users_uploads_sizes[read_file.user_id] = users_uploads_sizes[read_file.user_id] + file_size
                else:
                    users_uploads_sizes[read_file.user_id] = file_size

                if mode == 'delete':
                    os.remove(p_file)
                    print('%s deleted' % p_file)
                else:
                    print('test mode, %s not deleted' % p_file)
                f = open(status_file, 'a')
                f.write('{0}\t{1}\n'.format(round(file_size / 1000000.0, 1), p_file))
                f.close()
            else:
                not_exist += 1
                # print("%s is not a file"%p_file)
                #print(read_file.file_name, read_file.file_location)

        else:
            no_record += 1
            # print("Missing file ...")
            # print(read_file.file_name, read_file.file_location)

    output_reads_to_be_deleted(users_uploads_sizes, mode, start)

    print("==================================================================================")
    print(len(reads_files))
    print("total_size:", total_size / gb_ / 1024)
    print("no_record", no_record)
    print("not_exist:", not_exist)
    print("nt_included: ", nt_included)
    print("================================================================================")
    end = datetime.datetime.now()
    print("start:", start)
    print("end time: ", end)
    # import pandas as pd
    # data_files=pd.DataFrame(list(users_uploads_sizes_sorted_by_value.items()), columns=['User', 'Files_reads_size'])

@manager.command
@manager.option('-m', '--mode', help='test or delete mode')
def delete_user_reads2(mode='test'):
    # Delete user files associated with deleted users
    start = datetime.datetime.now()
    status_file = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'delete_user_reads_old_user%s_%s.txt' % (mode,str(start)))
    users = User.query.all()
    userids = set(str(user.id) for user in users)
    total_size = 0
    print("Removing read files, please wait ......")
    con = 0
    user_read_dir = app.config.get('FILE_UPLOAD_DIR')
    user_dirs = [f.name for f in os.scandir(user_read_dir) if f.is_dir()]

    for user in user_dirs:
        if user not in userids:
            species_dirs = [f.name for f in os.scandir(os.path.join(user_read_dir,user)) if f.is_dir()]
            for species in species_dirs:
                files = glob.glob(os.path.join(user_read_dir,user,species,"*.gz"))
                for file in files:
                    filename = os.path.join(user_read_dir,user,species,file)
                    file_size = os.path.getsize(filename)
                    file_date = os.path.getctime(filename)
                    file_date = datetime.datetime.fromtimestamp(file_date).strftime('%Y-%m-%dT%H:%M:%S')

                    if mode == 'delete':
                        os.remove(filename)
                        print('%s deleted' % filename)
                    else:
                        print('test mode, %s not deleted' % filename)

                    print(filename)
                    f = open(status_file, 'a')
                    f.write('{0}\t{1}\t{2}\n'.format(file_size, file_date, filename))
                    f.close()
                    con += 1
                    total_size += file_size

    print ("==================================================================================")
    print ("Number of files:{0}".format(con))
    print ("total_size (Tbyte):", total_size / gb_ / 1024)
    print ("================================================================================")
    end = datetime.datetime.now()
    print ("start:", start)
    print ("end time: ", end)

@manager.command
@manager.option('-m', '--mode', help='test or delete mode')
def delete_user_reads3(mode='test'):
    # Delete user read files that have been marked as having been successfully uploaded to EBI
    start = datetime.datetime.now()

    shared_ebi_folder = app.config["EBI_SHARED_FOLDER"]

    files_to_be_deleted = os.path.join(shared_ebi_folder, 'files_to_be_deletd.txt')
    status_file = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'delete_EBI_uploaded_user_reads_%s_%s.txt'
                               % (mode, str(start)))

    if os.path.isfile(files_to_be_deleted):
        with open(files_to_be_deleted) as f:
            EBI_files = f.read().splitlines()
    else:
        print ("Missing %s file " % files_to_be_deleted)

    total_size = 0
    not_exist = 0
    print("Removing read files, please wait ......")
    con = 0
    users_uploads_sizes = {}
    for read_file in EBI_files:
        con = con + 1
        if os.path.isfile(read_file):
            file_size = os.path.getsize(read_file)
            total_size += file_size
            file_size = round(file_size / 1000000.0, 1)
            userid = int(read_file.split('/')[3])
            if userid in users_uploads_sizes:
                users_uploads_sizes[userid] = users_uploads_sizes[userid] + file_size
            else:
                users_uploads_sizes[userid] = file_size

            if mode == 'delete':
                os.remove(read_file)
                print('%s deleted' % read_file)
            else:
                print('test mode, %s not deleted' % read_file)

            f = open(status_file, 'a')
            f.write('{0}\t{1}\n'.format(file_size, read_file))
            f.close()

        else:
            not_exist += 1
            f = open(status_file, 'a')
            f.write('Not present\t{0}\n'.format(read_file))
            f.close()

            print("%s is not a file"% read_file)

    if mode == 'delete':
        os.remove(files_to_be_deleted)
        f = open(files_to_be_deleted, 'a')
        f.write('\n')
        f.close()

    output_reads_to_be_deleted(users_uploads_sizes, mode, start)

    print ("==================================================================================")
    print ("Number of files:{0}".format(len(EBI_files)))
    print ("total_size (Tbyte):", total_size / gb_ / 1024)
    print ("not_exist:", not_exist)
    print ("================================================================================")
    end = datetime.datetime.now()
    print ("start:", start)
    print ("end time: ", end)



@manager.command
@manager.option('-m', '--mode', help='test or delete mode')
def delete_user_reads4(mode='test'):
    '''
    It deletes the user read files to free the storage
    Deletes files where the assembly never actually started
    '''
    start = datetime.datetime.now()
    status_file = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'delete_uploaded_unused_reads_%s_%s.txt' %
                               (mode, str(start)))
    users = db.session.query(User).all()
    counter = 1
    users_ids = []
    user_ids_ = []
    for user in users:
        print(counter, user.firstname, user.lastname, ', ', user.institution)
        users_ids.append(str(user.id))
        user_ids_.append(user.id)
        counter += 1

    current_time = datetime.datetime.utcnow()
    hundred_days_ago = current_time - datetime.timedelta(days=100)

    #    reads_files = db.session.query(UserUploads).filter(UserUploads.species.in_(dbass)).\
    reads_files = db.session.query(UserUploads).\
        filter(UserUploads.status.in_(['Uploaded'])).\
        filter(UserUploads.date_uploaded < hundred_days_ago).all()

    total_size = 0
    no_record = 0
    not_exist = 0
    users_uploads_sizes = {}
    print("Removing read files, please wait ......")
    nt_included = 0
    con = 0
    for read_file in reads_files:
        con = con + 1
        if read_file.file_name and read_file.file_location:
            p_file = os.path.join(read_file.file_location, read_file.file_name)
            if os.path.isfile(p_file):
                data = json.loads(read_file.data)
                if data["seq_platform"].strip().lower() != "ILLUMINA,ILLUMINA".lower() or \
                        data['seq_library'].strip().lower() != 'Paired,Paired'.lower() or \
                        data['seq_library'].strip().lower() == 'Single'.lower():
                    # print("not included ..........", p_file)
                    nt_included += 1
                    # print(data)
                    # print(data.keys())
                    continue  # print(data["seq_platform"])

                file_size = os.path.getsize(p_file)
                total_size += file_size
                if read_file.user_id in users_uploads_sizes:
                    users_uploads_sizes[read_file.user_id] = users_uploads_sizes[read_file.user_id] + file_size
                else:
                    users_uploads_sizes[read_file.user_id] = file_size

                if mode == 'delete':
                    os.remove(p_file)
                    print('%s deleted' % p_file)
                else:
                    print('test mode, %s not deleted' % p_file)

                f = open(status_file, 'a')
                f.write('{0}\t{1}\n'.format(round(file_size / 1000000.0, 1), p_file))
                f.close()

            else:
                not_exist += 1
                # print("%s is not a file"%p_file)
                # print(read_file.file_name, read_file.file_location)
        else:
            no_record += 1
            # print("Missing file ...")
            # print(read_file.file_name, read_file.file_location)

    output_reads_to_be_deleted(users_uploads_sizes, mode, start)
    print ("==================================================================================")
    print ("Files to be deleted", len(reads_files))
    print ("total_size:", total_size / gb_ / 1024)
    print ("no_record", no_record)
    print ("not_exist:", not_exist)
    print ("nt_included: ", nt_included)
    print ("================================================================================")
    end = datetime.datetime.now()
    print("start:", start)
    print("end time: ", end)



@manager.command
@manager.option('-m', '--mode', help='test or delete mode')
def delete_user_reads5(mode='test'):
    # Delete old orphaned user files that are more than a year old, i.e. which are not picked up by other tidyups
    start = datetime.datetime.now()
    status_file = os.path.join(app.config['ENTEROBASE_CACHE_FOLDER'], 'delete_orphaned_user_reads_%s_%s.txt' % (mode,str(start)))
    users = User.query.all()
    userids = set(str(user.id) for user in users)
    total_size = 0
    print("Removing read files, please wait ......")
    con = 0
    users_uploads_sizes = {}
    user_read_dir =  app.config.get('FILE_UPLOAD_DIR')
    user_dirs = [f.name for f in os.scandir(user_read_dir) if f.is_dir()]

    for user in user_dirs:
        species_dirs = [f.name for f in os.scandir(os.path.join(user_read_dir,user)) if f.is_dir()]
        for species in species_dirs:
            files = glob.glob(os.path.join(user_read_dir,user,species,"*.fq.gz")) + \
                    glob.glob(os.path.join(user_read_dir,user,species,"*.fastq.gz"))
            for file in files:

                filename = os.path.join(user_read_dir,user,species,file)
                file_size = os.path.getsize(filename)
                file_date = datetime.datetime.fromtimestamp(os.path.getctime(filename))
                date_diff = start - file_date
                year_diff = date_diff.days/365

                file_date = file_date.strftime('%Y-%m-%dT%H:%M:%S')

                if year_diff > 1:
                    userid = int(filename.split('/')[3])
                    if userid in users_uploads_sizes:
                        users_uploads_sizes[userid] = users_uploads_sizes[userid] + file_size
                    else:
                        users_uploads_sizes[userid] = file_size

                    if mode == 'delete':
                        os.remove(filename)
                        print('%s deleted' % filename)
                    else:
                        print('test mode, %s not deleted' % filename)

                    f = open(status_file, 'a')
                    f.write('{0}\t{1}\t{2}\n'.format(file_size, file_date, filename))
                    f.close()
                    con += 1
                    total_size += file_size

    output_reads_to_be_deleted(users_uploads_sizes, mode, start)

    print ("==================================================================================")
    print ("Number of files:{0}".format(con))
    print ("total_size (Tbyte):", total_size / gb_ / 1024)
    print ("================================================================================")
    end = datetime.datetime.now()
    print ("start:", start)
    print ("end time: ", end)


@manager.command
def compress_assemblies(dbname):
    log_handler = logging.FileHandler(os.path.join(app.root_path, '..', 'logs', 'compression.log'))
    log_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(funcName)s:%(lineno)d - %(message)s"))
    app.logger.addHandler(log_handler)

    dbase = get_database(dbname)
    if not dbase:
        app.logger.warning("compress_assemblies failed, could not find database %s" % dbname)
        return
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies

    #Filter paths to only those ending in a or q (should just be fasta/fastq and other variant extensions).
    #Non-paths and already-gzipped paths are excluded by this.
    path_filter = Assemblies.file_pointer.like('/%a') | Assemblies.file_pointer.like('/%q')

    #Get an initial count of how many assembly entries match the requirements.
    #This is only used for progress reporting.
    print("Checking database %s for assemblies to compress..." % dbname)
    expected = dbase.session.query(Assemblies) \
                .join(Strains, Strains.best_assembly == Assemblies.id) \
                .filter(path_filter) \
                .count()
    if expected == 0:
        print("No assemblies found.")
        return
    print("Approx %d assemblies to process." % expected)

    records_processed = 0
    compressed_count = 0
    start_time = time.time()
    last_report = start_time

    assemblies_per_batch = 100

    try:
        #Request assembly information in batches. Ordering by assembly id
        #is used to track progress.
        last_id = -1
        while True:
            batch = dbase.session.query(Assemblies) \
                    .join(Strains, Strains.best_assembly == Assemblies.id) \
                    .order_by(Assemblies.id) \
                    .filter(Assemblies.id > last_id) \
                    .filter(path_filter) \
                    .limit(assemblies_per_batch).all()
            if not batch:
                break
            last_id = batch[-1].id

            #Check the assemblies' files exist, and check for pairs (e.g. fasta file accompanying a fastq file)
            asminfo = []
            command = ['pigz', '-k']
            for asm in batch:
                fp = asm.file_pointer
                if not os.path.exists(fp):
                    app.logger.warning('File %s on assembly %d not found.' % (fp, asm.id))
                    continue
                files = [fp]
                command.append(fp)
                fp2 = fp[:-1] + ('a' if fp[-1] == 'q' else 'q')
                if os.path.exists(fp2):
                    files.append(fp2)
                    command.append(fp2)
                asminfo.append((asm, files))
            if not asminfo:
                continue

            #Run pigz compression
            subprocess.run(command)

            #Check success for each assembly, and update file_pointer if successful.
            to_remove = []
            for asm, files in asminfo:
                if all(os.path.exists(f + '.gz') for f in files):
                    asm.file_pointer += '.gz'
                    to_remove.extend(files)
                else:
                    app.logger.warning("Assembly %d: one or more files failed to compress." % asm.id)

            #Save changes and delete uncompressed files.
            dbase.session.commit()
            for f in to_remove:
                os.remove(f)

            records_processed += len(batch)
            compressed_count += len(to_remove)

            t = time.time()
            if t - last_report >= 60:
                last_report = t
                expected = max(expected, records_processed)
                timestr = ''
                if 0 < records_processed < expected:
                    time_left = int((t - start_time) * (expected - records_processed) / records_processed)
                    timestr = "%ds" % (time_left % 60)
                    time_left //= 60
                    if time_left:
                        timestr = "%dm" % (time_left % 60) + timestr
                        time_left //= 60
                    if time_left:
                        timestr = "%dh" % (time_left % 60) + timestr
                        time_left //= 60
                    if time_left:
                        timestr = "%dd" % time_left + timestr
                    timestr = " - %s remaining" % timestr

                print('%s -- %d/%d (%.1f%%%s), %d file(s) compressed' % (dbname, records_processed, expected, records_processed / expected * 100, timestr, compressed_count))

        print('Complete. %d file(s) compressed.' % compressed_count)
    except Exception as e:
        dbase.session.rollback()
        dbase.session.close()
        app.logger.exception("Error in compress_assemblies, error message: %s" % str(e))



@manager.command
@manager.option('-d', '--database', help='Name of database, or "all" (default)', dest='dbname')
@manager.option('-o', '--output-dir', help='Path of directory to store archive in (default "/data/enterobase_archive")', required=False, dest='output_dir')
def remove_old_assemblies(dbname='all', output_dir='/data/enterobase_archive'):
    if dbname == 'all':
        all_dbs = entero.config['ACTIVE_DATABASES']
        for name in all_dbs:
            remove_old_assemblies(name)
        return
    elif dbname == 'miu': #Ignore the miu database as it references assemblies in other databases
        return

    time_interval       = "INTERVAL '1 year'" #Delete superseded assemblies if superseding assembly is at least this old. (Must be a valid postgres interval expression)
    batch_size          = 20 #How many assemblies should be read from the database for processing at a time
    archive_dir         = os.path.join(output_dir, dbname)   #Directory to which archives of deleted assemblies are copied
    index_file          = 'index.csv'    #Name of a csv file to contain information about archived assemblies (stored in the above directory)
    field_names         = ['assembly', 'created', 'date_uploaded', 'original_file_pointer', 'strain']    #Names of the columns written to index.csv
    pipeline_jobs_file  = 'pipelines.csv'    #Name used for csv files created for each assembly to list pipeline jobs run against them

    dbase = get_database(dbname)
    if not dbase:
        app.logger.warning("could not find database %s" % dbname)
        return
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies
    AssemblyLookup = dbase.models.AssemblyLookup
    Traces = dbase.models.Traces
    TracesAssembly = dbase.models.TracesAssembly
    Schemes = dbase.models.Schemes

    #Used to catch keyboard interrupts, to allow safe manual termination.
    #The interrupt is only handled during the phase where files/database items are deleted.
    interrupted = False
    def interrupt_handler(signum, frame):
        nonlocal interrupted
        if not interrupted:
            interrupted = True
            print("Aborting...")

    # Prepare archive directory for this species
    os.makedirs(archive_dir, exist_ok=True)
    os.chdir(archive_dir)
    if os.path.exists(index_file):
        index_fh = open(index_file, 'a')
        index_writer = DictWriter(index_fh, fieldnames=field_names)
    else:
        index_fh = open(index_file, 'w')
        index_writer = DictWriter(index_fh, fieldnames=field_names)
        index_writer.writeheader()
        index_fh.flush()

    try:
        # Build query on the species database for assemblies to process.
        # ids_subquery collates assembly/trace/strain ids for non-best assemblies
        # where one of the following is true:
        #   * The assembly isn't associated to an existing strain,
        #   * There is at least one assembly newer than it which is itself at least {time_interval} old,
        #   * Both it and the strain's best assembly are at least {time_interval} old.
        # Note that the third case is to account for situations where the best_assembly is not the newest
        # assembly for the stain.
        # This is used to construct batch_query, which
        # returns modifiable sqlalchemy objects corresponding to those ids.
        ids_subquery = sqlalchemy.text(f"""
            WITH ta AS (
                SELECT
                    assemblies.id AS assembly_id, traces.id as trace_id, strains.id AS strain_id,
                    NOW() - assemblies.created AS age,
                    assemblies.id = strains.best_assembly AS best
                FROM assemblies
                LEFT JOIN trace_assembly ON assemblies.id = trace_assembly.assembly_id
                LEFT JOIN traces ON traces.id = trace_assembly.trace_id
                LEFT JOIN strains ON strains.id = traces.strain_id OR strains.best_assembly = assemblies.id
            )
            SELECT ta.assembly_id, ta.trace_id, ta.strain_id
            FROM ta
            LEFT JOIN (
                SELECT
                    strain_id,
                    MIN(age) AS min_age,
                    BOOL_OR(best) AS best_is_old
                FROM ta
                WHERE strain_id IS NOT NULL AND age > {time_interval}
                GROUP BY strain_id
            ) tb ON ta.strain_id = tb.strain_id
            WHERE NOT ta.best
            AND NOT EXISTS(SELECT * FROM strains_archive WHERE best_assembly = ta.assembly_id)
            AND (
                ta.strain_id IS NULL
                OR tb.min_age < ta.age
                OR (tb.best_is_old AND ta.age > {time_interval})
            )
            """).columns(assembly_id=sqlalchemy.Integer, trace_id=sqlalchemy.Integer, strain_id=sqlalchemy.Integer).alias()
        batch_query = (dbase.session.query(Assemblies, Traces, Strains)
            .join(ids_subquery, ids_subquery.c.assembly_id == Assemblies.id)
            .outerjoin(Traces, Traces.id == ids_subquery.c.trace_id)
            .outerjoin(Strains, Strains.id == ids_subquery.c.strain_id))

        #Check that there are assemblies to process
        total_assemblies = batch_query.count()
        print(f"{dbname}: {total_assemblies} assemblies to archive")
        if total_assemblies == 0:
            return

        #To check if a job is ready to be deleted after an associated assembly is removed, we need
        #to know what assemblies/assembly_lookup records reference what jobs. This is expensive to
        #query per batch, so we instead load this information at the start and store in memory for
        #ease of querying.
        #For each assembly/lookup, store a list of the job ids it references.
        asm_jobrefs = {}
        asl_jobrefs = {}
        #For each job id, store a count of how many assemblies/lookups reference it.
        job_refs_count = {}

        assembly_dir_exists = os.path.isdir('/share_space/assembly')
        if assembly_dir_exists:
            asm_expr = rf"SUBSTRING(file_pointer FROM '(?:{entero.config['OUTPUT_DIR_ROOT']}\d*|/share_space/assembly)/\d+/(\d+)/')::int"
        else:
            asm_expr = rf"SUBSTRING(file_pointer FROM '{entero.config['OUTPUT_DIR_ROOT']}\d*/\d+/(\d+)/')::int"
        asm_query = dbase.session.query(
            Assemblies.id,
            Assemblies.job_id,
            asm_expr)
        for i, j1, j2 in asm_query:
            jobs = set()
            if j1 is not None:
                jobs.add(j1)
            if j2 is not None:
                jobs.add(j2)
            if jobs:
                asm_jobrefs[i] = list(jobs)
                for j in jobs:
                    n = job_refs_count.get(j, 0)
                    job_refs_count[j] = n + 1

        escaped_root = entero.config['OUTPUT_DIR_ROOT'].replace('/', r'\\*/')
        asl_query = dbase.session.query(
            AssemblyLookup.id,
            rf"""ARRAY(SELECT DISTINCT (REGEXP_MATCHES(other_data::text, '(?:\\*"job_id\\*":\s*|{escaped_root}\d*\\*/\d+\\*/)(\d+)[^\d]', 'g'))[1]::int)""")
        for i, jobs in asl_query:
            if jobs:
                asl_jobrefs[i] = jobs
                for j in jobs:
                    n = job_refs_count.get(j, 0)
                    job_refs_count[j] = n + 1

        #So that jobrefs may be removed from the listing as assemblies are deleted, without reloading the listing.
        def remove_jobrefs(jobrefs, ids):
            for i in ids:
                jobs = jobrefs.get(i)
                if jobs is not None:
                    for job_id in jobs:
                        n = job_refs_count[job_id]
                        if n == 1:
                            del job_refs_count[job_id]
                        else:
                            job_refs_count[job_id] = n - 1
                    del jobrefs[i]

        while True:
            #Obtain new batch of assemblies to process
            batch_items = batch_query.limit(batch_size).all()
            if not batch_items:
                break

            #Identity info for the assemblies in the batch
            asm_ids = []
            asm_barcodes = []

            #Traces associated to assemblies in the batch.
            #This is used when deleting orphaned traces, to restrict that cleanup
            #step to only those which have been orphaned by deletion of assemblies.
            trace_ids = set()

            #All job ids identified as associated to assemblies in the batch,
            #through assembly/evaluation jobs or pipeline jobs.
            job_ids = set()

            #Assembly information to be written to index.csv
            index_records = []

            #Sequence files which have been copied to an archive directory,
            #so are to be deleted.
            files_to_delete = []

            #Database records in AssemblyLookup and UserJobs, to be used for
            #record deletions on completion of the batch.
            asl_records = []
            job_records = {}

            #Collect information about assembly files to copy
            for asm, tr, strain in batch_items:
                asm_ids.append(asm.id)
                if tr:
                    trace_ids.add(tr.id)

                #Get barcode and create backup directory
                barcode = dbase.encode(asm.id, dbname, 'assemblies')
                asm_barcodes.append(barcode)
                os.makedirs(barcode, exist_ok=True)

                #Get possible directories for the assembly job, based on file_pointer/job ids
                asm_job_ids = set(asm_jobrefs.get(asm.id, []))
                possible_job_dirs = []
                fp = asm.file_pointer
                if fp:
                    dirname = os.path.dirname(fp)
                    if os.path.isdir(dirname):
                        possible_job_dirs.append(dirname)
                for job_id in asm_job_ids:
                    dirname = f'{entero.config["OUTPUT_DIR_ROOT"]}{job_id//10000000 or ""}/{job_id//1000}/{job_id}'
                    if os.path.isdir(dirname):
                        possible_job_dirs.append(dirname)

                #Get associated assembly lookups
                asm_lookups = (dbase.session.query(AssemblyLookup, Schemes)
                    .join(Schemes, Schemes.id == AssemblyLookup.scheme_id)
                    .filter(AssemblyLookup.assembly_id == asm.id)
                    .all())
                asl_records.extend(asl for asl, _ in asm_lookups)

                #Get UserJobs records that reference this assembly
                asm_jobs = (db.session.query(UserJobs)
                    .filter((UserJobs.accession == barcode) | UserJobs.output_location.contains(barcode))
                    .all())

                #Compile info about all pipeline jobs
                asm_pipelines = {}
                for asl, schm in asm_lookups:
                    pipeline_info = asm_pipelines.get(schm.description)
                    if pipeline_info is None:
                        pipeline_info = {'job_ids': set(), 'st_ids': set()}
                        asm_pipelines[schm.description] = pipeline_info

                    job_set = pipeline_info['job_ids']
                    st_set = pipeline_info['st_ids']

                    #Job ids may be listed on an assembly lookup either as other_data->>'job_id', or
                    #as the directory of an output file. Additionally, other_data may be encoded as a string or dict,
                    #so search the raw json string to find these ids instead of querying the json as a dict.
                    asl_job_ids = asl_jobrefs.get(asl.id, [])
                    job_set.update(asl_job_ids)
                    asm_job_ids.update(asl_job_ids)
                    job_ids.update(asl_job_ids)

                    #Extract sequence type id, if present
                    try:
                        st_id = int(asl.other_data['results']['st_id'])
                        st_set.add(st_id)
                    except Exception:
                        pass

                for j in asm_jobs:
                    pipeline_info = asm_pipelines.get(j.pipeline)
                    if pipeline_info is None:
                        pipeline_info = {'job_ids': set(), 'st_ids': set()}
                        asm_pipelines[j.pipeline] = pipeline_info

                    job_set = pipeline_info['job_ids']
                    job_set.add(j.id)
                    asm_job_ids.add(j.id)
                    job_ids.add(j.id)

                #If any job ids were found via AssemblyLookup but not via UserJobs,
                #look for those job records as well
                missing_job_ids = set(asm_job_ids)
                missing_job_ids.difference_update(j.id for j in asm_jobs)
                if missing_job_ids:
                    missing_jobs = db.session.query(UserJobs).filter(UserJobs.id.in_(missing_job_ids)).all()
                    asm_jobs.extend(missing_jobs)

                #Add this assembly's jobs to the main list of records
                for j in asm_jobs:
                    job_records[j.id] = j

                # Copy/compress sequence files
                seqfiles = []
                if fp and os.path.isfile(fp):
                    seqfiles.append(fp)
                    fp2 = None
                    if fp.endswith('a'):
                        fp2 = fp[:-1] + 'q'
                    elif fp.endswith('q'):
                        fp2 = fp[:-1] + 'a'
                    elif fp.endswith('a.gz'):
                        fp2 = fp[:-4] + 'q.gz'
                    elif fp.endswith('q.gz'):
                        fp2 = fp[:-4] + 'a.gz'
                    if fp2 and os.path.isfile(fp2):
                        seqfiles.append(fp2)

                for fl in seqfiles:
                    fl_dest = shutil.copy(fl, barcode)
                    if not fl_dest.endswith('.gz'):
                        subprocess.run(['pigz', '-qf', '--', fl_dest])
                files_to_delete.extend(seqfiles)

                #Look through the identified possible job directories
                #for the cmd/log/err files and copy them if found
                for job_dir in possible_job_dirs:
                    found_files = []
                    for fname in ('cmd_file', 'log_file', 'err_file'):
                        p = os.path.join(job_dir, fname)
                        if os.path.isfile(p):
                            shutil.copy(p, barcode)
                            found_files.append(os.path.join(barcode, fname))
                    if found_files:
                        subprocess.run(['pigz', '-qf', '--'] + found_files)
                        break

                #Write pipeline job info to csv
                if asm_pipelines:
                    with open(os.path.join(barcode, pipeline_jobs_file), 'w') as fl:
                        pl_writer = writer(fl)
                        pl_writer.writerow(['pipeline', 'job_id', 'st_id'])
                        for pl, info in asm_pipelines.items():
                            pl_writer.writerow([pl, ';'.join(str(i) for i in info['job_ids']), ';'.join(str(i) for i in info['st_ids'])])

                #Make index record
                record = {
                    'assembly': barcode,
                    'created': asm.created,
                    'date_uploaded': asm.date_uploaded,
                    'original_file_pointer': fp
                }
                if strain:
                    record['strain'] = dbase.encode(strain.id, dbname, 'strains')
                index_records.append(record)

            #Begin modification phase, making sure keyboard interrupts are safely handled.
            signal.signal(signal.SIGINT, interrupt_handler)

            #Update index
            index_writer.writerows(index_records)
            index_fh.flush()

            #Find traces which will be orphaned after the assembly deletion
            orphaned_traces = (dbase.session.query(Traces)
                .filter(Traces.id.in_(trace_ids))
                .filter(~dbase.session.query(TracesAssembly)
                    .filter(TracesAssembly.c.trace_id == Traces.id)
                    .filter(TracesAssembly.c.assembly_id.notin_(asm_ids))
                    .exists())
                .all())

            #Delete queried assemblies/lookups
            for asm, _, _ in batch_items:
                dbase.session.delete(asm)
            for asl in asl_records:
                dbase.session.delete(asl)

            #Delete orhpaned traces
            for tr in orphaned_traces:
                dbase.session.delete(tr)

            #Delete copied sequence files
            for fl in files_to_delete:
                if os.path.exists(fl):
                    os.remove(fl)

            #Update job reference listing, removing items that have now been deleted.
            remove_jobrefs(asm_jobrefs, (asm.id for asm, _, _ in batch_items))
            remove_jobrefs(asl_jobrefs, (asl.id for asl in asl_records))

            #Clean up jobs with no remaining references
            for job_id in job_ids:
                if job_id in job_refs_count:
                    continue

                rec = job_records.get(job_id)
                if rec is not None:
                    db.session.delete(rec)

                loc_roots = [f'{entero.config["OUTPUT_DIR_ROOT"]}{job_id // 10000000 or ""}']
                if assembly_dir_exists:
                    loc_roots.append('/share_space/assembly')
                for loc_root in loc_roots:
                    path = f'{loc_root}/{job_id // 1000}/{job_id}/'
                    if os.path.isdir(path):
                        shutil.rmtree(path)

            # Delete jbrowse output directories
            for bc in asm_barcodes:
                jbrowse_dir = os.path.join(entero.config['JBROWSE_REPOSITORY'], bc[:3], bc[4:8], bc)
                if os.path.isdir(jbrowse_dir):
                    shutil.rmtree(jbrowse_dir)

            #Commit db changes
            db.session.commit()
            dbase.session.commit()

            #Re-enable keyboard interrupts
            signal.signal(signal.SIGINT, signal.default_int_handler)
            if interrupted:
                break

    except:
        raise
    finally:
        dbase.session.rollback()
        db.session.rollback()
        signal.signal(signal.SIGINT, signal.default_int_handler)
        index_fh.close()


@manager.command
@manager.option('-d', '--database', help='Name of database', dest='dbname')
@manager.option('-c', '--commit', help='If False (default), modifications are summarised but not performed', required=False, type=bool, dest='commit')
def delete_db_associates(dbname=None, commit=False):
    #Deletes directories, and records in the entero database, associated
    #with the specified species database. Does not delete the species
    #database itself.

    dbase = get_database(dbname)
    if not dbase:
        app.logger.warning(f"could not find database {dbname}")
        return
    dbcode = entero.config['ACTIVE_DATABASES'][dbname][4]
    Assemblies = dbase.models.Assemblies
    AssemblyLookup = dbase.models.AssemblyLookup

    #user_preferences and user_uploads contain species-specific records.
    #These don't require any special processing, so we just delete them.
    prefs_query = db.session.query(UserPreferences).filter_by(database=dbname)
    count = prefs_query.count()
    print(f'{count} user_preferences records')
    if count and commit:
        prefs_query.delete()
        db.session.commit()
    uploads_query = db.session.query(UserUploads).filter_by(species=dbname)
    count = uploads_query.count()
    print(f'{count} user_uploads records')
    if count and commit:
        uploads_query.delete()
        db.session.commit()

    #Method used to find/delete job directories for a given job id.
    #Looks in both /share_space/interact/outputs* and /share_space/assembly
    #Also updates the running counts of directories in those locations
    check_adirs = os.path.exists('/share_space/assembly')       #If the old /share_space/assembly output location is present, check there too
    odirs_count = 0 #Count of directories found in /share_space/interact/outputs*
    adirs_count = 0 #Count of directories found in /share_space/assembly
    def check_job_dirs(dir_id):
        nonlocal odirs_count, adirs_count
        opath = Path(f'{entero.config["OUTPUT_DIR_ROOT"]}{dir_id // 10000000 or ""}', str(dir_id // 1000), str(dir_id))
        if opath.is_dir():
            odirs_count += 1
            if commit:
                shutil.rmtree(opath)
        if check_adirs:
            apath = Path(f'/share_space/assembly', str(dir_id // 1000), str(dir_id))
            if apath.is_dir():
                adirs_count += 1
                if commit:
                    shutil.rmtree(apath)

    #For each entry in user_jobs associated to this database,
    #look for and remove associated directories.
    userjobs_jobids = {x[0] for x in db.session.query(UserJobs.id).filter_by(database=dbname)}
    print(f'{len(userjobs_jobids)} records in user_jobs')
    if userjobs_jobids:
        print(f'  Scanning for job directories (this may take a while)...')

        for job_id in userjobs_jobids:
            check_job_dirs(job_id)

        print(f'  {odirs_count} job dirs in {entero.config["OUTPUT_DIR_ROOT"]}*')
        if check_adirs:
            print(f'  {adirs_count} job dirs in /share_space/assembly')

        if commit:
            #Remove the database records themselves
            db.session.query(UserJobs).filter_by(database=dbname).delete()
            db.session.commit()

    #Job directories can become disconnected from the user_jobs table
    #while still being referenced in the species database. Check for directories
    #linked to these as well.
    print(f'Checking for additional job ids in {dbname}...')
    dbase_jobids = set()
    odirs_count = 0
    adirs_count = 0
    if check_adirs:
        asm_expr = rf"SUBSTRING(file_pointer FROM '(?:{entero.config['OUTPUT_DIR_ROOT']}\d*|/share_space/assembly)/\d+/(\d+)/')::int"
    else:
        asm_expr = rf"SUBSTRING(file_pointer FROM '{entero.config['OUTPUT_DIR_ROOT']}\d*/\d+/(\d+)/')::int"
    asm_query = dbase.session.query(Assemblies.job_id, asm_expr)
    escaped_root = entero.config['OUTPUT_DIR_ROOT'].replace('/', r'\\*/')
    asl_query = dbase.session.query(
        AssemblyLookup.id,
        rf"""ARRAY(SELECT DISTINCT (REGEXP_MATCHES(other_data::text, '(?:\\*"job_id\\*":\s*|{escaped_root}\d*\\*/\d+\\*/)(\d+)[^\d]', 'g'))[1]::int)""")
    for jobids in itertools.chain(asm_query, (x[1] for x in asl_query)):
        for job_id in jobids:
            if job_id is None or job_id in userjobs_jobids or job_id in dbase_jobids:
                continue
            dbase_jobids.add(job_id)
            check_job_dirs(job_id)

    print(f'  {len(dbase_jobids)} additional job ids found in {dbname}')
    if dbase_jobids:
        print(f'  {odirs_count} job dirs in {entero.config["OUTPUT_DIR_ROOT"]}*')
        if check_adirs:
            print(f'  {adirs_count} job dirs in /share_space/assembly')

    #Workspace directories
    workspace_dir = os.path.join(entero.config['BASE_WORKSPACE_DIRECTORY'], dbname)
    count = sum(1 for _ in glob.iglob(workspace_dir + '/*/*'))
    print(f'{count} workspace directories')
    if commit and os.path.isdir(workspace_dir):
        shutil.rmtree(workspace_dir)

    #User upload directories
    reads_paths = glob.glob(os.path.join(entero.config['FILE_UPLOAD_DIR'], f'*/{dbname}/'))
    print(f'{len(reads_paths)} user upload directories')
    if reads_paths and commit:
        for path in reads_paths:
            shutil.rmtree(path)

    #jbrowse output directories
    jbrowse_root = os.path.join(entero.config['JBROWSE_REPOSITORY'], dbcode)
    jbrowse_dirs = sum(1 for _ in glob.iglob(jbrowse_root + '/*/*/'))
    print(f'{jbrowse_dirs} jbrowse directories')
    if commit and os.path.isdir(jbrowse_root):
        shutil.rmtree(jbrowse_root)


@manager.command
@manager.option('-c', '--commit', help='If False (default), modifications are summarised but not performed',
                required=False, type=bool, dest='commit')
def delete_orphaned_job_dirs(commit=False):
    # Deletes job directories which are no longer referenced in the database.
    # References to the directory's associated job id are looked for in user_jobs,
    # as well as assemblies and assembly_lookup in each species' database.
    # A log of the deleted directories is written to the path below.
    log_path = '/data/enterobase_archive/deleted_job_dirs.txt'

    from tqdm import tqdm
    assembly_dir_exists = os.path.isdir('/share_space/assembly')
    escaped_root = entero.config['OUTPUT_DIR_ROOT'].replace('/', r'\\*/')

    # Get job id references from species databases
    print("Scanning species databases for job id references...")
    asm_refs = set()
    asl_refs = set()
    for idb, dbname in enumerate(entero.config['ACTIVE_DATABASES']):
        dbase = get_database(dbname)
        Assemblies = dbase.models.Assemblies
        AssemblyLookup = dbase.models.AssemblyLookup
        print(f'{idb + 1}/{len(entero.config["ACTIVE_DATABASES"])} {dbname}')

        # Get job ids from assemblies (check both the job_id column, and also extract from file_pointer
        if assembly_dir_exists:
            asm_expr = rf"SUBSTRING(file_pointer FROM '(?:{entero.config['OUTPUT_DIR_ROOT']}\d*|/share_space/assembly)/\d+/(\d+)/')::int"
        else:
            asm_expr = rf"SUBSTRING(file_pointer FROM '{entero.config['OUTPUT_DIR_ROOT']}\d*/\d+/(\d+)/')::int"
        asm_query = dbase.session.query(
            Assemblies.job_id,
            asm_expr)
        for jobs in asm_query:
            asm_refs.update((j for j in jobs if j is not None))

        # Check in assembly_lookup. This extracts job ids from the other_data column,parsing both
        # explicit job ids and output file paths.
        asl_query = dbase.session.query(
            AssemblyLookup.id,
            rf"""ARRAY(SELECT DISTINCT (REGEXP_MATCHES(other_data::text, '(?:\\*"job_id\\*":\s*|{escaped_root}\d*\\*/\d+\\*/)(\d+)[^\d]', 'g'))[1]::int)""")
        for _, jobs in asl_query:
            asl_refs.update(j for j in jobs if j not in asm_refs)

    print('Number of job ids found in...')
    print(f'    assemblies:         {len(asm_refs)}')
    print(f'    assembly_lookup:    {len(asl_refs)}')

    # Identify gaps in the user_jobs table's job id sequence
    print('Scanning job directories...')
    jobid_gaps = db.session.execute("""
        SELECT prev + 1 AS start, id AS end
        FROM (
            SELECT id, LAG(id) OVER (ORDER BY id) AS prev
            FROM user_jobs
        ) x
        WHERE id - prev > 1
    """)

    # Job directories are searched for using prefixed globs to increase speed, i,e.
    # if the entire range 1030000-1039999 is missing from user_jobs, we glob on <...>/103?/103????.
    # This code parses the gap list into subgaps that fit this prefix method.
    scan_ranges = []
    total_ids = 0
    for start, end in jobid_gaps:
        total_ids += end - start
        while start < end:
            d = 10000000
            n = 7
            while True:
                if start % d == 0 and start >= d and start + d <= end:
                    scan_ranges.append((start, d, n))
                    start += d
                    break
                d //= 10
                n -= 1

    # Search for job directories in {OUTPUT_DIR_ROOT} using the above-generated ranges.
    # /share_space/assembly is also checked if it is present.
    found_dirs = {}  # Key is job id, value is a bitmask of locations (1={OUTPUT_DIR_ROOT}, 2=/share_space/assembly)
    for start, d, n in tqdm(scan_ranges):
        p1 = start // 10000000 or ''
        p2 = list(str(start // 1000))
        p3 = list(str(start))
        for i in range(n):
            p3[-(i + 1)] = '?'
            if i >= 3:
                p2[-(i - 2)] = '?'
        pattern_end = f'{"".join(p2)}/{"".join(p3)}'
        for dir in glob.glob(f'{entero.config["OUTPUT_DIR_ROOT"]}{p1}/{pattern_end}'):
            job_id = int(dir.split('/')[-1])
            found_dirs[job_id] = 1
        if assembly_dir_exists:
            for dir in glob.glob(f'/share_space/assembly/{pattern_end}'):
                job_id = int(dir.split('/')[-1])
                found_dirs[job_id] = found_dirs.get(job_id, 0) + 2
    print(f'Found {len(found_dirs)} directories for job ids not in user_jobs')

    # Open file to output the list of deleted directories
    log_file_exists = os.path.isfile(log_path)
    with open(log_path, 'a') as log_file:
        if not log_file_exists:
            log_file.write(
                '# List of job directories deleted due to not being referenced in the db (see manage_cleanup.py/delete_orphaned_job_dirs)')

        # Filter referenced jobs
        print("Processing...")
        deleted_jobs = 0
        deleted_dirs = 0
        asm_ignores = 0
        asl_ignores = 0
        for job_id, dirmask in tqdm(found_dirs.items()):
            if job_id in asm_refs:
                asm_ignores += 1
            elif job_id in asl_refs:
                asl_ignores += 1
            else:
                deleted_jobs += 1
                deleted_dirs += (2 if dirmask == 3 else 1)
                if commit:
                    if dirmask & 1:
                        path = f'{entero.config["OUTPUT_DIR_ROOT"]}{job_id // 10000000 or ""}/{job_id // 1000}/{job_id}'
                        log_file.write(path + '\n')
                        shutil.rmtree(path)
                    if dirmask & 2:
                        path = f'/share_space/assembly/{job_id // 1000}/{job_id}'
                        log_file.write(path + '\n')
                        shutil.rmtree(path)

    if commit:
        print(f'{deleted_dirs} directories deleted for {deleted_jobs} job ids')
    else:
        print(f'{deleted_dirs} directories to delete for {deleted_jobs} job ids')
    print(f'Job ids ignored due to references from...')
    print(f'    assemblies:         {asm_ignores}')
    print(f'    assembly_lookup:    {asl_ignores}')


if __name__ == '__main__':
    manager.run()
