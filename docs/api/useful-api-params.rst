Useful EnteroBase API Parameters
================================

The API can be explored using the [interactive documentation](https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui "external link").  The query methods currently in the API are:

Some parameters used by a number of the query methods include:

The onlyfields API parameter
----------------------------
``onlyfields`` is a parameter for several query methods in the API and can have one or more
values naming available fields in the query results.  The outcome is that the query results
will only include those fields.  The default behaviour - when ``onlyfields`` is not set -
is for the results to include all of the available fields.  When multiple fields are named
in a URL used in an HTTP request while using the API then rather than ``onlyfields`` being
set to a list of fields, each desired field must have a separate `onlyfields=` term.
For example, the HTTP request

.. code-block:: html
    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?only_fields=download_fastq_link&only_fields=n50&barcode=SAL_IA5204AA_AS


uses the ``assemblies`` query method in order to obtain the n50 value and a download link for FASTQ data
only in the assemblies metadata for the assembly with the barcode SAL_IA5204AA_AS.  This gives the result

.. code-block:: json

    {
      "Assemblies": [
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica&file_format=fastq",
          "n50": 406329
        }
      ],
      "links": {
        "paging": {},
        "records": 1,
        "total_records": 1
      }
    }


The orderby API parameter 
-------------------------
``orderby`` is a parameter for several query methods in the API and can be set to the name
of one of the fields in the query method's results, in order to sort on that field.  Where this
option is available it generally takes the value "barcode" by default (see the
[interactive documentation](https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui) for details).

An example of this is the below query sorting on n50 in ascending order

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=n50&only_fields=download_fastq_link&only_fields=n50&limit=5&reldate=10&assembly_status=Assembled&sortorder=asc

Note that the behaviour when sorting on a field with a date (such as "lastmodified") is currently undefined and
in particular is not expected to sort in chronological order.


The sortorder API parameter
---------------------------
Refer to interactive documentation : https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui

``sortorder`` is a parameter for several query methods in the API and can have the values "asc"
or "desc" for ascending and descending, respectively.

An example of this is the below query sorting on n50 in ascending order

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=n50&only_fields=download_fastq_link&only_fields=n50&limit=5&reldate=10&assembly_status=Assembled&sortorder=asc



Note that the behaviour when sorting on a field with a date (such as "lastmodified") is currently undefined and
in particular is not expected to sort in chronological order.

The reldate API parameter
-------------------------
Refer to interactive documentation : https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui

``reldate`` is a parameter for several query methods in the API and can have an integer value.
(The name stands for "relative date".)  The value is the maximum age in days of content in the query
result.  For example, if reldate is 20 then the query will include everything from the past 20 days
that otherwise matches the query.

An example of this is the ``assemblies`` query

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=barcode&only_fields=download_fastq_link&only_fields=lastmodified&limit=5&reldate=10&assembly_status=Assembled&sortorder=asc


giving the result

.. code-block:: html
    
    {
      "Assemblies": [
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_EA1795AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-05-02T13:00:26.643198+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-05-01T22:02:56.892765+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA9491AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-30T17:32:38.119705+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_MA6721AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-28T08:31:08.800341+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_MA6715AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-28T08:31:08.675781+00:00"
        }
      ],
      "links": {
        "paging": {
          "next": "https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=barcode&assembly_status=Assembled&only_fields=download_fastq_link&limit=5&reldate=10&offset=5&sortorder=asc"
        },
        "records": 5,
        "total_records": 885
      }
    }


The sortorder API parameter
---------------------------
Refer to interactive documentation : https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui

``sortorder`` is a parameter for several query methods in the API and can have the values "asc"
or "desc" for ascending and descending, respectively.

An example of this is the below query sorting on n50 in ascending order

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=n50&only_fields=download_fastq_link&only_fields=n50&limit=5&reldate=10&assembly_status=Assembled&sortorder=asc



Note that the behaviour when sorting on a field with a date (such as "lastmodified") is currently undefined and
in particular is not expected to sort in chronological order.

# The barcode API parameter #
``barcode`` is a parameter for several query methods in the API and can have the barcode
of a scheme, locus, allele, assembly, ST, trace, strain or other data requested.  Barcodes
are explained on the [page about the API structure](api_structure).

Several of the query methods also support a syntax for specifying the barcode by including it
in the URL path (generally at the end of the path, before the parameters).

For example, a strain query for the barcode SAL_EA9564AA could be carried out either with  

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?barcode=SAL_EA9564AA

(with a HTTP GET request only)  
or  

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains/SAL_EA9564AA

(with either an HTTP GET or POST request).

both of these obtaining the results

.. code-block:: json

    {
      "Strains": [
        {
          "antigenic_formulas": null,
          "assembly_barcode": "SAL_KA3160AA",
          "city": null,
          "collection_date": null,
          "collection_month": null,
          "collection_time": null,
          "collection_year": 2015,
          "comment": null,
          "continent": "North America",
          "country": "United States",
          "county": null,
          "created": "2016-02-06T19:00:03.611332+00:00",
          "lab_contact": "United States Department of Agriculture, Food Safety and Inspection Service",
          "lastmodified": "2016-02-06T19:55:02.798303+00:00",
          "latitude": null,
          "longitude": null,
          "postcode": null,
          "region": "Iowa",
          "secondary_sample_accession": "SRS1278094",
          "serotype": "Agona",
          "source_details": "Animal-Swine-Sow",
          "source_niche": "Livestock",
          "source_type": "Air",
          "strain_barcode": "SAL_EA9564AA",
          "strain_name": "FSIS1605678",
          "uberstrain": "SAL_EA9564AA",
          "version": 1
        }
      ],
      "links": {
        "paging": {},
        "records": 1,
        "total_records": 1
      }
    }


The limit API parameter
-----------------------
Refer to interactive documentation : https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui

``limit`` is a parameter for several query methods in the API and can have an integer value.  The value specifies
the maximum number of records to be retained in the result from the underlying database query.  This would be used
to ensure an enormous number of records is not returned by a query where the number of records is uncertain
or when stepping through a large query result using the [``offset``](api_offset_parameter) parameter in conjunction
with ``limit``.

An example of this is the ``assemblies`` query

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=barcode&only_fields=download_fastq_link&only_fields=lastmodified&limit=5&reldate=10&assembly_status=Assembled&sortorder=asc


giving the result

.. code-block:: json

    {
      "Assemblies": [
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_EA1795AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-05-02T13:00:26.643198+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-05-01T22:02:56.892765+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA9491AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-30T17:32:38.119705+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_MA6721AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-28T08:31:08.800341+00:00"
        },
        {
          "download_fastq_link": "https://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_MA6715AA_AS&database=senterica&file_format=fastq",
          "lastmodified": "2017-04-28T08:31:08.675781+00:00"
        }
      ],
      "links": {
        "paging": {
          "next": "https://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=barcode&assembly_status=Assembled&only_fields=download_fastq_link&limit=5&reldate=10&offset=5&sortorder=asc"
        },
        "records": 5,
        "total_records": 885
      }
    }

The offset API parameter
------------------------
Refer to interactive documentation : https://enterobase.warwick.ac.uk/api/v2.0/swagger-ui

``offset`` is a parameter for several query methods in the API and can have an integer value.  The value specifies
a cursor position in the result from the underlying database query.  Generally, the
``offset`` parameter would be used in conjunction with [``limit``](api_limit_parameter).

Consider a somewhat artificial example (chosen so the result of the full query can in fact reasonably be
fetched with a single use of the API query method).  Supposing we wanted to step through the query

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?serotype=Derby&only_fields=strain_name&reldate=3&sortorder=asc&orderby=barcode


giving the result

.. code-block:: json

    {
      "Strains": [
        {
          "strain_name": "2015K-0409"
        },
        {
          "strain_name": "2015K-0383"
        },
        {
          "strain_name": "2015K-0381"
        },
        {
          "strain_name": "2015K-0374"
        }
      ],
      "links": {
        "paging": {},
        "records": 42,
        "total_records": 42
      }
    }


Then short consecutive chunks can be fetched by the query

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?serotype=Derby&only_fields=strain_name&reldate=3&sortorder=asc&offset=0&orderby=barcode&limit=2


giving the result (the same as the result at the beginning of the earlier query without the limit parameter)

.. code-block:: json

    {
      "Strains": [
        {
          "strain_name": "2015K-0409"
        },
        {
          "strain_name": "2015K-0383"
        }
      ],
      "links": {
        "paging": {
          "next": "https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?orderby=barcode&serotype=Derby&reldate=3&only_fields=strain_name&limit=2&sortorder=asc&offset=2"
        },
        "records": 2,
        "total_records": 42
      }
    }


and the query

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?serotype=Derby&only_fields=strain_name&reldate=3&sortorder=asc&offset=2&orderby=barcode&limit=2


giving the result

.. code-block:: json

    {
      "Strains": [
        {
          "strain_name": "2015K-0381"
        },
        {
          "strain_name": "2015K-0374"
        }
      ],
      "links": {
        "paging": {
          "next": "https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?orderby=barcode&serotype=Derby&reldate=3&only_fields=strain_name&limit=2&sortorder=asc&offset=4",
          "previous": "https://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?orderby=barcode&serotype=Derby&reldate=3&only_fields=strain_name&limit=2&sortorder=asc&offset=2"
        },
        "records": 2,
        "total_records": 42
      }
    }