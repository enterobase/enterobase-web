Enterobase API Usage
====================

Please see the :doc:`/enterobase-terms-of-use`.

The API is designed for rapid live updates of small amounts of data.  It is NOT designed large scale dumps 
of the entire database. 
**Consider that there are > 400,000 `Salmonella` genomes in EnteroBase (about 4TB); 
downloading these through the main REST API would be painfully slow and would 
hurt the service for everyone**.

If you simply wish to download assemblies this can be done efficiently for 1000s of assemblies by using the GUI 
as described in :doc:`/features/user-download-assemblies`  If you would like to discuss assembly downloads in
more detail please contact us (enterobase@warwick.ac.uk)

Download of scheme information without using the API, including bulk transfer using ftp is described here: :doc:`/features/user-download-schemes`
There is a specific method for fetching genotyping schemes 
(MLST/wgMLST/cgMLST), please read :doc:`api-download-schemes-assemblies`.

If you wish to use the API please contact us, explaining the purpose for which you wish to use the API (enterobase@warwick.ac.uk)


* API requests need to authenticated with a valid API Token.
* The API exists to be used; if you have suggestions for how to make it more useful for you, let us know.
* Please be responsible, do not flood the server with requests - add a 1-2 second pause.
* Do not submit large long running requests - Fetch only what you need. 
* Do not try to download 'all' the genome assembly data 
* No short read data are available through EnteroBase. You will have to retrieve them from NCBI/EBI/DDBJ.
* Bulk downloading of rMLST allele sequences and profiles is NOT permitted through this service. 

**API documentation:**

.. toctree::
    :maxdepth: 1
    
    api-getting-started
    renew-token
    api-download-schemes-assemblies
    useful-api-params
    endpoint-reference
    enterobase-bionumerics
    


3rd Party acknowledgements
^^^^^^^^^^^^^^^^^^^^^^^^^^
If you use data generated by 3rd party tools in EnteroBase, please cite both #
EnteroBase and the paper describing the specific tool. 

* **rMLST** is Copyright 2010-2016, University of Oxford. rMLST is described in: `Jolley et al. 2012 Microbiology 158:1005-15`_.

* **Serovar predictions (SISTR)** have been calculated using the pipeline 
  developed by the `SISTR team`_ and is described in  `Yoshida et al. 2016 PLoS ONE 11(1): e0147101`_


.. _`SISTR team`: https://lfz.corefacility.ca/sistr-app/
.. _`Yoshida et al. 2016 PLoS ONE 11(1): e0147101`: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518