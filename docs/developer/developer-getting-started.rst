Developing EnteroBase (Developers only)
=======================================

Introduction
============

.. toctree::
    :maxdepth: 1

    developer-overview
    developer-overall-structure
    developer-installation
    developer-user-permissions
    developer-celery
    developer-api

Database structure
==================

.. toctree::
    :maxdepth: 1

    developer-webdatabase
    developer-database-structure
    developer-metadata-and-results-config
    developer-nservdb
    developer-adding-database


GUI and displaying results
==========================

.. toctree::
    :maxdepth: 1

    developer-webpage-generation
    developer-enterobase-grid
    developer-query-support 
    developer-custom-views
    developer-jbrowse

Schemes
=======

.. toctree::
    :maxdepth: 1

    developer-schemes
    developer-full-example
    developer-implemented-schemes

Tools and Pipelines
===================

.. toctree::
    :maxdepth: 1

    developer-jobs
    developer-analysis-objects
    developer-snp-projects
    developer-trees
    developer-mstree

Maintenance and support
=======================

.. toctree::
    :maxdepth: 1

    developer-maintenance-scripts
    developer-beta-mode
    developer-help-files
