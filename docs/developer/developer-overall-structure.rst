Overall Directory Structure
===========================
entero
------
The main python package containing all the python code. filename conventions include:

*   **view.py**: Server side code for displaying the various GUI data grids
*   **forms.py**: Server side code for displaying and responding to GUI forms

Some key subdirectories are:

*   **static**: Static files such as js and html files that are delivered directly to the client
*   **main**:
*   **api_1_0**: Support for the internal api between the client and the server
*   **api_2_0**: Support for the external http api
*   **databases**: SQLalchemy based definition of postgres database tables

scripts
-------

The folder contains shell scripts, which should be run every so often (e.g.
using crontab) to update and maintain the database. The two scripts run most
regularly are:-

* **daily_update.sh** Checks databases to see if any assemblies need to be sent or any  schemes need to be called. Simply calls [update_all_schemes](Maintenance Scripts#markdown-header-update_all_schemes) and :ref:`update_assemblies<update_assemblies>` for the databases specified in the script. The script can be changed to add/remove databases, change priorities,job queues etc.
* **daily_import.sh** Updates all databases by calling :ref:`importSRA<importsra>`

docs
----

Contains the source documents for building the documentation that appears on readthedocs

https://enterobase.readthedocs.io/en/latest/

A local copy of the documentation can be built in various formats.   html formatted documentation can be found in *_build/html* subdirectory and can be viewed by opening index.html in a browser. To make the documentation first install the python virtual environment using *install/createEnvironmentForDocumenationBuild.sh* and then activate the entero_docs environment and within docs run 

.. code-block:: bash

    make clean
    make html

It is also possible to make documentation based on code comments using sphinx-apidoc e.g.

.. code-block:: bash

    sphinx-apidoc -f -o source ../entero ../entero/databases/* ../entero/ExtraFuncs/unidecode

logs
----

Contains the log files. Info log contains general information e.g when an
assembly was sent or when a user uploaded a read, whereas the error log
contains information about all the exceptions thrown. The system log contains
information from the any scripts that are run (see below)

Root files
----------

**manage.py**

The file containing scripts which run in the context of the app see `flask-scripts <https://flask-script.readthedocs.io/en/latest/>`_. The scriptsavailable are documented :doc:`here<developer-maintenance-scripts>`

**requirements.txt**

A file containing all the python modules required. If extra modules are
added, this file should be updated by *pip freeze > requirements.txt*

**packages.txt**

Contains all the required linux packages

js_docs
-------

The html sub directory contains the actual JavaScript documentation produced
from the inline code comments. Just open the index.html in this folder with a
browser to view the documentation. In order to update documentation jsdoc
needs to be installed (*npm install jsdoc*). Also a bootstrap template is
used (*npm install ink-docstrap*). In the config.json file change the
template in opts to the correct location (depending on where ink-docstrap was
installed).

.. code-block:: json

    "opts":{
        "destination":"html",
        "readme":"README.rst",
        "template":"/usr/local/lib/node_modules/ink-docstrap/template"
    }


If you want to use the default template, then just delete the template entry
in the config. Any new .js files need to be added to the include entry in the
config file.

.. code-block:: json

    "source": {
        "include":
            [
             "../entero/static/js/tree/d3_m_tree.js",
             "../entero/static/js/table/validation_grid.js",
              ............
            ]
    }


To produce the documentation just cd to the *jsdoc directory* and run 
.. code-block:: json

    jsdoc -c conf.json

