Beta mode operation
===================

A beta mode option is available that allows enterobase to operate using a URL such as:

.. code-block::

    https://enterobase.warwick.ac.uk/beta

Prefixes other than beta can be used by modifying class BetaConfig(Config): in entero/config.py. To run in beta mode the enterobase-web start script should use:

.. code-block::

    "entero:create_app('beta')"


In beta mode the code can be accessed through URLs such as http://enterobase-3:8001/beta/.  In order to be accessed through an nginx firewall with a URL such as https://enterobase.warwick.ac.uk/beta/ something along the lines of the following needs to be added to the nginx.conf file

.. code-block::

    location ^~ /beta {
        proxy_pass http://enterobase-3.warwick.ac.uk:8001;
        ...

There are a number of aspects of the code that are required to support the /beta prefix

Python code changes
-------------------

This is the starting point in that the config variable app.config['BETA_URL'] is an empty string in the normal mode or contains the beta url prefix.

The URLs supported by Enterobase are serviced using Flask bluprints that are initialised in entero/init.py.  The app.config['BETA_URL'] value is prefixed to ensure that that Flask services the set of URLs prefixed by /beta if required.  This also means that if the code asks for the URL associated with auth, the code will return the URL prefixed by /beta

.. code-block:: python

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix=app.config['BETA_URL'] + '/auth')

In addition, there is code in entero/init.py that ensures the beta_url suffix is autmatically added to the CALLBACK_ADDRESS that is included in CRobot job requests

html file changes
-----------------

The source of the html files are in the entero/templates directory.  Where the URL should optionally be prefixed with the beta URL {{beta_url}} is inserted in the html.  The Flask architcure then replaces this with /beta or a blank string depending on the mode. 

.. code-block::

   {% block tail_js %}
        <script src ="{{beta_url}}/static/js/main/enterobase.js?version={{config['JAVASCRIPT_VERSION']}}"></script>

The code that injects this additional substitution is the following in entero/main/views.py:

.. code-block:: python

    @app.context_processor
    def inject_paths():
        return dict(beta_url=app.config['BETA_URL'])

javascript changes
------------------

There are various places where the javascript needs to include beta mode related changes. This is done using the beta_url() method such as in the following example.

.. code-block:: js

   uri = beta_url() + uri

The code for defining beta_url is the following in templates/base.html 

.. code-block:: js

    function beta_url() {
          return "{{beta_url}}";
    }

Servicing of static files
-------------------------

The Flask default is that the URL for all static files are prefixed with /static, and are then retreived from the entero/static directory.  In beta mode the static files will be prefixed with /beta/static, so the following code has been added to main/views.py

.. code-block:: python

    @main.route('static/<path:path>')
    def send_beta_static(path):
        return app.send_static_file(path)
    
which duplicates the built in static file loading in teh normal mode, but ensures the static files are loaded in beta mode.

Managing cookies
----------------

If a beta mode system is accessed through the same root URL as system running in normal mode then they will share cookies and logins .  The following code in __init_.py ensures that cookies in the two mode remain separate.

.. code-block:: python

    app.config['SECRET_KEY'] = app.config['BETA_URL'].replace("/", "") + app.config['SECRET_KEY']
    app.config['SESSION_COOKIE_NAME'] = app.config['BETA_URL'].replace("/", "") + "_" + app.config['SESSION_COOKIE_NAME']








