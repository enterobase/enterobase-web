Trees
=====
Directory Structure
-------------------

.. code-block:: bash

    entero
    │      
    └───templates
    │   │   
    │   └───ms_tree
    │         enterobase_generic_tree.html
    │         MSTree_holder.html*
    │         PhyloTree_holder.html
    │   
    └───static
        │
        └───js
        │    │
        │    └───ms_tree
        │    │     base_tree.js*
        │    │     d3_m_tree.js*
        │    │     d3_p_tree.js
        │    │     grid.js*
        │    │     context.js*
        │    │
        │    └───SlickGrid
        │
        └───css
               bootstrap.min.css
               grapetree.css*

(files with an asterix need to synched with the standalone version of GrapeTree)

There are two trees phylogenetic (SNP) and MS (GrapeTree), both inherit from
the same class D3BaseTree (in base_tree.js) To display the trees the
following urls can be used:-

* *enterobase_root*/ms_tree/*tree_id* **or** *enterobase_root*/ms_tree?tree_id=*tree_id*
* *enterobase_root*/phylo_tree/*tree_id* **or** *enterobase_root*/phylo_tree/*tree_id*

The urls are specified in main/views and all call the generic method
generate_tree_page in the entero/main/views module . This method obtains
information about the tree, checks the user has permission (if a non-public
tree) and renders the template. Different templates are rendered depending on
the tree type, MSTree_holder.html and PhyloTree_holder.html. Both include
shared functionality in Enterobase_generic_tree.html (see below)

Integration of Grape Tree with Stand Alone Version
--------------------------------------------------
In order to to integrate the stand alone and Enterobase versions of
GrapeTree, the file structure and architecture of Grape Tree in Enterobase is
slightly complicated. In entero/templates/ms_tree is *MSTree_Holder.html*,
which is the same as the standalone version and any changes in this file will
effect both. Similarly in* entero/static/js/tree* , the following files have
to be synched with the stand alone version

* *base_tree.js*
* *d3_m_tree.js*
* *help.js*
* *grid.js*
* *context.js*

Also entero/static/css/grapetree.css also needs to be synched

MS_Tree_holder.html contains the following lines, thus ensuring
enterobase_generic_tree.html will only be included in the web version

.. code-block:: html
    
    <!--
    {% if tree_id%}
    {% include 'ms_tree/enterobase_generic_tree.html' %}
    {%endif %}
    -->

*entero_base_generic_tree.html* contains code specific to the enterobase
version and will attempt to load the tree whose id was supplied by flask (in
the variable tree_id). In this file, the method *processEnteroData* will
process the data obtained from enterobase e.g. convert legacy data, combine
layout and actual data. This data is then passed to the generic *loadMSTree*
function in *MSTree_holder.html*

Tree Data
---------
Trees are stored in user_preferences and manipulated via Analysis Objects
like any other Analysis Types. Trees are stored in three files data.json,
layout.json and a nwk file. The location of these files are specified in the
objects data, but are generally located in

.. code-block:: html

    <base_workspace_folder>/<database>/<user_id>/<tree_id>/

* **data_file**  The location of the file containing json data in the following format
    * **isolates** A list of the metadata for each strain. Each item in the list consists of a dictionary of  key/value pairs, with 'StrainID' being the strain id in the database and ID being the node ID that the strain is associated with. For legacy trees, this will be ST, but in more modern trees (which have node data specified in a nwk file, ID is the be assembly barcode , which are the identifiers used in the nwk file e.g 
 *{"StrainID":5641,"ID":"ESC_BA6583AA_AS","Name":"bug1","Country":"Germany",....}*

  
    * **metadata_options** A dictionary of fields to information about that field
        * **label** The only parameter required - the human readable label
        * **category** The type of field. For Enterobase data this can be one of four values
            * Custom - the field mirrors a custom column in Enterobase
            * User Added - a column added by the user that is specific to the tree
            * Metadata - standard metadata from Enterobase
            * Experiment - a column from experiment or scheme data
        * **coltype** - the type of data e.g. character or number
        * **grouptype** - how the categories are ordered e.g size alphabetical
        * **colorscheme** - either category or gradient
    * **current_metadata** The name of the metadata last displayed in the tree

* **layout_file** The location of the file containing the layout information of the tree
    * **node_positions** A dictionary of node id to an array of x,y co-ordinates e.g {"ST232":[762,232],"ST233":[22,33],...}
    * **display_category** The category currently displayed
    * **nodes_links** Extra information about the nodes/links:
        * **max_link_scale**
        * **max_link_length**
        * **base_node_size**
        * **size_power**
        * **scale**
        * **translate**
        * **link_font_size**
        * **show_link_labels**
        * **show_node_labels**
        * **node_font_size**
        * **custom_colors**
        * **hide_link_length**
        * **show_individual_segments**
        * **node_collapsed_value**
        * **node_text_value**
        * **manual_collapsing**
        * **square_root_scale**

* **nwk_file** the nwk file containing information for the node layout. The identifiers in the nwk file are assembly barcodes