Query support
=============

A key component of Enterobase is the ability to perform queries to select strains that meet certain criteria.  This can be done via:

    - The query dialog box
    - The API

Query dialog box
----------------

The query dialog box is generated using html code followed by extensive javascript at the start of the edit_strain_metadata.html template ( see :ref:`Template Hierarchy`). which is created by species/views.py:search_strains. 

The details of the grid and the search box are provided by the extensive javascript starting at '{% block windowready %}' in edit_strain_metadata.html 

The information to populate all the dropdowns in the query box is then obtained by using a series of call_restful_JSON calls starying with a call to the URL /get_experiment_details which is handled by main/views.py/get_experiment_details() right at the end of the javascript.

The javascript function submitQuery() is then used to submit the query consolidating the query information before moving to submitQuery2() which uses /get_data_for_experiment to retreive the selected strains, their metadata and the data for the currently selected exterimental_data pane.


Scheme specific query code
--------------------------
All schemes/pipeline jobs must identify the query method that is used to query the associated data.  It is identified by the  "query_method" parameter in the param column of the schemes table.  These data are used in initialisation code at the end of query_functions.py to populate the query_function_handle multilevel dictionary with information about what function to call for each species/scheme combination.

Selecting data to be displayed and transfer of data from client to server
.........................................................................
The query dialogbox is used to generate the info as to what strains are to be displayed which is passed to the code in **ExtraFuncs/query_functions.py** in SQL format.  This is then used to collate the data from a combination of sources such as:

    - Strain data from the strains table using **process_strain_query**
    - Experimental data from one of:
        - Assembly data from the assemblies table using **get_assembly_stats**.
        - MLST and ST (Nomenclature) data from NServ using the **__nserv_q** procedure
        - Generic Non-MLST data from the results entry of the other_data JSONB column from the assembly_lookup table entry for the strain/scheme combination using **process_generic_query**
        - AMR analysis data from the amr_analysis table using the **process_amr_analysis_query** procedure
        - Custom data in one or more custom columns using **process_custom_view_query** 

The **process_project_info_query** function is used to demonstrate the principles of such queries as described in :ref:`Scheme Based on an NCBI API call`¶

The SQL query associated with the query is modified as required to be able to select the requested strains, and the raw data from the various sources is converted to JSON to be returned to the client. The code for querying the experimental data takes one of two forms of queries:

    - A simple list of assembly ids such that the associated experimental data can be returned. One occasion when this is used is when the user has already selected a set of strains, and then switches to a different set of associated experimental data.
    - A full SQL query of the experimental data which is used to select the strains that meet the criteria.  This will be combined (AND or OR) with the accompanying query on the strain metadata.


 