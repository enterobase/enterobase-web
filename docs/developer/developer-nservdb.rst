EnteroBase Database Structure (NServ)
=====================================
Data in NServ are hosted in PostgreSQL. All the important columns for each table shown
in the boxes on left. The data transfer between
Python and PostgreSQL are handled by the SQLAlchemy module. NServ also adapts Flask and
Celery to maintain two sets of APIs for external communications: the Readonly APIs are
redirected to external users via the web-interface and API in Enterobase, and the three
“Nomenclature APIs” require write privileges and are currently only accessible from the
backend calculation engine in Enterobase. Most of the relationships among different tables are
hard-coded in PostgreSQL as foreign keys, except for the “value_indices” field in “types” table,
which is too large to be efciently handled in PostgreSQL. The validation of this feld is then
controlled by a Python function in NServ. A cache of the data in the “value_indices” field is also
maintained as Numpy array and used for rapid searching of STs via “type_search” API. All the
tables except for “fields_in_scheme” are versioned. All modifications to these tables are
automatically registered in the “archives” table using a trigger in PostgreSQL. There is also a
“barcode” field in most of the tables. The barcodes from different databases use different
prefixes to ensure their global uniqueness and thus can be used to retrieve data via the query
API, without prior-knowledge of their original databases.


.. figure:: /images/nserv_imgonly.png
    :width: 600px