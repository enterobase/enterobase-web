Installing The Source Code
==========================

* If not already installed *sudo apt-get install git*
* Navigate to the appropriate directory (this will be the parent directory of the code)
* *git clone https://my_user_name@bitbucket.org/enterobase/enterobase-web.git*


Software prerequisites
----------------------
Depending on your system you may have to install the following dependencies:-

* *sudo apt-get install build-essential*
* *sudo apt-get install zlib1g-dev*
* *sudo apt-get install libssl-dev*
* *sudo apt-get install bzip2  libbz2-dev*
* *sudo apt-get install libreadline8 libreadline8-dev*
* *sudo apt-get install sqlite3 libsqlite3-dev*
* *sudo apt-get –y install python-openssl*
* *sudo apt-get install postfix*

Tabix is required for vcf downloads in SNP projects:

* *sudo apt-get install tabix*

Installing Python
-----------------

In order to keep a clean local python environment pyenv needs to be installed:-

* In your home directory *git clone https://github.com/yyuu/pyenv.git ~/.pyenv*
* *echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc*
* *echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc*
* *echo 'eval "$(pyenv init -)"' >> ~/.bashrc*
* *exec $SHELL*

Installing virtualenv will help manage your virtual environments

* *git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv*
* *echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc*
* *exec "$SHELL"*

Next need to install the correct version of python within pyenv

* *pyenv install 3.8.8*

Now you are ready to create the virtual environment

* *pyenv virtualenv 3.8.8 entero*

To activate the environment

* *pyenv activate entero*

Next install all the required python packages, which are listed in the requirements.txt file in the base enterobase directory (make sure you are in your virtual environment)

* *pip install -r requirements.txt*

Installing an IDE
-----------------

First install an xwindows server such as x2go or vncserver(used at Warwick) such that you open x windows on your local machine

The pyCharm IDE is currently being used succesfully for enterobase-web development and should be installed as appropriate to the server environment.

PyCharm General Setup

* In the main menu go to File>New Project and select the directory where the enterobase has been cloned
* In the interpreter section select "Previously configured interpreter", then select 'Add interpreter/Add local interpreter', Set Environment to "existing" and select/find ~/.pyenv/versions/entero/bin/python
* Untick 'Create a main.py wlecome script'
* Click "select" and then "Create from existing sources"
* Make sure you tell wing to use a custom python executable and point it to the python file in your virtual environment (e.g. /home/<user_name>/.pyenev/versions/<virtual_env_name>/bin/Pyhton2.7). N.B Right click on the file popup in order to see hidden files and navigate to to .pyenv directory.

Debugging with PyCharm:-

The following allows a 2 process version of enterobase-web to be run or debugged.   <homedir> is the home directory of the user where enterobase-web has been installed, <username> the username and <port> is the port number that is being used

* Select 'Current File'/'Edit Configurations'
* Click '+' (top left) 'Python'
* Set Script path to "<homedir>/.pyenv/versions/2.7.14/envs/entero/bin/gunicorn"
* Set parameters to "entero:create_app('development')" -b 0.0.0.0:<port> --timeout 300 --name Entero --user=<username> --bind=unix:<homedir>/sock --log-file=<homedir>/enterobase-web/logs/gunilog.log --access-logfile=<homedir>/enterobase-web/logs/access.log -error-logfile=<homedir>/enterobase-web/logs/error.log --workers 2
* Set Working directory to <homedir>/enterobase-web

Note that it may be necessary to open up the port to allow the enterobase web server to be accessible from other servers or other machines on the network, e.g. 

.. code-block:: bash

    sudo ufw allow <port>/tcp

Running the app with Gunicorn
-----------------------------
create a file called start_gunicorn.sh with the following content

.. code-block:: bash

        NAME="enterobase"
        FLASKDIR=/home/admin/enterobase-web/   # Change this to location of app.py
        SOCKFILE=/var/www/enterobase/sock # change this to project_dir/sock (new file will be created)
        USER=admin                        # Change this to your user/group
        GROUP=admin
        NUM_WORKERS=8
        LOG=/var/www/enterobase
        echo "Starting $NAME as `whoami`"

        export PYTHONPATH=$FLASKDIR:$PYTHONPATH

        # Create the run directory if it doesn't exist
        RUNDIR=$(dirname $SOCKFILE)
        test -d $RUNDIR || mkdir -p $RUNDIR

        cd $FLASKDIR

        # Start your unicorn
        exec /home/$USER/.pyenv/versions/entero/bin/gunicorn "entero:create_app('production')" -b 0.0.0.0:8000 \
          --enable-stdio-inheritance \
          --access-logfile=$FLASKDIR/logs/access.log \
          --error-logfile=$FLASKDIR/logs/error.log \
          --name $NAME \
          --workers $NUM_WORKERS \
          --user=$USER --group=$GROUP \
          --log-file=$LOG/gunilog.log \
          --bind=unix:$SOCKFILE \
          --timeout 2000

The app can be run with *nohup start_gunicorn.sh &* , but installing this as a service may be more appropriate


Allowing Emails
---------------
On Jenner you need to add the IP address of the new server:-

* sudo vim /etc/postfix/main.cf
* Add the  IP address in parameter "mynetworks = "
* sudo /etc/init.d/postfix restart