Configuration info for metadata and results fields
==================================================

Configuration for the metadata associated with strains and also the experimental/analysis results are specified in the data_param table.
There is a row or field in the table for each of the different metadata and results values.  There is a row for each of the columns that are displayed on the GUI for a strain, and also rows for hidden data that can nevertheless by displayed in custom columns or incoporated into queries.

Each field can have the following columns

* **tabname** strains for LH pane columns or the name of the scheme for experimental data
* **name** The programatic name of the filed (used in the the python and JavaScript code)
* **sra_field** The path in the json data returned from metaparser after downloading the data from NCBI which corresponds to the field. e.g. For Source Niche *Sample,Metadata,Source Niche* would retrieve the Environment from the the following data

.. code-block::

    {
        "Sample":{
           "Metadata":{
                 "Source Niche":"Environment",
                  ........,
            },
            .......
        },
        ......
    }


* **mlst_field** Obsolete (the name of the field in the legacy MLST database)
* **display_order** The order of the column in the grid (can be negative)
* **nested_order** 0 for normal fields. If anything else then this will be displayed as a compound field e.g. continent, country etc. The number is the order in the compound field
* **label** The label that will be displayed to the user
* **datatype** Describes how the datatype will be validated. Can be 
    * text
    * combo - only values specified in *vals* are allowed
    * suggest - values in *vals* will be suggested but not enforced
    * integer
    * double
    * custom - validation is complex and is carried out by a method in the grid (see below)
* **min_value** If the datatype is an integer/double than this is the minimum value allowed
* **max_value** If the datatype is an integer/double than this is the maximum value allowed
* **required** 1 if a value has to given for the field, otherwise 0. If it is not essential but is present, it must still conform to any validation criteria
* **default_value** If no value is present, then this value will be entered into the grid
* **allow_pattern** If the field is text then it will have to conform to this Standard pattern matching
* **description** A brief description of the field
* **vals** A list of values (separated by |), which are either enforced (datatype is combo) or suggested (datatype is suggest) 
* **max_length** The maximum length (in characters) of the value
* **allow_multiple_values** If 1 then more than one value for a field is allowed. e.g Accession can have more than one value since many reads can be associated with the same strain.
* **no_duplicates** Not actually enforced
* **param** A jsonb column with additional data.  "admin_only": "true" will cause the column only to be visible and searchable for adminstrators.  Also used to hold additional information associated with genes in schemes.

Adding a Custom Validator
-------------------------

The method accepts a function which takes the column name and row_id . From this the value of the cell can be obtained (or other values in the row). The appropriate message (if validation fails) or empty an string should be passed on to the updateErrors() method which will return true of false

.. code-block:: javascript

    grid.addCustomCellValidator("column_name",function(col_name,row_id){
        var col_index = grid.getColumnIndex(col_name);
        var row_index = grid.getRowIndex(row_id)
        var value = grid.getValueAt(row_index,col_index);
        var msg ="";
        //validate value - change msg to reason failed if appropriate
        return grid.updateErrors(colName,rowID,msg)
    
    });
