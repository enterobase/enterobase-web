Database homepage
=================

From the main page, click on a database home link to be brought the dashboard for 
that database. This page provides a detailed overview of each genera and there 
are links to a number of tasks you can perform in EnteroBase.

* *Search strains* will allow you to query the database for records of interest (red). 
* *Upload Reads* will allow you to upload your own sequence reads for analysis (green).
* *Show my Jobs* will show analysis jobs related to your data (blue).
* *Load workspace* will open up `workspace dialog box <using-workspaces.html#user-space-os>`_.
* Manage your account through the dropdown in the top right.  

These links on the side bar and along the top (grey) will always be present as 
you navigate deeper into the website so you can easily jump to another task.

  .. image:: https://bitbucket.org/repo/Xyayxn/images/692044187-enterodash.png