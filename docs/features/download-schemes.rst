Downloading cgMLST-rMLST profiles from the website
==================================================

This is a worked example, taking you step by step through downloading cgMLST
allele profiles for Salmonella on
`EnteroBase <https://enterobase.warwick.ac.uk/>`_. The same process applies to
downloading rMLST allele profiles.

This example will use serovar Agona. `See this paper for more
details <http://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1003471>`_.

* **First, Login to EnteroBase and click 'Database Home' for Salmonella**

.. image:: https://bitbucket.org/repo/Xyayxn/images/199579721-1.png

* **From the database dashboard, click Search Species**

.. image:: https://bitbucket.org/repo/Xyayxn/images/1063368578-2.png

* **In the search window, do the following:**

.. image:: https://bitbucket.org/repo/Xyayxn/images/4214395826-3.png

* **Set experimental data as cgMLST**

.. image:: https://bitbucket.org/repo/Xyayxn/images/2170359770-4.png

* **You can look at individual records using the Eye**

.. image:: https://bitbucket.org/repo/Xyayxn/images/916459260-5.png

* **Right click on the cgMLST data (the window) and from the drop down click save all.**

.. image:: https://bitbucket.org/repo/Xyayxn/images/1458350067-6.png

* **It takes a bit for the data to be downloaded (~3600 alleles for each genome) when its done, you'll be prompted for a save file name. The file will be save in your 'Downloads' folder of your browser.** 

.. image:: https://bitbucket.org/repo/Xyayxn/images/1746242357-7.png

* **It is a tab-delimited text file which you could load into Excel.**

.. image:: https://bitbucket.org/repo/Xyayxn/images/598369913-8.png


.. image:: https://bitbucket.org/repo/Xyayxn/images/1650302314-9.png


* **Share and enjoy**

* **You can load it into your favourite bioinformatics or data analysis software - such as R or Excel - and do further bioinformatics analysis, data visualisation, etc with it!**

* `Here is the sample metadata I generated (click to download) <https://enterobase.warwick.ac.uk/static/example/agona-rmlst-meta.tsv>`_.
* and the `cgMLST profile to go with it (click to download) <https://enterobase.warwick.ac.uk/static/example/agona-rmlst.tsv>`_.