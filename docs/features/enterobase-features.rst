Key features in Enterobase
==========================

Introduction
------------

.. toctree::
    :maxdepth: 1

    species-index
    main-search-page
    using-the-browser
    metadata-fields
    experimental-fields
    amr-analysis

Searching and Viewing Results
-----------------------------

.. toctree::
    :maxdepth: 1

    locus-search
    search-st-types-from-alleles
    st-allele-search
    searching-within-neighbours

Downloading, editting and uploading data
----------------------------------------

.. toctree::
    :maxdepth: 1

    user-download-assemblies
    user-download-schemes
    user-download-annotations
    download-schemes
    editing-strain-metadata
    add-upload-reads

Advanced features
-----------------

.. toctree::
    :maxdepth: 1

    buddies
    ms-trees
    snp-projects
    enterobase-snps-jbrowse
    using-workspaces
    user-defined-content
    accessory-genome
    clustering
    jobs
