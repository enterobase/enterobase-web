Downloading assemblies
======================
If a search has been carried out, assemblies can be downloaded by clicking in the
boxes with the tick/ check icon heading (on the far left of the results table)
in order to select the desired strains and then clicking on the icon - on the
toolbar above the results table - with a blue circle with a white arrow inside
in order to download the selected assemblies. (Assemblies will not be available
for download in the case of entries for strains in the results from legacy MLST)

Note that most web browsers limit the number of files that can be downloaded at the
same time like this to about 1000 so if you are trying to download more than 1000
you will have to select blocks of just under 1000 and download each block separately.
To select a batch of strains click the select column for the first strain and then ‘shift click’ for the last strain


An example including a search and download of assemblies is on the :doc:`searching-within-neighbours` page.
Programmers may also be interested in download of assemblies :ref:`Downloading Assemblies using the API`.

  .. image:: /images/download-assemblies.png