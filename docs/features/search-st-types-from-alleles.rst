Search ST types from alleles
============================

**Go into a genus database.**

.. image:: https://bitbucket.org/repo/Xyayxn/images/1380358089-1.png

**Find ST(s)**

.. image:: https://bitbucket.org/repo/Xyayxn/images/4227650429-2.png

**Type in complete/partial allele profile**

.. image:: https://bitbucket.org/repo/Xyayxn/images/185328180-3.png

**Find all strains that carry corresponding alleles**

.. image:: https://bitbucket.org/repo/Xyayxn/images/1161917760-4.png