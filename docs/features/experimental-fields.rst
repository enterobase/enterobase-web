Experimental Data
=================

MLST schemes (7 Gene MLST, rMLST, cgMLST, wgMLST)
-------------------------------------------------
* ST (rST, cgST, wgST)
   An arbitrarily assigned integer that associates with a unique combination of allelic profile. 
   Negative numbers are given in 7 Gene MLST schemes and the rMLST scheme for profiles contain missing genes (loci). 
   The rMLST allelic profiles that have not been manually confirmed by `rmlst.org <https://rmlst.org/>`_ 
   will also be given a temporary negative number. 
* eBG (RMLST eBG, ST Cplx)
   Designations for clusters of STs in some 7 Gene MLST schemes as well as the rMLST scheme for *Salmonella* genus. 
   eBG is a number that is unrelated to the STs in the cluster, while ST Cplx is named after the most commonly identified ST in it. 
   Both are initially constructed using `eBURST <http://eburst.mlst.net/>`_ algorithm and now updated in first come first serve basis, 
   in order to avoid merging of closely relate clusters. 
* Serotype (Predicted)
   *Salmonella enterica* strains that are assigned into the same eBG/reBG often have identical serotype as well. 
   Thus we can use eBG/reBG designations of genomes to predict the serotypes. The prediction visualized in the experimental fields
   are the most frequent serotype in a eBG/reBG group. Click the cell to get a detailed destribution of serotypes in an eBG/reBG group. 
* Species (rMLST)
   The rMLST STs are a good predictor to the bacterial species of the strain. EnteroBase reports the species designation served 
   in `rmlst.org <https://rmlst.org/>`_ for all existing rSTs, and predict species of a new coming rST by summarising species 
   designations of known STs that share at least 10 alleles with the new rST. 
* HCxxx
   HCxxx are fields for Hierarchical Clustering of cgMLST (HierCC). HierCC uses single-linkage clustering method to assign cgMLST STs 
   into clusters of different levels. The number after HC presents the maximum amount of differences allowed in the group. 
   For example, strains in the same HC100 group connect each other in a single-linkage clustering tree with no more than 100 
   allelic differences. The numbers in the cells are group designations. A suggested designation for HC groups is 
   'HC<level id>|<group id>'. For example, all identified *Salmonella enterica* serovar Agama are in group HC2000|299. 
* Eye icon (genes)
   It is not feasible to show the allelic designations of 1000's of genes in a cgMLST or wgMLST scheme in the experimental fields. 
   The users can get allelic profile of a ST by clicking the related Eye icons. The allelic profiles of a genome (except for the private ones) 
   can also be downloaded as a file by choosing 'Download Allelic Profile' option from the right-click menu. 
* Differences
   The differences column is normally hidden, and only visible after a "Find ST(s)" search. The numbers in the cells present the allelic differences 
   between the found STs and the ST used for query.

Assembly stats
--------------
* Status
* Version
* Assembly Barcode
* Coverage
* N50, Length, Low Quality Bases & Contig Number (>= 200 bp)

Annotation
----------
* GBK Format
* GFF Format
* Eye icon

Serotype Prediction (SISTR) (in *Salmonella* database)
------------------------------------------------------
* Serovar
* Serogroup
* H1
* H2

Serotype Prediction (in *Escherichia* database)
-----------------------------------------------
* O Antigen
* H Antigen

Phylotypes
----------
* Clermont Type (ClermonTyping)
* Clermont Type (EzClermont)
* fimH (fimTyper)
* Pathovar (BlastFrost and derived from HierCC)
* Stx1/2/pInv/ST/LT/eae (BlastFrost)