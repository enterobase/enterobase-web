Metadata Fields
===============

Helper Columns
--------------

|check-icon|\ Select Column
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Multiple rows can be selected for bulk processing if the leftmost
checkbox is ticked. Multiple rows can also be selected in the usual way
by using shift+click. Selected rows are the target for assembling
multiple genomes or downloading multiple assemblies. By right clicking,
a context menu will allow you to select/unselect all rows.

Edit Column
~~~~~~~~~~~

This column shows whether you have editing rights on the row. A pencil
icon means that the row can be edited either directly by clicking onto a
cell or by clicking on the pencil icon and editing all the records at
once, A red background shows that the row contains errors and a yellow
background indicates changes have been made in the row.

Fields
------

Uberstrain
~~~~~~~~~~

Name
~~~~

A unique name given to the strain.

Data Source
~~~~~~~~~~~

This cell shows the SRA run accession associated with strain. If there
is a single record associated with the strain, the accession number will
be shown otherwise ‘many’ and the number of records in brackets will be
displayed. Alternatively, it will indicate that the data is legacy MLST
data and no genome as such is associated with the strain. Clicking on
this column will produce a popup showing a summary of the SRA data with
the following fields:

-  Accession No.
-  Sequencing Platform - the sequencing method
-  Sequencing Library
-  Insert Size
-  Experiment
-  Bases
-  Average Length
-  Status

Barcode
~~~~~~~

Source
~~~~~~

The source of the strain. It is divided into three hierarchical levels:

-  Source Niche
-  Source Type
-  Source Details

Collection Date
~~~~~~~~~~~~~~~

The date when the strain was collected. The should be in the format
DD/MM/YYYY, MM/YYYY or YYYY so 27/3/2010, 3/2010 or 2010 are all
acceptable. The date can either be typed in by clicking on the cell or
via a date dialog by clicking on the calendar icon.

Location
~~~~~~~~

The location where the strain was isolated. This is divided into
subsections according to the `GeoNames`_ ontology. Clicking on the cell
will open up a dialog showing these levels and an auto-correct facility
allows entry of all levels at once:

-  Continent - either Africa,Asia,Europe,North America,South America or
   Oceania.
-  Country - country names are derived from either `Google Maps API`_ or
   `GeoNames`_. For a full list of countries, go to `Wikipedia`_.
-  Region - this corresponds to the administration level 1 region.

Serovar
~~~~~~~

Subspecies
~~~~~~~~~~

Disease
~~~~~~~

Antigenic Formulas
~~~~~~~~~~~~~~~~~~

Lab Contact
~~~~~~~~~~~

Phage Type
~~~~~~~~~~

Antibiotic Resistance
~~~~~~~~~~~~~~~~~~~~~

Comment
~~~~~~~

Project
~~~~~~~

Sample
~~~~~~

Date Entere
~~~~~~~~~~~

.. _GeoNames: https://www.geonames.org/
.. _Google Maps API: https://developers.google.com/maps/
.. _Wikipedia: https://en.wikipedia.org/wiki/List_of_countries

.. |check-icon| image:: https://bitbucket.org/repo/Xyayxn/images/3241181882-check.png
.. |pencil-icon| image:: https://bitbucket.org/repo/Xyayxn/images/2965366802-pencil-icon.png%20=24x
