Tutorial: Making your own GrapeTree Links 
=========================================
GrapeTree offers a choice to publish your interactive GrapeTree analysis online. 
This can be done by using two URL parameters. 

Parameters:

* **tree** = *<online file for newick tree or json saved GrapeTree>*
* **metadata** = *<tab-delimited or comma-delimited table>*


Due to the `CORS restrictions <https://en.wikipedia.org/wiki/Cross-origin_resource_sharing>`_ 
in jscript codes, only three sources have been tested as working:

* Files from the same domain as the GrapeTree server. 
* Files in a GitHub public repository.
* DropBox files that have been publicly shared via links.


There are different ways of publishing data.

1. For normal users without a website. 
--------------------------------------

You can publish your analysis online via GitHub preview version.  For example: 

* https://achtman-lab.github.io/GrapeTree/MSTree_holder.html?tree=https://raw.githubusercontent.com/achtman-lab/EnteroMSTree/master/examples/ebola.date.json

You can shorten these links by `google URL shortener <https://goo.gl/>`_ or `bitly <https://bitly.com/>`_. i.e.:

* http://bit.ly/2H8py8F 

2. Owners of websites. 
----------------------
Please run the standalone version of GrapeTree and serve the URL under the same 
domain as your main website. GrapeTree can read links from your website, as 
long as they were under the same domain.

You can either host the tree files in a different local link, or fetch them from
external link at the backend and  re-distribute them via a 
`proxy link <http://promincproductions.com/blog/server-proxy-for-cross-origin-resource-sharing-cors/>`_.