Top level links: 

* **[Main top level page for all documentation](Home)**
* **[EnteroBase Features](Home)**
* **[Registering on EnteroBase and logging in](Enterobase%20website)**
* **[Tutorials](Tutorials)**
* **[Using the API](About%20the%20API)**
* **[About the underlying pipelines and other internals](EnteroBase Backend Pipeline)**
* **[How schemes in EnteroBase work](About%20EnteroBase%20Schemes)**
* **[FAQ](FAQ)**

# refMapper_matrix #

[TOC]

# Overview #

refMapper_matrix generates a SNP matrix (not a distance matrix) file from GFF files.  (Usually the GFF files will have
been created as annotations of SNPs subsequent to alignment of assemblies' sequences versus a reference assembly
by [refMapper].)

The refMapper_matrix pipeline is usually invoked as part of a
[workflow to create a SNP tree].

refMapper_matrix is currently in version 2.0.

# Summary #

refMapper_matrix reads GFF files (generally expected in a specific format written by [refMapper]) that
each annotate mutations in a genome assembly and also regions that successfully align with the reference, repetitive regions and regions with uncertain base calling or ambiguous alignment.  It outputs a single SNP matrix
file for all of the assemblies which documents mutations and their type (including identity,
synonymous and non-synonymous SNPs, stops and frameshift mutations) at positions in the genome assemblies
that have variation.

  [refMapper]: EnteroBase%20Backend%20Pipeline%3A%20refMapper
  [workflow to create a SNP tree]: EnteroBase%20Backend%20Pipeline#markdown-header-snp-trees