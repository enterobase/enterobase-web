EnteroBase Backend Pipeline Optional Genera Assembly Criteria
=============================================================

### Assembly criteria for Listeria ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 2.5 Mbp – 3.3 Mbp |
| N50 value | >20kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >70% contigs |

### Assembly criteria for Clostridioides ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 3.6 Mbp – 4.8 Mbp |
| N50 value | 20kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Klebsiella ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 4.3 Mbp – 7.3 Mbp |
| N50 value | >20kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Photorhabdus ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 4 Mbp – 6 Mbp |
| N50 value | >20kb |
| Number of contigs | <700 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Helicobacter ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 1.3 Mbp – 3 Mbp |
| N50 value | >20kb |
| Number of contigs | <300 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Vibrio ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 3 Mbp – 5.3 Mbp |
| N50 value | >20kb |
| Number of contigs | <700 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Mycobacterium ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 2.8 Mbp – 5.5 Mbp |
| N50 value | >15kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >70% contigs |

### Assembly criteria for Neisseria ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 1.8 Mbp – 2.4 Mbp |
| N50 value | >20kb |
| Number of contigs | <500 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |


### Assembly criteria for Peptoclostridium ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 2.5 Mbp – 4.8 Mbp |
| N50 value | >20kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Assembly criteria for Clostridium ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 2 Mbp – 7 Mbp |
| N50 value | >20kb |
| Number of contigs | <800 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >65% contigs |

### Default assembly criteria ###

|    Metrics                                           |    Criteria           |
|------------------------------------------------------|-----------------------|
| Number of bases | 4 Mbp – 5.8 Mbp |
| N50 value | >20kb |
| Number of contigs | <600 |
| Proportion of scaffolding placeholders (N’s) | <3% |
| Species assignment using Kraken | >70% contigs |