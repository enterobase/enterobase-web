Details of assembly methods and in silico genotyping for *Yersinia*
===================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`



+---------------------+--------------------+-------------------+-------+
| MLST – Classic      | Ribosomal MLST     | Core Genome MLST  | Whole |
|                     | (Jolley, 2012)     |                   | Genom |
|                     |                    |                   | e     |
|                     |                    |                   | MLST  |
+=====================+====================+===================+=======+
| 7 Loci              | 51 Loci            | 1,553 Loci        | 19,53 |
|                     |                    |                   | 1     |
|                     |                    |                   | Loci  |
+---------------------+--------------------+-------------------+-------+
| Conserved           | Ribosomal proteins | Core genes        | Any   |
| Housekeeping genes  |                    |                   | codin |
|                     |                    |                   | g     |
|                     |                    |                   | seque |
|                     |                    |                   | nce   |
+---------------------+--------------------+-------------------+-------+
| Highly conserved;   | Highly conserved;  | Variable; High    | Highl |
| Low resolution      | Medium resolution  | resolution        | y     |
|                     |                    |                   | varia |
|                     |                    |                   | ble;  |
|                     |                    |                   | Extre |
|                     |                    |                   | me    |
|                     |                    |                   | resol |
|                     |                    |                   | ution |
+---------------------+--------------------+-------------------+-------+
| Different scheme    | Single scheme      | Different scheme  | Diffe |
| for each            | across tree of     | for each          | rent  |
| species/genus       | life               | species/genus     | schem |
|                     |                    |                   | e     |
|                     |                    |                   | for   |
|                     |                    |                   | each  |
|                     |                    |                   | speci |
|                     |                    |                   | es/ge |
|                     |                    |                   | nus   |
+---------------------+--------------------+-------------------+-------+

7 Gene MLST
-----------

Achtman 7 gene MLST scheme
~~~~~~~~~~~~~~~~~~~~~~~~~~

The Achtman 7 gene MLST scheme is described in `Laukkanen-Ninios et al
(2011) Environ. Microbiol. 13(12), 3114-3127`_.

Genes included in Achtman 7 gene MLST (together with the length of
sequence used for MLST taken from table 3 in the above cited paper):

==== ======
Gene Length
==== ======
adk  389
argA 361
aroA 357
glnA 338
thrA 342
tmk  375
trpE 465
==== ======

McNally 7 gene MLST scheme
~~~~~~~~~~~~~~~~~~~~~~~~~~

The McNally 7 gene MLST scheme is described in `Hall et al (2015) J.
Clin. Microbiol. 53(1), 35-42`_.

Genes included in McNally 7 gene MLST (together with the length of
sequence used for MLST taken from table 2 in the above cited paper):

==== ======
Gene Length
==== ======
aarF 500
dfp  455
galR 500
glnS 442
hemA 490
speA 452
rfaE 429
==== ======

Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.

.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Laukkanen-Ninios et al (2011) Environ. Microbiol. 13(12), 3114-3127: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3988354/
.. _Hall et al (2015) J. Clin. Microbiol. 53(1), 35-42: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4290955/
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518