Details of assembly methods and in silico genotyping for *Salmonella*
=====================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`



+-----------------+----------------+---------------+-----------------+
| MLST – Classic  | Ribosomal MLST | Core Genome   | Whole Genome    |
|                 | (Jolley, 2012) | MLST          | MLST            |
+=================+================+===============+=================+
| 7 Loci          | 51 Loci        | 3,002 Loci    | 21,065 Loci     |
+-----------------+----------------+---------------+-----------------+
| Conserved       | Ribosomal      | Core genes    | Any coding      |
| Housekeeping    | proteins       |               | sequence        |
| genes           |                |               |                 |
+-----------------+----------------+---------------+-----------------+
| Highly          | Highly         | Variable;     | Highly          |
| conserved; Low  | conserved;     | High          | variable;       |
| resolution      | Medium         | resolution    | Extreme         |
|                 | resolution     |               | resolution      |
+-----------------+----------------+---------------+-----------------+
| Different       | Single scheme  | Different     | Different       |
| scheme for each | across tree of | scheme for    | scheme for each |
| species/genus   | life           | each          | species/genus   |
|                 |                | species/genus |                 |
+-----------------+----------------+---------------+-----------------+

7 Gene MLST
-----------

Classic MLST scheme is described in `Kidgell et al (2002) Infection,
Genetics and Evolution. 2(1) 39-45`_.

Genes included in 7 gene MLST (together with the length of sequence used
for MLST taken from table 1 in the above cited paper):

==== =============== ======
Gene Name/ Locus Tag Length
==== =============== ======
thrA STY0002         501
purE STY0582         399
sucA STY0779         501
hisD STY2281         501
aroC STY2616         501
hemD STY3622         432
dnaN STY3941         501
==== =============== ======


Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.


.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Kidgell et al (2002) Infection, Genetics and Evolution. 2(1) 39-45: http://mlst.warwick.ac.uk/mlst/mlst/mlst/dbs/Ecoli/AGroup/team/markPDF/5.pdf
.. _`Achtman et al. (2012) PLoS Pathog 8(6): e1002776`: http://doi.org/10.1371/journal.ppat.1002776
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518