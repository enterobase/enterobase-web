Details of assembly methods and in silico genotyping for *Vibrio*
=====================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`



+----------------+---------------+-----------------+
| Ribosomal MLST | Core Genome   | Whole Genome    |
| (Jolley, 2012) | MLST          | MLST            |
+================+===============+=================+
| 53 Loci        | 372 Loci      | 33,887 Loci     |
+----------------+---------------+-----------------+
| Ribosomal      | Core genes    | Any coding      |
| proteins       |               | sequence        |
|                |               |                 |
+----------------+---------------+-----------------+
| Highly         | Variable;     | Highly          |
| conserved;     | High          | variable;       |
| Medium         | resolution    | Extreme         |
| resolution     |               | resolution      |
+----------------+---------------+-----------------+
| Single scheme  | Different     | Different       |
| across tree of | scheme for    | scheme for each |
| life           | each          | species/genus   |
|                | species/genus |                 |
+----------------+---------------+-----------------+


Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.


.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Kidgell et al (2002) Infection, Genetics and Evolution. 2(1) 39-45: http://mlst.warwick.ac.uk/mlst/mlst/mlst/dbs/Ecoli/AGroup/team/markPDF/5.pdf
.. _`Achtman et al. (2012) PLoS Pathog 8(6): e1002776`: http://doi.org/10.1371/journal.ppat.1002776
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518