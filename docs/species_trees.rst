Precomputed species trees
==============================

*Salmonella* database
--------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/53257>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/53258>`_


*Escherichia* database (ECORPlus collection)
-----------------------------------------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/31835>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/46977>`_


*Streptococcus* database
--------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/53261>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/53262>`_


*Clostridioides* database
--------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/53253>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/53254>`_


*Vibrio* database
--------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/53265>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/53266>`_


*Yersinia* database
--------------------------
* `Core genomic SNPs <https:///enterobase.warwick.ac.uk/a/53269>`_
* `Accessory gene presences <https:///enterobase.warwick.ac.uk/a/53270>`_

