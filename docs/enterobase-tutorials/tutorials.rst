EnteroBase tutorials and worked examples
========================================
Here we provide step by step walkthroughs with real datasets
that will guide you EnteroBase's major features. Some of these datasets are
presented in `Zhou et al, bioRxiv 613554`_

Case study - Agama
------------------
The initial tutorials focus on a specific rare *Salmonella* serovar called Agama.
*Salmonella*  encompasses more than 1,586 defined serovars, with many being rare
and poorly understood. By codifying an entire genus (*Salmonella* in this case),
EnteroBase allows rapid analysis of the population structure of even rare serovars.
EnteroBase also allows you to look at both diversity within a limited local geographical area
and compare this to the diversity all over the world.

Our first :doc:`tutorial </enterobase-tutorials/search-agama>` will take you
through the **main interface and basic searches using EnteroBase** with Agama as an example.

The second tutorial introduces **User Space OS**. Creating datasets through workspaces and
sharing them with collaborators form a central part of staying organised on EnteroBase,
which you can read about :doc:`here </enterobase-tutorials/useros>`.

Deeper concepts underpinning EnteroBase
---------------------------------------
We also recommend reading about **representing population structure using MLST, rMLST and cgMLST** :doc:`here <deeper-lineages>`.
This is how populations are classified in EnteroBase, and will help you quickly navigate
genetic relationships between strains.

Since late 2018, we have expanded the MLST/rMLST/cgMLST concepts with **Hierarchical Clustering
of cgMLST STs (HierCC)**, a novel approach which supports analyses of population structures at multiple
levels of resolution. Both the HierCC method  and how to use it are described :doc:`here </features/clustering>`.

Best practice for managing your data
------------------------------------
The amount of data can quickly get overwhelming. We have a few tutorials on how to better curate and
manage your data. For instance, there are many cases where the same strain may have data from different
sources, which you would want to combine. In EnteroBase these are managed through
**Uberstains/substrains** relationships, which you can read about :doc:`here </features/main-search-page>`.

:doc:`This tutorial </enterobase-tutorials/tutorial-1>` will show you **how to edit
standard metadata** in EnteroBase, particular for data you own.

Visualisation tools
-------------------
* EnteroBase includes **GrapeTree**, which is explained :doc:`here </grapetree/grapetree-about>`.
* And the **SNP Project/Dendrogram**, which is covered :doc:`here </enterobase-tutorials/snp-tree-tutorial>`.

Inner mechanics of EnteroBase
-----------------------------
EnteroBase is a sprawling web resource with many **internal components**. These components and how they
interact with each other and the wider world are explained :doc:`here </pipelines/enterobase-pipelines>`.


All the tutorials
-----------------

.. toctree::
    :maxdepth: 1
    
    search-agama
    useros
    tutorial-1
    deeper-lineages
    ../grapetree/grapetree-about
    snp-tree-tutorial
    ../features/main-search-page
    ../features/clustering


.. _`Zhou et al, bioRxiv 613554`: https://doi.org/10.1101/613554
