Getting Started - Registering and logging in
============================================
EnteroBase runs entirely online, all you require is an updated web browser;
Ideally Google Chrome (https://www.google.com/chrome/). EnteroBase is
available at https://enterobase.warwick.ac.uk/ . The main page presents
available databases (*Salmonella*, *Escherichia*, *Moraxella* and *Yersinia*)
with an overview of the number of records. From here you can:

* Access a database by clicking the *Database Home* link (green). 
* Access User account options, such as logging in (blue), registering a new 
  account (red) and changing email/passwords are found in the top right.  

.. image:: https://bitbucket.org/repo/Xyayxn/images/3934638221-enteroopen.png

Anonymous user doesn’t have access to some important features such as downloading files (assembly and annotations files and save metadata to local files), creating GrapeTree and SNP projects and saving and sharing workspaces. So it is recommended to register and log in before starting to use EnteroBase. 

 
Registering
-----------
To Register on EnteroBase, visit the main webpage
(https://enterobase.warwick.ac.uk) and click *Register* in the top right.

This will direct you to the registration form where you should fill in a
username, password, email and details about yourself. Once the form is
filled, click **Register**.
 
This will send a verification email to your specified email address. Click
the link in this email to confirm your registration and you can then log into
the EnteroBase website.

.. image:: https://bitbucket.org/repo/Xyayxn/images/1865069026-register.png


Logging in
----------
To Log into EnteroBase, visit the main webpage
(https://enterobase.warwick.ac.uk) and click **Login** in the top
right.
 
This will direct you to the login page where you can enter your username and
password. If you have not create an account or have forgotten your password
there are links below to help you resolve this.

.. image:: https://bitbucket.org/repo/Xyayxn/images/1184238650-login.png

Having created a login, the best way to get familiar with using the
EnteroBase website is to look at some of the :doc:`tutorials </enterobase-tutorials/tutorials>`. Or if
you like you can just start by trying to use the website, particularly if you
have used similar websites in the past.

`Post an issue or bug about EnteroBase <https://bitbucket.org/enterobase/enterobase-web/issues?status=new&status=open>`_