import numpy as np
import sys, os, shutil, psycopg2
from psycopg2.extras import DictCursor, RealDictCursor
import ast, json, requests, ujson
from datetime import datetime
from alive_progress import alive_bar
from flask_script import Manager
from collections import OrderedDict
from sqlalchemy import or_
from datetime import datetime
import logging

from entero import create_app, app, get_database
from entero.ExtraFuncs.query_functions import process_strain_query
from entero.databases.system.models import UserJobs


# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)


# Function to open a given file for writing or reading
def _open_file(filename, mode = 'r'):
  try:
    fp = open(filename,mode)
  except IOError as err:
    sys.stderr.write('{}: {}\n'.format(sys.argv[0],err))
    exit(1)
  return fp


@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-s', '--scheme', help='name of the nserv scheme')
@manager.option('-l', '--levels', help='the levels of the HierCC used in nserv as a comma seperated list')
def importHierCCintoDataParam(database='tuberculosis', scheme='cgMLST-MTB', levels='0,2,5,10,15'):
    """
    Fills the DataParam with entries for each HierCC level so that they are shown on the website.
    Requires that the scheme has an info entry in nservs types table {hierCC: d{level}, d{level} ...}}
    This can be generated using NServ/manage_dmsz.py init_cgMLST_HierCC_production for cgMLSt schemes.
    The fields in DataParam are filled as follows for each HierCC level:
    id: the next free id in DataParam
    tabname: name of the scheme in nserv ( value of scheme in nservs schemes table)
    name: d{level}
    mlst_field: info,hierCC,d{level}
    display_order: the position of the column on the website (note that position 0 is taken by the ST column)
    nested_order: 0 (can be null)
    datatype: integer
    All other data fields are null

    All level need to be input even if they are already in DataParam so that the correct order is defined
    """
    dbase = get_database(database)
    if not dbase:
        app.logger.warning("Error in {}, could not find {} database".format(sys.argv[0], database))
        exit(1)
    DataParam = dbase.models.DataParam

    levels = levels.strip()
    levels = np.fromstring(levels, dtype = int, sep = ',')

    for count,level in enumerate(levels):
        #Check if level already exists in DataParam
        in_dp = dbase.session.query(DataParam).filter_by(tabname= '{}'.format(scheme),name= 'd{}'.format(level)).first()

        if in_dp:
            sys.stderr.write('Level {} is already defined for scheme {}\nThe level will be skipped\n'.format(level, scheme))
            continue
        try:
            tabname = scheme
            name = 'd{}'.format(level)
            mlst_field = 'info,hierCC,d{}'.format(level)
            display_order = count + 2
            nested_order = 0
            label = 'HC{}'.format(level)
            datatype = 'integer'
            print(tabname, name, mlst_field, display_order, nested_order, label, datatype)
            dp_entry = DataParam(tabname = tabname, name = name, mlst_field = mlst_field, display_order = display_order, nested_order = nested_order, label = label, datatype = datatype)
            dbase.session.add(dp_entry)
            dbase.session.commit()
            sys.stdout.write('Added level {}\n'.format(level))
        except Exception as e:
            app.logger.exception("importHierCCintoDataParam failed, error message: %s" % e.message)
            dbase.rollback_close_session()



@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-s', '--species', help='name of the only species that is to pass the QC. All species that differ from this species will be marked with FailedQC')
@manager.option('-c', '--count_only', help='only print the number of rows that would be edited. default = false')
@manager.option('-o', '--outfilepath', help='path to the log file. default: "/home/enterobase/enterobase-web/logs/manage_logs/setFailedQC.log"')
def set_FailedQC_for_deviating_species(database='tuberculosis', species='Mycobacterium tuberculosis', count_only = False, outfilepath = None):
    """
    Sets status FailedQC in the assemblies table of the database where top_species does not contain the specified species.
    Additionally adds in other_data the as reason failed: top_species.
    """
    dbase = get_database(database)
    if not dbase:
        app.logger.warning("Error in {}, could not find {} database".format(sys.argv[0], database))
        exit(1)

    sql = "SELECT * from assemblies where not top_species LIKE '{}%' And status In ('Assembled', 'Failed QC') ORDER BY id ASC".format(species)
    sql_result = dbase.execute_query(sql)
    count_rows = len(sql_result)
    sys.stdout.write("Found {} rows that will be set to FailedQC.\n".format(count_rows))
    #return at this point to do not change the found database entries
    if count_only:
        return

    logfilepath = ''
    if outfilepath:
        logfilepath = outfilepath
    else:
        logfilepath = "/home/enterobase/enterobase-web/logs/manage_logs/setFailedQC_{}_{}.log".format(database, datetime.now().strftime("%d_%m_%Y"))

    logfile = _open_file(logfilepath, 'a+')

    assemblies = dbase.models.Assemblies

    #set length of progress bar
    with alive_bar(len(sql_result)) as bar:
        for strain in sql_result:
            id = strain.get('id')
            barcode = strain.get('barcode')
            top_species = strain.get('top_species')
            rf_key = 'reason_failed'

            records = dbase.session.query(assemblies).filter(assemblies.id == id)
            record = records[0]
            other_data = {}
            if record.other_data:
                other_data = ast.literal_eval(record.other_data)

            #Add top_species to reasons failed
            #check if there is already a 'reason failed'
            if rf_key in other_data:
                #check that the reason is not the species
                if not "top_species" in other_data[rf_key]:
                    other_data[rf_key].append("top_species")
                #else there is already top_species as a reason
            else: # the assemblie did not fail QC before
                other_data[rf_key] = ["top_species"]

            #set status and other_data
            logfile.write('{}\t{}\t{}\t{}\n'.format(id, barcode, top_species, other_data))
            logfile.flush()
            varchar_other_data = json.dumps(other_data)
            record.other_data = varchar_other_data
            record.status = "Failed QC"
            # store in database
            dbase.session.commit()

            #progress bar
            bar()

    logfile.close()

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-f', '--filename', help='name of the output file. default: {database}_strains_{start}_{end}.out')
@manager.option('-s', '--start', help='The start strain id. default = 0')
@manager.option('-e', '--end', help='The end strain id. default = 10')
@manager.option('-a', '--anno', help='Only include barcodes of assemblies that do not have a valid annotation path')
@manager.option('-n', '--no_assembly', help='Instead list the number of assemblies in the file that do not have a valid file path')
def create_file_with_number_of_strains(database='tuberculosis', filename = None, start=0, end=10, anno = False, no_assembly = False):
    '''
    Function to create a file containing the best_assembly barcodes of the strains with stain_id [start, end].
    Only assemblies with the status 'Assembled' are considered.
    The file can be used to either exclude scheme jobs for assemblies when running manage.py's send_scheme_jobs_for_database
    or to choose specific assemblies to run scheme jobs for using send_scheme_jobs_for_barcodes
    '''

    fname = '{}_strains_{}_{}.out'.format(database, start, end)
    if filename:
        fname = filename

    # the database name
    species = database
    # the WHERE clause
    query = 'strains.id >= {} AND strains.id <= {}'.format(start, end)
    #set to using a WHERE clause
    query_type = 'query'
    user_id = 0
    show_substrains = False

    # get the strain info as dict and a list of the best_assembly ids
    strain_data,aids = process_strain_query(species, query, query_type, user_id, show_substrains)

    fp = _open_file(fname, 'w')
    dbase = get_database(database)

    sql = "select file_pointer, id, status, barcode from assemblies where id in ({}) AND status = 'Assembled'".format(aids)

    # include other_data of assembly_lookup which holds the gff file paths
    if anno:
        sql = "select a.file_pointer, a.id, a.status, a.barcode, l.other_data from assemblies as a left outer join assembly_lookup as l on a.id = l.assembly_id where a.id in ({}) AND a.status = 'Assembled' and l.st_barcode = 'EGG'".format(
            aids)

    results = dbase.execute_query(sql)
    file_skip_count = 0
    anno_skip_count = 0
    barcodes_written = 0

    print("\nCreating assembly barcode file: {}".format(fname))
    print('Skipping assemblies with status != Assembled')
    # the progress bar
    with alive_bar(len(results)) as bar:
        #Check the Assembly for each result and write the barcode into the file
        for res in results:
            file_p = res['file_pointer']
            a_id = res['id']
            a_status = res['status']
            a_barcode = res['barcode']
            # Check if file exists
            if not file_p or not os.path.isfile(file_p):
                print('Skipped file {}\nThe assembly with id: {}, barcode: {} does not exists.\n'.format(file_p, a_id, a_barcode))
                file_skip_count += 1
                if no_assembly:
                    fp.write('{}\t{}\n'.format(a_barcode, file_p))
                bar()
                continue
            elif no_assembly:
                continue
            # Check that it is a correct assembly
            # Should not run into this case because of the sql statement
            if not a_status == 'Assembled':
                print('Skipped id: {}, barcode: {}.\nstatus != Assembled'.format(a_id,a_barcode))
                bar()
                continue
            if anno:
                #get gff and gbk file paths
                other_data = res['other_data']
                # d = json.loads(other_data)
                # print(other_data)
                gff_file = other_data['results']['gff_file']
                gbk_file = other_data['results']['gbk_file']
                # print(gff_file)
                # print(gbk_file)
                #check if gff file path exists and add only if no valid gff file
                if os.path.isfile(gff_file) and os.path.isfile(gbk_file):
                    anno_skip_count += 1
                    bar()
                    continue
            fp.write('{}\n'.format(a_barcode))
            barcodes_written += 1
            bar()
    fp.close()
    print('\nCheck order: status = Assembled, assembly filepointer is valid, if anno: gff and gbk file do not exist.')
    print('barcodes written: {}'.format(barcodes_written))
    print('file skip count: {}'.format(file_skip_count))
    if anno:
        print('Anno skip count: {}'.format(anno_skip_count))


@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-f', '--filename', help='name of the file containing the assembly barcodes.')
def send_scheme_jobs_for_barcodes(database='tuberculosis', scheme='prokka_annotation', filename = None):
    '''
    Send scheme jobs to croboot
    Run this to send scheme jobs for a list of assembly barcodes in {filename}.
    The file can be created using create_file_with_number_of_strains.
    '''
    from entero.jobs.jobs import GenericJob

    if not filename or not os.path.isfile(filename):
        sys.stderr.write('\nERROR: Please provide a file containing the assembly barcodes\n')
        return

    dbase = get_database(database)
    Schemes = dbase.models.Schemes
    barcodes = []

    # read in the barcodes
    fp = _open_file(filename, 'r')
    lines = fp.readlines()
    fp.close()
    for line in lines:
        barcodes.append(line.strip())

    # Get the filepointer of the assemblies
    str_barcodes = ",".join(["'{}'".format(x) for x in barcodes])
    sql = "select file_pointer, id, status, barcode from assemblies where barcode in ({})".format(str_barcodes)
    results=dbase.execute_query(sql)
    # Check that all barcodes can be found
    if len(results) != len(barcodes):
        sys.stderr.write("Could not find all barcodes in {}\n".format(filename))
        return

    #Create the jobs
    scheme_obj = dbase.session.query(Schemes).filter(Schemes.description == scheme).one()
    print('\nCreating jobs:')
    with alive_bar(len(results)) as bar:
        for res in results:
            file_p = res['file_pointer']
            a_status = res['status']
            a_barcode = res['barcode']
            if not file_p or not os.path.isfile(file_p):
                print('Skipped {}: There is no file {}\n'.format(a_barcode, file_p))
                bar()
                continue
            if not a_status == 'Assembled':
                print('Skipped {}: status {} != Assembled'.format(a_barcode, a_status))
                bar()
                continue

            job = GenericJob(database=database,
                             scheme=scheme_obj,
                             assembly_barcode=res['barcode'],
                             assembly_filepointer=res['file_pointer'],
                             user_id=0,
                             priority=-3,
                             workgroup="public")
            job.send_job()
            bar()

@manager.command
@manager.option('-f', '--filename', help='name of the file containing the job tags of the jobs that are to be resend. One tag per line')
def resend_jobs_from_tag_file(filename=None) :
    '''
    Gets list of job tags from the tag file and resends the jobs with that tag.
    The tag file has to contain one tag per line.
    '''
    from entero.jobs.jobs import get_crobot_job

    if not filename or not os.path.isfile(filename):
        sys.stderr.write('\nERROR: Please provide a file containing the tags of the jobs you want to resend\n')
        return

    tags = []

    # read in the tags
    fp = _open_file(filename, 'r')
    lines = fp.readlines()
    fp.close()
    for line in lines:
        tags.append(line.strip())

    print('\nResending jobs from {}'.format(filename))
    with alive_bar(len(tags)) as bar:
        for tag in tags:
            job = get_crobot_job(int(tag))
            job.send_job()
            bar()

@manager.command
@manager.option('-f', '--filename', help='name of the file containing the job tags you want the file paths checked for')
@manager.option('-o', '--out', help='write the not existing filepaths to a file')
def check_filepaths_of_jobs_from_tag_file(filename=None, out=False) :
    '''
    Gets list of job tags from the tag file and checks the input file paths for the jobs.
    The tag file has to contain one tag per line.
    '''

    if not filename or not os.path.isfile(filename):
        sys.stderr.write('\nERROR: Please provide a file containing the inputs of the jobs you want checked\n')
        return

    count = 0
    if out:
        outfile = 'manage_infiles/non_existent_files.txt'
        outfp = _open_file(outfile,'w')

    # read in the tags
    fp = _open_file(filename, 'r')
    lines = fp.readlines()
    fp.close()

    sys.stdout.write('\nChecking the file paths:\n')
    with alive_bar(len(lines)) as bar:
        for line in lines:
            #bring line in json format
            line = line.strip().replace('""',"'").replace('"','').replace("'",'"')
            #load json as dict
            input = json.loads(line)
            filepath = input['genome_seq']
            if not os.path.isfile(filepath):
                count += 1
                if out:
                    outfp.write('{}\n'.format(filepath))
            bar()
    sys.stdout.write('\nNumber of file paths that do not exist: {}\n'.format(count))

    if out:
        outfp.close()

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-t', '--trade_ids', help='list of the trade ids in a comma separated format')
@manager.option('-s', '--strain_id', help='the strain id the traces do belong to')
@manager.option('-w', '--workspace', help='the workspace of the job')
@manager.option('-u', '--user_id', help='the user_id the jobs runs under')
def send_assembly_job(database = 'tuberculosis', trace_ids = [], strain_id = None,workgroup='backend', user_id = 1):
    '''
    Sends an assembly job for the given parameters.
    '''
    from entero.jobs.jobs import AssemblyJob

    if not trace_ids:
        sys.stderr.write("Please provide the trade ids you want to run assembly jobs for.\n")
        return
    if not strain_id:
        sys.stderr.write("Please provide the strain id you want to run assembly jobs for.\n")

    #Convert trace_ids
    trace_ids = map(int, trace_ids.strip().split(','))

    print("\nSending assembly jobs for the trace id(s): {}".format(','.join(str(t) for t in trace_ids)))

    job = AssemblyJob(database=database,
                      trace_ids=trace_ids,
                      strain_id = strain_id,
                      workgroup=workgroup,
                      user_id=user_id)

    job.send_job()

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-t', '--trace_ids', help='the trace id for which the assembly should run')
@manager.option('-w', '--workspace', help='the workspace of the job')
@manager.option('-u', '--user_id', help='the user_id the jobs runs under')
def send_assembly_job_for_trace_id(database = 'tuberculosis', trace_id = None ,workspace='backend', user_id = 1):
    '''
    Sends an assembly job for the given parameters.
    '''
    from entero.jobs.jobs import AssemblyJob

    if not trace_id:
        sys.stderr.write("Please provide the trace id you want to run assembly jobs for.\n")
        return

    print("\nSending assembly jobs for the trace id: {}".format(trace_id))

    job = AssemblyJob(database=database,
                      trace_ids=[int(trace_id)],
                      workspace=workspace,
                      user_id=user_id)

    job.send_job()

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-u', '--user_id', help='user id of the user whos data is to be owned by crobot')
@manager.option('-n', '--new_owner_id', help='user id of the user who is to be the new owner of the data (default: 0 (crobot))')
def change_strains_ownership(database = None, user_id = None, new_owner_id = 0):
    '''
    Change ownership of strains and assemblies to new_owner_id. default 0
    '''
    if not user_id:
        sys.stderr.write("No user_id given\n")
        exit(1)

    if not database:
        sys.stderr.write("No database given\n")
        exit(1)

    dbase = get_database(database)
    Strains = dbase.models.Strains
    strains = dbase.session.query(Strains).filter(Strains.owner==user_id).all()
    if len(strains)>0:
        print("Found %s strains owned by this user in database: %s "%(len(strains), database))
        with alive_bar(len(strains)) as bar:
            for starin in strains:
                starin.owner=new_owner_id
                bar()
        dbase.session.commit()
    else:
        print("User {} does not own any strains in database {}".format(user_id, database))

    Assemblies = dbase.models.Assemblies
    assemblies = dbase.session.query(Assemblies).filter(Assemblies.user_id == user_id).all()
    if len(assemblies) > 0:
        print("Found %s assemblies owned by this user in database: %s " % (len(assemblies), database))
        with alive_bar(len(assemblies)) as bar:
            for assem in assemblies:
                assem.user_id = new_owner_id
                bar()
        dbase.session.commit()
    else:
        print("User {} does not own any assemblies in database {}".format(user_id, database))

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-u', '--user_id', help='user id of the user whos data is to be removed')
def delete_user_data(user_id=None, database = None):
    '''
    Delete all data of the user in the species database, including the files at the referenced file_pointers.
    WARNING: Removed files can not be restored
    '''
    if not user_id:
        sys.stderr.write("No user_id given\n")
        exit(1)

    if not database:
        sys.stderr.write("No database given\n")
        exit(1)

    dbase  = get_database(database)
    if not dbase:
        app.logger.error ("Can not find %s database" % dbname)
        return

    try:
        #remove trace_assembly
        sys.stdout.write("Removing trace_assembly of user: {}\n".format(user_id))
        sql = "SELECT * FROM trace_assembly WHERE trace_id IN (SELECT t.id FROM traces AS t JOIN strains s ON t.strain_id = s.id WHERE s.owner != 0 AND s.owner = {})".format(user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} trace_assembly entries to be removed".format(len(results1)))
        sql = "DELETE FROM trace_assembly WHERE trace_id IN (SELECT t.id FROM traces AS t JOIN strains s ON t.strain_id = s.id WHERE s.owner != 0 AND s.owner = {})".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting trace_assembly")
        print("Removed {} trace_assembly entries".format(len(results1)))

        #remove strains_archive:
        sys.stdout.write("Removing strains_archive of user: {}\n".format(user_id))
        sql = "SELECT * FROM strains_archive WHERE owner != 0 AND owner = {}".format(user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} strains_archive entries to be removed".format(len(results1)))
        sql = "DELETE FROM strains_archive WHERE owner != 0 AND owner = {}".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting strains_archive")
        print("Removed {} strains_archive entries".format(len(results1)))

        #remove traces without deleting read_location user files, since they where not transferred from warwick:
        sys.stdout.write("Removing traces of user: {}\n".format(user_id))
        sql = "SELECT * FROM traces AS t JOIN strains s ON t.strain_id = s.id WHERE s.owner != 0 AND s.owner = {}".format(user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} traces entries to be removed".format(len(results1)))
        sql = "DELETE FROM traces WHERE strain_id IN (SELECT t.strain_id FROM traces AS t JOIN strains s ON t.strain_id = s.id WHERE s.owner != 0 AND s.owner = {})".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting traces")
        print("Removed {} traces entries".format(len(results1)))

        # remove assembly_lookup
        sys.stdout.write("Removing assembly_lookup of user: {}\n".format(user_id))
        sql = "SELECT al.st_barcode, al.status, al.other_data FROM assembly_lookup AS al JOIN assemblies a ON al.assembly_id = a.id WHERE a.user_id != 0 AND a.user_id ={}".format(
            user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} assembly_lookup entries to be removed".format(len(results1)))

        # Deleting gff and gbk files
        print("Removing the gff and gbk files:")
        for res in results1:
            other_data = res['other_data']
            status = res['status']
            barcode = res['st_barcode']

            # file pointer to gff, gbk file, or st results
            if status:
                # annotation
                if barcode == 'EGG':
                    #gff and gbk files are in the same dir
                    gff_file = other_data['results']['gff_file']
                    gff_dir = os.path.dirname(gff_file)
                    if os.path.isdir(gff_dir):
                        print("Removing the dir {}".format(gff_dir))
                        shutil.rmtree(gff_dir)
                # else:  # nomenclature: nothing to remove
            else:  # map.gff file pointer, other_data is in this case no dict but unicode
                other_data = eval(other_data)
                fp = other_data['file_pointer'].replace('\\', '')
                if os.path.isfile(fp):
                    fp_dir = os.path.dirname(fp)
                    print("Removing the dir {}".format(fp_dir))
                    shutil.rmtree(fp_dir)
        #deletes data because of action even if it is only a select statement
        sql = "SELECT * FROM assembly_lookup AS al JOIN assemblies a ON al.assembly_id = a.id WHERE a.user_id != 0 AND a.user_id ={}".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting assembly_lookup")
        print("Removed {} assembly_lookup entries".format(len(results1)))

        # removing strains
        sys.stdout.write("Removing strains of user: {}\n".format(user_id))
        sql = "SELECT * FROM strains WHERE owner != 0 AND owner = {}".format(user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} strains entries to be removed".format(len(results1)))
        sql = "DELETE FROM strains WHERE owner != 0 AND owner = {}".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting strains")
        print("Removed {} strains entries".format(len(results1)))

        # remove assemblies
        sys.stdout.write("Removing assemblies of user: {}\n".format(user_id))
        sql = "SELECT file_pointer, id, status, user_id FROM assemblies WHERE user_id != 0 AND user_id = {}".format(user_id)
        results1 = dbase.execute_query(sql)
        print("Found {} assemblies entries to be removed".format(len(results1)))

        # removing assembly files
        for res in results1:
            fp = res['file_pointer']
            if os.path.isfile(fp):
                fp_dir = os.path.dirname(fp)
                print("Removing the dir {}".format(fp_dir))
                shutil.rmtree(fp_dir)

        sql = "DELETE FROM assemblies WHERE user_id != 0 AND user_id = {}".format(user_id)
        results2 = dbase.execute_action(sql)
        if not results2:
            raise Exception("Error while deleting assemblies")
        print("Removed {} assemblies entries".format(len(results1)))

        dbase.session.commit()
    except Exception as e:
        sys.stderr.write("delete_user_data: {}\n".format(e))
        dbase.rollback_close_session()
    print("FINISHED")

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-u', '--user_id', help='user id of the user whos data is to be removed')
def call_get_sts_by_barcode():
    '''
    Function to simulate the download of an allelic profile of a workspace.
    (main/views/get_sts_by_barcode)
    '''
    barcode = 'MYC_AA0023AA_ST'
    url = app.config['SERVER_BASE_ADDRESS'] + "get_sts_by_barcode"
    print(requests.post(url, data={'barcode': barcode}).text)

    #get_sts_by_barcode
    st_dict = {}
    url = app.config['NSERV_ADDRESS'] + "/retrieve.api"
    resp = requests.post(url=url,
                         data={"barcode": barcode, "convert": 0, 'fieldnames': 'barcode,type_id,value_indices'},
                         timeout=app.config['NSERV_TIMEOUT'])
    print(resp.text)
    try:
        data = ujson.loads(resp.text)
    except:
        raise Exception("could not load response%s" % resp.text)

    for record in data:
        rec_dict = {}
        # rec_dict = OrderedDict()
        print('record[fieldvalues]: {}'.format(record['fieldvalues']))
        for allele in record['fieldvalues']:
            print('allele: {}'.format(allele))
            rec_dict[allele['fieldname']] = allele['value_id']
        #sort the locus names in alphabetical order for the allele profile output
        st_dict[record['barcode']] = OrderedDict(sorted(rec_dict.items()))
    print(st_dict)

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-t', '--no_top_species', help='run QC for assemblies with status Assembled and no top_species (mostly complete genomes)', action='store_true')
@manager.option('-f', '--filter_top_species', help='run QC for assemblies with status Assembled and a top_species different from Mycobacterium tuberculosis and below 65%', action='store_true')
def redo_evaluation(database='tuberculosis', no_top_species = None, filter_top_species = None):
    """
    Resend assemblies to the QA_evaluation
    """
    from entero.jobs.jobs import QA_evaluation_Job
    dbase = get_database(database)
    if not dbase:
        app.logger.warning("Error in {}, could not find {} database".format(sys.argv[0], database))
        exit(1)

    sql = None
    if no_top_species:
        sql ="SELECT a.id, a.file_pointer FROM assemblies as a JOIN strains as s On a.id = s.best_assembly where a.top_species IS NULL And a.status = 'Assembled'"
    elif filter_top_species:
        sql = "SELECT a.id, a.n50, a.contig_number, a.low_qualities, a.total_length, a.top_species, a.status, a.other_data, s.species, a.file_pointer, s.strain from assemblies as a Join strains as s ON a.id = s.best_assembly AND status = 'Assembled' ORDER BY total_length"
    sql_results = dbase.execute_query(sql)

    for res in sql_results:
        if filter_top_species:
            species_list = res['top_species'].split(';')
            perc = float(species_list[1].replace('%',''))
            if species_list[0] == 'Mycobacterium tuberculosis' and perc >= 65:
                continue

        assembly = dbase.models.Assemblies(status='Assembled', file_pointer=res['file_pointer'], id = res['id'])

        print('Send assembly id: {}, file_pointer: {}'.format(assembly.id, assembly.file_pointer))

        job = QA_evaluation_Job(assembly_id=res['id'],
                                assembly=assembly,
                                database=database,
                                user_id=0,
                                priority=2,
                                workgroup='user_upload')

        job.send_job()

@manager.command
@manager.option('-d', '--database', help='name of the species database')
@manager.option('-s', '--start', help='The first strain id to hide')
@manager.option('-e', '--end', help='The last strain id to hide')
@manager.option('-r', '--restore', help='show previously hidden strains again', action='store_true')
@manager.option('-t', '--test', help='Test the sql statements that would run without executing them', action='store_true')
def hide_strains(database = None, start = None, end = None, hide = True, restore = False, test = False):
    if restore:
        hide = False

    dbase = get_database(database)
    if not dbase:
        app.logger.warning("Error in {}, could not find {} database".format(sys.argv[0], database))
        exit(1)

    try:
        sql = "SELECT * FROM strains WHERE id >= {} AND id <= {}".format(start, end)
        if hide:
            sql += " AND uberstrain > 0"
        else:
            sql += " AND uberstrain < 0"

        results =dbase.execute_query(sql)
        print('Number of found strains that will be updated: {}'.format(len(results)))
        for item in results:
            id = item['id']
            uberstrain = item['uberstrain']
            new_uberstrain = uberstrain * (-1)
            sql = "UPDATE strains SET uberstrain={} WHERE id={}".format(new_uberstrain, id)

            print('Update strain id: {} change uberstrain from: {} to: {}'.format(id, uberstrain, new_uberstrain))
            if not test:
                dbase.execute_action(sql)

    except Exception as e:
        app.logger.exception("hide_strains failed, error message: %s"%str(e))
        dbase.rollback_close_session()

@manager.command
def run_app():
    from entero import app
    app.run(host='0.0.0.0',port=8000)

if __name__ == '__main__' :
    manager.run()