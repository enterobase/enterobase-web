eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv virtualenv-delete -f entero_docs

pyenv install -s 2.7.14
pyenv virtualenv 2.7.14 entero_docs
pyenv activate entero_docs
pip install -r ../docs/rtd.requirements.txt
