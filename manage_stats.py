from flask_script import Manager
from entero import create_app
import os
from datetime import datetime, timedelta
from flask import Flask

# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
app = Flask(__name__)

manager = Manager(entero)


@manager.command
def usage_stats(root_path = '/home/admin'):
    #  Produces a tsv file counting the number of assembly downloads each day for the period of the
    #  current up-downloads log file
    dates = {}
    last_date = None
    one_day = timedelta(days=1)
    for filename in ['up-downloads.log']:
        filepath = os.path.join(root_path, 'enterobase-web','logs', filename)
        with open(filepath) as file:
            for line in file:
                parts = line.rstrip().split(' ')
                if (parts[7] == 'Download'):
                    date = datetime.strptime(parts[0], '%Y-%m-%d')
                    if not last_date:
                        last_date = date -  one_day
                    while last_date != date:
                        last_date += one_day
                        dates[last_date] = 0
                    dates[date] += 1
        filepath = os.path.join(filename+'.tsv')
        with open(filepath,'w') as file:
            for date in dates:
                file.write('\t'.join([str(date),str(dates[date])])+'\n')









if __name__ == '__main__':
    manager.run()
