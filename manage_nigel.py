
#!flask/bin/python

import os
import time
from datetime import datetime
import psycopg2
from psycopg2.extras import RealDictCursor
from flask_script import Manager
from entero import create_app, app, get_database, rollback_close_connection

# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)

@manager.command
def verify_assemblies(dbname='helicobacter'):
    import sys, hashlib, shutil
    #  Add directory to the PYTHONPATH so that we can pick up the get_genome pipeline job code
    #  Need to add /home/nigeldyer/CRobot/pipelines as a pyCharm content root to avoid pyCharm marking import
    #  line as error
    sys.path.append("/home/nigeldyer/CRobot/pipelines")
    from genome_download import get_genome

    #  The replacement genomes will be placed here as a temporary holding place
    os.chdir("/home/nigeldyer/temp/verify_files")

    all_data = []
    dbase = get_database(dbname)
    if not dbase:
        app.logger.warning("Error in import_assemblies, could find %s database" % dbname)
        return all_data

    # Get all of the strains associated with assembled genome downloads from NCBI
    sql = "SELECT  assemblies.file_pointer as file,assemblies.barcode AS as_barcode," \
        "traces.barcode AS tr_barcode, traces.status as tr_status, traces.accession as accession, " \
        "assemblies.id as as_index " \
        "FROM strains " \
        "INNER JOIN assemblies ON strains.best_assembly= assemblies.id " \
        "INNER JOIN trace_assembly ON assemblies.id = trace_assembly.assembly_id " \
        "INNER JOIN traces ON traces.id = trace_assembly.trace_id " \
        "WHERE traces.status IN ('Complete Genome', 'Scaffold', 'Contig', 'Chromosome')"
    results = dbase.execute_query(sql)

    #  And now go through all the files calculating their MD5 values and group the entries by their
    # MD5 values, so all entries with the same files will be in the same group.   It is not possible to compare
    # the file that was previously downloaded with the file that would now be downloaded as some of the
    # source files have changed, sometimes just changing the text in the header line, and sometimes
    # because the source reads have been recompiled.
    all_accessions = {}
    accessions = {}
    md5_values = {}
    for result in results:
        if result['file']:
            all_accessions[result['tr_barcode']] = {'accession': result['accession'],
                        'file': result['file'],'as_barcode': result['as_barcode'], 'as_index': result['as_index']}
            with open(result['file'], "rb") as f1:
                bytes1 = f1.read()  # read file as bytes
                readable_hash = hashlib.md5(bytes1).hexdigest()
                md5_values.setdefault(readable_hash, []).append(result['tr_barcode'])

    #  Find the MD5 values associated with more than one file ie strain.
    for md5 in md5_values:
        if len(md5_values[md5]) > 1:
            trace_list = md5_values[md5]
            for trace in trace_list:
                accessions[trace] = all_accessions[trace]['accession']

    #  Use the CRobot get_genome code to download the correct assembled genome
    get_genome(accessions,"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi",'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi')

    # Now go through the strains replacing the previous incorrect file with the correct file and then
    # set the scheme job status to FAILED, which means that running the 'update all schemes' process for Heliobacter will
    # recalculate the results.
    assembly_lookup = dbase.models.AssemblyLookup
    count = 100
    for trace in accessions:
        print (trace)
        destfile = all_accessions[trace]['file']
        shutil.copyfile(trace+'_genomic.fna', destfile)
        assembly_id = all_accessions[trace]['as_index']
        records = dbase.session.query(assembly_lookup).filter(
             assembly_lookup.assembly_id == assembly_id).all()
        for record in records:
            record.status = 'FAILED'
        dbase.session.commit()
        count = count - 1
        if count <= 0:
            break

@manager.command
def modify_data_param():
    # For correcting DataParam fields in all the different species databases
    dbs = app.config['ACTIVE_DATABASES']
    for db_name in dbs:
        dbase = get_database(db_name)
        data_param = dbase.models.DataParam
        data_row=dbase.session.query(data_param).filter(data_param.tabname=='strains').\
            filter(data_param.name=='collection_date').one()
        if data_row.sra_field == u'Sample,Metadata,Date':
            data_row.sra_field = u'Sample,Metadata,Day'
            dbase.session.commit()


@manager.command
def test():
    list = os.listdir('.')
    for count, filename in enumerate(list):
        if '#' not in filename and 'NG' not in filename:
            src = filename  # foldername/filename, if .py file is outside folder
            dst = filename[0:7]+'#'+filename[7:]
            os.rename(src, dst)

@manager.command
@manager.option('-u', '--user_id')
@manager.option('-d', '--database')
def load_reads_from_folder(user_id=783,database='ecoli'):
    from entero.databases.system.models import UserUploads
    from flask import current_app
    from entero.upload.views import file_upload_success
    try:
        #check which files need uploading
        uploads = UserUploads.query.filter_by(species=database,
                                              user_id=user_id,
                                              status='Awaiting Upload'
                                             ).all()
        upload_dict = {}
        for upload in uploads:
            upload_dict[upload.file_name] = True

        directory = os.path.join(current_app.config['FILE_UPLOAD_DIR'], str(user_id), database)

        files = os.listdir(directory)
        temp_files = []
        for filename in files:
            temp_files.append(os.path.join(directory,filename))
        files = temp_files



        #check to see any files in the specified folder in dropbox
        for fi in files:
            name = os.path.split(fi)[1]
            upload = upload_dict.get(name)
            if upload:
                local_filename = os.path.join(directory,name)
                file_upload_success(user_id, name, directory, database)

    except Exception as e:
        app.logger.exception("load_reads_from_folder failed, error message: %s"%str(e))



@manager.command
@manager.option('-d', '--database', help='database')
def insert_data_param_row(database='ecoli'):
    dbase = get_database(database)
    data_param = dbase.models.DataParam
    nina_attribute = []
    factors = ['BFPathovar', 'Stx1', 'Stx2', 'ipaH', 'pInv', 'ST', 'LT', 'eae']
    disp_names = ['Pathovar', 'Stx1', 'Stx2', 'ipaH', 'pInv', 'ST', 'LT', 'eae']
    for x in range(8):
        nina_attribute.append(data_param())
        nina_attribute[x].tabname = 'AlternType'
        nina_attribute[x].name = factors[x]
        nina_attribute[x].label = disp_names[x]  # displayed to the user
        nina_attribute[x].datatype = 'text'
        nina_attribute[x].display_order = 4 + x
        nina_attribute[x].nested_order = 0
        nina_attribute[x].mlst_field = 'log,' + factors[x]
    dbase.session.add_all(nina_attribute)
    dbase.session.commit()  # method


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--start_id', help='start_id')
def fill_in_gaps(database='ecoli', start_id=''):
    """
    Goes through the database looking for gaps where the Clermont data ins not available or
    the blastfrost data is not available and runs a job to complete them
    """
    from entero.jobs.jobs import GenericJob
    from entero.ExtraFuncs.query_functions import process_medium_scheme_query
    import subprocess
    import json
    import shutil

    log = open("/home/nigeldyer/blastfrost/Updates_" + str(start_id) + ".txt", "a+")
    workspace_dir = "/home/nigeldyer/workspace/"
    dbase = get_database(database)
    jobSubmit = False
    if not dbase:
        print("Error, no database for %s" % database)
        return
    print("I found the correct %s database" % database)
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies
    sql = "SELECT * FROM public.strains WHERE id >= " + start_id + " ORDER BY id ASC"
#    sql = "SELECT * FROM public.strains ORDER BY id ASC"
    sql_result = dbase.execute_query(sql)
    print(len(sql_result))
    assembly_lookup = dbase.models.AssemblyLookup
    count = 0
    updating = 0
    no_hc = 0
    for strain in sql_result:
        count = count + 1
        if (count % 100) == 0:
            print (str(count) + " checked, "  + str(updating) + " updated ..." + str(no_hc) + " missing HC data")
        barcode = strain.get('barcode')
        best_assembly = strain.get('best_assembly')
        records = dbase.session.query(assembly_lookup).filter(
            assembly_lookup.assembly_id == best_assembly).filter(
            assembly_lookup.scheme_id == 15).all()
        if len(records) > 0:
            record = records[0]
            other_data = dict(record.other_data)
            results = dict(other_data['results'])
            option = ''
            desc = ""
            if 'BFPathovar' not in results or results['BFPathovar'] == '':
                desc = ", no pathovar"
                option = 'pathovars'
            if 'ClermonTyping' not in results or results['ClermonTyping'] == '':
                desc = desc + ", No ClermonTyping"
                option = 'all' if len(option) else 'clermontyping'
            if 'ezClermont' not in results or results['ezClermont'] == '':
                desc = desc + ", No ezClermont"
                option = 'all' if len(option) else 'ezclermont'

            if option:
                assembly_list = record.assembly_list
                id = strain.get('id')
                scheme = dbase.session.query(dbase.models.Schemes).filter_by(name="Phylotypes").one()
                if jobSubmit:
                    pipeline = scheme.param.get('pipeline')
                    if pipeline:
                        job = GenericJob(scheme=scheme,
                                       assembly_filepointer=assembly_list.file_pointer,
                                       assembly_barcode=assembly_list.barcode,
                                       assembly_id=assembly_list.id,
                                       database=database,
                                       user_id=4407,        #NigelDyer
                                       priority=-9,
                                       workgroup="user_upload"
                                       )
                        job.send_job()
                else:
                    strain_id = records[0].assembly_id
                    if option == 'pathovars' or option == 'all':
                        # get the HC clusters from NServ
                        data_returned = False
                        while not data_returned:
                            try:
                                exp_data, aids = process_medium_scheme_query(database, 'cgMLST', strain_id, "assembly_ids")
                                data_returned = True
                            except:
                                print ("Data fetch error, retry\n")
                                time.sleep(10)
                        exp_data = list(exp_data.values())
                        if exp_data:
                            exp_data = exp_data[0]
                            exp_data = json.dumps(exp_data)
                    else:
                        # Dummy data for when pathovars are not being calculated
                        exp_data = '{"d1":"1"}'
                    if exp_data:
                        tempDir = workspace_dir + "temp_" + str(start_id)
                        shutil.rmtree(tempDir,ignore_errors=True)
                        os.mkdir(tempDir)
                        os.chdir(tempDir)
                        # Command specifies whether one or all of the data types are being recalculated
                        #  The default when called by CRobot and this parameter is not provided is all
                        #  Could not get conda version to work within this script, although it does withoin CRobot
                        #                       cmd = "source /home/nigeldyer/miniconda3/etc/profile.d/conda.sh ;conda activate crobot3 ;python " \
                        cmd = "/home/nigeldyer/.pyenv/versions/crobot3/bin/python " \
                              "/home/nigeldyer/CRobot/pipelines/AlternType_V4.py  Escherichia '" + exp_data + "' " + \
                              assembly_list.file_pointer + '  ' + option
#                        result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, preexec_fn=os.setsid,
#                            executable='/bin/bash').communicate()

                        my_env = os.environ.copy()
                        my_env["PATH"] = "/home/nigeldyer/.pyenv/versions/crobot3/bin:/home/nigeldyer/.enteroLocal/bin:" + my_env["PATH"]
                        my_env["R_LIBS_USER"] = "/home/nigeldyer/.enteroLocal/Rlibs"
                        result = subprocess.Popen(cmd, shell=True,stdout=subprocess.PIPE, preexec_fn=os.setsid, env=my_env,
                                                  executable='/bin/bash').communicate()

                        newValues = result[0]
                        #  result[0] is the stdout result, which is empty if there is a problem
                        if newValues:
                            newValues = json.loads(result[0])
                            updates = ''
                            for t in newValues:
                                if t in results:
                                    if newValues[t] != results[t]:
                                        updates = updates + ", " + t + ":" + results[t] + " > " + newValues[t]
                                else:
                                    updates = updates + ", " + t + " > " + newValues[t]
                                results[t] = newValues[t]
                            other_data['results'] = results
                            record.other_data = other_data
                            updating = updating + 1
                            #   and store
                            dbase.session.commit()

                        else:
                            # result[1] is the stderr result.  The most informative info is after the last ValueError
                            if result[1]:
                                updates = result[1].split("ValueError")
                                updates = updates[-1]
                            else:
                                updates = "Undefined error"
                        now = datetime.now()  # current date and time
                        status = str(id) + " ," + barcode + " ," + now.strftime(
                            "%m/%d/%Y, %H:%M:%S") + " ," + assembly_list.barcode + desc + updates
                    else:
                        no_hc = no_hc + 1
                        now = datetime.now()  # current date and time
                        status = str(id) + " ," + barcode + " ," + now.strftime(
                            "%m/%d/%Y, %H:%M:%S") + " ," + assembly_list.barcode + " Failed to get HC data"
                    log.write(status + "\n")
                    print(status)
                    log.flush()

#                if updating > 10:
#                    break

    print (str(updating) + " entries being updated")
    log.close()


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--start_id', help='start_id')
@manager.option('-f', '--finish_id', help='finish_id')
def validate_data(database='ecoli', start_id='',finish_id=''):
    """
    Goes through the database looking for gaps where the Clermont data ins not available or
    the blastfrost data is not available and runs a job to complete them
    """
    from entero.jobs.jobs import GenericJob
    from entero.ExtraFuncs.query_functions import process_medium_scheme_query
    import subprocess
    import json
    import shutil

    log = open("/home/nigeldyer/blastfrost/Validates_" + str(start_id) + ".txt", "a+")
    workspace_dir = "/home/nigeldyer/workspace/"
    dbase = get_database(database)
    if not dbase:
        print("Error, no database for %s" % database)
        return
    print("I found the correct %s database" % database)
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies
    if finish_id == '':
        finish_id = str(int(start_id) + 10000)
    sql = "SELECT * FROM public.strains WHERE id >= " + start_id + " AND id < " + finish_id +  " ORDER BY id ASC"
#    sql = "SELECT * FROM public.strains ORDER BY id ASC"
    sql_result = dbase.execute_query(sql)
    print(len(sql_result))
    assembly_lookup = dbase.models.AssemblyLookup
    count = 0
    updating = 0
    no_hc = 0
    stxs_previously_done = 0
    for strain in sql_result:
        count = count + 1
        if (count % 100) == 0:
            print (str(count) + " checked, " + str(updating) + " updated ..." + str(no_hc) + " missing HC data" +
                   str(stxs_previously_done) + " skipped as OK")
        barcode = strain.get('barcode')
        id = strain.get('id')
        best_assembly = strain.get('best_assembly')
        records = dbase.session.query(assembly_lookup).filter(
            assembly_lookup.assembly_id == best_assembly).filter(
            assembly_lookup.scheme_id == 15).all()
        if len(records) > 0:
            record = records[0]
            other_data = dict(record.other_data)
            results = dict(other_data['results'])
            assembly_list = record.assembly_list
            now = datetime.now()  # current date and time
            if 'Stx1' not in results or 'Stx2' not in results or (results['Stx1'] == '-'  and results['Stx2'] == '-'):

                option = 'pathovars'

                scheme = dbase.session.query(dbase.models.Schemes).filter_by(name="Phylotypes").one()
                strain_id = records[0].assembly_id
                    # get the HC clusters from NServ
                data_returned = False
                while not data_returned:
                    try:
                        exp_data, aids = process_medium_scheme_query(database, 'cgMLST', strain_id, "assembly_ids")
                        data_returned = True
                    except:
                        print ("Data fetch error, retry\n")
                        time.sleep(10)
                exp_data = list(exp_data.values())
                if exp_data:
                    exp_data = exp_data[0]
                    exp_data = json.dumps(exp_data)
                    tempDir = workspace_dir + "temp_" + str(start_id)
                    shutil.rmtree(tempDir,ignore_errors=True)
                    os.mkdir(tempDir)
                    os.chdir(tempDir)
                    # Command specifies whether one or all of the data types are being recalculated
                    #  The default when called by CRobot and this parameter is not provided is all
                    #  Could not get conda version to work within this script, although it does withoin CRobot
                    #                       cmd = "source /home/nigeldyer/miniconda3/etc/profile.d/conda.sh ;conda activate crobot3 ;python " \
                    cmd = "source /home/nigeldyer/.pyenv/versions/crobot3/bin/activate ;python " \
                          "/home/nigeldyer/CRobot/pipelines/AlternType_V4.py  Escherichia '" + exp_data + "' " + \
                          assembly_list.file_pointer + '  ' + option
                    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, preexec_fn=os.setsid,
                        executable='/bin/bash').communicate()
                    newValues = result[0]
                    #  result[0] is the stdout result, which is empty if there is a problem
                    if newValues:
                        newValues = json.loads(result[0])
                        updates = ''
                        for t in newValues:
                            if t in results:
                                if newValues[t] != results[t]:
                                    updates = updates + ", " + t + ":" + results[t] + " > " + newValues[t]
                                    results[t] = newValues[t]
                            else:
                                updates = updates + ", " + t + " > " + newValues[t]
                                results[t] = newValues[t]
                        if updates:
                            other_data['results'] = results
                            record.other_data = other_data
                            updating = updating + 1
                            #   and store
                            dbase.session.commit()
                        else:
                            updates = 'OK'

                    else:
                        # result[1] is the stderr result.  The most informative info is after the last ValueError
                        if result[1]:
                            updates = result[1].split("ValueError")
                            updates = updates[-1]
                        else:
                            updates = "Undefined error"
                    status = str(id) + " ," + barcode + " ," + now.strftime(
                        "%m/%d/%Y, %H:%M:%S") + " ," + assembly_list.barcode + " ," + updates
                else:
                    no_hc = no_hc + 1
                    status = str(id) + " ," + barcode + " ," + now.strftime(
                        "%m/%d/%Y, %H:%M:%S") + " ," + assembly_list.barcode + " Failed to get HC data"
            else:
                stxs_previously_done = stxs_previously_done + 1;
                status = str(id) + " ," + barcode + " ," + now.strftime(
                    "%m/%d/%Y, %H:%M:%S") + " ," + assembly_list.barcode + " Already processed: stx1 or 2 = '+'"
            log.write(status + "\n")
            print(status)
            log.flush()

#                if updating > 10:
#                    break

    print (str(updating) + " entries being updated")
    log.close()



@manager.command
@manager.option('-d', '--database', help='database')
def dump_results(database='ecoli'):
    """
    Goes through the database looking for gaps where the Clermont data ins not available or
    the blastfrost data is not available and runs a job to complete them
    """

    dbase = get_database(database)

    if not dbase:
        print("Error, no database for %s" % database)
        return

    types = ['ClermonTyping', 'ezClermont', 'fimH',  'BFPathovar', 'Stx1', 'Stx2', 'ipaH', 'ST',  'LT', 'eae',  'pInv']
    log = open("/home/nigeldyer/blastfrost/results_dump.txt", "w")
    print("I found the correct %s database" % database)
    log.write("Barcode")
    for t in types:
        log.write("\t" + t )
    log.write("\n")
#    sql = "SELECT * FROM public.strains ORDER BY id ASC LIMIT 10000"
    sql = "SELECT * FROM public.strains ORDER BY id ASC"
    sql_result = dbase.execute_query(sql)
    print(len(sql_result))
    assembly_lookup = dbase.models.AssemblyLookup
    count = 0
    for strain in sql_result:
        count = count + 1
        if (count % 1000) == 0:
            print (str(count) + " checked")
        barcode = strain.get('barcode')
        best_assembly = strain.get('best_assembly')
        records = dbase.session.query(assembly_lookup).filter(
            assembly_lookup.assembly_id == best_assembly).filter(
            assembly_lookup.scheme_id == 15).all()
        if len(records) > 0:
            record = records[0]
            other_data = dict(record.other_data)
            results = dict(other_data['results'])
            log.write(barcode)
            for t2 in types:
                log.write("\t" + results.get(t2, ""))
            log.write("\n")




@manager.command
@manager.option('-d', '--database', help='database')
def update_schemes(database='ecoli'):
    '''
    A specific function to move data from a file to a publicly available field inside Phylotypes:
    First insert a new row in data param, by running function "insert_data_param_row", then
    query the custom column to get its id,
    query the Strains table to get the custom column values,
    insert the custom column values inside Assembly lookup,
    add new key value pairs to json string result
    '''
    import pandas as pd
    import math

    os.chdir('/home/nigeldyer/blastfrost')
#    fileroot = "results_dump_short"
    fileroot = "results_dump"


#    values = pd.read_csv('Subset.txt', sep='\t', header=None).values.tolist()
    values = pd.read_csv(fileroot+'.txt', sep='\t')
    values.fillna(value='', inplace=True)

    header = values.columns.tolist()
    values = values.values.tolist()

    logfile = open(fileroot + ".log", "a+")

    dbase = get_database(database)
    if not dbase:
        print("Error, no database for %s" % database)
        return
    count = 0
    for item in values:
        count = count + 1
        if (count % 100) == 0:
            print (barcode + " - " + str(count) + "...")
        barcode = item[0]
        sql = "SELECT * FROM public.strains WHERE barcode IN ('" + barcode + "')"
        sql_result = dbase.execute_query(sql)
#        print len(sql_result)
        assembly_lookup = dbase.models.AssemblyLookup
        for strain in sql_result:
            records = dbase.session.query(assembly_lookup).filter(
                assembly_lookup.assembly_id == strain.get('best_assembly')).filter(
                assembly_lookup.scheme_id == 15).all()
            if len(records) > 0:
                record = records[0]
                other_data = dict(record.other_data)
                results = dict(other_data['results'])

                differences = ''
                update = False
                data_present = False;

                for i in range(1, len(header)):
                    if item[i] != '':
                        data_present = True
                        if header[i] in results:
                            if results[header[i]] == '' and item[i] != '':
                                results[header[i]] = item[i]
                                update = True
                            elif results[header[i]] != item[i]:
                                differences = differences + " " if differences else "" + header[i] + ":" + results[header[i]] + " > " + item[i]
                        elif item[i] != '':
                            results[header[i]] = item[i]
                            update = True
                if differences:
                    logfile.write(barcode + "\t" + differences + "\n")
                    logfile.flush()
                if not data_present:
                    logfile.write(barcode + "\t No data present in input file\n")
                    logfile.flush()

                if update:
                    other_data['results'] = results
                    record.other_data = other_data
                    #   and store
                    dbase.session.commit()
    logfile.close()
   
@manager.command
def run_app():
    from entero import app
    app.run(host='0.0.0.0',port=5000)


@manager.command
def check_user_files():
    import psycopg2
    from psycopg2.extras import RealDictCursor
    import mmap
    #    logfile = open('/home/nigeldyer/file_errors.txt', "a+")
    logfile = open('/home/nigeldyer/file_errors.txt', "w")

    db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
    local_upload_dir = app.config['LOCAL_UPLOAD_DIR']
    db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
    #    SQL = "SELECT * FROM user_jobs WHERE pipeline='QAssembly' ORDER BY id DESC LIMIT 100"
#    SQL = "SELECT * FROM user_jobs WHERE pipeline='QAssembly' ORDER BY id DESC LIMIT 5000"
    SQL = "SELECT * FROM user_jobs WHERE pipeline='QAssembly' AND user_id != 0 ORDER BY id DESC LIMIT 5000"
    db_cursor.execute(SQL)
    results = db_cursor.fetchall()
    print(len(results))
    for result in results:
        accession = result['accession']
        userid = str(result['user_id'])
        database = result['database']
        files = accession.split('#')
        jobid = str(result['id'])
        date = str(result['date_sent'])
        jobok = True
        files_found = False
        for file in files[1:3]:
            filename = local_upload_dir + userid + '/' + database + '/' + file
            if os.path.exists(filename):
                files_found = True
                filesize = str(os.path.getsize(filename)/1000000)
                with open(filename, "r+b") as f:
                    mm = mmap.mmap(f.fileno(), 0)
                    location = mm.find('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
                    if location != -1:
                        logfile.write('\t'.join([jobid, userid, database, date, filesize, str(location), filename,'\n']))
                        jobok = False
        if jobok:
            logfile.write('\t'.join([jobid, userid, database, date, filesize,'OK' if files_found else 'No file','\n']))
        logfile.flush()


@manager.command
@manager.option('-d', '--dbName', help='database name')
@manager.option('-l', '--limit', help='database name')
@manager.option('-f', "--force")
@manager.option("-q", "--queue")
@manager.option("-p", "--priority")
def update_assemblies(dbName="senterica", limit=100, force=5, queue='backend', priority=0):
    if not get_database(dbName):
        app.logger.error("update_assemblies failed, can not find %s database" % dbName)
        return

    from entero.jobs.jobs import AssemblyJob
    force = int(force)
    force_any = int(force) * 2
    conn = None
    db_conn = None
    app.logger.info('Running update assemblies')
    # a list of traces accession for assemblies which have failed > 5 times
    fail_list = ""
    try:
        db_conn = psycopg2.connect(app.config['SQLALCHEMY_DATABASE_URI'])
        db_cursor = db_conn.cursor(cursor_factory=RealDictCursor)
        #        SQL = "SELECT accession,COUNT(id) AS num FROM user_jobs WHERE status LIKE 'FAILED%%' AND pipeline= 'QAssembly' GROUP BY accession HAVING count(id)>%i ORDER BY num DESC" % force
        SQL = "SELECT accession,COUNT(id) AS num FROM user_jobs " \
              "WHERE status LIKE 'FAILED%%' AND pipeline= 'QAssembly' AND database = '%s' " \
              "GROUP BY accession HAVING count(id)>%i " \
              "UNION SELECT accession,COUNT(id) AS num FROM user_jobs " \
              "WHERE pipeline= 'QAssembly' AND database = '%s' " \
              "GROUP BY accession HAVING count(id)>%i " \
              "UNION SELECT accession,COUNT(id) AS num FROM user_jobs " \
              "WHERE status IN ('QUEUE','RUNNING') AND pipeline= 'QAssembly' AND database = '%s' " \
              "GROUP BY accession " \
              "ORDER BY num DESC" % (dbName, force, dbName, force_any, dbName)

        db_cursor.execute(SQL)
        results = db_cursor.fetchall()
        fails = []
        for record in results:
            if record.get('accession'):
                # split accession to get trace accession identifier for user reads
                fails.append(record['accession'].split(':#')[0])
        if len(fails) > 0:
            fail_list = "','".join(fails)
            fail_list = "AND traces.accession NOT IN ('" + fail_list + "')"
            app.logger.info(
                '%d assemblies have previously failed sufficiently often that they will not be retried' % len(fails))
        db_conn.close()
    except Exception as e:
        if db_conn:
            db_conn.rollback()
            db_conn.close()
        app.logger.exception("update_assemblies failed, error message: %s" % str(e))
        return

    try:
        # no Assembly but there are reads
        SQL = "SELECT traces.id as tid, traces.read_location as rl,strains.owner as uid " \
              "FROM strains INNER JOIN traces on traces.strain_id = strains.id  " \
              "AND traces.seq_platform = 'ILLUMINA' AND traces.seq_library='Paired' " \
              "WHERE best_assembly IS NULL AND strains.uberstrain = strains.id %s " \
              "AND traces.read_location IS NOT NULL " \
              "ORDER BY strains.id DESC LIMIT %i " % (fail_list, int(limit))
        # no Assembly
        # SQL = "SELECT traces.id as tid, traces.read_location as rl,strains.owner as uid FROM strains INNER JOIN traces on traces.strain_id = strains.id  AND traces.seq_platform = 'ILLUMINA' AND traces.seq_library='Paired' WHERE best_assembly IS NULL AND strains.uberstrain = strains.id %s ORDER BY strains.id DESC LIMIT %i " % (fail_list,int(limit))
        # no Scheme info
        # SQL ="SELECT traces.id as tid FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Assembled' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id  LEFT  JOIN st_assembly ON st_assembly.assembly_id= best_assembly WHERE  st_assembly.st_id  IS NULL"
        # Reason failed is null
        # SQL ="SELECT strain,traces.id as tid,other_data,job_id FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Failed QC' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id WHERE other_data ILIKE '%[null]%'  ORDER BY job_id DESC"

        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][dbName][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        cursor.execute(SQL)
        results = cursor.fetchall()
        app.logger.info('Resending %d assemblies' % len(results))
        for res in results:
            send_priority = priority
            send_queue = queue
            user_id = 0
            if res['uid']:
                user_id = res['uid']
                send_priority = -9
                send_queue = "user_upload"

            job = AssemblyJob(trace_ids=[res['tid']],
                              database=dbName,
                              workgroup=send_queue,
                              priority=send_priority,
                              user_id=user_id)
            job.send_job()
        conn.close()

    except Exception as e:
        if conn:
            rollback_close_connection(conn)
            # conn.rollback()
            # conn.close()
        app.logger.exception("update_assemblies failed, error message: %s" % str(e))

@manager.command
def test(dbname='miu'):
    dbase = get_database(dbname)
    if not dbase:
        app.logger.exception("Failed to get database %s" % (dbname))
    dbase.create_dataparam()

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-u', '--user_name', help='database')
@manager.option('-i', '--included_traces_barcodes', help='Only include traces with these barcodes')
def update_metadata(database=None, user_name=None, included_traces_barcodes=None):
    '''
    Update metadata for selected strains.  INitially used to update incorrect study accession data in Enterobase,
    based on the list of project members obtained from ENA.

    :param database: required
    :param user_name:
    :param included_traces_barcodes:
    :return:
    '''
    from entero import db
    from entero.databases.system.models import User

    try:
        # EBI folder which is used to save EBI databases projects information
        # and other related to EBI, e.g. file contains list of fixed users reads files

        if not database or not user_name:
            print('Need to have both username and database supplied')
            return
        if included_traces_barcodes:
            included_traces_barcodes = included_traces_barcodes.split(',')
            print(included_traces_barcodes)
            print('==========================')
        dbase = get_database(database)
        if not dbase:
            app.logger.warning("No database is found for %s"%database)
            return
        user = db.session.query(User).filter(User.username==user_name).first()
        if not user:
            print("no record in he database for user: %s"%user_name)
            return
        strains = dbase.models.Strains
        traces = dbase.models.Traces
        assemblies = dbase.models.Assemblies
        traces_assembly = dbase.models.TracesAssembly
        qry = dbase.session.query(strains, traces, traces_assembly, assemblies) \
                .filter(strains.owner == user.id) \
                .filter(traces.barcode.in_(included_traces_barcodes)) \
                .filter(strains.best_assembly is not None) \
                .filter(traces.strain_id == strains.id) \
                .filter(traces.seq_platform == 'ILLUMINA') \
                .filter(traces.read_location is not None) \
                .filter(strains.best_assembly == assemblies.id)\
                .filter(assemblies.status == 'Assembled') \
                .filter(strains.study_accession == 'PRJEB39988') \
                .filter(strains.uberstrain > 0) \
                .filter(traces_assembly.columns['trace_id'] == traces.id) \
                .filter(traces_assembly.columns['assembly_id'] == strains.best_assembly)
        rows = qry.count()
        print("Number of strains to be updated: {0}".format(rows))
        for data in qry:
            print("Updating barcode {0}: strain = {1}".format(data[0].barcode, data[1].barcode))
            data[0].study_accession = 'PRJEB39546'
            data[0].secondary_study_accession = 'ERP123079'
        dbase.commit()
        print('Query successful')
    except Exception as e:
        print("Problem: {0}".format(str(e)))

@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-f', '--filename', help='database')
def update_respred(database=None,filename = None):
    '''
    Update metadata for selected strains.  INitially used to update incorrect study accession data in Enterobase,
    based on the list of project members obtained from ENA.

    :param database: required
    :param user_name:
    :param included_traces_barcodes:
    :return:
    '''
    try:
        dbase = get_database(database)
        if not dbase:
            app.logger.warning("No database is found for %s" % database)
            return

        respred_data = open(filename,'r')
        param_info = [line.strip() for line in respred_data]
        header = [x.strip() for x in param_info[0].split('\t')]
        strain_data = [[x.strip().strip('\"') for x in line.split('\t')] for line in param_info[1:]]
        strain_data = {strain[0]:strain[1:] for strain in strain_data}
        # strain_data = {'CLO_AA7966AA':strain_data['CLO_AA7966AA']}
        strain_list = [strain for strain in strain_data.keys()]
        strains = dbase.models.Strains
        assemblies = dbase.models.Assemblies
        qry = dbase.session.query(strains, assemblies).filter(strains.barcode.in_(strain_list)). \
                   filter(strains.best_assembly == assemblies.id)

        rows = qry.count()

        Analysis = dbase.models.AMRAnalysis
        for data in qry:
            strain_info = strain_data[data.Strains.barcode]

            amr_analysis = dbase.session.query(Analysis).filter_by(id=data.Assemblies.id).first()
            if not amr_analysis:
                amr_analysis = Analysis(id=data.Assemblies.id)
            resInfo = dict()
            for i in range(len(strain_info)):
                if strain_info[i] != 'ND':
                    genes = strain_info[i].split(',')
                    gene_list = {gene.strip(): [header[i+1]] for gene in genes}
                    resInfo.update({header[i+1]: gene_list})
            setattr(amr_analysis, 'barcode', data.Assemblies.barcode)
            setattr(amr_analysis,"respred_results", resInfo)
            dbase.session.add(amr_analysis)

        dbase.session.commit()
    except Exception as e:
        print (str(e))


@manager.command
def count_all_assemblies_uploaded_monthly_by_genus():
    '''
       This function counts how many assemblies were created for each genus each month (total and from users) across all front end databases
    '''
    from entero.ExtraFuncs.query_functions import combine_strain_and_exp_data, process_strain_query, \
        query_function_handle, dbhandle
    from dateutil.parser import parse
    import json

    # generic search criteria
    user_id = 0
    strain_query = 'all'
    strain_query_type = 'query'
    show_substrains = None
    extra_data = None
    exp = 'assembly_stats'
    options = {'only_editable': None, 'show_non_assemblies': None, 'no_legacy': 'true', 'only_with_data': None}
    for database, dbase in dbhandle.items():

        if not app.config['ACTIVE_DATABASES'][database][2]:
            # don't include private databases
            continue

        # using EnteroBase search functions to return all the strains insaide the database
        print("process strain query for database %s" % database)
        strain_data, aids = process_strain_query(database, strain_query, strain_query_type, user_id,
                                                 show_substrains=show_substrains)

        if not aids:
            exp_data = {}
        else:
            # print "query_function_handle"
            exp_data, aids = query_function_handle[database][exp](database, exp, aids, "assembly_ids", extra_data)

        return_data = combine_strain_and_exp_data(strain_data, exp_data, options)
        # load the results into a dict  as this function returns it as json string
        return_data = json.loads(return_data)
        # variable to count user uploads
        user_uploads = 0

        # create empty dictionary of all months from 2015 to 2021, filled with dummy values, just to be used for printing
        all_yearmonth_dict = {}

        for y in range(2015, 2022):
            for m in range(1, 13):
                ym = str(y) + "." + str(m)
                all_yearmonth_dict[ym] = 1
                print(ym),
        print ("\n")
        # these dicts hold the values
        yearmonth_total_dict = {}
        yearmonth_user_dict = {}

        print("Please wait while processing the data ....")
        count = 0
        for strain in return_data['strains']:
            count += 1
            # get created date and add it to the dict
            year = parse(strain['created']).date().year
            month = parse(strain['created']).date().month
            yearmonth = str(year) + "." + str(month)
            # test if the year.month is a key in the dict, if so then increase the number of strains for this year by 1
            # if not create a new item in the dict for the year
            if yearmonth in yearmonth_total_dict:
                yearmonth_total_dict[yearmonth] = yearmonth_total_dict[yearmonth] + 1
            else:
                yearmonth_total_dict[yearmonth] = 1

            # test if the strain is not public,e.g has been fetched from NCBI
            if not strain['study_accession'] and not strain['secondary_study_accession'] and not strain[
                'sample_accession'] and not strain['secondary_sample_accession']:
                # check if the strain has an owner
                if strain['owner']:
                    user_uploads += 1
                    # you may add counter for user uploads per years similar to the one which have been used for all strains
                    if yearmonth in yearmonth_user_dict:
                        yearmonth_user_dict[yearmonth] = yearmonth_user_dict[yearmonth] + 1
                    else:
                        yearmonth_user_dict[yearmonth] = 1

                # print created_year_dict
                # print json.dumps(yearmonth_total_dict, indent=4, sort_keys=True)
                # print "total number of strains : %s" % len(return_data['strains'])
                # print "no of users uploads %s" % user_uploads
                # print user_uploaded_year_dict
                # print json.dumps(yearmonth_user_dict, indent=4, sort_keys=True)
        # print the dictionaries, could be prettier/to a file for easier parsing
        for key in sorted(all_yearmonth_dict):
            if key in yearmonth_total_dict:
                print (yearmonth_total_dict[key] & ",")
            else:
                print("0,")
        print ("\n")
        for key in sorted(all_yearmonth_dict):
            if key in yearmonth_user_dict:
                print(yearmonth_user_dict[key] & ",")
            else:
                print("0,")
        print ("\n")


if __name__ == '__main__':
    manager.run()
