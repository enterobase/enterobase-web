
#!flask/bin/python

import os, datetime
import json, requests,ujson
from flask import make_response, send_file
from flask_script import Manager
from entero import create_app, dbhandle, app, get_database
from datetime import datetime
import unidecode
"""
Contains scripts that are useful for managing metadata.   Originally duplicated from manage_zhemin from where
they have been deleted; Methods currently provided:

-   get_metadata:   Gets specific metadata from a specific species scheme (Untested)
    audit_metadata: Compares the metadata for a species with that held on the SRA and corrects any differences
    audit_metadata_assembly_list: Compares the metadata with that on the SRA for a list of samples
          Can also be used to add NCBI identifiers to strains that were initially uploaded outside of NCBI
    audit_metadata_special_fromEBI: (Untested)
"""

# Set enviro with ENTERO_CONFIG=<See config.py for options>
entero = create_app(os.getenv('ENTERO_CONFIG') or 'development')
manager = Manager(entero)


@manager.command
@manager.option('-d', '--database', help='database')
@manager.option('-s', '--scheme', help='scheme')
@manager.option('-f', '--fieldname', help='metadata field')
def get_metadata(database=None, scheme='MLST_Achtman', fieldname='serotype'):
    dbase = get_database(database)
    if not dbase:     
        message = "No database found for the provided species: %s" % database
        app.logger.error(message)
        return make_response(message)    
    
    Strains = dbase.models.Strains
    Assemblies = dbase.models.Assemblies 
    AssemblyLookup = dbase.models.AssemblyLookup
    Schemes = dbase.models.Schemes
    scheme_id, scheme_desc = dbase.session.query(Schemes.id, Schemes.description).filter(Schemes.description.
                                                like('%{0}%'.format(scheme))).order_by(Schemes.id.desc()).first()
    search = [dict(st_barcode= x[0], scheme=scheme_desc, metadata=x[2])
              for x in dbase.session.query(AssemblyLookup.st_barcode, AssemblyLookup.scheme_id,
                                           Strains.__table__.c[fieldname])
              .filter(AssemblyLookup.st_barcode != 'EGG_ST')
              .filter(AssemblyLookup.scheme_id == scheme_id)
              .join(Assemblies).join(Strains).all()]
    with open('/share_space/interact/metadata.csv', 'w') as f:
        f.write('scheme,metadata,st_barcode\n')
        for j in search:
            j['metadata'] = '' if not j['metadata'] else j['metadata'].encode('ascii', 'ignore')
            f.write('%s\n' % '\t'.join(j.values()))
        f.close()
    return send_file('/share_space/interact/metadata.csv')


def __audit_user_metadata(fout, dbase):
    Strains = dbase.models.Strains
    from collections import defaultdict
    source_toUpdate = defaultdict(list)
    geo_toUpdate = defaultdict(list)
    for strain in dbase.session.query(Strains).filter(Strains.secondary_sample_accession == None):
        if not strain.source_niche or not strain.source_type:
            details = unidecode.unidecode(u'{0}, {1}, {2}, '.format(strain.source_details, strain.source_niche,
                                                        strain.source_type).replace(u'None, ', u''))
            if details:
                source_toUpdate[details[:-2]].append([strain.barcode, strain.source_niche, strain.source_type])
        if not strain.continent or not strain.country:
            details = unidecode.unidecode(u'{0}, {1}, {2}, '.format(strain.admin1, strain.admin2,
                                                                    strain.city).replace(u'None, ', u''))
            if details or strain.country:
                geo_toUpdate[(strain.country, details[:-2])].append([strain.barcode, strain.continent, strain.country])
    toUpdate = defaultdict(dict)
    if len(geo_toUpdate):
        geo_api = app.config['DOWNLOAD_URI'] + 'api/geo_format'
        for (country, raw), data in geo_toUpdate.items():
            try:
                if country:
                    r = ujson.loads(requests.get(geo_api, {'raw': raw, 'country': country}).text)
                else:
                    r = ujson.loads(requests.get(geo_api, {'raw': raw}).text)
                country, continent = r[0]['Country'], r[0]['Continent']
                for d in data:
                    if not d[1]:
                        toUpdate[d[0]]['continent'] = continent
                    if not d[2]:
                        toUpdate[d[0]]['country'] = country
            except:
                pass
    if len(source_toUpdate):
        source_api = app.config['DOWNLOAD_URI'] + 'api/host_format'
        for raw, data in source_toUpdate.items():
            try:
                r = ujson.loads(requests.get(source_api, {'raw': raw}).text)
                if r[0]['Source Probability'] >= 0.5:
                    n, t = r[0]['Source Niche'], r[0]['Source Type']
                    for d in data:
                        if not d[1]:
                            toUpdate[d[0]]['source_niche'] = n
                        if not d[2]:
                            toUpdate[d[0]]['source_type'] = t
            except:
                pass
    cnt = 0
    SA = dbase.models.StrainsArchive
    for strain in dbase.session.query(Strains).filter(Strains.barcode.in_(toUpdate.keys())):
        toChange = toUpdate[strain.barcode]
        for k, v in toChange.items():
            fout.write('"{0}"\t"{1}"\t"{2}"\t"{3}"\n'.format(strain.barcode, k, getattr(strain, k), v))
        
        if len(toChange):
            sa = SA()
            for c in strain.__table__.columns:
                if not c.primary_key:
                    setattr(sa, c.name, getattr(strain, c.name))
            strain.version += 1
            for k, v in toChange.items():
                setattr(strain, k, v)
            dbase.session.add(sa)
            cnt += 1
            if cnt % 1000 == 0:
                dbase.session.commit()
    dbase.session.commit()
    return toUpdate


def __cleanUp_metadata(data, records):
    # Extracts just the metadata of interest from all the metadata returned from the SRA
    for d in data:
        key = d['Sample']['Main Accession']
        metadata = d['Sample']['Metadata']
        metadata['Center'] = d['Sample']['Source']['Center']
        records[key] = metadata

def __get_getNCBIids(data, records):
    # Extracts just the metadata of interest from all the metadata returned from the SRA
    for d in data:
        key = d['Sample']['Metadata']['Strain']
        ncbi_data = {'Strains': {
                         'sample_accession': d['Sample']['Main Accession'],
                         'study_accession': d['Project']['Bioproject ID'],
                         'secondary_sample_accession': d['Sample']['SRA Accession'],
                         'secondary_study_accession': d['Project']['Bioproject ID'],
                         'email': d['Sample']['Source']['Contact'][0][0],
                         'owner': None},
                    'Traces': {
                         'accession': d['Run'],
                         'experiment_accession': d['Experiment']['Accession'],
                         'seq_library':  d['Sequencing']['Library'],
                         'seq_platform':  d['Sequencing']['Platform'],
                         'status': 'SRA'}
                        }
        records[key] = ncbi_data

def __update_record_metadata(fout, dbase, data) :
    """
    Using the metadata obtained from SRA we see if any of the strain data on Enterobase is inconsistent.
    """
    cnt = 0

    def tryFloat(d, default=None):
        try:
            return float(d)
        except:
            return default

    def tryInt(d, default=None):
        try:
            return int(d)
        except:
            return default
    # get handler for strains and strain_archives
    Strains = dbase.models.Strains
    strain_archive = dbase.models.StrainsArchive
    # get barcodes
    data2 = {}
    cur_records = {}
    for strain in dbase.session.query(Strains).filter(Strains.sample_accession.in_(data.keys())):
        #  data, d and data2 are the newly downloaded data from ncbi
        d = data[strain.sample_accession]
        # Sample code for just updating some parameters
        # data2[strain.barcode] = dict(continent=d.get('Continent', None), source_details=d.get('Source Details', None),
        #                             collection_date=tryInt(d.get('Day', None)),species=d.get('Species', None),
        #                             pathovar=d.get('Pathotype', None))
        data2[strain.barcode] = dict(strain=d.get('Strain', None), contact=d.get('Center', None),
            city=d.get('City', None), admin1=d.get('admin1', None), admin2=d.get('admin2', None),
            country=d.get('Country', None), continent=d.get('Continent', None) if 'Continent' in d else d.get('Continient', None),
            latitude=tryFloat(d.get('latitude', None)), longitude=tryFloat(d.get('longitude', None)),
            collection_date=tryInt(d.get('Day', None)), collection_month=tryInt(d.get('Month', None)), collection_year=tryInt(d.get('Year', None)),
            source_details=d.get('Source Details', None), source_niche=d.get('Source Niche', None), source_type=d.get('Source Type', None),
            species=d.get('Species', None), )
        cur_records[strain.barcode] = strain.as_dict()
    #  Go through all the records in the strain archive where the archive data differs from the current data
    #  where they differ, it has already ben changed so remove data from local copy of current data
    #  so it wont be updated again as part of the audit.
    for record in dbase.session.query(strain_archive).filter(strain_archive.barcode.in_(data2.keys())):
        r1 = cur_records[record.barcode]
        r0 = record.as_dict()
        for k, v0 in r0.items():
            v1 = r1.get(k, v0)
            if v1 != v0 and v1 != 'Air':
                r1.pop(k, None)
    for strain in dbase.session.query(Strains).filter(Strains.barcode.in_(data2.keys())):
        #  r1 = current data, r2/v2 = potential new data
        r1 = cur_records[strain.barcode]
        r2 = data2[strain.barcode]
        toChange = []
        for k, v2 in r2.items():
            v1 = r1.get(k, v2)
            try:
                if v1 and not isinstance(v1, int):
                    v1 = unidecode.unidecode(v1)
            except:
                pass
            try:
                if v2 and not isinstance(v2, int):
                    v2 = unidecode.unidecode(v2)
                    v2 = v2[:200]
            except:
                pass
            if v1 != v2 and (v1 or v2) and \
                    (not isinstance(v2, float) or not isinstance(v1, float) or abs(v1-v2) > 0.00001):
                fout.write('"{0}"\t"{1}"\t"{2}"\t"{3}"\n'.format(strain.barcode, k, v1, v2))
                toChange.append([k, v2])
        if len(toChange):
            sa = strain_archive()
            for c in strain.__table__.columns:
                if not c.primary_key:
                    setattr(sa, c.name, getattr(strain, c.name))
            strain.version += 1
            for k, v in toChange:
                setattr(strain, k, v)
            dbase.session.add(sa)
            cnt += 1
            if cnt % 1000 == 0:
                dbase.session.commit()
    dbase.session.commit()
    return

def __update_getNCBIids(fout, dbase, data) :
    strains = dbase.models.Strains
    traces = dbase.models.Traces
    qry = dbase.session.query(strains, traces)\
        .filter(strains.strain.in_(data.keys()))\
        .filter(traces.strain_id == strains.id).all()

    for entry in qry:
        ncbi_data=data[entry.Strains.strain]
        for type, values in ncbi_data.items():
            for key, value in values.items():
                setattr(getattr(entry, type), key, value)
    dbase.session.commit()

def __audit_assembly_metadata(dbname, organism):
    records = {}
    numcount = 1000
    last_batch = 0
    for offset_count in range(0, 1000000000, numcount):
        URI = app.config['DOWNLOAD_URI'] + 'assembly'
        params = dict(
            term=organism,
            reldate=99999,
            start=offset_count,
            num=numcount
        )
        try:
            resp = requests.get(url=URI, params=params, timeout=3000).text
            data = ujson.loads(resp)
            if isinstance(data, dict):
                if data.get("error"):
                    break
            app.logger.info('Getting {1} results from NCBI assembly for {0} from offset {2}.'.format(
                            organism,len(data), offset_count))
            if len(data) > 0:
                last_batch = offset_count
                __cleanUp_metadata(data, records)
            elif offset_count - last_batch > 20000:
                break
        except:
            break
    return records


def __audit_sra_metadata(dbname, organism):
    #  Get all of the records associated with a species going back 99999 days.   Edit the values to just get
    #  a subset of the data, e.g. for testing or to only go back so far.
    records = {}
    numcount = 2000
    numstart = 0
    numend = 1000000000
    last_batch = 0
    for offset_count in range(numstart, numend, numcount):
        URI = app.config['DOWNLOAD_URI'] + 'dump'
        params = dict(
            organism=organism,
            reldate=99999,
            start=offset_count,
            num=numcount
        )
        try:
            resp = requests.get(url = URI, params = params, timeout=3000).text
            data = ujson.loads(resp)
            if isinstance(data, dict):
                if data.get("error"):
                    break
            app.logger.info('Getting {1} results from SRA for {0} from offset {2}.'.format(
                            organism,len(data), offset_count))
            if len(data) > 0:
                last_batch = offset_count
                __cleanUp_metadata(data, records)
            elif offset_count - last_batch > 20000:
                break
        except requests.exceptions.ReadTimeout:
            app.logger.error('Read timeout, Suggest try again later')
            return records
        except Exception:
            break
    return records


@manager.command
@manager.option('-d', '--db', help='Database name or all (only if no file)')
def audit_metadata_special_fromEBI(db=None):
    for database in db.split(','):
        dbase = get_database(database)
        Strains = dbase.models.Strains
        for id, strain in enumerate(dbase.session.query(Strains).filter(Strains.contact == 'EBI')) :
            acc = strain.secondary_sample_accession
            if acc:
                q = requests.get('http://137.205.123.127/ET/meta/metadata', {'sample': acc})
                try:
                    data = json.loads(q.text)
                    strain.contact = data[0]['Sample']['Source']['Center']
                    app.logger.info('{0} {1}'.format(acc, strain.contact))
                    if id % 100 == 99:
                        dbase.session.commit()
                except:
                    pass
        dbase.session.commit()


@manager.command
@manager.option('-d', '--db', help='Database name or all (only if no file)')
def audit_metadata(db=None):
    """
    Imports public metadata from SRA associated with a specific database and compares it with the
    data currently on Enterobase, updating the Enterobase data if there are any discrepancies.
    The choice of which metadata to be updated can be made by editing __update_record_metadata.  If any of the
    metadata has been previously edited within Enterobase then the edited values will not be overwritten
    __audit_sra_metadata can be edited such that only some of the data is retrieved from the SRA
    """
    # 1. fetch all user data with empty space from database
    # 2. get corresponding metadata category
    # 3. get SRA metadata from MetaParser
    # 4. get assembly data from MetaParser
    # for each record :
    # 5. check whether it has been manually modified
    # 6. if not, update its metadata category
    for database in db.split(','):
        dbase = get_database(database)
        if not dbase:     
            message = "No database found for the provided species: %s" % database
            app.logger.error(message)
            print(message)
            return
        #  A database can be associated with multiple species names so get the
        names = dbhandle[database].DATABASE_DESC
        for name in names:
            records = __audit_sra_metadata(database, name)
            records.update(__audit_assembly_metadata(database, name))
            #  Note the the original of this code did the following line after looping through all the names
            # so only processed the last name
            if len(records):
                with open('{0}.{1}.{2}.audit.log'.format(database,name, datetime.now().strftime("%Y%m%d_%H_%M")),
                          'a') as fout:
                    __update_record_metadata(fout, dbase, records)
                    # fill in empty user data
                    __audit_user_metadata(fout, dbase)


def __audit_selected_sra_metadata(accessions,get_ncbi_ids = False):
    """
    Gets metadata for a set of accession numbers from SRA
    """
    records = {}
    data = []
    # Gets the metadata from SRA in blocks of 50 as longer requests exceed html request length limit
    for offset_count in range(0, len(accessions), 50):
        acc = accessions[offset_count:offset_count+50]
        params = dict(run=','.join(acc))
        URI = app.config['DOWNLOAD_URI'] + '/metadata'
        print("Calling the sever %s please wait ..." % URI)
        resp = requests.post(url=URI, params=params, timeout=100)
        if resp.status_code == 500:
            app.logger.error('Could not access metadata')
        elif resp.status_code == 404:
            app.logger.error('No response from NCBI')
        try:
            data = data + ujson.loads(resp.text)
        except:
            print("Error while loading the server response: ", resp.text)

    if len(data) > 0:
        if get_ncbi_ids:
            __get_getNCBIids(data, records)
        else:
            __cleanUp_metadata(data, records)
        return records
    return None


@manager.command
@manager.option('-d', '--db', help='Database name')
@manager.option('-f', '--file_name', help='List of accession codes to be updated)')
@manager.option('-g', '--get_ncbi_ids', help='get NCBI Ids)')
def audit_metadata_assembly_list(db=None, file_name=None, get_ncbi_ids=False):
    """
    Imports public metadata from SRA based on their accession numbers and compares the metadata with the
    data currently on Enterobase, updating the Enterobase data if there are any discrepancies.
    The choice of which metadata to be updated can be made by editting __update_record_metadata.  If any of the
    metadata has been previously edited within Enterobase then the editted values will not be overwritten
    The get_ncbi_ids, attaches accession number etc to strains that have been uploaded independently to NCBBI but are actually
    on NCBI.  The strains are identified by their SRR accession number, and the strain name must match the strain name that has
    previously been put into the Enterobase data
    """

    dbase = get_database(db)
    if not dbase:
        message = "No database found for the provided species: %s" % db
        app.logger.error(message)
        print(message)
        return

    with open(file_name) as f:
        accession_codes = f.read().splitlines()
    records = __audit_selected_sra_metadata(accession_codes,get_ncbi_ids)
    if len(records):
        with open('{0}.{1}.audit.log'.format(db, datetime.now().strftime("%Y%m%d_%H_%M")), 'a') as fout:
            if get_ncbi_ids:
                __update_getNCBIids(fout, dbase, records)
            else:
                __update_record_metadata(fout, dbase, records)


@manager.command
def run_app():
    from entero import app
    app.run(host='0.0.0.0', port=5000)


if __name__ == '__main__':
    manager.run()

