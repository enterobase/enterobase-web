import unittest, logging, requests, base64, urllib, json, time
from urllib.request import urlopen

from pyLib import unitTest_lib
from pyLib.configure import configure_reader

# specific_tests = ["test_01_getFile"]
# specific_tests = ["test_02_API_loadAssemblyStatus"]
# specific_tests = ["test_10_API_loadSchemes", "test_11_API_loadLoci", "test_12_API_loadsts"]
refreshReference = False


# ENTEROBASE_SERVER = "http://enterobase-3:8001/"
ENTEROBASE_SERVER = "https://enterobase.warwick.ac.uk/"

# ADDRESS = '%s/api/v2.0/login?username=%s&password=%s' %(ENTEROBASE_SERVER, ENTEROBASE_USERNAME, ENTEROBASE_PASSWORD)


config = configure_reader("settings.txt")

SPECIES = "senterica"
DATABASE_KEY =  config['SENTERICA_API_TOKEN']
TEST_SCHEME = "cgMLST"
API2_DELAY = 0.1

def get_logger():
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    return logging.getLogger("LOG")


class TestSequenceFunctions(unitTest_lib.EnteroTests):
    log = get_logger()
    # For persistently storing data as dictionary entries between tests.
    testConfig = {}
    refreshReference = refreshReference

    @classmethod
    def get_session(cls):
        if "session" not in cls.testConfig:
            cls.testConfig["session"] = requests.Session()
            # Requests library adds uneceesary INFO messages
            logging.getLogger("requests").setLevel(logging.WARNING)
            cls.testConfig["session"].get(ENTEROBASE_SERVER)
            headers = {'User-Agent': 'Mozilla/5.0'}
            payload = {'email':config['USERNAME'],'password':config['PASSWORD'],'remember_me':'y', 'submit':'Log In'}
            cls.testConfig["session"].post('{0}/auth/login'.format(ENTEROBASE_SERVER), headers=headers, data=payload)
        return cls.testConfig["session"]

    @classmethod
    def tearDownClass(cls):
        #  Called by unitTest.  Used to clear down any open ssl connections and prevent warning messages
        if "session" in cls.testConfig:
            cls.testConfig["session"].close()

    def test_01_getFile(self):
        """download files based on file id"""
        s = self.get_session()
        results = {}
        for assembly in ["SAL_YC5600AA_AS", "SAL_CD1022AA_AS"]:
            fileURL = '{0}upload/download?assembly_barcode={1}&database={2}'.format(ENTEROBASE_SERVER, assembly, SPECIES)
            webpage = s.get(fileURL).content
            filename = '{0}.fa'.format(assembly)
            fasta_file = open(filename, 'wb')
            fasta_file.write(webpage)
            fasta_file.close()
            hash = unitTest_lib.get_hash_for_file(filename)
            results[assembly] = hash
        self.compare_json("test_1.txt", results)

    def test_02_API_loadAssemblyStatus(self):
        command = 'api/v2.0/{0}/straindata?assembly_status=Assembled&limit=2'.format(SPECIES)
        response = urlopen(self.__create_request(ENTEROBASE_SERVER + command, DATABASE_KEY))
        data = json.load(response)
        self.compare_json("test_2.txt", data, [{"links": 'total____records'}])


# Request function
    def __create_request(self, request_str,api_token):
        base64string = base64.b64encode('{0}: '.format(api_token).encode('utf-8'))
        headers = {"Authorization": "Basic {0}".format(base64string.decode())}
        request = urllib.request.Request(request_str, None, headers)
        return request

    def test_10_API_loadSchemes(self):
        """Load all the schemes for senterica.   Data read from enterobase"""
        COMMAND = 'api/v2.0/{0}/schemes?sortorder=asc&orderby=barcode'.format(SPECIES)
        response = urlopen(self.__create_request(ENTEROBASE_SERVER + COMMAND, DATABASE_KEY))
        data = json.load(response)
        self.compare_json("test_10.txt", data)

    def test_11_API_loadLoci(self):
        """Load a couple of sets of loci for senterica.   Data read from NServ"""
        #  Enterobase currently incorporates API request throttling, so add delay
        time.sleep(API2_DELAY)
        INTERVAL = 50
        for i in [2, 5]:
            command = 'api/v2.0/{0}/{1}/loci?limit={2}&offset={3}'.format(SPECIES, TEST_SCHEME, INTERVAL,i*INTERVAL)
            response = urlopen(self.__create_request(ENTEROBASE_SERVER + command, DATABASE_KEY))
            data = json.load(response)
            self.compare_json("test_11_{0}.txt".format(i), data)

    def test_12_API_loadsts(self):
        """Load a couple of sets of sts for senterica.   Data read from NServ"""
        time.sleep(API2_DELAY)
        INTERVAL = 50
        for i in [5,8]:
            command = "api/v2.0/{0}/{1}/sts?show_alleles=true&limit={2}&offset={3}".format(SPECIES, TEST_SCHEME, INTERVAL, i*INTERVAL)
            response = urlopen(self.__create_request( ENTEROBASE_SERVER + command, DATABASE_KEY))
            data = json.load(response)
            self.compare_json("test_12_{0}.txt".format(i), data)


if __name__ == '__main__':
    # If singleTest variable declared then just run that test.   Following code does not
    # show errors if the variable not declared
    tests = unitTest_lib.value(globals(), 'specific_tests')
    if tests:
        suite = unittest.TestSuite()
        for test in tests:
            suite.addTest(TestSequenceFunctions(test))
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(suite)
    else:
        unittest.main(verbosity=2)

