from entero import dbhandle
import gzip

def get_ids_to_be_called(assembly_ids,ref_barcode,database):
	dbase = dbhandle[database]
	Schemes = dbase.models.Schemes
	Lookup = dbase.models.AssemblyLookup
	snp_scheme = dbase.session.query(Schemes).filter_by(description='snp_calls').first()
	
	already_called = dbase.session.query(Lookup).filter(Lookup.scheme_id==snp_scheme.id,
                                                        Lookup.assembly_id.in_(assembly_ids),
                                                        Lookup.st_barcode==ref_barcode).all()

	aids_already_called=[]
	for item in already_called:
		aids_already_called.append(item.assembly_id)
	aids = list(set(assembly_ids) - set(aids_already_called))
	return aids


class SNPMatrix(object):
	def __init__(self,gzip_file):
		pos_to_name={}
		self.data={}
		with gzip.open(self.data['matrix']) as f:
			for line in f:
				arr = line.strip().split("\t")
				if line.startswith("#Seq"):
					pos=0
					#get all the sample names and store their positions
					for name in arr[2:]:
						if name.startswith("#"):
							break
						pos_to_name[pos]=name
						pos+=1
				if line.startswith("##"):
					continue	
				positions=self.data.get(arr[0])
				if not positions:
					positions={}
					self.data[arr[0]]=positions
				info=positions[int(arr[1])]={}
				for base in arr[2:]:
					info[pos_to_name[pos]]=base
					
	def get_base(self,strain,contig,position):
		try:
			return self.data[contig][position][strain]
		except:
			return None
					
					
				
					