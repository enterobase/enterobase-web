import sys, numpy as np

def _reverse(graph):
    r = {}
    for src in graph:
        for (dst,c) in graph[src].items():
            if dst in r:
                r[dst][src] = c
            else:
                r[dst] = { src : c }
    return r

def _getCycle(n,rg,visited,cycle):
    visited.add(n)
    cycle += [n]
    if n not in rg:
        return []
    e = rg[n].keys()[0]
    if e not in visited:
        cycle = _getCycle(e,rg,visited,cycle)
    elif e in cycle :
        return cycle[cycle.index(e):]
    else :
        return []
    return cycle

def _mergeCycles(cycle,RG,g,rg, restriction):
    to_modify = ['', '', '', sys.maxint]
    for dst in cycle :
        in_s, in_c = rg[dst].items()[0]
        for out_s, out_c in iter(RG[dst].items()) :
            if bool(set(restriction[out_s]) & set(cycle)) :
                continue
            if out_s not in cycle :
                mod_c = out_c - in_c
                if mod_c < to_modify[3] :
                    to_modify = [dst, in_s, out_s, mod_c]
    dst, in_s, out_s = to_modify[:3]
    restriction[in_s][dst] = 1
    g[in_s].pop(dst)
    rg[dst].pop(in_s)
    RG[dst].pop(in_s)
    g[out_s][dst] = RG[dst][out_s]
    rg[dst][out_s] = RG[dst][out_s]
    
# --------------------------------------------------------------------------------- #

def mst(root,G):

    RG = _reverse(G)
    if root in RG:
        RG[root] = {}
    g = {k:{} for k in G}
    for s in RG:
        if len(RG[s]) == 0:
            continue
        d, c = min(iter(RG[s].items()), key=lambda x:x[1])
        if d in g:
            g[d][s] = RG[s][d]
        else:
            g[d] = { s : RG[s][d] }

    rg = _reverse(g)
    
    restriction = {k:{} for k in RG}
    cycles = [1]
    while len(cycles) > 0 :
        cycles = []
        visited = set()
        for n in rg:
            if n not in visited:
                cycle = _getCycle(n,rg,visited, [])
                if len(cycle) > 0 :
                    cycles.append(cycle)
        
        for cycle in cycles:
            _mergeCycles(cycle, RG, g, rg, restriction)

    return g

# --------------------------------------------------------------------------------- #

def load_matrix(string) :
    if sys.version_info[0] < 3:
        from StringIO import StringIO
    else:
        from io import StringIO
    sin = StringIO(string)
    profiles = np.loadtxt(sin, dtype=str)
    prof_names = profiles.T[0].copy()
    profiles.T[0] = range(1, profiles.shape[0]+1)
    profiles = profiles.astype(int)
    
    profiles = profiles[np.lexsort(profiles.T)]
    unique = np.ones(profiles.shape[0], dtype=bool)
    unique[1:] = (np.diff(profiles[:, 1:], axis=0) > 0).any(axis=1)
    nested = {}
    for id, st_id in enumerate(profiles.T[0]) :
        if unique[id] :
            group_id = st_id
        else :
            nested[st_id] = group_id
    profiles = profiles[unique]
    profiles = profiles[np.argsort(np.sum(profiles[:, 1:] > 0, axis=1))]
    
    distances = {}
    for id, profile in enumerate(profiles) :
        st_id = profile[0]
        presence = np.where(profile[1:] > 0)[0] + 1
        diffs = np.sum((profiles != profile)[:, presence], axis=1)
        if np.sum(diffs[id+1:] == 0) > 0 :
            to_merge = np.where(diffs[id+1:] == 0)[0][0] + id + 1
            nested[profile[0]] = profiles[to_merge][0]
        else :
            distances[profile[0]] = dict(zip(profiles.T[0], diffs))

    groups = {}
    for id, gid in iter(nested.items()):
        while gid in nested :
            gid = nested[gid]
        if gid not in groups :
            groups[gid] = [id]
        else :
            groups[gid].append(id)

    dist = {}
    for dst, conn in iter(distances.items()) :
        for src, length in iter(conn.items()):
            if src not in nested and src != dst :
                if src not in dist :
                    dist[src] = {dst:length}
                else :
                    dist[src][dst] = length

    max_conn = 0
    for src, conn in iter(dist.items()) :
        conn_weight = sum(conn.values())
        if conn_weight > max_conn :
            max_conn = conn_weight
    
    for src, conn in iter(dist.items()) :
        conn_weight = sum(conn.values())
        for dst in conn :
            conn[dst] += conn_weight/10.0/max_conn
    
  
    return prof_names, profiles, dist, groups, profiles[-1][0]
    

data = ''

    
def get_links_from_profiles(profiles):
    string_profiles=[]
    
    for profile in profiles:
       
        string_profiles.append("\t".join(map(str,profile)))
    
    string = "\n".join(string_profiles)
    #g, groups, root = load_matrix("\n".join(string_profiles))
    prof_names, profiles, g, groups, root = load_matrix(string)
    id_lookup = {i2:i1 for i1, i2 in enumerate(profiles.T[0])}
    h = mst(int(root),g)
    links = []
    node_map ={}
  
    count=0
    for s in h:
        source_arr = [prof_names[x-1] for x in [s] + groups.get(s, [])]
        source_id = int(source_arr[0])
            
        node_map[source_id]= map(int,source_arr)
        source = ','.join([prof_names[x-1] for x in [s] + groups.get(s, [])])
        
       
       
        for t in h[s]:
            diff = np.sum((profiles[id_lookup[s]] != profiles[id_lookup[t]])
                          [(profiles[id_lookup[s]] > 0) & (profiles[id_lookup[t]] > 0)]) - 1
            target_arr =[prof_names[x-1] for x in [t] + groups.get(t, [])]            
            target_id = target_arr[0]
            node_map[target_id]= map(int,target_arr)
            links.append({"source":source_id,"target":target_id,"distances":diff})
   
    return {"links":links,"mapping":node_map}            
      
    
if __name__ == "__main__":
    prof_names, profiles, g, groups, root = load_matrix(open(sys.argv[1]).read())
    # prof_names, profiles, g, groups, root = load_matrix('1\t1\t1\t1\n2\t1\t0\t2\n3\t2\t1\t1\n4\t1\t1\t1\n')
    
    id_lookup = {i2:i1 for i1, i2 in enumerate(profiles.T[0])}

    h = mst(int(root),g)
    for s in h:
        source = ','.join([prof_names[x-1] for x in [s] + groups.get(s, [])])
        for t in h[s]:
            diff = np.sum((profiles[id_lookup[s]] != profiles[id_lookup[t]])[(profiles[id_lookup[s]] > 0) & (profiles[id_lookup[t]] > 0)]) - 1
            target = ','.join([prof_names[x-1] for x in [t] + groups.get(t, [])])
            print ("({0}) - ({1}) : {2}".format(source, target, diff))
