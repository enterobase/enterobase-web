
from entero.jobs.jobs import LocalJob,AssemblyBasedJob,network_job_classes,RemoteJob
from entero import celery,app,dbhandle
from random import randint
import sys,os,time
import requests

class UGSGenerator(LocalJob,AssemblyBasedJob):

	def __init__(self,**kwargs):
		super(UGSGenerator,self).__init__(**kwargs)
		#if celery is turned off in the config, this job can still use celerywith the following
		self.use_celery=True

	def process_job(self):
		success=True
		#read the genome file
		try:
			with open(self.assembly_filepointer) as f:
				for line in f:
					pass
			#also have access to the assembly barcode,params and scheme_id
			taxon = self.params.get("taxon")
			barcode = self.assembly_barcode
			scheme_id = self.scheme_id
			#generate the ugs (don't tell anyone its just a random number)
			ugs = randint(1,10000)
			#going to store in lookup table
			lookup=self.get_lookup()
			lookup.other_data['results']['genome_stat']=ugs
			
		except Exception as e:
			#write a message to the log - the stack trace will also be included
			app.logger.exception("Problem with ugs_generator job,assembly barcode=%s" % self.assembly_barcode)
			success=False
		self.job_processed(success)

#needed so that Enteobase knowa which class to create based on pipeline
network_job_classes['ugs_generator']=getattr(sys.modules[__name__],"UGSGenerator")



class RemoteUGSJob(RemoteJob,AssemblyBasedJob):
	def __init__(self,**kwargs):
			super(RemoteUGSJob,self).__init__(**kwargs)
			#has just been created to send
			if  self.results:
				self.assembly_barcode=self.results['assembly_barcode']
				self.scheme_id=self.results['scheme_id']
				self.database=self.results['database']
				
	def send_job(self):
		success=True
		try:
			# WVN 28/2/18 Attempt at fix to get new test to pass
			# WVN 1/3/18 Zhemin says he does not know what this is; so I have to assume
			# he did not develop the pipeline.  Nabil thinks this was a pipeline in
			# development by Martin for certain jobs (like JBrowse)

			URI = app.config['SERVER_BASE_ADDRESS'] + "ugs_pipeline"
			# URI = app.config['CROBOT_URI']+"/ugs_pipeline"
			data={
				#data to send (the remote sever needs access to where the assemblied are stored)
				"assembly_filepointer":self.assembly_filepointer,
				#or send whole sequence (not recomended)
				#"sequence" :open(self.assembly_filepointer).read(),
				"taxon":self.params['taxon'],
				#the address the remote server will send the results back to
				"callback":"https://"+app.config['CALLBACK_ADDRESS']+"/website_callback_debug",
				#all required to be sent back
				"database":self.database,
				"scheme_id":self.scheme_id,
				"assembly_barcode":self.assembly_barcode
				}
			resp = requests.post(url = URI, data = data)
			if resp.text != 'OK':
				raise Exception("UGS job did not send -response %s" % resp.text)
		except Exception as e:
			app.logger.exception("UGS job could not be sent for assembly % s" % self.assembly_barcode)
			success=False
		# WVN 1/5/18 Now returns value for testing purpose
		return self.job_sent(success)

	def process_job(self):
		success=True
		#read the genome file
		version= 'ND'
		try:
			version=self.results['version']
			lookup=self.get_lookup()
			lookup.other_data['results']['genome_stat']=self.results['genome_stat']

		except Exception as e:
			#write a message to the log - the stack trace will also be included
			app.logger.exception("Problem with ugs_generator job,assembly barcode=%s" % self.assembly_barcode)
			success=False
		self.job_processed(success,version)
	
#needed so that Enteobase knows which class to create based on pipeline
network_job_classes['uni_gen_stat']=getattr(sys.modules[__name__],"UGSGenerator")






