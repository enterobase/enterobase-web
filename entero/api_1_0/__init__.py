from flask import Blueprint
from . import decorators, errors, upload, jobs, core
from flask_restful import Api, Resource
from entero.databases.system.models import User, UserJobs, UserPermissionTags
from entero import dbhandle
from sqlalchemy import not_

api_bp = Blueprint('api', __name__)
api = Api(api_bp, catch_all_404s=True)


class TodoNext(Resource):
    def __init__(self,**kwargs):
        # smart_engine is a black box dependency
        self.smart_engine = kwargs['smart_engine']

    def get(self):
        return self.smart_engine.next_todo()


api.add_resource(upload.UploadFieldsAPI, 'uploadfields', endpoint='uploadfields')
api.add_resource(upload.GetUploadFieldsAPI, 'uploadfields/<string:dbname>', endpoint='getuploadfields')
api.add_resource(upload.StrainMetaAPI, 'strains/<string:dbname>', endpoint='strains')
api.add_resource(upload.SchemesAPI,'schemes/<string:database>/<string:scheme>',endpoint='schemes')
api.add_resource(jobs.AssembleAPI,'assemble/<string:dbname>',endpoint='assemble')
api.add_resource(upload.S3UploadAPI,'s3upload',endpoint='s3upload')
api.add_resource(jobs.NservAPI,'call_nserv/<string:dbname>',endpoint='call_nserv')


for key in dbhandle.keys():

    # Raw metadata
    api.add_resource(core.AbstractAPI,key+'/strains',endpoint='pubstrains.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.Strains, table_name= 'strains'))
    api.add_resource(core.AbstractAPI,key+'/assemblies',endpoint='assemblies.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.Assemblies, table_name= 'assemblies'))
    api.add_resource(core.AbstractAPI,key+'/traces',endpoint='traces.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.Traces, table_name= 'traces'))

    # Lookup resources
    api.add_resource(core.LookupAPI,key+'/assemblytraces',endpoint='assemblytraces.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.TracesAssembly, table_name= 'trace_assembly'))

    # Linked to NServ
    api.add_resource(core.AbstractAPI,key+'/schemes',endpoint='pubschemes.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.Schemes, table_name= 'schemes'))
    api.add_resource(core.AbstractAPI,key+'/assemblysts',endpoint='assemblysts.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.AssemblyLookup, table_name= 'assembly_lookup'))

    # Strain ST API
    api.add_resource(core.StrainSTAPI,key+'/straindata',endpoint='straindata.%s' % key, resource_class_args=[key],
                     resource_class_kwargs=dict(table = dbhandle[key].models.Strains, table_name= 'strains'))

    #NServ dependant
    Schemes = dbhandle[key].models.Schemes
    try:
        for scheme in dbhandle[key].session.query(Schemes).filter(Schemes.description != 'assembly_stats').filter(Schemes.param['scheme'].astext.like('%_%')).filter(not_(Schemes.description.like('%CRISP%'))).all():
            api.add_resource(core.NServAPI, '%s/%s/loci' % (key, scheme.description),endpoint='loci.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'loci'))
            api.add_resource(core.NServAPI, '%s/%s/sts' % (key, scheme.description),endpoint='sts.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'STs'))
            api.add_resource(core.NServAPI, '%s/%s/alleles' % (key, scheme.description),endpoint='alleles.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'alleles'))
        for scheme in dbhandle[key].session.query(Schemes).filter(Schemes.description.like('CRISPR')).all():
            api.add_resource(core.NServAPI, '%s/%s/loci' % (key, scheme.description),endpoint='loci.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'block_types'))
            api.add_resource(core.NServAPI, '%s/%s/sts' % (key, scheme.description),endpoint='sts.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'CRISPRs'))
            api.add_resource(core.NServAPI, '%s/%s/alleles' % (key, scheme.description),endpoint='alleles.%s.%s' % (key, str(scheme.description)), resource_class_args=[scheme.param['scheme'].split('_')[0], scheme.param['scheme'].split('_')[1],key],
                             resource_class_kwargs=dict(table_name= 'blocks'))
    except:
        pass

# Global Lookup API
api.add_resource(core.NServLookupAPI, 'lookup', endpoint='nserv.lookup')

api.add_resource(core.AbstractSystemAPI,'userjobs',endpoint='jobs', resource_class_args=('senterica'), resource_class_kwargs=dict(table = UserJobs, table_name= 'jobs'))
api.add_resource(core.AbstractSystemAPI,'users',endpoint='users', resource_class_args=('senterica'), resource_class_kwargs=dict(table = User, table_name= 'users'))
api.add_resource(core.AbstractSystemAPI,'userperms',endpoint='userperms', resource_class_args=('senterica'), resource_class_kwargs=dict(table = UserPermissionTags, table_name= 'user_permission_tags'))
api.add_resource(core.AbstractSystemAPI,'useruploads',endpoint='useruploads', resource_class_args=('senterica'), resource_class_kwargs=dict(table = UserPermissionTags, table_name= 'user_uploads'))

api.add_resource(core.DBAPI,'/',endpoint='dbinfo')
api.add_resource(core.DBinfo,'<string:database>',endpoint='dbinfodb')


