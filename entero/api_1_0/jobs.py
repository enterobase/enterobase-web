from flask_restful import Resource
from flask import request
from flask_login import current_user
from entero import db, app, get_database, rollback_close_system_db_session
from entero.databases.system.models import UserJobs
from sqlalchemy.orm import load_only
from sqlalchemy import or_
import ujson, requests
from entero.jobs.jobs import AssemblyJob,network_job_classes, get_pipeline_job_result
from entero.ExtraFuncs.workspace import get_access_permission
from entero.decorators import active_server

current_jobs = ["SUBMIT","WAIT RESOURCE","QUEUE","RUNNING"]

class AssembleAPI(Resource):
    def put(self,dbname):
        dbase=get_database(dbname)
        if not dbase:
            return "database %s is not found"%dbname, 404
        
        if  not current_user.is_authenticated():
            # need to put in method to add authorization for non-website calls
            return "No authorization",401
        accession = request.form.get('accessions')
        priority =request.form.get('priority')
        if not priority:
            priority=9
            
        if not accession:
            return "accessions required in put request ",400
        accs = accession.split(",")
        
        URI_BASE = app.config['CROBOT_URI']+"/head/set_priority"
        # Adding access permission check
        #allowed_users, permission=current_user.get_edit_meta_permissions(dbname)
        Strains=dbase.models.Strains
        Traces=dbase.models.Traces
        sts=[]
        recs = dbase.session.query(Strains).options(load_only('owner','release_date','id','created')).join(Traces).filter(Traces.accession.in_(accs)).all()
        for x in recs:sts.append(x.__dict__)
        permissions=get_access_permission(dbname, current_user.id,
                                          strains=sts, 
                                          return_all_strains_permissions=True)            
        for rec in recs:
            if permissions.get(rec.id)!=3:                
            #if not rec. in allowed_users and rec[0]!=current_user.id:  
                return "No authorization",401          
             
        for acc in accs:
            acc_names = acc.split(";")
            #check job is not already in queue
            acc_co = ",".join(acc_names)
            try:
                job =db.session.query(UserJobs).filter(UserJobs.accession.like(acc_co+"%"),UserJobs.status.in_(current_jobs)).first()
                #job already running , cannot increase its priority
                if job and job.status == 'RUNNING':
                    continue
                if job:
                    URI = URI_BASE + "?job_tag=%i&priority=-9"%job.id
                    resp = requests.get(url = URI)
                    app.updownload_logger.info("Altering priority:"+URI)
                    data =None
                    try:
                        data = ujson.loads(resp.text)
                    except Exception:
                        app.logger.error("Cannot set job priority: response from crobot:\n%s" % resp.text)
                    if job.user_id==0:
                        job.user_id = current_user.id
                    db.session.add(job)
                    db.session.commit()
                    continue

                ids = get_database(dbname).get_trace_ids_by_accession(acc_names)
                job =AssemblyJob(trace_ids=ids,
                                 database=dbname,
                                 workgroup="public",
                                 priority=priority,
                                 user_id=current_user.id)
                job.send_job()
            except Exception as e:
                rollback_close_system_db_session()
                app.logger.exception("Error in submitted Assemblies. error message: %s" % (str(e)))
                return 'Error in submitted Assemblies',503
        return "Assemblies submitted",201
    
  
class NservAPI(Resource):
    def put(self,dbname):
        if not active_server():
            return "Enterobase temporarily unable to perform scheme jobs due to maintenance", 401
        dbase=get_database(dbname)
        if not dbase:
            return "database %s is not found" % dbname, 404
        if  not current_user.is_authenticated():
            # need to put in method to add authorization for non-website calls
            return "No authorization", 401
        scheme = request.form.get('scheme')
        if not scheme:
            return "accessions required in put request ", 400
        ass_ids = request.form.get('assembly_ids')
        if not ass_ids:
            return "accessions required in put request ", 400
        #In the original code the priority was taken from the API, subsequently the priority was
        # hardcoded to -9
        # priority = request.form.get('priority')
        priority = -9
        Assemblies = dbase.models.Assemblies
        Schemes=dbase.models.Schemes
        ids_list = ass_ids.split(",")
        #  If a large number of jobs are requested by a user then assign a priority that is between the standard
        # user priority (-9) and that used for processing assemblies from NCBI (0)
        if len(ids_list) > 100:
            priority = -1
        barcode = ''
        try:
            ###############################################################################
            #check permission, using centeral permission check
            ###############################################################################    
            Strains=dbase.models.Strains
            strains=dbase.session.query(Strains).options(load_only('owner','release_date','id','created')).filter(Strains.best_assembly.in_(ids_list)).all()            
            sts=[]
            for x in strains:sts.append(x.__dict__)
            permissions=get_access_permission(dbname, current_user.id, strains=sts,
                                              return_all_strains_permissions=True)     
            # if not permission:
            for strain in strains:
                # if not strain[0] in allowed_users and strains[0]!=current_user.id:
                if permissions.get(strain.id) != 3:
                    return "No authorization", 401

            assemblies = dbase.session.query(Assemblies).filter(Assemblies.id.in_(ids_list)).all()

            scheme_obj =dbase.session.query(Schemes).filter(Schemes.description==scheme).one()
            #bit of a hack , but the class key is usually the same as the pipeline
            #although it is possible for it to be different (if a pipeline is serving two purposes)
            key = scheme
            if scheme_obj.param['pipeline']=='nomenclature':
                key='nomenclature'
            scheme_class = network_job_classes[key]
            
            for assembly in assemblies:
                barcode = assembly.barcode
                parent_scheme = scheme_obj.param.get('depends_on_scheme')
                if parent_scheme and get_pipeline_job_result(dbname, assembly.id, parent_scheme) is None:
                    parent_scheme_obj = (dbase.session.query(Schemes).
                                         filter(or_(Schemes.description == parent_scheme,
                                         Schemes.param['scheme'].astext == parent_scheme)).one())
                    parent_key = parent_scheme
                    if parent_scheme_obj.param['pipeline'] == 'nomenclature':
                        parent_key = 'nomenclature'
                    parent_scheme_class = network_job_classes[parent_key]

                    job = parent_scheme_class(database=dbname,
                                       scheme=parent_scheme_obj,
                                       assembly_barcode=assembly.barcode,
                                       assembly_id=assembly.id,
                                       assembly_filepointer=assembly.file_pointer,
                                       user_id=current_user.id,
                                       priority=priority,
                                       workgroup="user_upload")
                else:
                    job = scheme_class(database=dbname,
                                    scheme =scheme_obj,
                                    assembly_barcode =assembly.barcode,
                                    assembly_id=assembly.id,
                                    assembly_filepointer=assembly.file_pointer,
                                    user_id=current_user.id,
                                    priority=priority,
                                    workgroup="user_upload")
                job.send_job()
            dbase.session.close()
        except Exception as e:
            dbase.rollback_close_session()
            app.logger.exception("****Error in sending %s job for assembly %s in %s database, error message: %s" %
                                 (scheme, barcode, dbname, str(e)))
            return "Error in sending %s job" % (scheme), 400
        return "Nomen submitted", 201
    
