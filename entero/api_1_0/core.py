from flask import jsonify
from entero import app, get_database
from entero.databases.system.models import User, DatabaseInfo
from flask_restful import Resource, reqparse, inputs
from flask import request, url_for
from entero import dbhandle, db
import json
import requests
from six import text_type

from sqlalchemy import or_

''' Helper methods'''
'''
def check_api_auth(request, database=None):
    if request.headers.has_key('Authorization'):
        is_authorized=False
        data = User.decode_api_token(request.authorization.username)
        if data is not None:
            #adding this check to be sure that the api_aceess is not revoked
            user=db.session.query(User).filter(User.username==data['username']).first()     
            if user is not None and not database:
                is_authorized=True
            elif user is not None and database != None:
                results=db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id==user.id).all()
                for res in results:
                    if res.species==database and  str(res.field)=='api_access':                    
                        is_authorized=True
                        break                       
        if data is not  None and is_authorized:         
        #if data is not None: 
            if data.has_key('api_access_%s' %database) or database == None \
               or data.get('administrator') == 1:
                if ('rMLST' in request.url or request.url.startswith('user')) and data.get('administrator') != 1:
                    return  'You do not have access to these data. Do not contact Nabil'
                return data
            else:
                return 'You are not authorised to access this database, please request access from us <enterobase@dsmz.de>.'
        return 'Invalid token, please confirm your token from https://enterobase.dsmz.de/'
    else:
        return 'Please authenticate all requests using Basic-Auth and a valid token. Register at https://enterobase.dsmz.de/ and request an API Token from us <enterobase@dsmz.de>'
'''    
def bad_response(message,status_code = 401):
    response = jsonify({ 'code': status_code, 'message' : message})
    response.status_code = status_code
    return response

'''Top level API'''
class DBAPI(Resource):
    def __init__(self):
        links = ['']
        
    def format_output(self, res):
        out = res.as_dict()
        out.pop('id')
        out.pop('val')
        links = {}
        links['current'] =url_for('api.dbinfo', _external=True)
        links['strains'] =url_for('api.pubstrains.%s' % out['name'], _external=True)
        links['schemes'] =url_for('api.pubschemes.%s' % out['name'], _external=True)
        links['assemblies'] =url_for('api.assemblies.%s' % out['name'], _external=True)
        links['jobs'] =url_for('api.jobs', _external=True)
        links['users'] =url_for('api.users', _external=True)
        links['assemblyst']=url_for('api.assemblysts.%s' % out['name'], _external=True)
        links['straindata']=url_for('api.straindata.%s' % out['name'], _external=True)
        links['traces']=url_for('api.traces.%s' % out['name'], _external=True)
        links['useruploads']=url_for('api.useruploads', _external=True)
        links['userperms']=url_for('api.userperms', _external=True)
        out['links'] = links
        out['tables'] = json.loads(res.val)
        return out 
    
    def get(self):
        auth = User.check_api_auth(request)
        if isinstance(auth,dict):
            dat = [] 
            for res in DatabaseInfo.query.all():
                if auth.has_key('api_access_%s' %res.name) or auth.get('administrator') == 1 :
                    dat.append(self.format_output(res))
            if len(dat) < 1:
                return bad_response('You are not authorised to access these databases, please request access from us '+ app.config['ENTERO_MAIL_ADDRESS'] +'.')
            return dat
        
        else:
            return bad_response(auth)

class DBinfo(DBAPI):
    
    def get(self, database):
        auth = User.check_api_auth(request,database)
        if isinstance(auth,dict):
            dat = []
            try:
                return self.format_output(DatabaseInfo.query.filter(DatabaseInfo.name==database).one())
            except Exception:
                return bad_response('No such database: %s' %database, 404)
        else:
            return bad_response(auth)
    

class LookupAPI(Resource):
    
    def __init__(self, *args, **kwargs):
        self.table = kwargs['table']
        self.table_name = kwargs['table_name']
        self.database_name = args[0]
        self.database = dbhandle[args[0]]
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('offset', type=int, default=0, \
                            help='Query offset ')
        self.getparser.add_argument('limit', type=int, default=200, \
                            help='records per request')
        self.getparser.add_argument('orderby', default=None, \
                            help='order by field', choices =list(self.table.c.keys()))
        self.getparser.add_argument('sortorder', default='asc', \
                            help='Sort order, asc / desc', choices=['asc', 'desc'])
        for column in self.table.c.keys():
            self.getparser.add_argument(column, action='append', type=str, \
                                help='Invalid %s' % column)

    
    def get_data_keys(self, args):
        args.pop('limit')
        args.pop('offset')
        args.pop('orderby')
        args.pop('sortorder')
        return args

    def get_output_template(self, limit, offset, total_records, records):
        links = {}
        out = {}
        out['total_records'] =  total_records
        out['records'] = records
        import re
        links['current'] = request.url
        # Strip old offset and limit settings.
        links['previous'] = re.sub('\Slimit=\d+', '', request.url)
        links['previous'] = re.sub('\Soffset=\d+', '', links['previous'])
        links['next'] = re.sub('\Slimit=\d+', '', request.url)
        links['next'] = re.sub('\Soffset=\d+', '' , links['next'])
        join_char = '?'
        if re.search('\?\w+', links['previous']):
            join_char = '&'
        # Rewrite offset and limit settings.
        links['previous'] += '%soffset=%s&limit=%s' % (join_char, str(offset), str(limit))
        links['next'] += '%soffset=%s&limit=%s' % (join_char, str(offset+limit), str(limit))
        # Remove next link if required
        if (offset + limit) >= total_records:
            links.pop('next')
        out['paging'] = links
        return out
    
    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])
        if row.has_key('collection_time'): row['collection_time'] =   str(row['collection_time'])
        if row.has_key('release_date'): row['release_date'] =   str(row['release_date'])
        if row.has_key('best_assembly'):
            row['best_assembly_link'] = url_for('api.assemblies.%s' % self.database_name,  _external=True) + '?id=%s' % row['best_assembly']
        if row.has_key('assembly_id'):
            row['assembly_link'] = url_for('api.assemblies.%s' % self.database_name,  _external=True) + '?id=%s' % row['assembly_id']
        if row.has_key('trace_id'):
            row['trace_link'] = url_for('api.traces.%s' % self.database_name,  _external=True) + '?id=%s' % row['trace_id']    
        if row.has_key('file_pointer') and row.get('file_pointer') != None:
            row['file_link'] = url_for('upload.download_assemblies',  _external=True) + '?assembly_id=%s' % row['id'] + '&database=%s' % self.database_name
        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
        if row.has_key('lastmodifiedby') and row.get('lastmodifiedby') != None:
            row['lastmodifiedby_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['lastmodifiedby']
        row.pop('index_id', None)
        row.pop('_labels', None)
        return row
    
    def get(self):
        auth = User.check_api_auth(request,self.database_name)
        if isinstance(auth,dict):
            args = self.getparser.parse_args() 
            limit = args['limit']
            offset = args['offset']
            if limit< 0 or offset < 0 :
                return bad_response('Limit and offset must be positive integers', 400)
            dat = []
            try:
                data_keys = self.get_data_keys(args.copy())
                clause_list = []
                for key in data_keys:
                    if data_keys[key]:
                        col_list = []
                        for val in data_keys[key]:
                            for item in val.split(','):
                                col_list.append(item)
                        clauses = or_( * [getattr(self.table.c, key) == x for x in col_list])
                        clause_list.append(clauses)
                if len(clause_list) > 0:
                    query =  self.database.session.query(self.table).filter(clause_list[0])
                    for clause in clause_list[1:]:
                        query =  query.filter(clause)
                else:
                    query =  self.database.session.query(self.table)
                if args['orderby']:
                    query = query.order_by('%s %s' %(args['orderby'], args['sortorder']))
                results = query.limit(limit).offset(offset)
                total_records = query.count()
                for res in results:
                    dat.append(self.clean_row(res.__dict__))
                out = self.get_output_template(limit, offset, total_records, len(dat))
                out[self.table_name] = dat
                return out
            except Exception as e:
                app.logger.exception("get in LookupAPI class failed, error message: %s"%str(e))
                self.database.rollback_close_session()
                return bad_response("Error", 503)
        else:
            return bad_response(auth)
            
            
class AbstractAPI(Resource):        
    
    def __init__(self, *args, **kwargs):
        self.table = kwargs['table']
        self.table_name = kwargs['table_name']
        self.database_name = args[0]
        self.database = get_database(args[0])
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('offset', type=int, default=0, \
                            help='Query offset ')
        self.getparser.add_argument('limit', type=int, default=100, \
                            help='records per request')
        self.getparser.add_argument('orderby', default='id', \
                            help='order by field', choices =list(self.table.__table__.c.keys()))
        self.getparser.add_argument('sortorder', default='asc', \
                            help='Sort order, asc / desc', choices=['asc', 'desc'])
        #DATA = self.database.models.DataParam
        #self.data_param = {}
        #self.bionumerics = {}
        #for res in self.database.session.query(DATA).filter(DATA.tabname=='strains').all():
            #self.data_param['%s.%s' %(self.table_name, res.name)] = res.as_dict()
        #for res in self.database.session.query(DATA).filter(DATA.tabname=='bionumerics').all():
            #self.bionumerics[res.name] = res.as_dict()
        for column in self.table.__table__.c.keys():
            self.getparser.add_argument(column, action='append', type=str, \
                                help='Invalid %s' % column)
    
    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])
        if row.has_key('collection_time'): row['collection_time'] =   str(row['collection_time'])
        if row.has_key('release_date'): row['release_date'] =   str(row['release_date'])
        if self.table_name == 'schemes' and row['description'] != 'assembly_stats'  and row['description'] != 'CRISPOL':
            data = {}
            data['alleles'] = url_for('api.alleles.%s.%s' % (self.database_name, row['description']),  _external=True) 
            data['loci'] = url_for('api.loci.%s.%s' % (self.database_name, row['description']),  _external=True) 
            data['STs'] = url_for('api.sts.%s.%s' % (self.database_name, row['description']),  _external=True) 
            row['data_link'] = data
        if row.has_key('best_assembly'):
            row['best_assembly_link'] = url_for('api.assemblies.%s' % self.database_name,  _external=True) + '?id=%s' % row['best_assembly']
        if row.has_key('assembly_id'):
            row['assembly_link'] = url_for('api.assemblies.%s' % self.database_name,  _external=True) + '?id=%s' % row['assembly_id']
        if row.has_key('file_pointer') and row.get('file_pointer') != None:
            row['file_link'] = url_for('upload.download_assemblies',  _external=True) + '?assembly_id=%s' % row['id'] + '&database=%s' % self.database_name
        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
        if row.has_key('lastmodifiedby') and row.get('lastmodifiedby') != None:
            row['lastmodifiedby_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['lastmodifiedby']        
        row.pop('index_id', None)
        if row.has_key('scheme_id'):
            row['scheme_link'] = url_for('api.pubschemes.%s' % self.database_name,  _external=True) + '?id=%s' % row['scheme_id']
        if row.has_key('st_barcode') and row.get('st_barcode') != None:
            row['st_barcode_link'] = url_for('api.nserv.lookup',  _external=True) + '?barcode=%s' % row['st_barcode']
        if row.get('barcode') != None:
            if row.get('barcode').endswith('_SC') \
               or row.get('barcode').endswith('_ST') \
               or row.get('barcode').endswith('_AL') \
               or row.get('barcode').endswith('_LO'):
                row['barcode_link'] = url_for('api.nserv.lookup',  _external=True) + '?barcode=%s' % row['barcode']
            elif row.get('barcode','null').endswith('_AS'):
                row['traces_link'] = url_for('api.assemblytraces.%s' % self.database_name, _external=True) + '?assembly_id=%s' % row['id']
            elif row.get('barcode','').endswith('_TR'):
                row['assembly_link'] = url_for('api.assemblytraces.%s' % self.database_name, _external=True) + '?trace_id=%s' % row['id']            
        #elif not native and not bionumerics:
            #for row_key in row.keys():
                #row_native =  '%s.%s' %(self.table_name, row_key)
                #if self.data_param.has_key(row_native):
                    #row[self.data_param[row_native]['label']] = row[row_key]
                    #row.pop(row_key)
        return row

    def format_link(self, query, offset, limit):
        query['limit'] = limit
        query['offset'] = offset
        query_string = ''
        for key in query.keys():
            query_string += '&%s=%s'  % (str(key), str(query[key]))
        return str('?' + query_string[1:])


    def get_output_template(self, limit, offset, total_records, records):
        links = {}
        out = {}
        out['total_records'] =  total_records
        out['records'] = records
        import re
        links['current'] = request.url
        # Strip old offset and limit settings.
        links['previous'] = re.sub('\Slimit=\d+', '', request.url)
        links['previous'] = re.sub('\Soffset=\d+', '', links['previous'])
        links['next'] = re.sub('\Slimit=\d+', '', request.url)
        links['next'] = re.sub('\Soffset=\d+', '' , links['next'])
        join_char = '?'
        if re.search('\?\w+', links['previous']):
            join_char = '&'
        # Rewrite offset and limit settings.
        links['previous'] += '%soffset=%s&limit=%s' % (join_char, str(offset), str(limit))
        links['next'] += '%soffset=%s&limit=%s' % (join_char, str(offset+limit), str(limit))
        # Remove next link if required
        if (offset + limit) >= total_records:
            links.pop('next')
        out['paging'] = links
        return out
    
    def get_data_keys(self, args):
        args.pop('limit')
        args.pop('offset')
        args.pop('orderby')
        args.pop('sortorder')
        return args
        
    def get(self):
        auth = User.check_api_auth(request, self.database_name)
        if isinstance(auth,dict):
            try: 
                args = self.getparser.parse_args() 
                limit = args['limit']
                offset = args['offset']
                if limit< 0 or offset < 0 :
                    return bad_response('Limit and offset must be positive integers', 400)
                dat = []
                data_keys = self.get_data_keys(args.copy())
                clause_list = [] 
                for key in data_keys:
                    if data_keys[key]:
                        col_list = []
                        for val in data_keys[key]:
                            for item in val.split(','):
                                col_list.append(item)
                        clauses = or_( * [getattr(self.table, key) == x for x in col_list])
                        clause_list.append(clauses)
                if len(clause_list) > 0:
                    query =  self.database.session.query(self.table).filter(clause_list[0])
                    for clause in clause_list[1:]:
                        query =  query.filter(clause)
                else:
                    query =  self.database.session.query(self.table)
                results = query.limit(limit).offset(offset)
                total_records = query.count()
                for res in results:
                    dat.append(self.clean_row(res.as_dict()))
                out = self.get_output_template(limit, offset, total_records, len(dat))
                out[self.table_name] = dat
                return out
            except Exception:
                self.database.session.rollback()
                self.database.session.close()
                app.logger.exception("Error in Abstract API v1 ")                
                return bad_response('Please upgrade to API v2.0')
        else:
            return bad_response(auth)
        
class AbstractSystemAPI(Resource):
    
    def __init__(self, *args, **kwargs):
        
        self.table = kwargs['table']
        self.table_name = kwargs['table_name']        
        self.database_name = 'system'
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('offset', type=int, default=0, \
                            help='Query offset ')
        self.getparser.add_argument('limit', type=int, default=200, \
                            help='records per request')
        self.getparser.add_argument('orderby', default='id', \
                            help='order by field', choices =list(self.table.__table__.c.keys()))
        self.getparser.add_argument('sortorder', default='asc', \
                            help='Sort order, asc / desc', choices=['asc', 'desc'])
        for column in self.table.__table__.c.keys():
            self.getparser.add_argument(column, action='append', type=str, \
                                help='Invalid %s' % column)
    
    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])

        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
        row.pop('index_id', None)
        return row

    def format_link(self, query, offset, limit):
        query['limit'] = limit
        query['offset'] = offset
        query_string = ''
        for key in query.keys():
            query_string += '&%s=%s'  % (str(key), str(query[key]))
        return str('?' + query_string[1:])


    def get_output_template(self, limit, offset, total_records, records):
        links = {}
        out = {}
        out['total_records'] =  total_records
        out['records'] = records
        import re
        links['current'] = request.url
        # Strip old offset and limit settings.
        links['previous'] = re.sub('\Slimit=\d+', '', request.url)
        links['previous'] = re.sub('\Soffset=\d+', '', links['previous'])
        links['next'] = re.sub('\Slimit=\d+', '', request.url)
        links['next'] = re.sub('\Soffset=\d+', '' , links['next'])
        join_char = '?'
        if re.search('\?\w+', links['previous']):
            join_char = '&'
        # Rewrite offset and limit settings.
        links['previous'] += '%soffset=%s&limit=%s' % (join_char, str(offset), str(limit))
        links['next'] += '%soffset=%s&limit=%s' % (join_char, str(offset+limit), str(limit))
        # Remove next link if required
        if (offset + limit) >= total_records:
            links.pop('next')
        out['paging'] = links
        return out
    
    def get_data_keys(self, args):
        args.pop('limit')
        args.pop('offset')
        args.pop('orderby')
        args.pop('sortorder')
        return args
        
    def get(self):
        auth = User.check_api_auth(request, self.database_name)
        if isinstance(auth,dict):
            args = self.getparser.parse_args() 
            limit = args['limit']
            offset = args['offset']
            if limit< 0 or offset < 0 :
                return bad_response('Limit and offset must be positive integers', 400)
            dat = []
            data_keys = self.get_data_keys(args.copy())
            clause_list = [] 
            for key in data_keys:
                if data_keys[key]:
                    col_list = []
                    for val in data_keys[key]:
                        for item in val.split(','):
                            col_list.append(item)
                    clauses = or_( * [getattr(self.table, key) == x for x in col_list])
                    clause_list.append(clauses)
            if len(clause_list) > 0:
                query =  db.session.query(self.table).filter(clause_list[0])
                for clause in clause_list[1:]:
                    query =  query.filter(clause)
            else:
                query =  db.session.query(self.table)
            results = query.order_by('%s %s' %(args['orderby'], args['sortorder'])).limit(limit).offset(offset)
            total_records = query.count()
            for res in results:
                dat.append(self.clean_row(res.as_dict()))
            out = self.get_output_template(limit, offset, total_records, len(dat))
            out[self.table_name] = dat
            return out
        else:
            return bad_response(auth)

class StrainSTAPI(AbstractAPI):

    def __init__(self, *args, **kwargs):
        self.table = kwargs['table']
        self.table_name = kwargs['table_name']
        self.database_name = args[0]
        self.database = dbhandle[args[0]]
        DATA = self.database.models.DataParam        
        SCHEMES = self.database.models.Schemes        
        self.bionumerics = {}
        self.scheme_key = {}
        self.api = {}
        self.api_labels = {} 
        self.bionumerics_labels = {}
        for res in self.database.session.query(DATA).filter(DATA.tabname=='api').all():
            self.api[res.name] = res.as_dict()
            self.api_labels[res.label] = res.as_dict()
        for res in self.database.session.query(DATA).filter(DATA.tabname=='bionumerics').all():
            self.bionumerics[res.name] = res.as_dict()
            self.bionumerics_labels[res.label] = res.as_dict()
        for res in self.database.session.query(SCHEMES).all():
            self.scheme_key[res.id] = res.as_dict()
            self.scheme_key[res.description] = res.as_dict()
        self.tables = dict(strains = self.database.models.Strains, assemblies=self.database.models.Assemblies, assembly_lookup=self.database.models.AssemblyLookup)
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('offset', type=int, default=0, \
                            help='Query offset ')
        self.getparser.add_argument('limit', type=int, default=50, \
                            help='records per request')
        self.getparser.add_argument('native', type=inputs.boolean, default=False, \
                            help='Return all internal database fields')
        self.getparser.add_argument('bionumerics', type=inputs.boolean, default=False, \
                            help='Return bionumerics formatted fields')        
        self.getparser.add_argument('sortorder', default='asc', \
                            help='Sort order, asc / desc', choices=['asc', 'desc'])
        choices_list = []
        for table in self.tables:
            for column in self.tables[table].__table__.c.keys():
                self.getparser.add_argument('%s.%s' % (self.tables[table].__table__.name, column), action='append', type=str, \
                                            help='Invalid %s' % column)
                choices_list.append('%s.%s' % (self.tables[table].__table__.name, column))
        for key in (self.bionumerics_labels.keys() + self.api_labels.keys()):
            self.getparser.add_argument(key, action='append', type=str, \
                                        help='Invalid %s' % key)
            choices_list.append(key)            
        self.getparser.add_argument('orderby', default='strains.id', \
                            help='order by field', choices =choices_list)
    def convert_labels(self, data_keys):
        new_keys = {} 
        for key in data_keys:
            if data_keys[key]:
                if key in self.bionumerics_labels:
                    new_keys[self.bionumerics_labels[key]['name']] = data_keys[key]
                elif key in self.api_labels:
                    new_keys[self.api_labels[key]['name']] = data_keys[key]
                else: 
                    new_keys[key] = data_keys[key]
        return new_keys
    
    def get(self):
        auth = User.check_api_auth(request, self.database_name)
        if isinstance(auth,dict):
            args = self.getparser.parse_args() 
            limit = args['limit']
            offset = args['offset']
            native = args['native']
            bionumerics = args['bionumerics']
            if limit< 0 or offset < 0 :
                return bad_response('Limit and offset must be positive integers', 400)
            dat = []
            data_keys = self.get_data_keys(args.copy())
            data_keys = self.convert_labels(data_keys)
            data_keys.pop('native',None)
            data_keys.pop('bionumerics',None)
            clause_list = []
            query =  self.database.session.query(self.tables['strains'], self.tables['assemblies']) 
            select_from_list = []
            for key in data_keys:
                if data_keys[key]:
                    table_name = key.split('.')[0]
                    value_name = key.split('.')[1]
                    col_list = []
                    for val in data_keys[key]:
                        for item in val.split(','):
                            col_list.append(item)
                    clauses = or_( * [getattr(self.tables[table_name], value_name) == x for x in col_list])
                    if table_name == 'assembly_lookup':
                        select_from_list.append(clauses)
                    else:
                        clause_list.append(clauses)
            if len(select_from_list) > 0: 
                query = query.select_from(self.tables['assembly_lookup']).filter(self.tables['assemblies'].id == self.tables['assembly_lookup'].assembly_id)
                for clause in select_from_list:
                    query = query.filter(clause)
            query = query.filter(self.tables['strains'].best_assembly == self.tables['assemblies'].id)
            if not native:
                query = query.filter(self.tables['strains'].id == self.tables['strains'].uberstrain)
            for clause in clause_list:
                query =  query.filter(clause)
            total_records = query.count()
            results = query.order_by('%s %s' %(args['orderby'], args['sortorder'])).limit(limit).offset(offset)
            for res in results:
                all_table_dat = {}
                for table_res in res:
                    all_table_dat.update(self.__clean_table(table_res, native, bionumerics))
                    sts = [] 
                for st in res[1].nserv_sts:
                    fresh_st =  self.__clean_table(st, native, bionumerics)
                    if fresh_st != {}:
                        
                        sts.append(fresh_st)
                all_table_dat['sts'] = sts
                dat.append(all_table_dat)
            out = self.get_output_template(limit, offset, total_records, len(dat))
            out[self.table_name] = dat
            return out
        else:
            return bad_response(auth)
        
    def __clean_table(self, table_res, native, bionumerics):
        table_dict = self.clean_row(table_res.as_dict())
        table_dat = {} 
        for key in table_dict:
            row_native =  '%s.%s' %(table_res.__tablename__, key)            
            if bionumerics:
                if self.bionumerics.has_key(row_native):
                    table_dat[self.bionumerics[row_native]['label']] = table_dict[key]
                    if row_native == 'assembly_lookup.scheme_id':
                        table_dat['scheme_name'] = self.scheme_key[table_dict[key]]['description']
            elif native:
                table_dat['%s.%s' %(table_res.__tablename__, key)] = table_dict[key]
                if row_native == 'assembly_lookup.scheme_id':
                    table_dat['scheme_name'] = self.scheme_key[table_dict[key]]['description']                
            else:
                if self.api.has_key(row_native):
                    table_dat[self.api[row_native]['label']] = table_dict[key]
                    if row_native == 'assembly_lookup.scheme_id':
                        table_dat['scheme_name'] = self.scheme_key[table_dict[key]]['description']                
        return table_dat
        
class NServLookupAPI(Resource):
    def __init__(self):
        
        self.getparser = reqparse.RequestParser()
        self.getparser.add_argument('barcode', required=True, \
                            help='Barcode')
        self.SERVER = app.config['NSERV_ADDRESS']

    
    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])

        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
              #  row.pop('index_id', None)
        return row

    def get_output_template(self, records):
        links = {}
        out = {}
        out['records'] = records
        return out
        
    def get(self):
        
        auth = User.check_api_auth(request)
        if isinstance(auth,dict):
            args = self.getparser.parse_args() 
            dat = []
            data_keys = args.copy()
            clause_list = [] 
            lookup = dict(SAL='senterica',ESC='ecoli',YER='yersinia',MOR='moraxella',LIS='listeria',MIU='miu',
                          MYC='mycobacterium', NEI='neisseria',CLO='clostridium')
            if auth.get('administrator') == 1:
                clean_code = args['barcode'].split(',')
            else:
                clean_code = [] 
                for bar in args['barcode'].split(','):
                    if auth.has_key('api_access_%s' %lookup.get(bar[:3], 'Nabil')):
                        clean_code.append(bar)
            if len(clean_code) < 1: 
                return bad_response('You do not have access to these records')
            get_string = self.SERVER + '/retrieve.api?barcode=%s' % ','.join(clean_code)
            try:
                response= requests.get(get_string, timeout=90)
                dat =  json.loads(response.text)
                new_dat = list(dat)
                for rec in dat:
                    if rec.get('scheme') == 'rMLST' and rec.has_key('allele_id') and auth.get('administrator') != 1 :
                        new_dat.remove(rec)
                out = self.get_output_template(len(new_dat))
                out['results'] = new_dat
                return out                
            except requests.exceptions.ReadTimeout:
                return bad_response('Read timeout, please try again later.', 408)                
            except requests.exceptions.ConnectTimeout:
                return bad_response('Connection timeout, please try again later.', 408)
        else:
            return bad_response(auth)
            
    
        
class NServAPI(Resource):
    
    def __init__(self, *args, **kwargs):
        
        self.table_name = kwargs['table_name']
        self.database_name = args[0]
        self.scheme_name = args[1]
        self.web_database_name = args[2]
        self.SERVER = app.config['NSERV_ADDRESS']
        self.getparser = reqparse.RequestParser()

        if self.table_name == 'loci':
            check_string = self.SERVER + '/search.api/%s/%s/%s' %(self.database_name, self.scheme_name, self.table_name)
        elif self.table_name == 'STs':
            check_string = self.SERVER + '/search.api/%s/%s/%s?ST_id=2' %(self.database_name, self.scheme_name, self.table_name)
        elif self.table_name == 'alleles':
            check_string =  self.SERVER + '/search.api/%s/%s/%s?allele_id=1' %(self.database_name, self.scheme_name, self.table_name)
            self.getparser.add_argument('locus', required=True, help='Please specify a locus name (locus)')
        elif self.table_name == 'block_types' or self.table_name == 'CRISPRs' or  self.table_name == 'blocks' :
            check_string =  self.SERVER + '/search.api/%s/%s/%s?field_id=1' %(self.database_name, self.scheme_name, self.table_name)        
        nserv_db = {} 
        try:
            response= requests.get(check_string,timeout=app.config['NSERV_TIMEOUT'])
            nserv_db = json.loads(response.text)[0]
            if isinstance(nserv_db, text_type):
                app.logger.error('API CALL FAILED. %s' % check_string)
        except ValueError as e:
            app.logger.exception('API CALL FAILED. %s' % check_string)
        self.getparser.add_argument('offset', type=int, default=0, \
                            help='Query offset ')
        self.getparser.add_argument('limit', type=int, default=50, \
                            help='records per request')
        #self.getparser.add_argument('orderby', default='index_id', \
                            #help='order by field', choices =list(nserv_db.keys()))
        self.getparser.add_argument('sortorder', default='asc', \
                            help='Sort order, asc / desc', choices=['asc', 'desc'])
        self.getparser.add_argument('scalar', default='false', \
                            help='Force return of scalar value', choices=['true', 'false'])
        for column in nserv_db.keys():
            self.getparser.add_argument(column, action='append', type=str, \
                                help='Invalid %s' % column)
    
    def clean_row(self, row):
        if row.has_key('lastmodified'): row['lastmodified'] =   str(row['lastmodified'])
        if row.has_key('created'): row['created'] =   str(row['created'])
        if row.has_key('date_sent'): row['date_sent'] =   str(row['date_sent'])
        if row.has_key('date_uploaded'): row['date_uploaded'] =   str(row['date_uploaded'])
        if row.has_key('barcode') and row.get('barcode') != None:
            row['barcode_link'] = url_for('api.nserv.lookup',  _external=True) + '?barcode=%s' % row['barcode']
        if row.has_key('job_id') and row.get('job_id') != None:
            row['job_id_link'] = url_for('api.jobs',  _external=True) + '?id=%s' % row['job_id']
        if row.has_key('user_id') and row.get('user_id') != None:
            row['user_id_link'] = url_for('api.users',  _external=True) + '?id=%s' % row['user_id']
      #  row.pop('index_id', None)
        return row

    def format_link(self, query, offset, limit):
        query['limit'] = limit
        query['offset'] = offset
        query_string = ''
        for key in query.keys():
            query_string += '&%s=%s'  % (str(key), str(query[key]))
        return str('?' + query_string[1:])


    def get_output_template(self, limit, offset, total_records, maxi, records):
        links = {}
        out = {}
        out['total_records'] =  total_records
        out['records'] = records
        import re
        links['current'] = request.url
        # Strip old offset and limit settings.
        links['previous'] = re.sub('\Slimit=\d+', '', request.url)
        links['previous'] = re.sub('\Soffset=\d+', '', links['previous'])
        links['next'] = re.sub('\Slimit=\d+', '', request.url)
        links['next'] = re.sub('\Soffset=\d+', '' , links['next'])
        join_char = '?'
        if re.search('\?\w+', links['previous']):
            join_char = '&'
        # Rewrite offset and limit settings.
        links['previous'] += '%soffset=%s&limit=%s' % (join_char, str(offset), str(limit))
        links['next'] += '%soffset=%s&limit=%s' % (join_char, str(offset+limit), str(limit))
        # Remove next link if required
        if (offset + limit) >= total_records:
            links.pop('next')
        out['paging'] = links
        return out
    
    def get_data_keys(self, args):
        args.pop('limit')
        args.pop('offset')
        args.pop('sortorder')
        return args
        
    def get(self):
        auth = User.check_api_auth(request, self.web_database_name)
        if isinstance(auth,dict):
            args = self.getparser.parse_args() 
            limit = args['limit']
            offset = args['offset']
            scalar = args.pop('scalar', 'false')
            if limit< 0 or offset < 0 :
                return bad_response('Limit and offset must be positive integers', 400)
            dat = []
            data_keys = self.get_data_keys(args.copy())
            clause_list = [] 
            for key in data_keys:
                if data_keys[key]:
                    array = []                    
                    for value in data_keys.get(key):
                        delimited = value.split(',')
                        for delim in delimited:
                            if isinstance(delim, int):
                                clean_value = delim
                            else:
                                clean_value = "'%s'" % delim
                            array.append(clean_value)
                    if len(array) > 0:
                        clause_list.append( ' %s in (%s) ' % (key, ','.join(array) ))
            filter = '' 
            if len(clause_list) > 0:
                filter = ' AND '.join(clause_list)
                filter = ' AND ' + filter 
            iterate = dict(alleles='allele_id',loci='field_id',STs='ST_id')
            #query = '%s >= %d AND accepted %%%% 2=1 ' %(iterate[self.table_name],offset)
            if len(clause_list) > 0:
                query = '%s >= %d ' %('index_id',offset)
            else:
                query = '%s >= %d ' %(iterate[self.table_name],offset)
            sort = 'order by %s limit %d' %(iterate[self.table_name], limit)
            post_string = self.SERVER + '/search.api/%s/%s/%s' % (self.database_name, self.scheme_name, self.table_name)
            
         #  get_string = self.SERVER + '/search.api/%s/%s/%s?filter=%s > %d AND accepted= %% 2=1 AND limit = %d AND %s' % (self.database_name, self.scheme_name, self.table_name, iterate[self.table_name], offset, limit, ' AND '.join(clause_list))
            params = dict(filter = '%s %s %s' %(query, filter , sort))
            if scalar == 'true':
                params['fieldnames'] = 'index_id,type_id,info'
            #params = dict(filter = 'accepted %%%% 2=1 AND %s limit %d' %(' AND '.join(clause_list),limit))
            try:
                response = requests.post(post_string, data=params,timeout=app.config['NSERV_TIMEOUT'])
    # fieldnames = index_id,type_id 

                if response.text == '"list index out of range"':
                    return bad_response('No data', 404)
                else:
                    nserv_db =  json.loads(response.text)
            except Exception as e: 
                return bad_response('No connection to back end database, please try again later. If problem persists, please inform us '+ app.config['ENTERO_MAIL_ADDRESS']+ ' with this error message %s gives %s' %(post_string, response.text), 503)
                
            # http://137.205.52.26//NServ/search.api/Salmonella/UoW/alleles?filter=index_id > 0&accepted=%% 2=1&limit=50
            #response= requests.get(self.SERVER + '/search.api/%s/%s/%s?filter=%s >= %d&accepted=%%%% 2=1&limit=%d' % (self.database_name, self.scheme_name, self.table_name, 'index_id', offset, limit))
            #nserv_db = json.loads(response.text)
            for res in nserv_db:
                dat.append(self.clean_row(res))
            params = dict(filter = '%s %s' %(query, filter), fieldnames='count( %s )' %iterate[self.table_name])
            try: 
                response = requests.post(post_string , data=params,timeout=app.config['NSERV_TIMEOUT'])
                nserv_db =  json.loads(response.text)
            except Exception as e: 
                return bad_response('No connection to back end database, please try again later. If problem persists, please inform us '+ app.config['ENTERO_MAIL_ADDRESS']+ ' with this error message %s gives %s' %(post_string, response.text), 503)
            
            total_records = int(nserv_db[0]['count'])
            #response= requests.get(self.SERVER + '/search.api/%s/%s/%s?behave=max' %(self.database_name, self.scheme_name, self.table_name))
            out = self.get_output_template(limit, offset, total_records, total_records, len(dat))
            out[self.table_name] = dat
            return out
        else:
            return bad_response(auth)
        