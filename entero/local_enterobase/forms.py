# H.F. 20/10/2020
from flask_wtf import FlaskForm as Form
from wtforms import StringField, SubmitField, ValidationError
from wtforms.validators import IPAddress, URL, Length, Regexp
from wtforms.validators import Required
from wtforms.widgets import TextArea
import requests
import re
from flask_login import current_user
from ..databases.system.models import LocalEnterobaseClient, LocalEnterobaseClientRequest


def validate_server_up(form, field):
    """H.F. 20/12/20
    Validator for checking if the URL returns anything

    Args:
        form (Form): wtforms form
        field (Field): wtforms form field

    Raises:
        ValidationError: cannot connect to URL
    """

    try:
        r = requests.get(field.data if field.data.startswith("http://") \
                            or field.data.startswith("https://") else \
                            "https://"+field.data, timeout=3)
    except:
        raise ValidationError("Cannot connect to {}.".format(field.data))


def validate_server_uri(form, field):
    """
    Validator that checks if an input field is a valid URL or IP address.

    Args:
        form (Form): wtforms form
        field (Field): wtforms form field

    Raises:
        ValidationError: returns ValidationError if the field data is not a valid URL or IP address
    """

    url_check = URLv2(require_tld= False)
    ip_check = IPv2()
    
    url_flag = False
    try:
        url_check(form, field)
    except ValidationError:
        url_flag = True
    
    ip_flag = False
    try:
        ip_check(form, field)
    except ValidationError:
        ip_flag = True

    if ip_flag and url_flag:
        raise ValidationError("\"{}\" is not a valid URL or IP Address.".format(field.data))


def validate_integer_field(field_name="worker connections"):
        """
        Validator that checks if a field contains a valid number of worker connections.

        Args:
            form (Form): wtforms form
            field (Field): wtforms form field

        Raises:
            ValidationError: returns ValidationError if the field data is not a valid number of worker connections
        """

        def _validate_integer_field(form, field):
            """
            Validates a integer field i.e. [0-9]+

            Args:
                form (NGINXConfigForm): NGINX config form
                field (StringField): field of form

            Raises:
                ValidationError: not a valid integer
            """
            port_regex = re.compile("^[0-9]+$")
            if not bool(port_regex.match(str(field.data))):
                raise ValidationError("\"{}\" is not a valid {}. This value must be an integer.".format(field.data, field_name))
        
        return _validate_integer_field


def validate_float_field(field_name="field"):
    """
    Validates a float field i.e. [0-9]+.[0-9]+

    Args:
        field_name (str, optional): Name of the field. Defaults to "field".
    """

    def _validate_float_field(form, field):
        """
        Validates a float field i.e. [0-9]+.[0-9]+

        Args:
            form (NGINXConfigForm): NGINX config form
            field (StringField): field of form

        Raises:
            ValidationError: not a valid floating point number
        """
        port_regex = re.compile("^[0-9]+[.]?[0-9]+$")
        if not bool(port_regex.match(str(field.data))):
            raise ValidationError("\"{}\" is not a valid {}. This value must be a number.".format(field.data, field_name))
    
    return _validate_float_field


class NGINXConfigForm(Form):
    """
    NGINX configuration page form.
    """
    server_url_ip = StringField('Web Server URL/IP', validators=[validate_server_uri])
    http_port = StringField('HTTP Port', validators=[validate_integer_field(field_name="\"Port number\"")])
    https_port = StringField('HTTPS Port', validators=[validate_integer_field(field_name="\"Port number\"")])
    worker_connections = StringField('Worker Connections', validators=[validate_integer_field(field_name="number of \"Worker Connections\"")])
    client_max_body_size = StringField('Client Max Body Size (M)', validators=[validate_float_field(field_name="\"Client Max Body Size\" value")])
    keepalive_timeout = StringField('Keep Alive Timeout (s)', validators=[validate_float_field(field_name="\"Keep Alive Timeout\" value")])
    sendfile_max_chunk = StringField('Send File Max Chunk (k)', validators=[validate_float_field(field_name="\"Send File Max Chunk\" value")])
    local_enterobase_uri = StringField('Local Enterobase Server URL/IP', validators=[validate_server_uri])
    local_enterobase_port = StringField('Local Enterobase Server Port', validators=[validate_integer_field(field_name="\"Port number\"")])
    submit = SubmitField('Download nginx.conf')


class URLv2(URL):
    """
    Extension of the wtforms URL validator that enforces an http or https prefix.
    """
    def __init__(self, require_tld=True, message=None):
        regex = (
            r"^https?://"
            r"(?P<host>[^\/\?:]+)"
            r"(?P<port>:[0-9]+)?"
            r"(?P<path>\/.*?)?"
            r"(?P<query>\?.*)?"
            r"(?P<top_level_domain>\.[a-z]+)$"
        )
        super(URL, self).__init__(regex, re.IGNORECASE, message)
        raise Exception ('forms.py line 151 contains reference to undefined HostnameValidation')
#        self.validate_hostname = HostnameValidation(
#            require_tld=require_tld,
#            allow_ip=True,
#        )


class IPv2(IPAddress):
    """
    Extension of the wtforms IP validator that enforces an http or https prefix.
    """
    def __call__(self, form, field):

        # Checks if the IP starts with https, if so it is removed
        value = field.data
        if value.startswith("https://"):
            value = value[8:]
        
        # Checks if the port is valid
        IP_port_split = value.split(":")
        if len(IP_port_split) > 1:
            port_regex = re.compile("^[0-9]+$")
            if not bool(port_regex.match(IP_port_split[1])):
                raise ValidationError("\"{}\" is not a valid port number. This value must be an integer.".format(IP_port_split[1]))
            validate_integer_field(IP_port_split[1])
            value = IP_port_split[0]

        valid = False
        if value:
            valid = (self.ipv4 and self.check_ipv4(value)) or (self.ipv6 and self.check_ipv6(value))

        if not valid:
            message = self.message
            if message is None:
                message = field.gettext('Invalid IP address.')
            raise ValidationError(message)


def user_logged_in(form, field):
    """H.F. 20/12/20
    Check if the user chosen as the admin is logged in as the admin.

    Args:
        form (Form): page form
        field (Field): form field

    Raises:
        ValidationError: User is not logged in as the admin
    """
    if field.data != current_user.username:
        raise ValidationError("You must be logged in as the user who you want to be admin. \
        You have requested {} as the admin, however you are logged in as {}.".format(field.data, current_user.username))


def user_has_already_registered(form, field):
    """H.F. 14/02/20
    Validator for checking if a user has already registered a Local Enterobase Client.

    Args:
        form (Form): page form
        field (Field): form field

    Raises:
        ValidationError: This user is already an administrator to a Local Enterobase instance
        ValidationError: This user has already requested a Local Enterobase instance
    """
    if bool(LocalEnterobaseClient.query.filter_by(admin=current_user).first()):
        raise ValidationError("This user is already an administrator to a Local Enterobase instance.")
    if bool(LocalEnterobaseClientRequest.query.filter_by(admin=current_user).first()):
        raise ValidationError("This user has already requested a Local Enterobase instance.")


def local_enterobase_client_name_exists(form, field):
    """H.F. 20/12/20
    Validator for checking if a Local Enterobase Client exists.

    Args:
        form (Form): page form
        field (Field): form field

    Raises:
        ValidationError: Local Enterobase Client name exists
        ValidationError: Local Enterobase Client request with same name exists
    """
    if bool(LocalEnterobaseClient.query.filter_by(client_name=field.data).first()):
        raise ValidationError("A Local Enterobase with the same client name already exists.")
    if bool(LocalEnterobaseClientRequest.query.filter_by(client_name=field.data).first()):
        raise ValidationError("A Local Enterobase with the same client name has already been requested.")


def local_enterobase_client_url_exists(form, field):
    """H.F. 20/12/20
    Validator for checking if a Local Enterobase Client exists.

    Args:
        form (Form): page form
        field (Field): form field

    Raises:
        ValidationError: Local Enterobase Client URL exists
        ValidationError: Local Enterobase Client request with same URL exists
    """

    if bool(LocalEnterobaseClient.query.filter_by(client_uri=field.data).first()):
        raise ValidationError("A Local Enterobase with the same URL already exists.")
    if bool(LocalEnterobaseClientRequest.query.filter_by(client_uri=field.data).first()):
        raise ValidationError("A Local Enterobase with the same URL has already been requested.")


class RegisterLocalEnterobaseClientForm(Form):
    client_name = StringField('Local Enterobase Name', validators=[Required(), Length(min=1, max=64), local_enterobase_client_name_exists])
    client_uri = StringField('External URL', validators=[Required(), Regexp('^https', 0, "URL must begin with \'https\'."), validate_server_uri, Length(min=1, max=200), local_enterobase_client_url_exists])
    description = StringField('Description & Justification (400 words max)', widget=TextArea(), render_kw={"rows":"3","id":"description", "maxlength":"400"}, validators=[Length(min=0,max=400)])
    username = StringField('Central Enterobase Username of Local Enterobase Admin', validators=[Required(), user_logged_in, user_has_already_registered])
    submit = SubmitField('Continue')
