# H.F. 20/10/2020
# Templating convention: {{<name of variable>:<default value>}}

events {
    worker_connections  {{worker_connections:1024}};
}
  
http {

    sendfile on;
    sendfile_max_chunk {{sendfile_max_chunk:1024}}k;
    tcp_nopush on;
    client_max_body_size {{client_max_body_size:4000}}M;
    keepalive_timeout {{keepalive_timeout:65}}s;
    
    log_format upstreamlog '$server_name to: $upstream_addr [$request] '
	 'upstream_response_time $upstream_response_time '
	 'msec $msec request_time $request_time';

    upstream local_enterobase {
        server {{local_enterobase_uri:}}:{{local_enterobase_port:8000}};
    }

    server {
        # listen on port {{http_port:}} (http)
        listen {{http_port:80}};
        #please change the following using your server uri, e.g. myserver.com
        server_name {{server_url_ip:}};
        #location / {
            # redirect any requests to the same URL but on https
            #please change the following using your server uri, e.g. myserver.com
            return 301 https://{{server_url_ip:}}$request_uri;
    # }
    }

    server {
        # listen on port {{https_port:}} (https)
        listen {{https_port:443}} ssl;
        #please change the following text and set your server uri, e.g. myserver.com
        server_name {{server_url_ip:}};

        # location of the self-signed SSL certificate
        #It is a good idea to use your own signed certificate 
        #and replace these with the actual file names for key and pem files
        #otherwise use the provided ssl certificate and you need to map your home folder with (home/nginx_user)
        #ass illustarted in the command 
        ssl_certificate /home/nginx_user/certs/cert.pem;
        ssl_certificate_key /home/nginx_user/certs/key.pem;

        # write access and error logs to $HOME/logs  
        #Please note that you need to map you home folder with (home/nginx_user/) folder
        #in addition you need to create a folder called logs inside your home folder as illustrated
        #in run nginx command

        access_log /home/nginx_user/logs/local_enteroBase_access.log upstreamlog;
        error_log /home/nginx_user/logs/local_enteroBase_error.log; 

        location / {
            # forward application requests to the gunicorn server
            proxy_pass http://local_enterobase;
            proxy_redirect off;
            proxy_set_header Host $http_host;     
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }
}
