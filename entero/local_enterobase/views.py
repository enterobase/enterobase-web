# H.F. 20/10/2020
from entero import db, app, dbhandle
from entero.entero_email import send_email
from . import local_enterobase
from .forms import NGINXConfigForm, RegisterLocalEnterobaseClientForm, validate_server_uri
from ..decorators import admin_required
from ..databases.system.models import LocalEnterobaseClient, LocalEnterobaseClientRequest, User, \
    LocalEnterobaseClientTestFileResults
from ..auth.views import RegistrationForm

from flask import render_template, send_file, request, flash, redirect, jsonify, url_for, session
from flask_login import login_required, current_user
from .utils import get_json, get_md5, remove_http_s, get_nginx_defaults
import io
import os
import json
from re import sub
from datetime import datetime
from werkzeug.security import gen_salt


@local_enterobase.route('/register', methods=['POST', 'GET'])
@login_required
def local_enterobase_client_registration_request():
    """ H.F 07/12/2020
    View for registering a local enterobase client.

    Returns:
        template : rendered template of register_local_enterobase_client.html (GET) OR redirect to home page (POST)
    """

    user = current_user
    form = RegisterLocalEnterobaseClientForm()
    if request.method == 'GET':
        return render_template('local_enterobase/register.html', form=form)

    local_enterobase_client_request = LocalEnterobaseClientRequest()

    if form.validate_on_submit():
        local_enterobase_client_request.description = form.description.data
        local_enterobase_client_request.client_name = form.client_name.data
        local_enterobase_client_request.client_uri = form.client_uri.data
        local_enterobase_client_request.admin_id = current_user.id
        local_enterobase_client_request.admin = current_user
        local_enterobase_client_request.test_token = gen_salt(24)
        local_enterobase_client_request.test_status = "Pending"

        db.session.add(local_enterobase_client_request)
        db.session.commit()

        send_email(user.email, 'We Have Received Your Local Enterobase Registration Request!', 'oauth/email/request_received_test_token', local_enterobase_client=local_enterobase_client_request, user=user)
        flash('Your Local Enterobase registration request has been received, \
        please use the following token to perform your upload test: {}.'.format(local_enterobase_client_request.test_token))
        
        return redirect(url_for("main.index"))
    return render_template('local_enterobase/register.html', form=form)


@local_enterobase.route('/dashboards', methods=['GET'])
@admin_required
def dashboard_navigator():
    return render_template('local_enterobase/dashboard_navigator.html')


@local_enterobase.route('/dashboards/<local_enterobase_client_name>', methods=['GET', 'POST'])
@admin_required
def local_enterobase_client_dashboard(local_enterobase_client_name):
    """ H.F. 20/12/20
    View for the Central Enterobase administrator to approve/revoke Local Enterobase clients.

    Args:
        local_enterobase_client_name (str): Name of Local Enterobase client
    """
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(client_name=local_enterobase_client_name).first()
    is_request = False # this variable describes which table the Local Enterobase is in (i.e. request table or regular table)
    admins_and_requests = []
    if bool(local_enterobase_client):
        lead_admin = local_enterobase_client.admin
        admins_and_requests = list(LocalEnterobaseClientAdmin.query.filter_by(local_enterobase_client_id=local_enterobase_client.id))
    else:
        local_enterobase_client = LocalEnterobaseClientRequest.query.filter_by(client_name=local_enterobase_client_name).first()
        if bool(local_enterobase_client):
            lead_admin = local_enterobase_client.admin
            is_request = True
        else:
            flash("Local Enterobase client \"{}\" does not exist.".format(local_enterobase_client_name))
            return redirect("/")

    if is_request:
        local_enterobase_client_form = ApproveRegisterLocalEnterobaseClientForm(
            client_name=local_enterobase_client.client_name,
            client_uri=local_enterobase_client.client_uri,
            description=local_enterobase_client.description,
            username=local_enterobase_client.admin.username,
            current_status=local_enterobase_client.current_status,
            test_status=local_enterobase_client.test_status,
            average_upload_time=local_enterobase_client.average_upload_time,
            average_assembly_time=local_enterobase_client.average_assembly_time
            )
    else:
        local_enterobase_client_form = ApproveRegisterLocalEnterobaseClientForm(
            client_name=local_enterobase_client.client_name,
            client_uri=local_enterobase_client.client_uri,
            description=local_enterobase_client.description,
            username=local_enterobase_client.admin.username,
            current_status=local_enterobase_client.current_status)

    lead_admin_form = RegistrationForm(
        email=lead_admin.email,
        username=lead_admin.username,
        firstname= lead_admin.firstname,
        lastname=lead_admin.lastname,
        department = lead_admin.department,
        institution = lead_admin.institution,
        city = lead_admin.city,
        country=lead_admin.country)

    if local_enterobase_client_form.validate_on_submit():
        if local_enterobase_client_form.approve.data:
            if not is_request:
                local_enterobase_client.current_status = "Approved"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been approved.".format(local_enterobase_client_name))
                return redirect("/local_enterobase/dashboards/{}".format(local_enterobase_client_name))
            elif is_request:
                new_lec = LocalEnterobaseClient(
                    client_name=local_enterobase_client.client_name,
                    client_uri=local_enterobase_client.client_uri,
                    description=local_enterobase_client.description,
                    current_status="Approved",
                    admin=lead_admin,
                    admin_id=lead_admin.id
                )
                db.session.add(new_lec)
                new_lec.generate_client_credentials()
                db.session.delete(local_enterobase_client)
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been approved.".format(local_enterobase_client_name))
                return redirect("/local_enterobase/dashboards/{}".format(local_enterobase_client_name))
            else:
                flash("Local Enterobase client \"{}\" does not exist.".format(local_enterobase_client_name))
                return redirect("/")
        if local_enterobase_client_form.revoke.data:
            if not is_request:
                local_enterobase_client.current_status = "Revoked"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been temporarily revoked.".format(local_enterobase_client_name))
                return redirect("/local_enterobase/dashboards/{}".format(local_enterobase_client_name))
        if local_enterobase_client_form.delete.data:
            if not is_request:
                db.session.delete(local_enterobase_client)
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been permanently deleted.".format(local_enterobase_client_name))
                return redirect("/")
            elif is_request:
                db.session.delete(local_enterobase_client)
                db.session.commit()
                flash("Local Enterobase client \"{}\" request has been permanently deleted.".format(local_enterobase_client_name))
                return redirect("/")
        if local_enterobase_client_form.reinstate.data:
            if not is_request:
                lec_to_reinstate = LocalEnterobaseClient.query.filter_by(id=local_enterobase_client.id).first()
                lec_to_reinstate.current_status = "Reinstated"
                db.session.commit()
                flash("Local Enterobase client \"{}\" has been reinstated.".format(local_enterobase_client_name))
                return redirect("/local_enterobase/dashboards/{}".format(local_enterobase_client_name))
        if local_enterobase_client_form.deny.data:
            if is_request:
                local_enterobase_client.current_status = "Denied"
                db.session.commit()
                flash("Local Enterobase client \"{}\" request has been temporarily denied.".format(local_enterobase_client_name))
                return redirect("/local_enterobase/dashboards/{}".format(local_enterobase_client_name))

    return render_template('local_enterobase/dashboard.html', local_enterobase_client_form=local_enterobase_client_form, lead_admin=lead_admin_form, user_admin_forms=admins_and_requests)


#@local_enterobase.route('/admin_dashboards/<local_enterobase_client_name>/<local_enterobase_client_admin_username>', methods=['GET', 'POST'])
#@admin_required
def local_enterobase_client_admin_dashboard(local_enterobase_client_name, local_enterobase_client_admin_username):
    """ H.F. 27/12/20
    This function is no longer used!
    View for the Central Enterobase administrator to approve/revoke Local Enterobase client admins.

    Args:
        local_enterobase_client_name (str): Name of Local Enterobase client
        local_enterobase_client_admin_name (str): Name of Local Enterobase client admin
    """
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(client_name=local_enterobase_client_name).first()
    if not bool(local_enterobase_client):
        flash("Local Enterobase client \"{}\" does not exist.".format(local_enterobase_client_name))
        return redirect("/")
    local_enterobase_client_admin_user = User.query.filter_by(username=local_enterobase_client_admin_username).first()
    if not bool(local_enterobase_client_admin_user):
        flash("User \"{}\" does not exist.".format(local_enterobase_client_admin_username))
        return redirect("/")
    local_enterobase_client_admin = LocalEnterobaseClientAdmin.query.filter_by(user=local_enterobase_client_admin_user).first()
    if not bool(local_enterobase_client_admin):
        flash("Local Enterobase client admin or admin request \"{}\" does not exist for \"{}\".".format(local_enterobase_client_admin_username, local_enterobase_client_name))
        return redirect("/")

    local_enterobase_client_form = ApproveRegisterLocalEnterobaseClientForm(
        client_name=local_enterobase_client.client_name,
        client_uri=local_enterobase_client.client_uri,
        description=local_enterobase_client.description,
        username=local_enterobase_client.admin.username,
        current_status=local_enterobase_client.current_status,
        average_test_upload_time=local_enterobase_client.average_test_upload_time,
        average_test_assembly_time=local_enterobase_client.average_test_assembly_time
    )

    admin_form = ApproveLocalEnterobaseAdminForm(
        email=local_enterobase_client_admin.user.email,
        username=local_enterobase_client_admin.user.username,
        firstname= local_enterobase_client_admin.user.firstname,
        lastname=local_enterobase_client_admin.user.lastname,
        department = local_enterobase_client_admin.user.department,
        institution = local_enterobase_client_admin.user.institution,
        city = local_enterobase_client_admin.user.city,
        country=local_enterobase_client_admin.user.country,
        current_status=local_enterobase_client_admin.current_status
    )

    if admin_form.validate_on_submit():
        if admin_form.approve.data:
            local_enterobase_client_admin.current_status = "Approved"
            db.session.commit()
            flash("Admin \"{}\" has been approved for Local Enterobase client \"{}\".".format(local_enterobase_client_name, local_enterobase_client_admin_username))
            return redirect("/local_enterobase/admin_dashboards/{}/{}".format(local_enterobase_client_name, local_enterobase_client_admin_username))
        if admin_form.deny.data:
            local_enterobase_client_admin.current_status = "Denied/Revoked"
            db.session.commit()
            flash("Admin \"{}\" has been denied/revoked for Local Enterobase client \"{}\".".format(local_enterobase_client_name, local_enterobase_client_admin_username))
            return redirect("/local_enterobase/admin_dashboards/{}/{}".format(local_enterobase_client_name, local_enterobase_client_admin_username))
        if admin_form.delete.data:
            db.session.delete(local_enterobase_client_admin)
            db.session.commit()
            flash("Admin \"{}\" has been permanently for Local Enterobase client \"{}\".".format(local_enterobase_client_name, local_enterobase_client_admin_username))
            return redirect("/")

    return render_template('local_enterobase/admin_dashboard.html', local_enterobase_client_form=local_enterobase_client_form, admin_form=admin_form)


@local_enterobase.route('/get_test_files', methods=['POST'])
def get_upload_test_files():
    """
    Endpoint for getting the location of test files

    Returns:
        json: json object containing the location of the files
    """
    test_token = request.form.get("test_token")
    client_request = LocalEnterobaseClientRequest.query.filter_by(test_token=test_token).first()

    if not bool(client_request):
        return jsonify({"error": "Invalid test token"}), 401

    if client_request.test_already_completed():
        return jsonify({"error": "Test token has already been used"}), 400

    try:
        test_files = get_json(app.config.get("LOCAL_ENTEROBASE_UPLOAD_TEST_FILES"))
    except Exception as e:
        print(e)
        return jsonify({"error": "Error with server, please contact the Warwick Enterobase administrators"}), 501

    return jsonify(test_files), 200


@local_enterobase.route('/start_upload_test_token', methods=['POST'])
def start_upload_test_token():
    """
    Generates a token for uploading a test file and stores the time of the request

    Returns:
        json: json reponse containing the file's test token
    """
    test_token = request.form.get("test_token")
    filename = request.form.get("filename")
    file_test_token = gen_salt(24)

    client_request = LocalEnterobaseClientRequest.query.filter_by(test_token=test_token).first()

    if not bool(client_request):
        return jsonify({"error": "Invalid test token"}), 401

    if client_request.test_already_completed():
        return jsonify({"error": "Test token has already been used"}), 400

    client_request_id = client_request.id

    existing_file = LocalEnterobaseClientTestFileResults.query.filter_by(filename=filename, client_request_id=client_request_id)

    for e in existing_file:
        db.session.delete(e)

    test_file_record = LocalEnterobaseClientTestFileResults(
        client_request_id=client_request_id,
        filename=filename,
        file_test_token=file_test_token,
        start_time=datetime.utcnow(),
        test_type="upload"
    )

    db.session.add(test_file_record)
    db.session.commit()

    return jsonify({
        "file_test_token": file_test_token
    }), 200


@local_enterobase.route('/upload_test_file', methods=['POST'])
def upload_test_file():
    """
    Facilitates the uploading of test files. Calculates the md5 checksum of the uploaded test file against the provided md5 and the expected md5.
    Also records the current time which allows the upload time to be calculated.

    Returns:
        json, status_code: success or failure message
    """
    try:
        test_files_expected_results = get_json(app.config.get("LOCAL_ENTEROBASE_UPLOAD_TEST_FILES_EXPECTED_RESULTS"))
    except Exception:
        return jsonify({"error": "Error with server, please contact the Warwick Enterobase administrators"}), 501

    upload_test_files_expected_results = test_files_expected_results["upload_test_files"]

    fastq = request.files.get("fastq")
    file_test_token = request.form.get("file_test_token")
    filename = request.form.get("filename")
    md5_checksum = request.form.get("md5")
    test_token = request.form.get("test_token")

    if fastq is None:
        return jsonify({"error": "No fastq file has been received"}), 400

    end_time = datetime.utcnow()

    test_file_record = LocalEnterobaseClientTestFileResults.query.filter_by(file_test_token=file_test_token, filename=filename).first()

    if not bool(test_file_record):
        return jsonify({"error": "Invalid file_test_token or filename"}), 401

    calculated_md5 = get_md5(fastq, filename, test_token)

    if filename not in upload_test_files_expected_results:
        return jsonify({"error": "A file with this filename ({}) was not expected, please check the name of the file you uploaded.".format(filename)}), 400

    if md5_checksum==calculated_md5==upload_test_files_expected_results[filename]:
        test_file_record.calculate_and_save_upload_time(end_time)
        db.session.commit()
        return jsonify({"success": "Successful file upload"}), 200
    else:
        print("md5 sent: {}. md5 calculated from file sent: {}. expected md5: {}.".format(md5_checksum, calculated_md5, upload_test_files_expected_results[filename]))
        return jsonify({"error": "The uploaded file or md5 checksum does not match our expected value"}), 400


@local_enterobase.route('/assembly_test_file', methods=['POST'])
def assembly_test_file():
    """
    Facilitates the uploading of test files. Calculates the md5 checksum of the uploaded test file against the provided md5 and the expected md5.
    Also records the current time which allows the upload time to be calculated.

    Returns:
        json, status_code: success or failure message
    """
    try:
        test_files_expected_results = get_json(app.config.get("LOCAL_ENTEROBASE_UPLOAD_TEST_FILES_EXPECTED_RESULTS"))
    except Exception:
        return jsonify({"error": "Error with server, please contact the Warwick Enterobase administrators"}), 501
    
    assembly_test_files_expected_results = test_files_expected_results["assembly_test_files"]

    assembly_time = request.form.get("assembly_time")
    fastq = request.files.get("fastq")
    filename = request.form.get("filename")
    md5_checksum = request.form.get("md5")
    test_token = request.form.get("test_token")

    if fastq is None:
        return jsonify({"error": "No fastq file has been received"}), 400

    client_request = LocalEnterobaseClientRequest.query.filter_by(test_token=test_token).first()

    if not bool(client_request):
        return jsonify({"error": "Invalid test token"}), 401

    if client_request.test_already_completed():
        return jsonify({"error": "Test has already been performed successfully"}), 400

    client_request_id = client_request.id

    calculated_md5 = get_md5(fastq, filename, test_token)

    if filename not in assembly_test_files_expected_results:
        return jsonify({"error": "A file with this filename ({}) was not expected, please check the name of the file you uploaded.".format(filename)}), 400

    if md5_checksum==calculated_md5==assembly_test_files_expected_results[filename]["md5"]:
        test_file_record = LocalEnterobaseClientTestFileResults(
            client_request_id=client_request_id,
            filename=filename,
            test_type="assembly",
            time_taken = assembly_time
        )
        db.session.add(test_file_record)
        db.session.commit()
        return jsonify({"success":"Successful file upload"}), 200
    else:
        return jsonify({"error": "The uploaded file or md5 checksum does not match our expected value"}), 400


@local_enterobase.route('/end_upload_test', methods=['POST'])
def end_upload_test():
    """
    Checks if the user has uploaded all of the required files and changes their test status to "Completed".
    They can now be approved by an Enterobase admin.

    Returns:
        json, status_code: success or error message
    """
    try:
        test_files_expected_results = get_json(app.config.get("LOCAL_ENTEROBASE_UPLOAD_TEST_FILES_EXPECTED_RESULTS"))
    except Exception:
        return jsonify({"error": "Error with server, please contact the Warwick Enterobase administrators"}), 501

    test_token = request.form.get("test_token")

    local_enterobase_client_request = LocalEnterobaseClientRequest.query.filter_by(test_token=test_token).first()

    if not bool(local_enterobase_client_request):
        return jsonify({"error": "Invalid test token"}), 401

    if local_enterobase_client_request.test_already_completed():
        return jsonify({"error": "Test token has already been used"}), 400

    expected_upload_files = set(str(k) for k in test_files_expected_results["upload_test_files"].keys())
    expected_assembly_files = set(str(k) for k in test_files_expected_results["assembly_test_files"].keys())

    upload_test_file_results = LocalEnterobaseClientTestFileResults.query.filter_by(client_request_id=local_enterobase_client_request.id, test_type="upload").all()
    assembly_test_file_results = LocalEnterobaseClientTestFileResults.query.filter_by(client_request_id=local_enterobase_client_request.id, test_type="assembly").all()
    
    upload_test_file_times = {}
    assembly_test_file_times = {}

    for test_file in upload_test_file_results:
        if test_file.filename in expected_upload_files:
            if not test_file.time_taken:
                break
            expected_upload_files.remove(test_file.filename)
            upload_test_file_times[test_file.filename] = test_file.time_taken

    for test_file in assembly_test_file_results:
        if test_file.filename in expected_assembly_files:
            if not test_file.time_taken:
                break
            expected_assembly_files.remove(test_file.filename)
            assembly_test_file_times[test_file.filename] = test_file.time_taken

    if expected_upload_files or expected_assembly_files:
        return jsonify({"error": "Not all test files have been uploaded"}), 400

    upload_times = upload_test_file_times.values()
    average_upload_time = sum(upload_times)/len(upload_times)

    assembly_times = assembly_test_file_times.values()
    average_assembly_time = sum(assembly_times)/len(assembly_times)


    local_enterobase_client_request.average_test_file_upload_time = average_upload_time
    local_enterobase_client_request.average_test_file_assembly_time = average_assembly_time
    local_enterobase_client_request.test_status = "Completed"

    for upload_test_file in upload_test_file_results:
        db.session.delete(upload_test_file)

    for assembly_test_file in assembly_test_file_results:
        db.session.delete(assembly_test_file) 

    db.session.commit()

    return jsonify({
        "average_upload_time": average_upload_time,
        "average_assembly_time": average_assembly_time,
        "file_upload_times": upload_test_file_times,
        "file_assembly_times": assembly_test_file_times
    })


@local_enterobase.route('/nginx_config', methods=['GET', 'POST'])
@login_required
def nginx_config():
    """
    NGINX configuration page view, displays the form and handles a submission

    Returns:
        template: nginx_config.html
    """
   
    dirname = os.path.dirname(__file__)
    with open(os.path.join(dirname, 'nginx.conf'), 'r') as config_file :
            config_file_data = config_file.read()
    
    kwargs = get_nginx_defaults(config_file_data)

    form = NGINXConfigForm(**kwargs)

    if form.validate_on_submit():
        # Read in the file
        
        server_url_ip = remove_http_s(form.server_url_ip.data)
        local_enterobase_uri = remove_http_s(form.local_enterobase_uri.data)

        # The regex replaces the variable name followed by the default value (any characters except ":", "{" and end of line ("$"))

        config_file_data = sub(r'{{server_url_ip:[^$:{]*}}', str(server_url_ip), config_file_data)
        config_file_data = sub(r'{{http_port:[^$:{]*}}', str(form.http_port.data), config_file_data)
        config_file_data = sub(r'{{https_port:[^$:{]*}}', str(form.https_port.data), config_file_data)
        config_file_data = sub(r'{{worker_connections:[^$:{]*}}', str(form.worker_connections.data), config_file_data)
        config_file_data = sub(r'{{sendfile_max_chunk:[^$:{]*}}', str(form.sendfile_max_chunk.data), config_file_data)
        config_file_data = sub(r'{{keepalive_timeout:[^$:{]*}}', str(form.keepalive_timeout.data), config_file_data)
        config_file_data = sub(r'{{client_max_body_size:[^$:{]*}}', str(form.client_max_body_size.data), config_file_data)
        config_file_data = sub(r'{{local_enterobase_uri:[^$:{]*}}', str(local_enterobase_uri), config_file_data)
        config_file_data = sub(r'{{local_enterobase_port:[^$:{]*}}', str(form.local_enterobase_port.data), config_file_data)

        form = NGINXConfigForm(**kwargs)
        
        return send_file(io.BytesIO(config_file_data), as_attachment=True, attachment_filename='nginx.conf') 
     
    return render_template('local_enterobase/nginx_config.html', form=form)


@local_enterobase.route('/get_active_databases',methods=["GET"])
def get_active_databases():
    """
    Returns the Warwick Enterobase active databases along with their numbers of strains and labels

    Returns:
        Response: JSON response of active databases
    """

    active_databases  = app.config['ACTIVE_DATABASES']
    active_databases_info = []

    for db_name in active_databases:
        db = dbhandle[db_name]
        total_strains = db.getStrainNumber()
        db_info = {}
        db_info['name'] = db_name
        db_info['label'] = active_databases[db_name][0]
        db_info['total_strains'] = total_strains
        active_databases_info.append(db_info)
    return jsonify(active_databases_info), 200


@local_enterobase.route('/display_test_token', methods=['GET'])
@login_required
def display_local_enterobase_test_token():
    """
    Endpoint for displaying the test token for a Local Enterobase Client request for a user

    Returns:
        Response: JSON success or error
    """
    local_enterobase_client_request = LocalEnterobaseClientRequest.query.filter_by(admin_id=current_user.id).first()

    if not bool(local_enterobase_client_request):
        return jsonify({"error": "User does not have a Local Enterobase request"}), 400

    return jsonify({"test_token": local_enterobase_client_request.test_token}), 200


@local_enterobase.route('/display_oauth_credentials', methods=['GET'])
@login_required
def display_local_enterobase_oauth_credentials():
    """
    Endpoint for displaying the oauth credentials for a Local Enterobase Client request for a user

    Returns:
        Response: JSON success or error
    """
    local_enterobase_client = LocalEnterobaseClient.query.filter_by(admin_id=current_user.id).first()

    if not bool(local_enterobase_client):
        return jsonify({"error": "User does not have a Local Enterobase instance"}), 400

    return jsonify({
        "client_id": local_enterobase_client.client_id,
        "client_secret": local_enterobase_client.client_secret
    }), 200
