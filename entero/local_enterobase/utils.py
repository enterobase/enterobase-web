# H.F. 13/02/2021
import os
import json
import hashlib
import re
from werkzeug.utils import secure_filename


def get_json(filename):
    """
    Reads json files (these json files contain information regarding the upload test files)

    Args:
        filename (str): name of file

    Returns:
        dict: dictionary of json contents
    """
    this_directory = os.path.realpath(os.path.dirname(__file__))
    json_path = os.path.join(this_directory, filename)
    json_file_contents = json.load(open(json_path))
    return json_file_contents


def get_md5(file_contents, filename, temp_folder):
    """
    Calculates the md5 checksum of a file

    Args:
        file_contents (str): contents of file
        filename (str): name of file
        temp_folder (str): folder to temporarily save file in
    """

    def _calculate_md5(filename):
        md5_hash = hashlib.md5()
        with open(filename, "rb") as f:
            for byte_block in iter(lambda: f.read(4096), b""):
                md5_hash.update(byte_block)
            return(md5_hash.hexdigest())

    filename = secure_filename(filename)
    folder = os.path.join('~/share_space/user_reads', temp_folder)

    if not os.path.exists(folder):
        os.makedirs(folder)

    file_path=os.path.join(folder, filename)
    raise Exception('Utils.py line 50 commented out as it does not work, showing this code had not been tested')
#   file_contents.save(file_path)
    calculated_md5=_calculate_md5(file_path)
    os.remove(file_path)

    return calculated_md5


def remove_http_s(uri):
    """
    Removes http:// or https:// from the start of a uri.

    Args:
        uri (str): IP or URL

    Returns:
        str: uri without http:// or https://
    """
    
    if uri.startswith("http://"):
        uri = uri[7:]
    elif uri.startswith("https://"):
        uri = uri[8:]
    return uri


def get_nginx_defaults(nginx_config):
    """
    Obtains the default values from the nginx.conf file

    Args:
        nginx_config (str): nginx file contents

    Returns:
        dict: dictionary of nginx default values
    """

    templates = re.findall(r"{{[^}]*:[^}]*}}", nginx_config)
    defaults = {}

    for t in templates:
        t = t[2:-2]
        var, default = t.split(":")
        defaults[var] = default

    return defaults
