from flask_wtf import FlaskForm as Form
from flask_wtf import RecaptchaField
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, HiddenField
from wtforms.validators import Length, Email, Regexp, EqualTo, DataRequired
from wtforms import ValidationError
from entero.databases.system.models import User
from flask_login import current_user



class EditUserPermissionsForm(Form):
    modify_isolates= BooleanField("Modify isolates")
    modify_loci= BooleanField("Modify loci")
    modify_schemes = BooleanField("Modify schemes")
    submit = SubmitField('Update')

class LoginForm(Form):
    email = StringField('User Name or Email', validators=[DataRequired(), Length(1, 64)])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in between sessions (uses cookies)')
    submit = SubmitField('Log In')

class DeleteForm(Form):
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Delete my Account')

class AddUserBuddyForm(Form):
    buddy_id  = SelectField("Buddy Username")
    view_data = BooleanField("view data")
    edit_data = BooleanField("edit data")
    delete_data = BooleanField("delete data")
    submit = SubmitField('Add')

class EditBuddyForm(Form):
    view_data = BooleanField("view data")
    edit_data = BooleanField("edit data")
    delete_data = BooleanField("delete data")
    submit = SubmitField('Update')

class ChangeUserDetailsForm(Form):
    username = StringField('Username', validators=[
        DataRequired(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Username must have only letters, numbers, dots or underscores.')])

    firstname = StringField('First Name', validators=[
        DataRequired(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z]*$', 0,
                                          'First name must have only letters.')])

    lastname = StringField('Last Name', validators=[
        DataRequired(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z]*$', 0,
                                          'Last name must have only letters.')])
    department = StringField('Department', validators=[
        DataRequired(), Length(1, 64)])
    institution = StringField('Institution', validators=[
        DataRequired(), Length(1, 64)])

    city = StringField('City', validators=[
        DataRequired(), Length(1, 64)])
    country = StringField('Country', validators=[
        DataRequired(), Length(1, 64)])
    
    submit = SubmitField('Update')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            if field.data != current_user.username:

                raise ValidationError('Username already in use.')


class RegistrationForm(Form):
    email = StringField('Email*', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    username = StringField('Username*', validators=[
        DataRequired(), Length(1, 60), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Username must have only letters, numbers, dots or underscores.')])

    firstname = StringField('First Name*', validators=[
        DataRequired(), Length(1, 60), Regexp('^[A-Za-z][A-Za-z]*$', 0,
                                          'First name must have only letters.')])
    lastname = StringField('Last Name*', validators=[
        DataRequired(), Length(1, 60), Regexp('^[A-Za-z][A-Za-z]*$', 0,
                                          'Last name must have only letters.')])
    # department = StringField('Department', validators=[
    #     DataRequired(), Length(1, 60)])
    department = StringField('Department')
    institution = StringField('Institution*', validators=[
        DataRequired(), Length(1, 60)])

    city = StringField('City*', validators=[
        DataRequired(), Length(1, 60)])
    country = StringField('Country*', validators=[
        DataRequired(), Length(1, 60)])
    password = PasswordField('Password*', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField('Confirm Password*', validators=[DataRequired()])
    captcha = StringField('Enter the code shown below', validators=[DataRequired()])
    agreement = BooleanField('I agree to the EnteroBase terms and conditions', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use.')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')




class ChangePasswordForm(Form):
    old_password = PasswordField('Old password', validators=[DataRequired()])
    password = PasswordField('New password', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField('Confirm new password', validators=[DataRequired()])
    submit = SubmitField('Update')


class PasswordResetRequestForm(Form):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    submit = SubmitField('Reset Password')


class PasswordResetForm(Form):
    email = StringField('Email', validators=[DataRequired(), Length(1, 64),
                                             Email()])
    password = PasswordField('New Password', validators=[
        DataRequired(), EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Reset Password')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first() is None:
            raise ValidationError('Unknown email address.')


class ChangeEmailForm(Form):
    email = StringField('New Email', validators=[DataRequired(), Length(1, 64),
                                                 Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Update')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered.')