from flask import render_template, redirect, request, url_for,flash,current_app,jsonify,make_response, session
from flask_login import login_user, logout_user, login_required, current_user, AnonymousUserMixin
from . import auth
from entero import db, get_database, get_database_label
from entero.entero_email import send_email, sendmail
from ..databases.system.models import User, UserPermissionTags, BuddyPermissionTags,query_system_database, UserUploads
from .forms import LoginForm, RegistrationForm, ChangePasswordForm,\
    PasswordResetRequestForm, PasswordResetForm, ChangeEmailForm,\
    ChangeUserDetailsForm, DeleteForm
from entero import app
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from ..decorators import auth_login_required, user_permission_required
import json, ujson
from entero.cell_app.tasks import set_strain_ownership
from entero.ExtraFuncs.workspace import get_access_permission
import io
import base64
# Import CAPTCHA utilities from a separate file
from entero.captcha_utils import generate_captcha_text, create_captcha_image

def invalid_email(email):
    return 1


@auth.before_app_request
def before_request(): 
    global current_user
    if current_user:
        if current_user.is_authenticated():
            if not current_user.confirmed and request.endpoint[:5] != 'auth.':
                return redirect(url_for('auth.unconfirmed'))

        
@auth.before_request
def before_request():
    pass

#@app.route('/api/v1.0/login', methods = ['GET','POST'])
#def get_auth_token():
    #if request.headers.has_key('Authorization')
        #print request.headers
        #auth = request.authorization
        #print auth
        #user = User.query.filter_by(username=auth.username).first()
        #if user is None:
            #response = jsonify({ 'code': 401, 'message' : 'Not authorized. No such username. Please create an account at enterobase.dsmz.de.'})
            #response.status_code = 401 
            #return response            
        #if user is not None and user.verify_password(auth.password) :
            #access =  UserPermissionTags.query.filter_by(user_id=user.id, field='api_access').first()
            #if user.administrator or access:
                #token = user.generate_api_token(expiration=31536000)
                #return jsonify({ 'token': token.decode('ascii') })
            #else:
                #response = jsonify({ 'code': 401, 'message' : 'Not authorized. You do not have valid permissions, please contact an administrator'})
                #response.status_code = 401
                #return response                
        #else:
            #response = jsonify({ 'code': 401, 'message' : 'Not authorized. Invalid password'})
            #response.status_code = 401 
            #return response
    #else:
        #response = jsonify({ 'code': 401, 'message' : 'Not authorized. Please use Basic Auth.'})
        #response.status_code = 401
        #return response

    
    
    #print request.args
    #print request.json.get('username')
    #username =  request.args.get('username')
    #password =  request.args.get('password')
    #if username == None or password == None:
        #response = jsonify({'code': 401, 'message': 'You must specify a username and passsword'})
        #response.status_code = 401
        #return response
    #user = User.query.filter_by(username=username).first()
    #if user is not None and user.verify_password(password):
        #token = current_user.generate_confirmation_token()
        #return jsonify({ 'token': token.decode('ascii') })
    #return 403

@auth.route("/get_buddy_details",methods = ['GET','POST'])
@auth_login_required
def get_buddy_details():
    print ("")
    #workspace_folders
    ws_types=[]
    for ws_type in app.config['ANALYSIS_TYPES']:
        ws_types.append(ws_type)
    ws_types.append("edit_strain_metadata")
    ws_types.append("workspace_folders")
    ws_list= "('"+"','".join(ws_types)+"')"
    ws_types = ws_types[:-1]
    if not current_user:
        app.logger.error("Error in get_buddy_details, current_user is None!")
        return ujson.dumps({})
    user_id=current_user.id
    species=request.args.get('species')
    
    #get id and user names for dropdown
    users = User.query.order_by(User.lastname).all()
    id_to_name={}
    for user in users:
        if user.id==0 or user.id==user_id:
            continue
        name = "%s %s (%s)" % (user.firstname,user.lastname,user.username)
        id_to_name[user.id] =name
    sql = "SELECT buddy_id,field,value,name FROM buddy_permission_tags LEFT JOIN user_preferences ON value= user_preferences.id::varchar WHERE buddy_permission_tags.user_id=%i AND field IN %s AND species = '%s' ORDER BY buddy_id" % (user_id,ws_list,species)
    buddies = query_system_database(sql)
   #buddy id to a list of shared analysis
    shared_analysis= {}
    edit_metadata={}
    shared_folders={}
 

    prev_buddy= 0
    for bud in buddies:
        #print bud['buddy_id'], bud['field'], bud['value']
        bud_id = bud['buddy_id']
        if bud_id != prev_buddy:
            shared_analysis[bud_id]=[]
            shared_folders[bud_id]=[]
            edit_metadata[bud_id] = False
        if bud['field']=="workspace_folders":
            #print "shared folders:::: ", bud['field']
            value_=json.loads(bud['value']).get('shared_folder')
            #print "value ",value_
            shared_folders[bud_id].append([value_,bud['name'],bud['field']])
        elif bud['field'] == 'edit_strain_metadata':
            if bud['value'] == 'True':
                edit_metadata[bud_id]=True        
        elif bud['field'] in ws_types:
            #print "It is not", bud['field']
            shared_analysis[bud_id].append([bud['value'],bud['name'],bud['field']])        
                
        prev_buddy=bud_id
  
    send = {
            "shared_analysis":shared_analysis,
            "id_to_name":id_to_name,
            "shared_folders":shared_folders,
            "edit_metadata":edit_metadata
            }
    return ujson.dumps(send)


@auth.route("/update_buddy_details",methods = ['GET','POST'])
@auth_login_required
def update_buddy_details():
    user_id=current_user.id
    database = request.form.get("database")
    if not database:
        app.logger.error("No database is provided")
        return json.dumps("Failed")
    db_label=get_database_label(database)

    data = request.form.get("data")
    action = request.form.get("action")    
    if not data:
        app.logger.error("No data is provided")
        return json.dumps("Failed")
   
    buddy_id = request.form.get("buddy_id")
    if not buddy_id or not buddy_id.isdigit():
        app.logger.error("Error in update_buddy_details, No buddy_id is provided")
        return json.dumps("Failed")
    buddy_id = int(buddy_id)
    
    if not action:
        app.logger.error("No action arg is provided")
        return json.dumps("Failed")
    
    changes=''
    change_type=''
    item_id=None

    if action == "add_analysis":
        sql = "SELECT name, id,type FROM user_preferences WHERE id in (%s) AND database='%s'" % (data,database)
        records = query_system_database(sql)
        ws_to_add=[]
        
        ids_names={}
        for rec in records:
            ids_names[rec['id']]=rec['name']
            ws_to_add.append([rec['id'],rec['type']])
            
        for item in ws_to_add:
            item_id= item[0]
            existing =db.session.query(BuddyPermissionTags).filter_by(species=database,user_id=user_id,buddy_id=buddy_id,value=str(item[0])).first()
            if not existing:
                entry = BuddyPermissionTags(species=database,user_id=user_id,buddy_id=buddy_id,value=str(item[0]),field=item[1])
                db.session.add(entry)
                if item[1]=='ms_sa_tree':
                    change=ids_names[item[0]]
                    change_type='ms tree'
                elif item[1]=='main_workspace':
                    change=ids_names[item[0]]
                    change_type='workspace'
                elif item[1]=='snp_sa_project':
                    change=ids_names[item[0]]
                    change_type='snp project'                
                else:                    
                    change=ids_names[item[0]]
                    change_type=item[1]
                    item_id=None
                if not changes:
                    changes=change
                else:
                    changes=changes+'\n'+change
                    
        db.session.commit()
    elif action == 'remove_analysis':
        removed={}
        sql = "SELECT name, id FROM user_preferences WHERE id in (%s) AND database='%s'" % (data,database)
        del_records = query_system_database(sql)
        ids_names={}
        for re in del_records:
            ids_names[re['id']]=re['name']            
        id_list = data.split(",")        
        id_liist = map(str,id_list)
        records = db.session.query(BuddyPermissionTags).filter(BuddyPermissionTags.user_id==user_id,BuddyPermissionTags.buddy_id==buddy_id,BuddyPermissionTags.value.in_(id_list),BuddyPermissionTags.species==database).all()
        for rec in records:
            db.session.delete(rec)
            if rec.field=='main_workspace':
                change_type='workspace'
                change=ids_names[int(rec.value)]
            elif rec.field=='ms_sa_tree':
                change=ids_names[int(rec.value)]
                change_type='ms tree'
            elif rec.field=='snp_sa_project':
                change=ids_names[int(rec.value)]
                change_type='snp project'            
            else:
                change=rec.field, ids_names[int(rec.value)]
                change_type=rec.field
            if not changes:
                changes=change
            else:
                changes=changes+'\n'+change            
        db.session.commit()
    elif action == 'edit_strain_metadata':
        records = db.session.query(BuddyPermissionTags).filter_by(user_id=user_id,buddy_id=buddy_id,field='edit_strain_metadata',species=database).all()
        if len(records) == 0:
            record= BuddyPermissionTags(user_id=user_id,buddy_id=buddy_id,field='edit_strain_metadata',value=data,species=database)
            db.session.add(record)
            changes=data
        else:
            if len(records)>1:
                for record in records[1:]:
                    db.session.delete(record)
            record= records[0]
            record.value=data
        db.session.commit()
    #I have extended yje method to accept two more options:
    #-add_workspace_folders to allow the user to share his folder with another user
    #-remove_workspace_folders to allow the user to delete sharing his folder with another user
    #the value will be user perfences id for database folder and the shared folder name and id
    
    elif action == 'add_workspace_folder':        
        sql = "SELECT id,data FROM user_preferences WHERE type='workspace_folders' AND database='%s' and user_id=%s" % (database, user_id)
        record = query_system_database(sql)
        if len(record)==1:
            record=record[0]
            data=json.loads(data)
            value_=json.dumps({'id':record['id'],'shared_folder':data})            
            rec = db.session.query(BuddyPermissionTags).filter(BuddyPermissionTags.user_id==user_id,BuddyPermissionTags.buddy_id==buddy_id,BuddyPermissionTags.value==value_,BuddyPermissionTags.species==database,BuddyPermissionTags.field=='workspace_folders' ).first()
            if rec:
                return json.dumps("Failed")            
            
            record= BuddyPermissionTags(user_id=user_id,buddy_id=buddy_id,field='workspace_folders',value=value_,species=database)
            db.session.add(record)
            db.session.commit()  
            changes=data[1].split('/')[-1]
            item_id='folder/shared'
        else:
            return json.dumps("Failed")
        
    elif action == 'remove_workspace_folder':
        sql = "SELECT id,data FROM user_preferences WHERE type='workspace_folders' AND database='%s' and user_id=%s" % (database, user_id)
        record = query_system_database(sql)
        if len(record)==1:
            data=json.loads(data)            
            record=record[0]        
            value_=json.dumps({'id':record['id'],'shared_folder':data})
            rec = db.session.query(BuddyPermissionTags).filter(BuddyPermissionTags.user_id==user_id,BuddyPermissionTags.buddy_id==buddy_id,BuddyPermissionTags.value==value_,BuddyPermissionTags.species==database,BuddyPermissionTags.field=='workspace_folders' ).first()
            db.session.delete(rec)
            db.session.commit()  
            changes=data[1].split('/')[-1]            
            
        else:
            return json.dumps("Failed")
        #send buddy email about this changes
    if changes:
        send_buddy_email(buddy_id,user_id,action, changes, change_type, database, db_label, item_id)
    return json.dumps("OK")


def send_buddy_email(buddy_id, user_id, action, name, change_type, database_short_name, database_name, item_id=None):
    '''
    Send an email to the buddy to let him know the changes
    '''
    try:
        user=db.session.query(User).filter(User.id==user_id).first()                                       
        buddy=db.session.query(User).filter(User.id==buddy_id).first()    
        subject={}
        subject['add_analysis']='A user has shared a %s with you'%change_type
        subject['remove_analysis']='Permission to access a %s has been revoked'%change_type
        subject['add_workspace_folder']='A user has shared a workspace folder with you'
        subject['remove_workspace_folder']='Permission to access a workspace folder has been revoked'  
        subject['edit_strain_metadata']="A user has granted you edit metadata"
    
    
        messages={}
        #messages['remove_workspace_folder']='%s %s has revoked your access to his workspace folder %s and this will no longer appear in the Shared Folder under his name'%(user.firstname, user.lastname, name)

        if item_id:
            #folder/shared
            if action=="add_workspace_folder":
                url = app.config["SERVER_BASE_ADDRESS"] + "species/%s/%s/?folder_name=%s"%(database_short_name,item_id,name)#user.username)

                messages[
                    "add_workspace_folder"] = '%s %s has shared his workspace folder %s (%s) in the %s database with you ' % (
                user.firstname, user.lastname, name, url, database_name)

            messages['add_analysis']='%s %s has shared his %s %s (%s) in the %s database with you '%(user.firstname, user.lastname, change_type, name, app.config["SERVER_BASE_ADDRESS"]+url_for("main.get_analysis_item", id=item_id),database_name)
        else:
            messages[
                "add_workspace_folder"] = '%s %s has shared his workspace folder %s in the %s database with you ' % (
            user.firstname, user.lastname, name, database_name)

            messages['add_analysis']='%s %s has shared his %s %s in the %s database with you '%(user.firstname, user.lastname, change_type, name, database_name)


        #messages['remove_analysis']='%s %s has revoked your access to his %s %s and this will no longer appear in the Shared Folder under his name'%(user.firstname, user.lastname, change_type, name)
        
        #messages['edit_strain_metadata']="%s %s has granted you edit metadata (%s)"%(user.firstname, user.lastname, name)
        if messages.get(action):
        #if (action=='edit_strain_metadata' and name ==True) or messages.get(action):
            body='Dear %s;\n\n%s\n\nKind regards,\n\nEnteroBase Team'%(buddy.firstname,  messages.get(action))
            sendmail(to=buddy.email,subject = subject[action],txt=body)
    except Exception as e:
        #rollback_close_system_db_session()
        app.logger.exception("sending buddy update email error, error message: %s"%str(e))
        
    


@auth.route("/edit_user/<user_id>", methods = ['GET','POST'])
def edit_user(user_id):
    form = ChangeUserDetailsForm()
    user = User.query.filter_by(id = user_id).first()
    
    
    if form.validate_on_submit():
        #check username does not already exist
        User.query.filter_by(id = user_id).first()
        user.username=form.username.data,
        user.firstname= form.firstname.data,
        user.lastname=form.lastname.data,
        user.department = form.department.data,
        user.institution = form.institution.data,
        user.city = form.city.data,
        user.country=form.country.data
       
        db.session.commit()
       
        flash(user.username +"'s user details have been updated")
        return redirect(url_for('.edit_users'))
    form.department.data = user.department
    form.firstname.data = user.firstname
    form.lastname.data = user.lastname
    form.city.data= user.city
    form.institution.data = user.institution
    form.username.data = user.username
    form.country.data   = user.country 
    
    
    return render_template('auth/edit_user.html',form=form,help = app.config['WIKI_BASE'])


@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous() or current_user.confirmed:
        return redirect(url_for('main.index'))
    return render_template('auth/unconfirmed.html',help = app.config['WIKI_BASE'])




@auth.route('/login', methods=['GET', 'POST'])
def login():
    try:
        form = LoginForm()
        if form.validate_on_submit():
            #print "Getting user ...."
            user = User.query.filter_by(email=form.email.data).first()
            if user is None:
                #print "User is none"
                user = User.query.filter_by(username=form.email.data).first()
                #print "User: ", user
            if user is not None and user.verify_password(form.password.data):
                #print "User is not none"
                #remove any cookies
                resp = make_response(redirect(request.args.get('next') or url_for('main.index')))
                resp.set_cookie('remember_token',"",expires=0);
                login_user(user, form.remember_me.data)
                #Check users email is ok.
                #if  not User.is_valid_email_address():
                if current_user is None:
                    app.logger.info('{0} user apparently verified password, but current user is None'.format(user.username))
                if not User.is_valid_email_address(current_user.email):
                    flash("Your registered email address " + current_user.email + " appears to be invalid. Please update it when you can.", "email")

                #return redirect(url_for('main.index'))
                return resp
            flash('Invalid username or password.')
        return render_template('auth/login.html', form=form, help = app.config['WIKI_BASE']+'/register-login.html')
    except Exception as e:
        #rollback_close_system_db_session()
        app.logger.exception("login error, error message: %s"%str(e))
        flash("The database is not available now, please try later.")
        return redirect(url_for('main.index'))

@auth.route('/delete-account', methods=['GET', 'POST'])
@login_required
def delete_account():
    form = DeleteForm()
    global current_user
    if form.validate_on_submit():
        
        user = db.session.query(User).filter_by(id=current_user.id).first()
        uploads = db.session.query(UserUploads).filter_by(user_id=current_user.id).all()
        for upload in uploads:
            db.session.delete(upload)
        logout_user()
        db.session.delete(user)
        db.session.commit()
        flash('Your account has been deleted.')
        current_user=None;
        resp = make_response(redirect(url_for('main.index',logout=True)))
        resp.set_cookie('remember_token',"",expires=0);
        return resp
    return render_template('auth/delete.html', form=form, help = app.config['WIKI_BASE']+'/register-login.html')

@auth.route('/logout')
@login_required
def logout():
    flash('You have been logged out.')
    #remove the remember me cookie
    logout_user()
    current_user=None;
    resp = make_response(redirect(url_for('main.index',logout=True)))
    resp.set_cookie('remember_token',"",expires=0);
    #logout_user()
    return resp


@auth.route('/new_user_req', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()

    def generate_and_store_captcha():
        """Helper function to generate and store CAPTCHA."""
        captcha_text = generate_captcha_text()
        session['captcha_text'] = captcha_text
        captcha_image = create_captcha_image(captcha_text)
        buffered = io.BytesIO()
        captcha_image.save(buffered, format="PNG")
        return base64.b64encode(buffered.getvalue()).decode('utf-8')

    # Generate CAPTCHA on first load (GET request)
    if request.method == 'GET' or not session.get('captcha_text'):
        captcha_image_data = generate_and_store_captcha()
    else:
        captcha_image_data = None

    reject = False
    if form.validate_on_submit():
        input_captcha = form.captcha.data.strip()
        stored_captcha = session.get('captcha_text', '').strip()

        if input_captcha.upper() != stored_captcha.upper():
            flash('CAPTCHA validation failed. Please try again.')
            captcha_image_data = generate_and_store_captcha()
            return render_template('auth/new_user_req.html', form=form, captcha_image_data=captcha_image_data,
                                   help=app.config['WIKI_BASE'] + '/register-login.html')

        # Temporary fix for spam users
        inst = form.institution.data
        dept = form.department.data
        if inst.isdigit() or dept in ['English Literature', "Medical Services", "Science",
                                      "Engineering", "Technical Sciences", "Cultural Sciences", "Command", "English"]:
            reject = True
        if inst in ["NA", "Director", "God", "Asst. Director", "FM"]:
            reject = True

        if reject:
            return render_template('auth/register.html', form=form,
                                   help=app.config['WIKI_BASE'] + '/register-login.html',
                                   captcha_image_data=captcha_image_data)

        user = User(email=form.email.data,
                    username=form.username.data,
                    firstname=form.firstname.data,
                    lastname=form.lastname.data,
                    password=form.password.data,
                    department=form.department.data,
                    institution=form.institution.data,
                    city=form.city.data,
                    country=form.country.data,
                    confirmed=0)

        addUser(user)
        token = user.generate_confirmation_token()
        send_email(user.email, 'Confirm Your Account',
                   'auth/email/confirm', user=user, token=token)
        flash(
            'An email requesting confirmation has been sent to you after which you will be able to log into EnteroBase with your username and password.')
        return redirect(url_for('auth.login'))

    # If the form is not validated or on GET, regenerate CAPTCHA if missing
    if captcha_image_data is None:
        captcha_image_data = generate_and_store_captcha()

    return render_template('auth/new_user_req.html', form=form, captcha_image_data=captcha_image_data,
                           help=app.config['WIKI_BASE'] + '/register-login.html')


def addUser(user):
    db.session.add(user)
    db.session.commit()
    databases =app.config['ACTIVE_DATABASES']
    for key in databases:
        if databases[key][2]:
            permissions = UserPermissionTags(user_id=user.id, species=key, field='upload_reads', value='True')
            db.session.add(permissions)
            permissions = UserPermissionTags(user_id=user.id, species=key, field='view_species', value='True')
            db.session.add(permissions)
            if key == 'senterica':
                permissions = UserPermissionTags(user_id=user.id, species=key, field='allowed_schemes', value='cgMLST_v2')
            db.session.add(permissions)
    db.session.commit()
    if app.config['USE_CELERY']:     
        set_strain_ownership.apply_async(args=[user.id, user.email],queue='job')
    else:
        set_strain_ownership(user.id, user.email)
    
    

#helper method to get user id from the token
#returns the user or none if the token is invalid or has expired
#token has attribute user id in flag
def  getUserFromToken(token,flag):
    s = Serializer(current_app.config['SECRET_KEY'])
    try:
        data = s.loads(token)
    except:
        return  None
    
    user_id = data.get(flag)   
    user = User.query.filter_by(id=user_id).first()
    return user

@auth.route('/confirm/<token>')
def confirm(token):
    user = getUserFromToken(token, "confirm")
    if not user:
        return  redirect(url_for('main.index'))
    
    #confirm the user and send to login just in case
    #the token has been stolen
    user.confirm()
    flash('You have confirmed your account. Thanks! Please log in to continue')
    return redirect("auth/login");
    


@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email(current_user.email, 'Confirm Your Account',
               'auth/email/confirm', user=current_user, token=token)
    flash('A new confirmation email has been sent to you by email at %s ' %current_user.email)
    return redirect(url_for('main.index'))


@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            db.session.commit()
            flash('Your password has been updated.')
            return redirect(request.args.get('next') or url_for('main.index'))
        else:
            flash('Invalid password.')
    return render_template("auth/change_password.html", form=form,help = app.config['WIKI_BASE']+ '/register-login.html')


@auth.route('/reset', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous():
        return redirect(url_for('main.index'))
    form = PasswordResetRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_reset_token()
            send_email(user.email, 'Reset Your Password',
                       'auth/email/reset_password',
                       user=user, token=token,
                       next=request.args.get('next'))
            flash('An email with instructions to reset your password has been '
              'sent to you. Feel free to login with your username and password.')   
            return redirect(url_for('main.index'))
        else:
            flash("The email address given cannot be found in the database")
        
    return render_template('auth/reset_password.html', form=form, help = app.config['WIKI_BASE']+'/register-login.html')

@auth.route('/reset/<token>', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous():
        return redirect(url_for('main.index'))
    form = PasswordResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            return redirect(url_for('main.index'))
        if user.reset_password(token, form.password.data):
            login_user(user)
            flash('Your password has been updated.')
            return redirect(url_for('main.index'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/reset_password.html', form=form, help = app.config['WIKI_BASE']+'/register-login.html')


@auth.route('/change_user_details', methods=['GET', 'POST'])
@auth_login_required
def change_user_details():
    form = ChangeUserDetailsForm( )
    user = current_user

    if form.validate_on_submit():
        
        user.username=form.username.data
        user.firstname= form.firstname.data
        user.lastname=form.lastname.data
        user.department = form.department.data
        user.institution = form.institution.data
        user.city = form.city.data
        user.country=form.country.data

        db.session.commit()

        flash('Your user details have been updated')
        return redirect(request.args.get('next') or url_for('main.index'))
    if not form.is_submitted():
        form.department.data = user.department
        form.firstname.data = user.firstname
        form.lastname.data = user.lastname
        form.city.data= user.city
        form.institution.data = user.institution
        form.username.data = user.username
        form.country.data   = user.country 
    
    return render_template('auth/change_user_details.html', form=form, help = app.config['WIKI_BASE']+'/register-login.html')




@auth.route('/change-email', methods=['GET', 'POST'])
@login_required
def change_email_request():
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            send_email(new_email, 'Confirm your email address',
                       'auth/email/change_email',
                       user=current_user, token=token)
            flash('An email with instructions to confirm your new email '
                  'address has been sent to you.')
            return redirect(request.args.get('next') or url_for('main.index'))
        else:
            flash('Invalid email or password.')
    return render_template("auth/change_email.html", form=form, help = app.config['WIKI_BASE']+'/register-login.html')


@auth.route('/change-email/<token>')
def change_email(token):
    user =getUserFromToken(token,'change_email')
    if (not user):
        flash('Invalid Token.')
        return redirect(url_for('main.index')) 
    if user.change_email(token):
        flash('Your email address has been updated.')
    else:
        flash('Invalid request.')
    return redirect(url_for('main.index'))


@auth.route("/delete_strains/<database>",methods=['POST'])
#the following is only work for adminstrator and curator and users
#with specual permission (delete strains)
#so if the curent user is the strain owner and do not have thhis permission
#he cann not delete it, so it is right to have it as we check the permission inside the method based on strains and user
#not based the user only
#K.M 16/4/2020
@user_permission_required("edit_strain_metadata")
def delete_strains(database):
    #Some of this code should move to the databse.py
    try:
        ids = request.form.get("ids")
        # print database, ids
        if ids:
            id_list = ids.split(",")
        else:
            return ujson.dumps({"status":"failed","msg":"no strain ids are provided"})
        dbase = get_database(database)
        if not dbase:
            return ujson.dumps({"status": "failed", "msg": "no valid database is provided"})

        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains).filter(Strains.id.in_(id_list))
        #sts = []
        #for x in strains:sts.append(x.__dict__)
        #check strains's delete permission
        #K.M 16/4/2020
        #permissions=get_access_permission(database, current_user.id,
        #                                  strains=sts,
        #                                  return_all_strains_permissions=True)
        for strain in strains:
            # if strain.id in permissions:
            #    if permissions[strain.id]==3:
                    strain.uberstrain = -strain.uberstrain
            #        print ("I can deleted it ...", strain.strain)


        dbase.session.commit()
        return ujson.dumps({"status":"OK"})
    except Exception as e:
        app.logger.exception("Error in delete_strains for %s, error message: %s"%(database, str(e)))
        dbase.rollback_close_session()
        return ujson.dumps({"status":"failed","msg":"There was an error trying to delete strains"})
        
@auth.route("/update_release_dates/<database>",methods = ['POST'])
@user_permission_required("edit_strain_metadata")
def update_release_dates(database):
    ids = request.form.get("ids")
    dates = request.form.get("dates")
  
    dbase = get_database(database)
    try:
        id_list = ids.split(",")
        date_list = dates.split(",")
        id_to_date = {}
        for index,strain_id in enumerate(id_list):
            id_to_date[int(strain_id)]= date_list[index]
        Strains = dbase.models.Strains
        strains = dbase.session.query(Strains).filter(Strains.id.in_(id_list)).all()
        for strain in strains:
            strain.release_date = id_to_date[strain.id]
        dbase.session.commit()
        return ujson.dumps({"status":"OK"})
    except Exception  as e:
        dbase.rollback_close_session()
        app.logger.exception("Error in update_release_dates, error message: %s" % str(e))
        return ujson.dumps({"status":"Error"})    
    


@auth.route("/get_user_permissions",methods = ["GET","POST"])
def get_user_permissions():
    try:
        if not current_user or not current_user.is_authenticated():
            return json.dumps({})
        database = request.args.get("database")
        permissions = {}
        perms = (db.session.query(UserPermissionTags).filter_by(user_id=current_user.id, species=database,
                                                                value='True').all())
        for perm in perms:
            permissions[perm.field]="True"
        if current_user.administrator or "edit_strain_metadata" in permissions:
            return ujson.dumps({
                "delete_strains":"True",
                "change_strain_owner":"True",
                "change_assembly_status":"True",
                "change_assembly_release_date":"True"
            })
        return ujson.dumps(permissions)

    except Exception  as e:
        db.session.rollback()
        db.session.close()
        app.logger.exception("Error in get_user_permissions, error message: %s" % str(e))
        return ujson.dumps({})


@app.route('/auth/reload_captcha', methods=['GET'])
def reload_captcha():
    captcha_text = generate_captcha_text()
    session['captcha_text'] = captcha_text
    captcha_image = create_captcha_image(captcha_text)

    buffered = io.BytesIO()
    captcha_image.save(buffered, format="PNG")
    captcha_image_data = base64.b64encode(buffered.getvalue()).decode('utf-8')

    return jsonify({'captcha_image_data': captcha_image_data})

    
    

