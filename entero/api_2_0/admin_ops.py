from entero import app, get_database
from flask import request, make_response, jsonify
from entero.databases.system.models import User

API_VERSION ='2.0'
'''
def check_api_auth(request, database=None):
    if request.headers.get('Authorization') and request.authorization !=None:
        data = User.decode_api_token(request.authorization.username)
        is_authorized=False
        if data is not None:   
            #adding this check to be sure that the api_aceess is not revoked
            user=db.session.query(User).filter(User.username==data['username']).first() 
            if not user :
                is_authorized=False
            elif user.administrator==1:
                is_authorized=True
            elif not database:
                is_authorized=True
            elif database != None:
                results=db.session.query(UserPermissionTags).filter(UserPermissionTags.user_id==user.id).all()
                for res in results:                                                                                        
                    if res.species==database and str(res.field)=='api_access':                      
                        is_authorized=True
                        break            
        if data is not None and is_authorized: 
            if data.has_key('api_access_%s' %database) or database == None \
               or data.get('administrator') == 1:
                if ('rMLST' in request.url or request.url.startswith('user')) and data.get('administrator') != 1:
                    return  'You do not have access to these data.'
                return data
            else:
                return 'You are not authorised to access this database, please request access from us <{}>.'.format(app.config['ENTERO_MAIL_ADDRESS'])
        return 'Invalid token, please confirm your token from {}'.format(app.config['SERVER_BASE_ADDRESS'])
    else:
        return 'Please authenticate all requests using Basic-Auth and a valid token. Register at {} and request an API Token from us <{}>'.format(app.config['SERVER_BASE_ADDRESS'],app.config['ENTERO_MAIL_ADDRESS'])

'''
@app.route('/api/v%s/<string:database>/%s' %(API_VERSION, 'eb_status'))
def eb_status(database, **kwargs):

    curr_db = get_database(database)
    if not curr_db:
        return make_response('Can not find % database, please check your request'%database, 400)

    Strains  = curr_db.models.Strains
    try:
    #    query = curr_db.session.query(Strains.custom_columns, Strains.barcode).filter(or_(Strains.custom_columns.has_key('6690'),\
     #                                                     Strains.custom_columns.has_key('6689'),\
      #                                                    Strains.custom_columns.has_key('14482')
       #                                                   )).all()
        query = curr_db.session.query(Strains.custom_columns, Strains.barcode).filter(Strains.custom_columns.has_key('6779'))\
            .filter(Strains.id == Strains.uberstrain).all()
        clean_query = [dict(x[0], **(dict(barcode = x[1]))) for x in query ]
        return jsonify(clean_query)
    except Exception as e:
        curr_db.rollback_close_session()
        app.logger.exception("Error in eb_status for %s, error message: %s"%(database, str(e)))
        return make_response(
            'Error while access the %s database.'%database,503)


@app.route('/api/v%s/<string:database>/%s' %(API_VERSION, 'strain_list'))
def strain_list(database, **kwargs):
    auth = User.check_api_auth(request, database)
    if isinstance(auth, dict):
        curr_db = get_database(database)
        if not curr_db:
            return make_response('Can not find % database, please check your request'%database, 400)

        Strains  = curr_db.models.Strains
        Assemblies  = curr_db.models.Assemblies

        try:
            query = curr_db.session.query(Strains.strain).filter(Assemblies.status == 'Assembled').join(Assemblies).all()
            clean_query = [x[0] for x in query ]
            return jsonify(clean_query)
        except  Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in strain_list for %s, error message: %s"%(database, str(e)))
            return make_response('Error while access the %s database.'%database,503)
    return make_response('You are not authorised to access the API, please request access from us <{}>.'
                         .format(app.config['ENTERO_MAIL_ADDRESS']), 403)

import datetime
@app.route('/api/v%s/<string:database>/%s/<string:offset>/<string:limit>' %(API_VERSION, 'assembly_dump'))
def assembly_dump(database, offset, limit, **kwargs):
    auth = User.check_api_auth(request, database)
    if isinstance(auth, dict):
        curr_db = get_database(database)
        if not curr_db:
            return make_response('Can not find % database, please check your request' % database, 400)

        try:
            Strains  = curr_db.models.Strains
            Traces  = curr_db.models.Traces
            Assemblies  = curr_db.models.Assemblies
            query = curr_db.session.query(Strains.strain, Strains.secondary_sample_accession, Strains.barcode,
                                          Strains.secondary_study_accession, Strains.release_date,
                                          Strains.sample_accession, Strains.contact,Traces.seq_platform,
                                          Traces.accession, Traces.total_bases, Traces.average_length,
                                          Traces.seq_library, Traces.experiment_accession, Assemblies.status,
                                          Assemblies.file_pointer, Assemblies.barcode, Assemblies.n50,
                                          Assemblies.top_species, Assemblies.low_qualities,
                                          Assemblies.coverage, Assemblies.total_length, Assemblies.contig_number,
                                          Assemblies.pipeline_version).\
                filter(Strains.release_date < datetime.datetime.now()).\
                filter(Assemblies.status.in_(['Assembled', 'Failed QC'])).join(Assemblies).join(Traces)
            if offset == 'count':
                return jsonify(query.count())
            else:
                clean_query = []
                for record in query.offset(int(offset)).limit(int(limit)):
                    x = record._asdict()
                    new_record = dict(strain_name = x.get('strain'), run_accession = x.get('accession'), seq_platform = x.get('seq_platform','').title(),
                                      assembly_total_length = x.get('total_length'), sequenced_by = x.get('contact'), low_mapping_qualities = x.get('low_qualities'),
                                      assembler_identifier = x.get('barcode'), read_coverage = x.get('coverage'),
                                      total_read_bases = x.get('total_bases'), contig_number = x.get('contig_number'), experiment_accession = x.get('experiment_accession'),
                                      assembly_n50 = x.get('n50'), pipeline_version = x.get('pipeline_version'), sample_accession = x.get('sample_accession'),
                                      secondary_study_accession = x.get('secondary_study_accession'), seq_libary = x.get('seq_library'), file_pointer = x.get('file_pointer'))
                    if x.get('top_species'):
                        new_record['species'] = x.get('top_species').split(';')[0]
                    # Fix assembly status
                    if x.get('status') == 'Assembled':
                        new_record['status'] = 'Good'
                    else:
                        new_record['status'] = x.get('status')
                    # Fix the date:
                    if x.get('release_date'):
                        new_record['release_date'] = x.get('release_date').date()
                    new_record['listed_species'] = app.config['ACTIVE_DATABASES'][database][0]
                    new_record['assembled_by'] = 'EnteroBase assembly pipeline (EnSuit)'
                    new_record = dict((k, v) for k, v in iter(new_record.items()) if v)
                    clean_query.append(new_record)
                return jsonify(clean_query)

        except Exception as e:
            curr_db.rollback_close_session()
            app.logger.exception("Error in assembly_dump for %s, error message: %s" % (database, str(e)))
            return make_response('Error while access the %s database.' % database, 503)
    return make_response('You are not authorised to access the API, please request access from us <{}>.'.
                         format(app.config['CONTACT_EMAIL_ADDRESS']), 403)


