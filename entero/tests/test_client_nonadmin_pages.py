import pytest
# import flask
import ujson

from entero.config import Config
from entero.test_utilities import publicDBList, fullDBList, nonPublicDBList, jackDBList, nonJackDBList, testbossDBList, nonTestbossDBList, getATagURLList, fakeConfigActiveDBHash

# WVN 15/3/18 Only moderately slow and we really want to test the home page.
# @pytest.mark.slow
class TestHomePageNotLoggedIn:
    @pytest.fixture(scope = "class")
    def home_page_response(self, eb_client):
        return eb_client.get("/")
    
    @pytest.fixture(scope = "class")
    def home_page_response_data(self, home_page_response):
        return home_page_response.get_data(as_text = True)

    def test_home_page_status(self, home_page_response):
        assert home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    def test_home_page_has_EnteroBase_in_content(self, home_page_response_data):
        assert "EnteroBase" in home_page_response_data
        
    def test_home_page_has_Available_Databases_in_content(self, home_page_response_data):
        assert "Available Databases" in home_page_response_data

    # Replaced multiple standard tests with single parameterised test
    @pytest.mark.parametrize("db_name", publicDBList)
    def test_home_page_has_public_readable_db_name_in_content(self, home_page_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName in home_page_response_data
        
    @pytest.mark.parametrize("db_name", nonPublicDBList)   
    def test_home_page_doesnt_have_nonpublic_readable_db_name_in_content(self, home_page_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName not in home_page_response_data

    @pytest.fixture(scope = "class")
    def home_page_response_links_n_statuses(self, eb_client, home_page_response_data):
        return [(url, eb_client.get(url).status_code) for url in getATagURLList("/", home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    # These tests really are slow
    @pytest.mark.slow
    def test_no_bad_links(self, home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)

# WVN 15/3/18 Only moderately slow and we really want to test the home page.
# @pytest.mark.slow
class TestHomePageJackLoggedIn:
    @pytest.fixture(scope = "class")
    def home_page_response(self, eb_logged_in_client):
        return eb_logged_in_client.get("/")
    
    @pytest.fixture(scope = "class")
    def home_page_response_data(self, home_page_response):
        return home_page_response.get_data(as_text = True)

    def test_home_page_status(self, home_page_response):
        assert home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is logged in
    def test_home_page_has_EnteroBase_in_content(self, home_page_response_data):
        assert "EnteroBase" in home_page_response_data

    # Current the only client logs in with the jack login which sees everything
    # may want to change this
    @pytest.mark.parametrize("db_name", jackDBList)    
    def test_home_page_has_jack_readable_db_name_in_content(self, home_page_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName in home_page_response_data
        
    @pytest.mark.parametrize("db_name", nonJackDBList)    
    def test_home_page_doesnt_have_nonjack_readable_db_name_in_content(self, home_page_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert len(nonJackDBList) == 0 or readableDBName not in home_page_response_data       

    @pytest.fixture(scope = "class")
    def home_page_response_links_n_statuses(self, eb_logged_in_client, home_page_response_data):
        return [(url, eb_logged_in_client.get(url).status_code) for url in getATagURLList("/", home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)


# WVN 15/3/18 Only moderately slow and we really want to test the home page.
# @pytest.mark.slow
class TestHomePageTestbossLoggedIn:
    @pytest.fixture(scope = "class")
    def home_page_response(self, eb_testboss_logged_in_client):
        return eb_testboss_logged_in_client.get("/")
    
    @pytest.fixture(scope = "class")
    def home_page_response_data(self, home_page_response):
        return home_page_response.get_data(as_text = True)

    def test_home_page_status(self, home_page_response):
        assert home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is logged in
    def test_home_page_has_EnteroBase_in_content(self, home_page_response_data):
        assert "EnteroBase" in home_page_response_data

    # Current the only client logs in with the jack login which sees everything
    # may want to change this
    @pytest.mark.parametrize("db_name", testbossDBList)    
    def test_home_page_has_testboss_readable_db_name_in_content(self, home_page_response_data, db_name):
        readableDBName = fakeConfigActiveDBHash[db_name][0]
        assert readableDBName in home_page_response_data
  
    # testboss is supposed to be a login with admin privileges and such logins see all of the databases on
    # the home page.  pytest.mark.parametrize seems to automatically fail any test with an empty list so
    # it's easier not to have this (almost) redundant test.
    #@pytest.mark.parametrize("db_name", nonTestbossDBList)    
    #def test_home_page_doesnt_have_non_testboss_readable_db_name_in_content(self, home_page_response_data, db_name):
    #    readableDBName = fakeConfigActiveDBHash[db_name][0]
    #    assert len(nonTestbossDBList) == 0 or readableDBName not in home_page_response_data

    @pytest.fixture(scope = "class")
    def home_page_response_links_n_statuses(self, eb_testboss_logged_in_client, home_page_response_data):
        return [(url, eb_testboss_logged_in_client.get(url).status_code) for url in getATagURLList("/", home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)


class TestGetUserWorkspacesEndpointNotLoggedIn:
    # @pytest.fixture(scope = "class", params = ["senterica", "ecoli"])
    @pytest.fixture(scope = "class", params = publicDBList)
    def endpoint_response(self, eb_client, request):
        return eb_client.get("/species/get_user_workspaces?database=" + request.param)

    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        # Martin said in e-mail to use ujson but
        # examples I have use json
        return ujson.loads(endpoint_response.get_data())
        # return json.loads(endpoint_response.get_data())

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200

    def test_endpoint_response_data_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None

    def test_endpoint_response_data_has_workspace_folders_key(self, endpoint_response_data):
        assert "workspace_folders" in endpoint_response_data

    # This appears as "null" if you visit the URL in a browser
    def test_endpoint_response_data_workspace_folders_is_none(self, endpoint_response_data):
        assert endpoint_response_data["workspace_folders"] is None

class TestGetUserWorkspacesEndpointJackLoggedIn:
    @pytest.fixture(scope = "class", params = jackDBList)
    def endpoint_response(self, eb_logged_in_client, request):
        # return eb_logged_in_client.get("/species/get_user_workspaces?database=senterica")
        return eb_logged_in_client.get("/species/get_user_workspaces?database=" + request.param)

    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        # Martin said in e-mail to use ujson but
        # examples I have use json
        return ujson.loads(endpoint_response.get_data())
        # return json.loads(endpoint_response.get_data())

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200

    def test_endpoint_response_data_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None

    def test_endpoint_response_data_has_workspace_folders_key(self, endpoint_response_data):
        assert "workspace_folders" in endpoint_response_data

    def test_endpoint_response_data_workspace_folders_len_gt_0(self, endpoint_response_data):
        assert len(endpoint_response_data["workspace_folders"]) > 0
        
class TestGetUserWorkspacesEndpointTestbossLoggedIn:
    @pytest.fixture(scope = "class", params = testbossDBList)
    def endpoint_response(self, eb_testboss_logged_in_client, request):
        return eb_testboss_logged_in_client.get("/species/get_user_workspaces?database=" + request.param)

    @pytest.fixture(scope = "class")
    def endpoint_response_data(self, endpoint_response):
        # Martin said in e-mail to use ujson but
        # examples I have use json
        return ujson.loads(endpoint_response.get_data())
        # return json.loads(endpoint_response.get_data())

    def test_endpoint_response_status(self, endpoint_response):
        assert endpoint_response.status_code == 200

    def test_endpoint_response_data_not_none(self, endpoint_response_data):
        assert endpoint_response_data is not None

    def test_endpoint_response_data_has_workspace_folders_key(self, endpoint_response_data):
        assert "workspace_folders" in endpoint_response_data

    def test_endpoint_response_data_workspace_folders_len_gt_0(self, endpoint_response_data):
        assert len(endpoint_response_data["workspace_folders"]) > 0

class TestSpeciesHomePageNotLoggedIn:
    @pytest.fixture(scope = "class", params = publicDBList)
    # def species_home_page_url(self, eb_client, request):
    def species_home_page_url(self, request):
        return "/species/index/" + request.param

    @pytest.fixture(scope = "class")
    def species_home_page_response(self, eb_client, species_home_page_url):
        # return eb_client.get("/species/index/senterica")
        # return eb_client.get("/species/index/" + request.param)
        return eb_client.get(species_home_page_url)

    @pytest.fixture(scope = "class")
    def species_home_page_response_data(self, species_home_page_response):
        return species_home_page_response.get_data(as_text = True)

    def test_species_home_page_status(self, species_home_page_response):
        assert species_home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    def test_species_home_page_has_EnteroBase_in_content(self, species_home_page_response_data):
        ## VTEMP
        # print "species_home_page_response_data: ", species_home_page_response_data
        # assert "EnteroBase" in species_home_page_response.get_data(as_text = True)
        assert "EnteroBase" in species_home_page_response_data

    def test_species_home_page_has_Login_Required(self, species_home_page_response_data):
        assert "Login Required" in species_home_page_response_data
        
    def test_species_home_page_has_Register(self, species_home_page_response_data):
        assert "Register" in species_home_page_response_data
        
    def test_species_home_page_has_Search_Strains(self, species_home_page_response_data):
        assert "Search Strains" in species_home_page_response_data

    @pytest.fixture(scope = "class")
    def species_home_page_response_links_n_statuses(self, eb_client, species_home_page_response_data, species_home_page_url):
        return [(url, eb_client.get(url).status_code) for url in getATagURLList(species_home_page_url, species_home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, species_home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, species_home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, species_home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)

class TestSpeciesHomePageJackLoggedIn:
    @pytest.fixture(scope = "class", params = jackDBList)
    def species_home_page_url(self, request):
        # return eb_logged_in_client.get("/species/index/senterica")
        return "/species/index/" + request.param

    @pytest.fixture(scope = "class")
    def species_home_page_response(self, eb_logged_in_client, species_home_page_url):
        return eb_logged_in_client.get(species_home_page_url)

    @pytest.fixture(scope = "class")
    def species_home_page_response_data(self, species_home_page_response):
        return species_home_page_response.get_data(as_text = True)

    def test_species_home_page_status(self, species_home_page_response):
        assert species_home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is logged in
    def test_species_home_page_has_EnteroBase_in_content(self, species_home_page_response_data):
        # assert "EnteroBase" in species_home_page_response.get_data(as_text = True)
        assert "EnteroBase" in species_home_page_response_data

    def test_species_home_page_doesnt_have_Login_Required(self, species_home_page_response_data):
        assert not "Login Required" in species_home_page_response_data
       
    def test_species_home_page_has_jack(self, species_home_page_response_data):
        assert "jack" in species_home_page_response_data
       
    def test_species_home_page_has_Search_Strains(self, species_home_page_response_data):
        assert "Search Strains" in species_home_page_response_data

    @pytest.fixture(scope = "class")
    def species_home_page_response_links_n_statuses(self, eb_logged_in_client, species_home_page_response_data, species_home_page_url):
        return [(url, eb_logged_in_client.get(url).status_code) for url in getATagURLList(species_home_page_url, species_home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, species_home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, species_home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, species_home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)

class TestSpeciesHomePageTestbossLoggedIn:
    @pytest.fixture(scope = "class", params = testbossDBList)
    def species_home_page_url(self, request):
        # return eb_testboss_logged_in_client.get("/species/index/" + request.param)
        return "/species/index/" + request.param

    @pytest.fixture(scope = "class")
    def species_home_page_response(self, eb_testboss_logged_in_client, species_home_page_url):
        return eb_testboss_logged_in_client.get(species_home_page_url)

    @pytest.fixture(scope = "class")
    def species_home_page_response_data(self, species_home_page_response):
        return species_home_page_response.get_data(as_text = True)

    def test_species_home_page_status(self, species_home_page_response):
        assert species_home_page_response.status_code == 200

    # Rudimentary checks on content for a user that is logged in
    def test_species_home_page_has_EnteroBase_in_content(self, species_home_page_response_data):
        assert "EnteroBase" in species_home_page_response_data

    def test_species_home_page_doesnt_have_Login_Required(self, species_home_page_response_data):
        assert not "Login Required" in species_home_page_response_data
       
    def test_species_home_page_has_testboss(self, species_home_page_response_data):
        assert "testboss" in species_home_page_response_data
       
    def test_species_home_page_has_Search_Strains(self, species_home_page_response_data):
        assert "Search Strains" in species_home_page_response_data

    @pytest.fixture(scope = "class")
    def species_home_page_response_links_n_statuses(self, eb_testboss_logged_in_client, species_home_page_response_data, species_home_page_url):
        return [(url, eb_testboss_logged_in_client.get(url).status_code) for url in getATagURLList(species_home_page_url, species_home_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = home_page_response_links_n_statuses)
    # def home_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, species_home_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", home_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, species_home_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, species_home_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in species_home_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)


# Although not linked from the main home page for a logged-in user the
# registration and login pages "work" for logged-in users at present
# (but I don't think they are required to do so; so I have not written
# similar tests for a normal user - jack - or an admin level user - testboss).
# Also, decided not to write tests to check for bad links from the login or registration
# pages for now because of the issues with side effects for the URLs comin from 
# the getATagList function
class TestRegistrationPageNotLoggedIn:
    @pytest.fixture(scope = "class")
    def reg_page_response(self, eb_client):
        return eb_client.get("/auth/register")
    
    @pytest.fixture(scope = "class")
    def reg_page_response_data(self, reg_page_response):
        return reg_page_response.get_data(as_text = True)

    def test_reg_page_status(self, reg_page_response):
        assert reg_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    def test_reg_page_has_register_in_content(self, reg_page_response_data):
        assert "Register" in reg_page_response_data
        
    # Replaced multiple standard tests with single parameterised test
    @pytest.mark.parametrize("form_field", ["Username", "Firstname", "Lastname", "Email", "Department", "Institution", "City", "Country", "Password", "Confirm password"])    
    def test_reg_page_has_form_field_in_content(self, reg_page_response_data, form_field):
        assert form_field in reg_page_response_data

# Like with the registration page, the login page is only required to work for a not-logged-in user;
# but may work for logged-in users.
class TestLoginPageNotLoggedIn:
    @pytest.fixture(scope = "class")
    def login_page_response(self, eb_client):
        return eb_client.get("/auth/login")
    
    @pytest.fixture(scope = "class")
    def login_page_response_data(self, login_page_response):
        return login_page_response.get_data(as_text = True)

    def test_login_page_status(self, login_page_response):
        assert login_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    # This one isn't parameterised because it may check to see if Login is in a heading
    def test_login_page_has_login_in_content(self, login_page_response_data):
        assert "Login" in login_page_response_data
        
    # Replaced multiple standard tests with single parameterised test
    @pytest.mark.parametrize("text", ["User Name or Email", "Password", "Keep me logged in", "Log In", "Forgot your password", "Click here to reset", "New user", "Click here to register"])    
    def test_login_page_has_text_in_content(self, login_page_response_data, text):
        assert text in login_page_response_data

#class TestSpeciesSearchStrainsNotLoggedIn:
#    @pytest.fixture(scope = "class", params = publicDBList)
#    def species_search_strains_url(self, request):
#        return "/species/" + request.param + "/search_strains"
#
#    @pytest.fixture(scope = "class")
#    # def species_search_strains_response(self, eb_client, request):
#    #    return eb_client.get("/species/" + request.param + "/search_strains")
#    def species_search_strains_response(self, eb_client, species_search_strains_url):
#        return eb_client.get(species_search_strains_url)
#
#    @pytest.fixture(scope = "class")
#    def species_search_strains_response_data(self, species_search_strains_response):
#        return species_search_strains_response.get_data(as_text = True)
#
#    def test_species_search_strains_status(self, species_search_strains_response):
#        assert species_search_strains_response.status_code == 200
#
#    # Rudimentary checks on content for a user that is not logged in
#    def test_species_search_strains_has_EnteroBase_in_content(self, species_search_strains_response_data):
#        assert "EnteroBase" in species_search_strains_response_data
#
#    def test_species_search_strains_has_Log_In(self, species_search_strains_response_data):
#        assert "Log In" in species_search_strains_response_data
#        
#    def test_species_search_strains_has_Register(self, species_search_strains_response_data):
#        assert "Register" in species_search_strains_response_data
#    
#    # Expect to have this because of the side bar - not sure it's worth bothering    
#    # def test_species_search_strains_has_Search_Strains(self, species_search_strains_response_data):
#    #    assert "Search Strains" in species_search_strains_response_data
#
#    @pytest.fixture(scope = "class")
#    def species_search_strains_response_links_n_statuses(self, eb_client, species_search_strains_response_data, species_search_strains_url):
#        return [(url, eb_client.get(url).status_code) for url in getATagURLList(species_search_strains_url, species_search_strains_response_data)]
#
#    # Attempt at getting parameterisation from a fixture result
#    # This also fails with error that a function object is not iterable
#    # (Also, see below for other failed approach.)
#    # @pytest.fixture(scope = "class", params = search_strains_response_links_n_statuses)
#    # def search_strains_a_link_n_status(self, request):
#    #    return request.param
#
#    @pytest.mark.slow
#    def test_no_bad_links(self, species_search_strains_response_links_n_statuses):
#        # not sure what status codes should be allowed - 200 obviously and probably 302
#        # and maybe 301
#        # 4xx and 5xx for client and server errors are not allowed but
#        # handled by the two tests below.
#        for (linkURL, status) in species_search_strains_response_links_n_statuses:
#            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)
#
#    # Apparently can't use a list that results from a fixture to
#    # parameterise a test - gives error that a function object is
#    # not iterable
#    # @pytest.mark.parametrize("linkURL", search_strains_response_links)
#    # def test_no_client_error_links(self, linkURL):
#    @pytest.mark.slow
#    def test_no_client_error_links(self, species_search_strains_response_links_n_statuses):
#        # 4xx not allowed
#        for (linkURL, status) in species_search_strains_response_links_n_statuses:
#            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)
#
#    @pytest.mark.slow
#    def test_no_server_error_links(self, species_search_strains_response_links_n_statuses):
#        # 5xx not allowed
#        for (linkURL, status) in species_search_strains_response_links_n_statuses:
#            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)
#

class TestSpeciesGenericPageNotLoggedIn:
    from itertools import product
    # @pytest.fixture(scope = "class", params = publicDBList)
    # Dubious about including "upload_reads" and "my_strains" (the curate page) in this list
    # at present
    # In the case of the not-logged-in client, other pages will need different tests than below
    # because most of them will lack content due to redirection to a login page.
    @pytest.fixture(scope = "class", params = product(publicDBList, ["search_strains", "download_data"]))
    def species_generic_page_url(self, request):
        # return "/species/" + request.param + "/generic_page"
        return "/species/" + request.param[0] + "/" + request.param[1]

    @pytest.fixture(scope = "class")
    # def species_generic_page_response(self, eb_client, request):
    #    return eb_client.get("/species/" + request.param + "/generic_page")
    def species_generic_page_response(self, eb_client, species_generic_page_url):
        return eb_client.get(species_generic_page_url)

    @pytest.fixture(scope = "class")
    def species_generic_page_response_data(self, species_generic_page_response):
        return species_generic_page_response.get_data(as_text = True)

    def test_species_generic_page_status(self, species_generic_page_response):
        assert species_generic_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    def test_species_generic_page_has_EnteroBase_in_content(self, species_generic_page_response_data):
        assert "EnteroBase" in species_generic_page_response_data

    def test_species_generic_page_has_Log_In(self, species_generic_page_response_data):
        assert "Log In" in species_generic_page_response_data
        
    def test_species_generic_page_has_Register(self, species_generic_page_response_data):
        assert "Register" in species_generic_page_response_data
    
    # Expect to have this because of the side bar - not sure it's worth bothering    
    # def test_species_generic_page_has_Search_Strains(self, species_generic_page_response_data):
    #    assert "Search Strains" in species_generic_page_response_data

    @pytest.fixture(scope = "class")
    def species_generic_page_response_links_n_statuses(self, eb_client, species_generic_page_response_data, species_generic_page_url):
        return [(url, eb_client.get(url).status_code) for url in getATagURLList(species_generic_page_url, species_generic_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = generic_page_response_links_n_statuses)
    # def generic_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, species_generic_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", generic_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, species_generic_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, species_generic_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)


class TestSpeciesGenericPageJackLoggedIn:
    from itertools import product
    # @pytest.fixture(scope = "class", params = publicDBList)
    # Dubious about including "upload_reads" and "my_strains" (the curate page) in this list
    # at present
    @pytest.fixture(scope = "class", params = product(publicDBList, ["search_strains", "download_data", "my_jobs"]))
    def species_generic_page_url(self, request):
        # return "/species/" + request.param + "/generic_page"
        return "/species/" + request.param[0] + "/" + request.param[1]

    @pytest.fixture(scope = "class")
    # def species_generic_page_response(self, eb_logged_in_client, request):
    #    return eb_logged_in_client.get("/species/" + request.param + "/generic_page")
    def species_generic_page_response(self, eb_logged_in_client, species_generic_page_url):
        return eb_logged_in_client.get(species_generic_page_url)

    @pytest.fixture(scope = "class")
    def species_generic_page_response_data(self, species_generic_page_response):
        return species_generic_page_response.get_data(as_text = True)

    def test_species_generic_page_status(self, species_generic_page_response):
        assert species_generic_page_response.status_code == 200

    # Rudimentary checks on content for a user that is not logged in
    def test_species_generic_page_has_EnteroBase_in_content(self, species_generic_page_response_data):
        assert "EnteroBase" in species_generic_page_response_data

    def test_species_generic_page_doesnt_have_Log_In(self, species_generic_page_response_data):
        assert "Log In" not in species_generic_page_response_data
        
    def test_species_generic_page_doesnt_have_Register(self, species_generic_page_response_data):
        assert "Register" not in species_generic_page_response_data

    def test_species_generic_page_has_Log_Out(self, species_generic_page_response_data):
        assert "Log Out" in species_generic_page_response_data
    
    # Expect to have this because of the side bar - not sure it's worth bothering    
    # def test_species_generic_page_has_Search_Strains(self, species_generic_page_response_data):
    #    assert "Search Strains" in species_generic_page_response_data

    @pytest.fixture(scope = "class")
    def species_generic_page_response_links_n_statuses(self, eb_logged_in_client, species_generic_page_response_data, species_generic_page_url):
        return [(url, eb_logged_in_client.get(url).status_code) for url in getATagURLList(species_generic_page_url, species_generic_page_response_data)]

    # Attempt at getting parameterisation from a fixture result
    # This also fails with error that a function object is not iterable
    # (Also, see below for other failed approach.)
    # @pytest.fixture(scope = "class", params = generic_page_response_links_n_statuses)
    # def generic_page_a_link_n_status(self, request):
    #    return request.param

    @pytest.mark.slow
    def test_no_bad_links(self, species_generic_page_response_links_n_statuses):
        # not sure what status codes should be allowed - 200 obviously and probably 302
        # and maybe 301
        # 4xx and 5xx for client and server errors are not allowed but
        # handled by the two tests below.
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert status in [200, 301, 302], "Problematic linked URL %s has status %d" % (linkURL, status)

    # Apparently can't use a list that results from a fixture to
    # parameterise a test - gives error that a function object is
    # not iterable
    # @pytest.mark.parametrize("linkURL", generic_page_response_links)
    # def test_no_client_error_links(self, linkURL):
    @pytest.mark.slow
    def test_no_client_error_links(self, species_generic_page_response_links_n_statuses):
        # 4xx not allowed
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert not(400 <= status and status < 500), "Problematic linked URL %s has status %d" % (linkURL, status)

    @pytest.mark.slow
    def test_no_server_error_links(self, species_generic_page_response_links_n_statuses):
        # 5xx not allowed
        for (linkURL, status) in species_generic_page_response_links_n_statuses:
            assert not(500 <= status and status < 600), "Problematic linked URL %s has status %d" % (linkURL, status)


