# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
from flask import url_for
import threading
from selenium import webdriver
import time

# import urllib2

@pytest.fixture(scope='module')
def eb_selenium_client(app):
    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    ebDriver = None
    try:
        ebDriver = webdriver.Chrome(chrome_options = options)
    except: # I don't really like this - copied from book...
        pass
    return ebDriver

#  Method to start Flask server for Selenium client's usage
@pytest.fixture(scope='module')
def eb_flask_server_thread(app):
    # server_thread = threading.Thread(target = app.run, kwargs = {"debug": False})
    server_thread = threading.Thread(target = app.run)
    server_thread.start()

    # give the server some time to ensure it is up - not sure 1 second from e.g.
    # is enough
    time.sleep(1)

    # Don't really need to return the resource as such
    # return server_thread
    yield server_thread

    # Code after yield works like teardown supposedly
    # so don't need to use the addfinalizer syntax
    # request.addfinalizer
    server_thread.join() 

# @pytest.mark.usefixtures("live_server")
class TestLiveServer:
    # We want to get tests with the Selenium client working eventually; but in
    # theory most of the same code can be covered via some combination of tests
    # based on the Flask client and Javascript based unit tests.
    @pytest.mark.skip(reason = "Don't really know how to write a working test with the selenium client yet")
    def test_home_page_w_selenium(eb_selenium_client, eb_flask_server_thread):
    # def test_home_page_w_selenium(self, eb_selenium_client):
        #response = eb_selenium_client.get("/")
        response = eb_selenium_client.get("http://localhost:5000/")
        # Based on Pytest Flask documentation - doesn't use Selenium though
        # Also, url_for not working for root/ home
        # response = urllib2.urlopen(url_for("/", _external = True))
        # response = urllib2.urlopen("http://localhost:5000/")
        assert response.status_code == 200
        # Rudimentary checks on content for a user that is not logged in
        # assert "EnteroBase" in response.get_data(as_text = True)
    #    assert "Available Databases" in response.get_data(as_text = True)
    #    assert "Salmonella" in response.get_data(as_text = True)
    #    assert "Escherichia/Shigella" in response.get_data(as_text = True)
    #    assert "Yersinia" in response.get_data(as_text = True)
    #    assert "Moraxella" in response.get_data(as_text = True)
    #    assert "Clostridioides" in response.get_data(as_text = True)
