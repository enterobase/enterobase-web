# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

# from urllib2 import HTTPError
# import urllib2
import json
import re

# WVN 21/2/18 Could have a quasi-loop thing for parameterised testing
# for different barcodes but there would need to be different
# code to check the internals of the results returned.
# So far just getting sensible results back for a scheme, allele, ST barcode query
# and not for locus, assembly
# and partial success for trace, strains
class TestLookupQueryCaseLocus1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_locus1_barcode = "SAL_AA0001AA_LO"
    lookup_query_locus1 = lookup_query_base_url + "?barcode=" + lookup_query_locus1_barcode
    lookup_barcode_query_locus1 = lookup_query_base_url + "/" + lookup_query_locus1_barcode

    # "Standard" lookup query with a locus barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_locus1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_locus1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_locus1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_locus1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_locus1_jack_auth_response_data(self, lookup_query_locus1_jack_auth_response):
        return json.loads(lookup_query_locus1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_locus1_bad_auth_status(self, lookup_query_locus1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_locus1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_locus1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on locus barcodes")
    def test_lookup_query_locus1_jack_auth_status(self, lookup_query_locus1_jack_auth_response):
        assert lookup_query_locus1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_query_locus1_non_empty(self, lookup_query_locus1_jack_auth_response_data):
        assert lookup_query_locus1_jack_auth_response_data is not None
        assert len(lookup_query_locus1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
# WVN 21/2/18 Basing this on the output for my only successful lookup query
# so far which is for a scheme barcode (and has its own format, different
# than a scheme query)
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_query_locus1_has_records_key(self, lookup_query_locus1_jack_auth_response_data):
        assert "records" in lookup_query_locus1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_query_locus1_has_results_key(self, lookup_query_locus1_jack_auth_response_data):
        assert "results" in lookup_query_locus1_jack_auth_response_data

    # GET and POST Lookup queries with a locus barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_locus1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_locus1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_locus1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_locus1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_locus1_jack_auth_response_data(self, lookup_barcode_get_query_locus1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_locus1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on locus barcodes")
    def test_lookup_barcode_get_query_locus1_bad_auth_status(self, lookup_barcode_get_query_locus1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_locus1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_locus1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_get_query_locus1_jack_auth_status(self, lookup_barcode_get_query_locus1_jack_auth_response):
        assert lookup_barcode_get_query_locus1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_get_query_locus1_non_empty(self, lookup_barcode_get_query_locus1_jack_auth_response_data):
        assert lookup_barcode_get_query_locus1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_locus1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_get_query_locus1_has_records_key(self, lookup_barcode_get_query_locus1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_locus1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_get_query_locus1_has_results_key(self, lookup_barcode_get_query_locus1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_locus1_jack_auth_response_data

    # POST query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_locus1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_locus1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_locus1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_locus1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_locus1_jack_auth_response_data(self, lookup_barcode_post_query_locus1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_locus1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on locus barcodes")
    def test_lookup_barcode_post_query_locus1_bad_auth_status(self, lookup_barcode_post_query_locus1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_locus1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_locus1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on locus barcodes")
    def test_lookup_barcode_post_query_locus1_jack_auth_status(self, lookup_barcode_post_query_locus1_jack_auth_response):
        assert lookup_barcode_post_query_locus1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_post_query_locus1_non_empty(self, lookup_barcode_post_query_locus1_jack_auth_response_data):
        assert lookup_barcode_post_query_locus1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_locus1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_post_query_locus1_has_records_key(self, lookup_barcode_post_query_locus1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_locus1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on locus barcodes")
    def test_lookup_barcode_post_query_locus1_has_results_key(self, lookup_barcode_post_query_locus1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_locus1_jack_auth_response_data

class TestLookupQueryCaseAssembly1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_assembly1_barcode = "SAL_IA5204AA_AS"
    lookup_query_assembly1 = lookup_query_base_url + "?barcode=" + lookup_query_assembly1_barcode
    lookup_barcode_query_assembly1 = lookup_query_base_url + "/" + lookup_query_assembly1_barcode

    # "Standard" lookup query with a assembly barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_assembly1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_assembly1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_assembly1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_assembly1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_assembly1_jack_auth_response_data(self, lookup_query_assembly1_jack_auth_response):
        return json.loads(lookup_query_assembly1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_assembly1_bad_auth_status(self, lookup_query_assembly1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_assembly1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_assembly1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on assembly barcodes")
    def test_lookup_query_assembly1_jack_auth_status(self, lookup_query_assembly1_jack_auth_response):
        assert lookup_query_assembly1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_query_assembly1_non_empty(self, lookup_query_assembly1_jack_auth_response_data):
        assert lookup_query_assembly1_jack_auth_response_data is not None
        assert len(lookup_query_assembly1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assemlby barcodes")
    def test_lookup_query_assembly1_has_records_key(self, lookup_query_assembly1_jack_auth_response_data):
        assert "records" in lookup_query_assembly1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_query_assembly1_has_results_key(self, lookup_query_assembly1_jack_auth_response_data):
        assert "results" in lookup_query_assembly1_jack_auth_response_data

    # GET and POST Lookup queries with a assembly barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_assembly1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_assembly1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_assembly1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_assembly1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_assembly1_jack_auth_response_data(self, lookup_barcode_get_query_assembly1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_assembly1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on assembly barcodes")
    def test_lookup_barcode_get_query_assembly1_bad_auth_status(self, lookup_barcode_get_query_assembly1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_assembly1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_assembly1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on assembly barcodes")
    def test_lookup_barcode_get_query_assembly1_jack_auth_status(self, lookup_barcode_get_query_assembly1_jack_auth_response):
        assert lookup_barcode_get_query_assembly1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_get_query_assembly1_non_empty(self, lookup_barcode_get_query_assembly1_jack_auth_response_data):
        assert lookup_barcode_get_query_assembly1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_assembly1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_get_query_assembly1_has_records_key(self, lookup_barcode_get_query_assembly1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_assembly1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_get_query_assembly1_has_results_key(self, lookup_barcode_get_query_assembly1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_assembly1_jack_auth_response_data

    # POST query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_assembly1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_assembly1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_assembly1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_assembly1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_assembly1_jack_auth_response_data(self, lookup_barcode_post_query_assembly1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_assembly1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on assembly barcodes")
    def test_lookup_barcode_post_query_assembly1_bad_auth_status(self, lookup_barcode_post_query_assembly1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_assembly1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_assembly1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on assembly barcodes")
    def test_lookup_barcode_post_query_assembly1_jack_auth_status(self, lookup_barcode_post_query_assembly1_jack_auth_response):
        assert lookup_barcode_post_query_assembly1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_post_query_assembly1_non_empty(self, lookup_barcode_post_query_assembly1_jack_auth_response_data):
        assert lookup_barcode_post_query_assembly1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_assembly1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_post_query_assembly1_has_records_key(self, lookup_barcode_post_query_assembly1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_assembly1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on assembly barcodes")
    def test_lookup_barcode_post_query_assembly1_has_results_key(self, lookup_barcode_post_query_assembly1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_assembly1_jack_auth_response_data

@pytest.mark.slow
class TestLookupQueryCaseTrace1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_trace1_barcode = "SAL_KA1677AA_TR"
    lookup_query_trace1 = lookup_query_base_url + "?barcode=" + lookup_query_trace1_barcode
    lookup_barcode_query_trace1 = lookup_query_base_url + "/" + lookup_query_trace1_barcode

    # "Standard" lookup query with a trace barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_trace1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_trace1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_trace1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_trace1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_trace1_jack_auth_response_data(self, lookup_query_trace1_jack_auth_response):
        return json.loads(lookup_query_trace1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_trace1_bad_auth_status(self, lookup_query_trace1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_trace1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_trace1_bad_auth_response.status_code == 401

    def test_lookup_query_trace1_jack_auth_status(self, lookup_query_trace1_jack_auth_response):
        assert lookup_query_trace1_jack_auth_response.status_code == 200

    def test_lookup_query_trace1_non_empty(self, lookup_query_trace1_jack_auth_response_data):
        assert lookup_query_trace1_jack_auth_response_data is not None
        assert len(lookup_query_trace1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    def test_lookup_query_trace1_has_records_key(self, lookup_query_trace1_jack_auth_response_data):
        assert "records" in lookup_query_trace1_jack_auth_response_data

    def test_lookup_query_trace1_has_results_key(self, lookup_query_trace1_jack_auth_response_data):
        assert "results" in lookup_query_trace1_jack_auth_response_data

    # GET and POST Lookup queries with a trace barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_trace1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_trace1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_trace1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_trace1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_trace1_jack_auth_response_data(self, lookup_barcode_get_query_trace1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_trace1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on trace barcodes")
    def test_lookup_barcode_get_query_trace1_bad_auth_status(self, lookup_barcode_get_query_trace1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_trace1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_trace1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on trace barcodes")
    def test_lookup_barcode_get_query_trace1_jack_auth_status(self, lookup_barcode_get_query_trace1_jack_auth_response):
        assert lookup_barcode_get_query_trace1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_get_query_trace1_non_empty(self, lookup_barcode_get_query_trace1_jack_auth_response_data):
        assert lookup_barcode_get_query_trace1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_trace1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_get_query_trace1_has_records_key(self, lookup_barcode_get_query_trace1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_trace1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_get_query_trace1_has_results_key(self, lookup_barcode_get_query_trace1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_trace1_jack_auth_response_data

    # POST query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_trace1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_trace1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_trace1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_trace1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_trace1_jack_auth_response_data(self, lookup_barcode_post_query_trace1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_trace1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on trace barcodes")
    def test_lookup_barcode_post_query_trace1_bad_auth_status(self, lookup_barcode_post_query_trace1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_trace1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_trace1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on trace barcodes")
    def test_lookup_barcode_post_query_trace1_jack_auth_status(self, lookup_barcode_post_query_trace1_jack_auth_response):
        assert lookup_barcode_post_query_trace1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_post_query_trace1_non_empty(self, lookup_barcode_post_query_trace1_jack_auth_response_data):
        assert lookup_barcode_post_query_trace1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_trace1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_post_query_trace1_has_records_key(self, lookup_barcode_post_query_trace1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_trace1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on trace barcodes")
    def test_lookup_barcode_post_query_trace1_has_results_key(self, lookup_barcode_post_query_trace1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_trace1_jack_auth_response_data


class TestLookupQueryCaseScheme1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_scheme1_barcode = "SAL_AA0010AA_SC"
    lookup_query_scheme1 = lookup_query_base_url + "?barcode=" + lookup_query_scheme1_barcode
    lookup_barcode_query_scheme1 = lookup_query_base_url + "/" + lookup_query_scheme1_barcode

    # "Standard" lookup query with a scheme barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_scheme1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_scheme1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_scheme1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_scheme1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_scheme1_jack_auth_response_data(self, lookup_query_scheme1_jack_auth_response):
        return json.loads(lookup_query_scheme1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_scheme1_bad_auth_status(self, lookup_query_scheme1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_scheme1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_scheme1_bad_auth_response.status_code == 401

    def test_lookup_query_scheme1_jack_auth_status(self, lookup_query_scheme1_jack_auth_response):
        assert lookup_query_scheme1_jack_auth_response.status_code == 200

    def test_lookup_query_scheme1_non_empty(self, lookup_query_scheme1_jack_auth_response_data):
        assert lookup_query_scheme1_jack_auth_response_data is not None
        assert len(lookup_query_scheme1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    def test_lookup_query_scheme1_has_records_key(self, lookup_query_scheme1_jack_auth_response_data):
        assert "records" in lookup_query_scheme1_jack_auth_response_data

    def test_lookup_query_scheme1_has_results_key(self, lookup_query_scheme1_jack_auth_response_data):
        assert "results" in lookup_query_scheme1_jack_auth_response_data

    # GET and POST Lookup queries with a scheme barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_scheme1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_scheme1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_scheme1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_scheme1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_scheme1_jack_auth_response_data(self, lookup_barcode_get_query_scheme1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_scheme1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on scheme barcodes")
    def test_lookup_barcode_get_query_scheme1_bad_auth_status(self, lookup_barcode_get_query_scheme1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_scheme1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_scheme1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on scheme barcodes")
    def test_lookup_barcode_get_query_scheme1_jack_auth_status(self, lookup_barcode_get_query_scheme1_jack_auth_response):
        assert lookup_barcode_get_query_scheme1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_get_query_scheme1_non_empty(self, lookup_barcode_get_query_scheme1_jack_auth_response_data):
        assert lookup_barcode_get_query_scheme1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_scheme1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_get_query_scheme1_has_records_key(self, lookup_barcode_get_query_scheme1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_scheme1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_get_query_scheme1_has_results_key(self, lookup_barcode_get_query_scheme1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_scheme1_jack_auth_response_data

    # POST query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_scheme1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_scheme1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_scheme1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_scheme1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_scheme1_jack_auth_response_data(self, lookup_barcode_post_query_scheme1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_scheme1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on scheme barcodes")
    def test_lookup_barcode_post_query_scheme1_bad_auth_status(self, lookup_barcode_post_query_scheme1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_scheme1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_scheme1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on scheme barcodes")
    def test_lookup_barcode_post_query_scheme1_jack_auth_status(self, lookup_barcode_post_query_scheme1_jack_auth_response):
        assert lookup_barcode_post_query_scheme1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_post_query_scheme1_non_empty(self, lookup_barcode_post_query_scheme1_jack_auth_response_data):
        assert lookup_barcode_post_query_scheme1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_scheme1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_post_query_scheme1_has_records_key(self, lookup_barcode_post_query_scheme1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_scheme1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on scheme barcodes")
    def test_lookup_barcode_post_query_scheme1_has_results_key(self, lookup_barcode_post_query_scheme1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_scheme1_jack_auth_response_data

@pytest.mark.slow
class TestLookupQueryCaseST1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_ST1_barcode = "SAL_AA0019AA_ST"
    lookup_query_ST1 = lookup_query_base_url + "?barcode=" + lookup_query_ST1_barcode
    lookup_barcode_query_ST1 = lookup_query_base_url + "/" + lookup_query_ST1_barcode

    # "Standard" lookup query with a ST barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_ST1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_ST1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_ST1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_ST1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_ST1_jack_auth_response_data(self, lookup_query_ST1_jack_auth_response):
        return json.loads(lookup_query_ST1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_ST1_bad_auth_status(self, lookup_query_ST1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_ST1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_ST1_bad_auth_response.status_code == 401

    def test_lookup_query_ST1_jack_auth_status(self, lookup_query_ST1_jack_auth_response):
        assert lookup_query_ST1_jack_auth_response.status_code == 200

    def test_lookup_query_ST1_non_empty(self, lookup_query_ST1_jack_auth_response_data):
        assert lookup_query_ST1_jack_auth_response_data is not None
        assert len(lookup_query_ST1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    def test_lookup_query_ST1_has_records_key(self, lookup_query_ST1_jack_auth_response_data):
        assert "records" in lookup_query_ST1_jack_auth_response_data

    def test_lookup_query_ST1_has_results_key(self, lookup_query_ST1_jack_auth_response_data):
        assert "results" in lookup_query_ST1_jack_auth_response_data

    # GET and POST Lookup queries with a ST barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_ST1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_ST1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_ST1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_ST1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_ST1_jack_auth_response_data(self, lookup_barcode_get_query_ST1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_ST1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on ST barcodes")
    def test_lookup_barcode_get_query_ST1_bad_auth_status(self, lookup_barcode_get_query_ST1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_ST1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_ST1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on ST barcodes")
    def test_lookup_barcode_get_query_ST1_jack_auth_status(self, lookup_barcode_get_query_ST1_jack_auth_response):
        assert lookup_barcode_get_query_ST1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_get_query_ST1_non_empty(self, lookup_barcode_get_query_ST1_jack_auth_response_data):
        assert lookup_barcode_get_query_ST1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_ST1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_get_query_ST1_has_records_key(self, lookup_barcode_get_query_ST1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_ST1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_get_query_ST1_has_results_key(self, lookup_barcode_get_query_ST1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_ST1_jack_auth_response_data

    # POST query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_ST1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_ST1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_ST1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_ST1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_ST1_jack_auth_response_data(self, lookup_barcode_post_query_ST1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_ST1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on ST barcodes")
    def test_lookup_barcode_post_query_ST1_bad_auth_status(self, lookup_barcode_post_query_ST1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_ST1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_ST1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on ST barcodes")
    def test_lookup_barcode_post_query_ST1_jack_auth_status(self, lookup_barcode_post_query_ST1_jack_auth_response):
        assert lookup_barcode_post_query_ST1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_post_query_ST1_non_empty(self, lookup_barcode_post_query_ST1_jack_auth_response_data):
        assert lookup_barcode_post_query_ST1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_ST1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_post_query_ST1_has_records_key(self, lookup_barcode_post_query_ST1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_ST1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on ST barcodes")
    def test_lookup_barcode_post_query_ST1_has_results_key(self, lookup_barcode_post_query_ST1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_ST1_jack_auth_response_data



class TestLookupQueryCaseAllele1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_allele1_barcode = "SAL_AP0358AA_AL"
    lookup_query_allele1 = lookup_query_base_url + "?barcode=" + lookup_query_allele1_barcode
    lookup_barcode_query_allele1 = lookup_query_base_url + "/" + lookup_query_allele1_barcode

    # "Standard" lookup query with a allele barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_allele1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_allele1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_allele1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_allele1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_allele1_jack_auth_response_data(self, lookup_query_allele1_jack_auth_response):
        return json.loads(lookup_query_allele1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_allele1_bad_auth_status(self, lookup_query_allele1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_allele1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_allele1_bad_auth_response.status_code == 401

    def test_lookup_query_allele1_jack_auth_status(self, lookup_query_allele1_jack_auth_response):
        assert lookup_query_allele1_jack_auth_response.status_code == 200

    def test_lookup_query_allele1_non_empty(self, lookup_query_allele1_jack_auth_response_data):
        assert lookup_query_allele1_jack_auth_response_data is not None
        assert len(lookup_query_allele1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    def test_lookup_query_allele1_has_records_key(self, lookup_query_allele1_jack_auth_response_data):
        assert "records" in lookup_query_allele1_jack_auth_response_data

    def test_lookup_query_allele1_has_results_key(self, lookup_query_allele1_jack_auth_response_data):
        assert "results" in lookup_query_allele1_jack_auth_response_data

    # GET and POallele Lookup queries with a allele barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_allele1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_allele1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_allele1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_allele1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_allele1_jack_auth_response_data(self, lookup_barcode_get_query_allele1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_allele1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on allele barcodes")
    def test_lookup_barcode_get_query_allele1_bad_auth_status(self, lookup_barcode_get_query_allele1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_allele1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_allele1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on allele barcodes")
    def test_lookup_barcode_get_query_allele1_jack_auth_status(self, lookup_barcode_get_query_allele1_jack_auth_response):
        assert lookup_barcode_get_query_allele1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_get_query_allele1_non_empty(self, lookup_barcode_get_query_allele1_jack_auth_response_data):
        assert lookup_barcode_get_query_allele1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_allele1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_get_query_allele1_has_records_key(self, lookup_barcode_get_query_allele1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_allele1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_get_query_allele1_has_results_key(self, lookup_barcode_get_query_allele1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_allele1_jack_auth_response_data

    # POallele query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_allele1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_allele1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_allele1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_allele1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_allele1_jack_auth_response_data(self, lookup_barcode_post_query_allele1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_allele1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on allele barcodes")
    def test_lookup_barcode_post_query_allele1_bad_auth_status(self, lookup_barcode_post_query_allele1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_allele1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_allele1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on allele barcodes")
    def test_lookup_barcode_post_query_allele1_jack_auth_status(self, lookup_barcode_post_query_allele1_jack_auth_response):
        assert lookup_barcode_post_query_allele1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_post_query_allele1_non_empty(self, lookup_barcode_post_query_allele1_jack_auth_response_data):
        assert lookup_barcode_post_query_allele1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_allele1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_post_query_allele1_has_records_key(self, lookup_barcode_post_query_allele1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_allele1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on allele barcodes")
    def test_lookup_barcode_post_query_allele1_has_results_key(self, lookup_barcode_post_query_allele1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_allele1_jack_auth_response_data


@pytest.mark.slow
class TestLookupQueryCaseStrain1(object):
    lookup_query_base_url = "/api/v2.0/lookup"
    lookup_query_strain1_barcode = "SAL_EA9564AA"
    lookup_query_strain1 = lookup_query_base_url + "?barcode=" + lookup_query_strain1_barcode
    lookup_barcode_query_strain1 = lookup_query_base_url + "/" + lookup_query_strain1_barcode

    # "Standard" lookup query with a strain barcode  
    @pytest.fixture(scope='class') 
    def lookup_query_strain1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_query_strain1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_strain1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_query_strain1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_query_strain1_jack_auth_response_data(self, lookup_query_strain1_jack_auth_response):
        return json.loads(lookup_query_strain1_jack_auth_response.get_data(as_text=True))

    def test_lookup_query_strain1_bad_auth_status(self, lookup_query_strain1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_query_strain1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_query_strain1_bad_auth_response.status_code == 401

    def test_lookup_query_strain1_jack_auth_status(self, lookup_query_strain1_jack_auth_response):
        assert lookup_query_strain1_jack_auth_response.status_code == 200

    def test_lookup_query_strain1_non_empty(self, lookup_query_strain1_jack_auth_response_data):
        assert lookup_query_strain1_jack_auth_response_data is not None
        assert len(lookup_query_strain1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    def test_lookup_query_strain1_has_records_key(self, lookup_query_strain1_jack_auth_response_data):
        assert "records" in lookup_query_strain1_jack_auth_response_data

    def test_lookup_query_strain1_has_results_key(self, lookup_query_strain1_jack_auth_response_data):
        assert "results" in lookup_query_strain1_jack_auth_response_data

    # GET and POstrain Lookup queries with a strain barcode embedded in the URL (rather than parameters
    # at the end
    # GET query first
    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_strain1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.lookup_barcode_query_strain1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_strain1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.lookup_barcode_query_strain1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_get_query_strain1_jack_auth_response_data(self, lookup_barcode_get_query_strain1_jack_auth_response):
        return json.loads(lookup_barcode_get_query_strain1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on strain barcodes")
    def test_lookup_barcode_get_query_strain1_bad_auth_status(self, lookup_barcode_get_query_strain1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_get_query_strain1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_get_query_strain1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on strain barcodes")
    def test_lookup_barcode_get_query_strain1_jack_auth_status(self, lookup_barcode_get_query_strain1_jack_auth_response):
        assert lookup_barcode_get_query_strain1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_get_query_strain1_non_empty(self, lookup_barcode_get_query_strain1_jack_auth_response_data):
        assert lookup_barcode_get_query_strain1_jack_auth_response_data is not None
        assert len(lookup_barcode_get_query_strain1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_get_query_strain1_has_records_key(self, lookup_barcode_get_query_strain1_jack_auth_response_data):
        assert "records" in lookup_barcode_get_query_strain1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_get_query_strain1_has_results_key(self, lookup_barcode_get_query_strain1_jack_auth_response_data):
        assert "results" in lookup_barcode_get_query_strain1_jack_auth_response_data

    # POstrain query 
    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_strain1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.post(self.lookup_barcode_query_strain1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_strain1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.post(self.lookup_barcode_query_strain1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def lookup_barcode_post_query_strain1_jack_auth_response_data(self, lookup_barcode_post_query_strain1_jack_auth_response):
        return json.loads(lookup_barcode_post_query_strain1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Lookup queries known to fail on strain barcodes")
    def test_lookup_barcode_post_query_strain1_bad_auth_status(self, lookup_barcode_post_query_strain1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert lookup_barcode_post_query_strain1_bad_auth_response.status_code == ebHTTPNotAuth
        assert lookup_barcode_post_query_strain1_bad_auth_response.status_code == 401

    @pytest.mark.skip(reason = "Lookup queries known to fail on strain barcodes")
    def test_lookup_barcode_post_query_strain1_jack_auth_status(self, lookup_barcode_post_query_strain1_jack_auth_response):
        assert lookup_barcode_post_query_strain1_jack_auth_response.status_code == 200

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_post_query_strain1_non_empty(self, lookup_barcode_post_query_strain1_jack_auth_response_data):
        assert lookup_barcode_post_query_strain1_jack_auth_response_data is not None
        assert len(lookup_barcode_post_query_strain1_jack_auth_response_data) > 0

# WVN 20/2/18 Not sure what the content should actually be
    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_post_query_strain1_has_records_key(self, lookup_barcode_post_query_strain1_jack_auth_response_data):
        assert "records" in lookup_barcode_post_query_strain1_jack_auth_response_data

    @pytest.mark.skip(reason = "Prerequisite lookup query known to fail on strain barcodes")
    def test_lookup_barcode_post_query_strain1_has_results_key(self, lookup_barcode_post_query_strain1_jack_auth_response_data):
        assert "results" in lookup_barcode_post_query_strain1_jack_auth_response_data
