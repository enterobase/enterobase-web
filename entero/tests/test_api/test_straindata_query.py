# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

# from urllib2 import HTTPError
# import urllib2
import json
import re

@pytest.mark.slow
class TestStraindataQueryCase1(object):
    straindata_query1 = "/api/v2.0/senterica/straindata?serotype=Agona&limit=50"
  
    @pytest.fixture(scope='class') 
    def straindata_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.straindata_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def straindata_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.straindata_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def straindata_query1_jack_auth_response_data(self, straindata_query1_jack_auth_response):
        return json.loads(straindata_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_straindata_query1_bad_auth_status(self, straindata_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert straindata_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert straindata_query1_bad_auth_response.status_code == 401

    def test_straindata_query1_jack_auth_status(self, straindata_query1_jack_auth_response):
        assert straindata_query1_jack_auth_response.status_code == 200

    def test_straindata_query1_non_empty(self, straindata_query1_jack_auth_response_data):
        assert straindata_query1_jack_auth_response_data is not None
        assert len(straindata_query1_jack_auth_response_data) > 0

    def test_straindata_query1_has_straindata_key(self, straindata_query1_jack_auth_response_data):
        assert "straindata" in straindata_query1_jack_auth_response_data

    def test_straindata_query1_has_links_key(self, straindata_query1_jack_auth_response_data):
        assert "links" in straindata_query1_jack_auth_response_data

    def test_straindata_query1_records_gt_0(self, straindata_query1_jack_auth_response_data):
        assert straindata_query1_jack_auth_response_data["links"]["records"] > 0
