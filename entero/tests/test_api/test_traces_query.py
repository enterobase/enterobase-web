# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

# from urllib2 import HTTPError
# import urllib2
import json
import re

class TestTracesQueryCase1(object):
    traces_query1 = "/api/v2.0/senterica/traces?barcode=SAL_KA1677AA_TR"
  
    @pytest.fixture(scope='class') 
    def traces_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.traces_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def traces_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.traces_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def traces_query1_jack_auth_response_data(self, traces_query1_jack_auth_response):
        return json.loads(traces_query1_jack_auth_response.get_data(as_text=True))

    def test_traces_query1_bad_auth_status(self, traces_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert traces_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert traces_query1_bad_auth_response.status_code == 401

    def test_traces_query1_jack_auth_status(self, traces_query1_jack_auth_response):
        assert traces_query1_jack_auth_response.status_code == 200

    def test_traces_query1_non_empty(self, traces_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        # responseData = json.loads(traces_query1_jack_auth_response.get_data(as_text=True))
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        # assert responseData is not None
        # assert len(responseData) > 0
        assert traces_query1_jack_auth_response_data is not None
        assert len(traces_query1_jack_auth_response_data) > 0

    def test_traces_query1_has_traces_key(self, traces_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        assert "Traces" in traces_query1_jack_auth_response_data

    def test_traces_query1_has_links_key(self, traces_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        assert "links" in traces_query1_jack_auth_response_data

class TestTracesBarcodeQueryCase1(object):
    traces_barcode_query1 = "api/v2.0/senterica/traces/SAL_KA1677AA_TR"
 
    # GET request 
    @pytest.fixture(scope='class') 
    def traces_barcode_get_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.get(self.traces_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def traces_barcode_get_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.get(self.traces_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def traces_barcode_get_query1_jack_auth_response_data(self, traces_barcode_get_query1_jack_auth_response):
        return json.loads(traces_barcode_get_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_traces_barcode_get_query1_bad_auth_status(self, traces_barcode_get_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert traces_barcode_get_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert traces_barcode_get_query1_bad_auth_response.status_code == 401

    def test_traces_barcode_get_query1_jack_auth_status(self, traces_barcode_get_query1_jack_auth_response):
        assert traces_barcode_get_query1_jack_auth_response.status_code == 200

    def test_traces_barcode_get_query1_non_empty(self, traces_barcode_get_query1_jack_auth_response_data):
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        assert traces_barcode_get_query1_jack_auth_response_data is not None
        assert len(traces_barcode_get_query1_jack_auth_response_data) > 0

    def test_traces_barcode_get_query1_has_traces_key(self, traces_barcode_get_query1_jack_auth_response_data):
        assert "Traces" in traces_barcode_get_query1_jack_auth_response_data

    @pytest.mark.skip(reason = "Known that test fails due to key being called paging inconsistently with the other type of query where it is called links")
    # def test_traces_barcode_get_query1_has_links_key(self, traces_query1_jack_auth_response):
    def test_traces_barcode_get_query1_has_links_key(self, traces_barcode_get_query1_jack_auth_response_data):
        # At present the relevant key is called "paging" in case of the assembly barcode GET request
        # and "links" (like the standard assembly query) in hte case of a POST request.
        assert "links" in traces_barcode_get_query1_jack_auth_response_data

    # POST request
    @pytest.fixture(scope='class') 
    def traces_barcode_post_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.post(self.traces_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def traces_barcode_post_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.post(self.traces_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def traces_barcode_post_query1_jack_auth_response_data(self, traces_barcode_post_query1_jack_auth_response):
        return json.loads(traces_barcode_post_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_traces_barcode_post_query1_bad_auth_status(self, traces_barcode_post_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert traces_barcode_post_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert traces_barcode_post_query1_bad_auth_response.status_code == 401

    def test_traces_barcode_post_query1_jack_auth_status(self, traces_barcode_post_query1_jack_auth_response):
        assert traces_barcode_post_query1_jack_auth_response.status_code == 200

    def test_traces_barcode_post_query1_non_empty(self, traces_barcode_post_query1_jack_auth_response_data):
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        assert traces_barcode_post_query1_jack_auth_response_data is not None
        assert len(traces_barcode_post_query1_jack_auth_response_data) > 0

    def test_traces_barcode_post_query1_has_traces_key(self, traces_barcode_post_query1_jack_auth_response_data):
        assert "Traces" in traces_barcode_post_query1_jack_auth_response_data

    # def test_traces_barcode_post_query1_has_links_key(self, traces_query1_jack_auth_response):
    def test_traces_barcode_post_query1_has_links_key(self, traces_barcode_post_query1_jack_auth_response_data):
        # At present the relevant key is called "paging" in case of the assembly barcode GET request
        # and "links" (like the standard assembly query) in the case of a POST request.
        assert "links" in traces_barcode_post_query1_jack_auth_response_data

    # PUT request
    @pytest.fixture(scope='class') 
    def traces_barcode_put_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.put(self.traces_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def traces_barcode_put_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.put(self.traces_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def traces_barcode_put_query1_jack_auth_response_data(self, traces_barcode_put_query1_jack_auth_response):
        return json.loads(traces_barcode_put_query1_jack_auth_response.get_data(as_text=True))

    def test_traces_barcode_put_query1_bad_auth_status(self, traces_barcode_put_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert traces_barcode_put_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert traces_barcode_put_query1_bad_auth_response.status_code == 401

    def test_traces_barcode_put_query1_jack_auth_status(self, traces_barcode_put_query1_jack_auth_response):
        # jack login is not an admin login so not allowed to do this
        # assert traces_barcode_put_query1_jack_auth_response.status_code == 403
        assert traces_barcode_put_query1_jack_auth_response.status_code == ebHTTPNotAuth

    # Keeping the code for these tests here for now from my earlier misunderstanding
    # because they could possibly be re-purposed for the testboss login
#    def test_traces_barcode_put_query1_non_empty(self, traces_barcode_put_query1_jack_auth_response_data):
#        # These two asserts really are checking pretty much the same thing - no
#        # point in breaking out into separate tests
#        assert traces_barcode_put_query1_jack_auth_response_data is not None
#        assert len(traces_barcode_put_query1_jack_auth_response_data) > 0
#
#    def test_traces_barcode_put_query1_has_traces_key(self, traces_barcode_put_query1_jack_auth_response_data):
#        assert "Traces" in traces_barcode_put_query1_jack_auth_response_data
#
#    def test_traces_barcode_put_query1_has_links_key(self, traces_barcode_put_query1_jack_auth_response_data):
#        # At present the relevant key is called "paging" in case of the assembly barcode GET request
#        # and "links" (like the standard assembly query) in the case of a POST request.
#        assert "links" in traces_barcode_put_query1_jack_auth_response_data TestAllelesQueryCase1(object):

# Possibly could add a parameterised test here which checks for a mismatch between
# the database and loci query results

