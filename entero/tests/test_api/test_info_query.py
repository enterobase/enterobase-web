# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
import json
import re

# Apparently having multiple asserts in a unit test is considered evil (because
# it's supposed to test exactly one thing) although some of the example code
# with tests in Flask (for test_minitwit.py) does exactly that.
# If I break asserts out into their own test functions/ methods then
# it will work better to put them all in the same class with e.g.
# the query response as a member.
# On the other hand, running to problems already with the asserts for return
# codes failing and then preventing me from seeing the outcome of later asserts
# so should be in separate tests.
class TestInfoRootQueryCase(object):
    info_root_query = "/api/v2.0"
  
    @pytest.fixture(scope='class') 
    def info_root_query_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.info_root_query, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def info_root_query_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.info_root_query, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def info_root_query_jack_auth_response_data_hash(self, info_root_query_jack_auth_response):
        responseData = json.loads(info_root_query_jack_auth_response.get_data(as_text=True))
        responseDataHash = enHashInfoQueryResponseData(responseData)
        return responseDataHash

    def test_info_root_query_bad_auth_status(self, info_root_query_bad_auth_response):
        # WVN 14/2/18
        # Query with a deliberately bad key and check status code
        # 401 correct status to get here rather than 403
        # assert info_root_query_bad_auth_response.status_code == ebHTTPNotAuth
        assert info_root_query_bad_auth_response.status_code == 401

    # def test_info_query(self, eb_api_client, eb_jack_api_key):
    def test_info_root_query_jack_auth_status(self, info_root_query_jack_auth_response):
    
        # Info query that should work
        # response = eb_api_client.get("/api/v2.0", headers=get_api_headers(eb_jack_api_key))
        assert info_root_query_jack_auth_response.status_code == 200
        # assert response.status_code == 200
    
    def test_info_root_query_jack_response_data_has_senterica_key(self, info_root_query_jack_auth_response_data_hash):
        # default info query gives results concerning all the available databases although
        # Swagger UI just doing this for Salmonella
        # Below approach for reading out the data used in examples does not work (since
        # response is in format due to Flask client rather than urllib2
        # responseData = json.load(response)
        # responseData = json.loads(response.get_data(as_text=True))
        # Trying strict=True because of warning message about schema - not sure if this is
        # where it is required.  (Doesn't work as argument of response.get_data...)
        # Doesn't work as argument to json.loads() either.
        # responseData = json.loads(info_root_query_jack_auth_response.get_data(as_text=True))
        # responseDataHash = enHashInfoQueryResponseData(json.loads(responseData.get_data(as_text=True)))
        # responseDataHash = enHashInfoQueryResponseData(responseData)
        #  assert "description" in responseData[0]
        # assert "senterica" in responseDataHash
        # assert "description" in responseDataHash["senterica"]
        assert "senterica" in info_root_query_jack_auth_response_data_hash

    def test_info_root_query_jack_response_data_senterica_has_description_key(self, info_root_query_jack_auth_response_data_hash):
        assert "description" in info_root_query_jack_auth_response_data_hash["senterica"]
