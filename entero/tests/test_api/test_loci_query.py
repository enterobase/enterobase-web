# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

# from urllib2 import HTTPError
# import urllib2
import json
import re

# Possibly could add a parameterised test here which checks for a mismatch between
# the database and loci query results
class TestLociQueryCaseSentericaAchtman(object):
    loci_query1 = "/api/v2.0/senterica/MLST_Achtman/loci?limit=50&scheme=MLST_Achtman"
  
    @pytest.fixture(scope='class') 
    def loci_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.loci_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def loci_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.loci_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def loci_query1_jack_auth_response_data(self, loci_query1_jack_auth_response):
        return json.loads(loci_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 403 when it should get status code 401")
    def test_loci_query1_bad_auth_status(self, loci_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert loci_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert loci_query1_bad_auth_response.status_code == 401

    def test_loci_query1_jack_auth_status(self, loci_query1_jack_auth_response):
        assert loci_query1_jack_auth_response.status_code == 200

    def test_loci_query1_non_empty(self, loci_query1_jack_auth_response_data):
        assert loci_query1_jack_auth_response_data is not None
        assert len(loci_query1_jack_auth_response_data) > 0

    def test_loci_query1_has_loci_key(self, loci_query1_jack_auth_response_data):
        assert "loci" in loci_query1_jack_auth_response_data

    def test_loci_query1_has_links_key(self, loci_query1_jack_auth_response_data):
        assert "links" in loci_query1_jack_auth_response_data

    def test_loci_query1_links_record_count(self, loci_query1_jack_auth_response_data):
        assert loci_query1_jack_auth_response_data["links"]["total_records"] == 7

    def test_loci_query1_loci_element_count(self, loci_query1_jack_auth_response_data):
        assert len(loci_query1_jack_auth_response_data["loci"]) == 7

class TestLociQueryCaseSentericaAchtmanMalformed(object):
    loci_query1 = "/api/v2.0/senterica/MLST_Achtman/loci?barcode=foobar&limit=50&scheme=MLST_Achtman"
  
    @pytest.fixture(scope='class') 
    def loci_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.loci_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def loci_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.loci_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def loci_query1_jack_auth_response_data(self, loci_query1_jack_auth_response):
        return json.loads(loci_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 403 when it should get status code 401")
    def test_loci_query1_bad_auth_status(self, loci_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert loci_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert loci_query1_bad_auth_response.status_code == 401

    def test_loci_query1_jack_auth_status(self, loci_query1_jack_auth_response):
        # 400 is status code for malformed request 
        assert loci_query1_jack_auth_response.status_code == 400
