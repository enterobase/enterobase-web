from flask import Blueprint
from flask import abort
from flask_login import current_user

adminbp = Blueprint('enteroadmin', __name__)

@adminbp.before_request
def restrict_bp_to_admins():
    if not current_user.is_authenticated() and not current_user.administrator:
        abort(403)


