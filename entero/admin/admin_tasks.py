from entero import app, dbhandle, db, config, get_database, rollback_close_system_db_session
from entero.databases.system.models import DatabaseInfo, UserJobs, User, UserPermissionTags, UserAutoCorrect, UserPreferences
from sqlalchemy import create_engine, not_, update, or_, and_
#from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import load_only
import requests
from requests.exceptions import ReadTimeout
import json
import datetime
import os
from sqlalchemy.sql.sqltypes import Integer, Float, String
from entero.cell_app.tasks import dictget
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import create_engine
import operator
from six import text_type

def job_report():
    weekago = datetime.datetime.now() - datetime.timedelta(days=30)
    week_jobs = db.session.query(UserJobs, User).filter(UserJobs.date_sent > weekago).join(User).all()
    app.logger.info('Number of jobs %d' %len(week_jobs))
    database = {}
    user_id = {}
    user_usage = {}
    pipeline = {}
    fget = open('/home/nabil/entero-web', 'wb')
    time = {} 
    for x,usr in week_jobs: 
        timetime = x.date_sent.strftime('%Y-%m-%d %H')
        if time.get(timetime):
            time[timetime] += 1 
        else:
            time[timetime] = 1 
        # Just get basic counts
        usernamestring = '%s.%s' %(usr.username, usr.institution)
        if database.get(x.database):
            database[x.database] += 1
        else:
            database[x.database] = 1 
        if user_id.get(usernamestring):
            user_id[usernamestring] += 1
        else:
            user_id[usernamestring] = 1 
        if pipeline.get(x.pipeline):
            pipeline[x.pipeline] += 1
        else:
            pipeline[x.pipeline] = 1
        usernamestring = '%s.%s' %(usr.username, usr.institution.encode("ascii", "ignore"))
        if user_usage.get(usernamestring):
            if user_usage[usernamestring].get(x.pipeline):
                user_usage[usernamestring][x.pipeline] += 1
            else:
                user_usage[usernamestring][x.pipeline] = 1
        else:
            user_usage[usernamestring] = {x.pipeline  :  1}
    fget.write( '\n\nDATABASE usage\nSpecies\tNo. of Jobs\n')
    for x,y in sorted(database.items(), key=operator.itemgetter(1), reverse=True ):
        fget.write( '%s\t%s\n' %(x,y))
    fget.write( '\n\nPipeline usage\nPipeline\tNo. of Jobs\n')
    for x,y in sorted(pipeline.items(), key=operator.itemgetter(1), reverse=True ):
        fget.write( '%s\t%s\n' %(x,y))
    fget.write( '\n\nUser usage\nUsername\tInstitution\tNo. of Jobs\n')
    for x,y in sorted(user_id.items(), key=operator.itemgetter(1), reverse=True ):
        fget.write( '%s\t%s\t%s\n' %(x.encode('ascii', 'ignore').split('.')[0],
                                   x.encode('ascii', 'ignore').split('.')[1], y))
        for a,b in sorted(user_usage[x.encode('ascii', 'ignore')].items(), key=operator.itemgetter(1), reverse=True ): 
            fget.write( '\t%s\t%s\n' %(a, b))            
    fget.write( '\n\nTime usage\nTimestamp\tNo. of Jobs\n')
    for x,y in sorted(time.items(), key=operator.itemgetter(0), reverse=True ):
        fget.write( '%s\t%s\n' %(x,y))            
    fget.close()
    
    app.logger.info('done')
            
def clean_metadata(database_name, option='all'):
    """
    Metadata should be updated with what is in NBCI, or autocorrected according to Autocorrect table, UNLESS the field (itself)
    has been edited already (by checking against strain_archive table).
    
    1. First build list of records to update, and which fields are safe for each one.
    2. Sync with NCBI - Effected records should go into 'Externally updated metadata..' workspace
    3. Apply auto-correct rules - Effected records should go into 'Autocorrected records..' workspace
    
    The following auto-generated workspaces also need to be updated:
    1. Possibly duplicated records
    2. Genera-wide representatives (for both schemes 7 gene and rMLST)
    3. Serotype conflicts 
    
    """
    accessions = [] 
    curr_db = get_database(database_name)
    if not curr_db:
        app.logger.warning("clean_metadata failed, could not find %s database"%database_name)
        return
    try:
        Strains =  curr_db.models.Strains
        workspaces_list = ['Possibly duplicated records',
                           'Genera-wide representative strains (7 Gene)',
                            'Genera-wide representative strains (rMLST)',
                            'Externally updated metadata records',
                            'Autocorrected records',
                            'Records with conflicting serotype',
                            'Paired SRA-Legacy records',
                            'STs without genomes (7 Gene)',
                            '7 Gene MLST conflicts with rMLST',
                            'Predicted serovar records',
                            'STs without public genomes (rMLST)']
        if option in ['all', 'uberstrains']:
            # Find duplicate records and populate a workspace.
            duplicates = find_duplicate_records(database_name)
            build_or_update_workspace(database_name, workspaces_list[0], 76, strain_ids = duplicates)

            # Find dataset of legacy records paired with SRA
            legacy_pairs = find_legacy_pairs(database_name)
            build_or_update_workspace(database_name, workspaces_list[6], 76, strain_ids = legacy_pairs)
        if option in ['all', 'representatives']:
            # Create Representative workspace
            mlst_rep, legacy_no_rep = find_scheme_repesentatives(database_name, 'MLST_Achtman')
            build_or_update_workspace(database_name, workspaces_list[1], 76, strain_ids = mlst_rep)
            build_or_update_workspace(database_name, workspaces_list[7], 76, strain_ids = legacy_no_rep)
            mlst_rep, legacy_no_rep = find_scheme_repesentatives(database_name, 'rMLST')
            build_or_update_workspace(database_name, workspaces_list[2], 76, strain_ids = mlst_rep)

        if option in ['all', 'serovar'] and database_name == 'senterica':
            # Serotypes with confliciting information.
            predicted_sero, no_good, rmlst_conflicts = bad_sero(database_name)
            build_or_update_workspace(database_name, workspaces_list[9], 76, strain_ids = predicted_sero)
            build_or_update_workspace(database_name, workspaces_list[5], 76, strain_ids = no_good)
            build_or_update_workspace(database_name, workspaces_list[8], 76, strain_ids = rmlst_conflicts)
        if option in ['all', 'autocorrect', 'sra_sync' ]:

            # Generate dictionaries of all records and columns that have not been user modified
            # Do NOT Override existing, explicit  user changes
            safe_dict_sra, safe_dict_user = fetch_updatable_records(database_name, lazy= True)

            # Update SRA derived records with external data (If required)
            if option in ['all', 'sra_sync']:
                updated = sync_sra_fields(database_name, safe_dict_sra)
                #updated = [1, 2, 3, 4, 5]
                build_or_update_workspace(database_name, workspaces_list[3], 76, strain_ids = updated, append=True)
            if option in ['all', 'autocorrect']:
                safe_dict_user.update(safe_dict_sra)
                auto_correct_strains = []
                for strain_change in curr_db.session.query(Strains).all():
                    rules = db.session.query(UserAutoCorrect).filter().all()
                    dirty  = False
                    for rule in rules:
                        if getattr(strain_change, rule.field) == rule.old_value:
                            if rule.field in safe_dict_user.get(strain_change.secondary_sample_accession, []):
                                setattr(strain_change, rule.field, rule.new_value)
                                dirty = True
                    if dirty:
                        auto_correct_strains.append(strain_change.id)
                        curr_db.update_record(76, strain_change, 'strains', False, commit=False)
                curr_db.session.commit()
                build_or_update_workspace(database_name, workspaces_list[4], 76, strain_ids = set(auto_correct_strains), append=True)
    except Exception as e:
        app.logger.exception("clean_metadata failed, error messsage: %s"%str(e))
        curr_db.rollback_close_session()
        rollback_close_system_db_session()



    
def bad_sero(database_name):
    no_good = [] 
    predicted_sero = [] 
    rmlst_conflicts = [] 
    curr_db = get_database(database_name)

    try:
        AssemblyLookup =  curr_db.models.AssemblyLookup
        Assembly =  curr_db.models.Assemblies
        Strains =  curr_db.models.Strains
        all_data = curr_db.session.query(AssemblyLookup, Strains, Assembly.status)\
            .filter(Strains.best_assembly == AssemblyLookup.assembly_id)\
            .filter(AssemblyLookup.scheme_id.in_([12, 3, 1]))\
            .filter(Strains.best_assembly == Assembly.id)\
            .options(Load(AssemblyLookup).load_only('other_data', 'scheme_id'),\
                     Load(Strains).load_only('id', 'serotype'))\
            .all()
        strain_table = {}
        all_updates = []
        for dat in all_data:
            if dat[2] == 'legacy':
                continue
            strain_record = dat[1]
            assembly_info = dat[0]
            assembly_results = assembly_info.other_data.get('results')
            if not strain_table.get(strain_record.id):
                strain_table[strain_record.id] = [strain_record.serotype, None, None, None]
                if not strain_table[strain_record.id][0]:
                    strain_table[strain_record.id][0] = ''
            if assembly_results:
                if assembly_info.scheme_id == 1:
                    strain_table[strain_record.id][1] = assembly_results.get('predicted_serotype', None)
                if assembly_info.scheme_id == 3:
                    strain_table[strain_record.id][2] = assembly_results.get('predicted_serotype', None)
                if assembly_info.scheme_id == 12:
                    strain_table[strain_record.id][3] = assembly_results.get('serovar', None)
        for strain_id in strain_table:
            # MLST is not rMLST.
            if strain_table[strain_id][1] != strain_table[strain_id][2] \
               or not strain_table[strain_id][1] or not strain_table[strain_id][2]:
                rmlst_conflicts.append(strain_id)
            # Clean up blank serovar field
            if (strain_table[strain_id][0] == '' or strain_table[strain_id][0] is None) \
               and len(set(strain_table[strain_id][1:])) == 1 \
               and strain_table[strain_id][2] is not None:
                updator = Strains.__table__.update()\
                    .where(Strains.__table__.c.get('id') == strain_id)\
                    .values(serotype = strain_table[strain_id][2] + ' (Predicted)')
                all_updates.append(updator)
                predicted_sero.append(strain_id)
            # Handle mismatching serovar for review
            if (strain_table[strain_id][0].replace(' (Predicted)','') != strain_table[strain_id][1]) \
               and len(set(strain_table[strain_id][1:])) > 1:
                no_good.append(strain_id)
        if len(all_updates) > 0 :
            for update in all_updates:
                curr_db.session.execute(update)
            curr_db.session.commit()
        return list(set(predicted_sero)), list(set(no_good)), list(set(rmlst_conflicts))
    except Exception as e:
        app.logger.exception("bad_sero failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
        return [], [], []


def find_scheme_repesentatives(database_name, scheme):
    curr_db = get_database(database_name)
    try:
        AssemblyLookup = curr_db.models.AssemblyLookup
        Strains = curr_db.models.Strains
        Assemblies = curr_db.models.Assemblies
        Traces = curr_db.models.Traces
        Schemes = curr_db.models.Schemes
        strain_ids = []
        legacy_ids = []
        scheme_barcodes_q = curr_db.session.query(AssemblyLookup.st_barcode).filter(Schemes.description == scheme)\
            .filter(AssemblyLookup.st_barcode.isnot(None)).join(Schemes).distinct().all()
        scheme_barcodes = [r[0] for r in scheme_barcodes_q]
        # Find complete genomes
        rep = curr_db.session.query(Strains.id, Strains.release_date, AssemblyLookup.st_barcode, Strains.strain)\
            .filter(Strains.best_assembly == Assemblies.id).join(Assemblies)\
            .filter(Strains.created < datetime.datetime.now().replace(day=1) )\
            .filter(Traces.seq_platform == 'Complete Genome').join(Traces)\
            .filter(AssemblyLookup.st_barcode.in_(scheme_barcodes)).join(AssemblyLookup)\
            .order_by(AssemblyLookup.st_barcode, Strains.release_date).all()
        barcode_dict_complete_st = {}
        for r in rep:
            if not barcode_dict_complete_st.get(r[2]):
                barcode_dict_complete_st[r[2]] = r[0]
                strain_ids.append(r[0])
        scheme_barcodes = [bar for bar in scheme_barcodes if bar not in barcode_dict_complete_st.keys()]
        # Check assembly quality.
        rep = curr_db.session.query(Strains.id, Assemblies.n50, AssemblyLookup.st_barcode, Strains.strain)\
            .filter(Strains.best_assembly == Assemblies.id).join(Assemblies)\
            .filter(Strains.created < datetime.datetime.now().replace(day=1) )\
            .filter(not_(and_(Traces.accession.like('traces%'), Traces.seq_platform == 'ILLUMINA'))).join(Traces)\
            .filter(AssemblyLookup.st_barcode.in_(scheme_barcodes)).join(AssemblyLookup)\
            .order_by(AssemblyLookup.st_barcode, Assemblies.n50).all()
        barcode_dict_size = {}
        for r in rep:
            if barcode_dict_size.get(r[2]):
                barcode_dict_size[r[2]].append( (r[0],r[1],r[3]) )
            else:
                barcode_dict_size[r[2]] = [ (r[0], r[1],r[3]) ]
        seen_names = []
        for size_list in barcode_dict_size:
            big_strain = (0,0)
            for r in barcode_dict_size[size_list]:
                if r[1] is not None and r[1] > big_strain[1] and r[2] not in seen_names:
                    big_strain = r
                    seen_names.append(r[2])
            if big_strain[0] == 0:
                legacy_ids.append(barcode_dict_size[size_list][0][0])
            else:
                strain_ids.append(big_strain[0])

        return strain_ids, legacy_ids
    except Exception as e:
        app.logger.exception("find_scheme_repesentatives, error message: %s"%str(e))
        curr_db.rollback_close_session()
        return [],[]

    
def update_custom_fields(database_name, file_path, column = "reBG & dominant serovar"):
    curr_db = get_database(database_name)
    if not curr_db:
        app.logger.warning("update_custom_fields failed, could not find % database"%database_name)
        return
    import csv
    if not os.path.exists(file_path):
        app.logger.warning("update_custom_fields failed, could not find % file"% file_path)
        return

    from sqlalchemy.orm.attributes import flag_modified
    try:
        with open(file_path) as f:
            update_dict = {}
            data = csv.DictReader(f, dialect=csv.excel_tab)
            for row in data:
                if not update_dict.get(row[column]):
                    update_dict[row[column]] = []
                update_dict[row[column]].append(row['Barcode'])


            Strains =  curr_db.models.Strains
            custom_id = str(db.session.query(UserPreferences.id,UserPreferences.name).filter(UserPreferences.name == column).first()[0])
            for key, value in iter(update_dict.items()):
                strain_list = curr_db.session.query(Strains).filter(Strains.barcode.in_(value)).all()
                for strain in strain_list:
                    if not strain.custom_columns:
                        strain.custom_columns = {}
                    if ' - ' not in strain.custom_columns[custom_id] and key != '':
                        pass
                    strain.custom_columns[custom_id] = key
                    if strain.custom_columns[custom_id] == '':
                        strain.custom_columns[custom_id] = 'Missing'
                    flag_modified(strain, 'custom_columns')
                    curr_db.session.add(strain)
                curr_db.session.commit()
    except Exception as e:
        app.logger.warning("update_custom_fields failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
        rollback_close_system_db_session()
    

def build_or_update_workspace(database_name, workspace_name, user_id, strain_ids = None, append = False):
    # Check if named workspace already exists, if not create.
    try:
        try:
            ws = db.session.query(UserPreferences).filter(UserPreferences.name == workspace_name)\
                .filter(UserPreferences.database == database_name)\
                .filter(UserPreferences.user_id == user_id).one()
        except NoResultFound:
            running_path = os.path.dirname(os.path.abspath(__file__))
            with open(os.path.join(running_path, 'template_workspace.json')) as f:
                ws = UserPreferences(user_id = user_id, name = workspace_name, type = 'main_workspace', database = database_name)
                ws.data = json.dumps(json.loads(f.read()))
                db.session.add(ws)

    
        data = json.loads(ws.data)
        # Overwrite strain IDs with new list if not None.
        if isinstance(strain_ids, list) and len(strain_ids) > 0:
            if  append:
                data['data']['strain_ids'] = list(set(strain_ids + data['data']['strain_ids']))
            else:
                data['data']['strain_ids'] = strain_ids
            ws.data = json.dumps(data)
            db.session.commit()
        elif len(data['data']['strain_ids']) < 1 :
            data['data']['strain_ids'] = [1]
            ws.data = json.dumps(data)
            db.session.commit()
        return ws.id

    except Exception as e:
        app.logger.exception("build_or_update_workspace failed, error message: %s"%str(e))
        rollback_close_system_db_session()


def find_legacy_pairs(database_name):
    curr_db = get_database(database_name)
    try:
        Strains =  curr_db.models.Strains
        Traces =  curr_db.models.Traces
        strain_ids = []
        legacy_records = curr_db.session.query(Strains.uberstrain).filter(Traces.seq_platform == '7GeneMLST').join(Traces).all()
        legacy_records_uberstrains = [r[0] for r in legacy_records]

        # Fetch all strains linked legacy strain uberstrains:
        # This will fetch legacy strains that are uberstrains (uberstrain will point to iteself, and include substrains)
        # This will fetch legacy strains that are substrain (uberstrain will point to SRA-record, and include substrains)
        x = curr_db.session.query(Strains.id, Strains.uberstrain).filter(Strains.uberstrain.in_(legacy_records_uberstrains)).all()
        temp_dict = {}
        if len(x) > 1:
            for z in x :
                if temp_dict.get(z[1]):
                    temp_dict[z[1]].append(z[0])
                else:
                    temp_dict[z[1]] = [z[0]]

        for z in temp_dict:
            if len(temp_dict[z]) > 1:
                strain_ids += temp_dict[z]
        return list(set(strain_ids))
    except Exception as e:
        app.logger.exception("find_legacy_pairs failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
        return []


    
    
def find_duplicate_records(database_name):
    strain_ids = [] 
    curr_db = get_database(database_name)
    if not database_name:
        app.logger.warning("find_duplicate_records failed, no databae is provided")

    try:
        Strains =  curr_db.models.Strains
        strain_names = curr_db.session.query(Strains.strain, Strains.id).filter(Strains.uberstrain == Strains.id).filter(Strains.strain.isnot(None)).all()
        strain_name_only = {}

        for strain_name in strain_names:
            if strain_name_only.get(strain_name[0]):
                strain_name_only[strain_name[0]].append(strain_name[1])
            else:
                strain_name_only[strain_name[0]] = [strain_name[1]]
        # Exact matching duplicates in the data
        for s in strain_name_only :
            if len(strain_name_only[s])  > 1:
                strain_ids += strain_name_only[s]
        return list(set(strain_ids))
    except Exception as e:
        app.logger.exception("find_duplicate_records failed, error message: %s"%str(e))
        curr_db.rollback_close_session()

def fetch_updatable_records(database_name, lazy = False): 
    strain_ids = [] 
    curr_db = get_database(database_name)
    if not curr_db:
        app.logger.warning("fetch_updatable_records failed, no database is provided")
        return {}, {}

    Strains = curr_db.models.Strains
    StrainsArchive = curr_db.models.StrainsArchive
    Traces = curr_db.models.Traces
    
    sra_dict = {}
    user_dict = {}
    try:
        if lazy:
            seq_records = curr_db.session.query(Strains).filter(Traces.seq_platform == 'ILLUMINA').join(Traces).filter(Strains.version == 1).all()
        else:
            seq_records = curr_db.session.query(Strains).filter(Traces.seq_platform == 'ILLUMINA').join(Traces).all()
        # Bug 1 :
        #seq_records = curr_db.session.query(Strains).filter(Strains.id==17352).filter(Traces.seq_platform == 'ILLUMINA').join(Traces).all()

        for seq_record in seq_records:
            column_names = curr_db.models.Strains.__table__.columns.keys()
            # Version is 1 so, has not been untouched, skip this part.
            # Record has been modified if version > 1
            if seq_record.version > 1:
                # Check Archive and adjust columns to update.
                archived = curr_db.session.query(StrainsArchive).filter(StrainsArchive.barcode == seq_record.barcode).filter(StrainsArchive.version == 1).first()
                if archived:
                    # Check if any values for columns had been modified.
                    for key in curr_db.models.Strains.__table__.columns.keys():
                        if getattr(archived, key) != getattr(seq_record, key):
                            column_names.remove(key)
                else:
                    column_names = []
            if seq_record.secondary_sample_accession and (seq_record.secondary_sample_accession.startswith('ERS') \
               or seq_record.secondary_sample_accession.startswith('SRS') \
               or seq_record.secondary_sample_accession.startswith('DRS')) :
                sra_dict[seq_record.secondary_sample_accession] = column_names
            else:
                user_dict[seq_record.barcode] = column_names

        return sra_dict, user_dict
    except Exception as e:
        app.logger.exception("fetch_updatable_records failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
        return {},{}

def sync_sra_fields(database_name, sra_dict):
    strain_ids = []
    curr_db = get_database(database_name)
    if not curr_db:
        app.logger.warning("sync_sra_fields failed, no valid database name is provided")
        return strain_ids
    DP = curr_db.models.DataParam
    Strains = curr_db.models.Strains
    dataparms = curr_db.session.query(DP.name, DP.sra_field).filter(DP.tabname =='strains').filter(DP.sra_field.isnot(None)).all()
    all_column_names = [r[0] for r in dataparms]
    data_param_dict = {}
    for x in dataparms:
        data_param_dict[x[0]] = x[1].split(',')
    data_param_dict.pop('release_date')
    data_param_dict.pop('collection_time')
    chunks = [sra_dict.keys()[x:x+50] for x in range(0, len(sra_dict), 50)]
    
    for chunk in chunks:
        URI = config.Config.DOWNLOAD_URI + '/metadata'
        params = dict(
            sample = ','.join(chunk)
        )        
        try:
            resp = requests.get(url = URI, params = params, timeout=200)
            if resp.status_code == 500:
                app.logger.error('Could not access metadata')
            elif resp.status_code == 404:
                app.logger.error('No response from NCBI')
            else:
                data = dict(error = 'No response')
                try:
                    data = json.loads(resp.text)
                except:
                    continue
                if isinstance(data,dict):
                    if data.get("error"):
                        continue
                if len (data) > 0:
                    all_updates = [] 
                    for rec in data:
                        acc = dictget(rec,["Sample","SRA Accession"])
                        if not acc:
                            continue
                        strain_list = curr_db.session.query(Strains).filter(Strains.secondary_sample_accession == acc).all()
                        for strain in strain_list:
                            dirty = False
                            change = {}
                            for column in sra_dict[acc]:
                                if data_param_dict.get(column):
                                    value = dictget(rec, list(data_param_dict[column]))
                                    goodvalue = False
                                    if not value:
                                        updator = strain.__table__.update()\
                                            .where(strain.__table__.c.get('id') == strain.id)\
                                            .values(column = None)
                                        all_updates.append(updator)
                                        continue
                                    # Handle Int type
                                    try:
                                        if strain.__table__.columns.get(column).type is Integer:
                                            if getattr(strain, column) != int(value):
                                                value = int(value)
                                                goodvalue = True
                                    except ValueError:
                                        continue
                                    # Handle Float type 
                                    if  strain.__table__.columns.get(column).type  is Float:
                                        if getattr(strain, column) != float(value):
                                            value = float(value)
                                            goodvalue = True
                                    # Handle String type
                                    if  strain.__table__.columns.get(column).type  is String:
                                        if str(getattr(strain, column)) != str(value):
                                            value = str(value)
                                            goodvalue = True
                                    if goodvalue:
                                        if (column == 'serotype' and (value != '' or value is not None)) \
                                           or column != 'serotype':
                                            #change[column] = getattr(strain, column)
                                            #change['%s-type'%column] = type(getattr(strain, column))
                                            updator = strain.__table__.update()\
                                                .where(strain.__table__.c.get('id') == strain.id)\
                                                .values(column = value)
                                            #change['%s-new' %column] = value
                                            #change['%s-newtype'%column] = type(value)
                                            all_updates.append(updator)
                                            dirty = True
                            if dirty:
                                strain_ids.append(strain.id)
                    if len(all_updates) > 0 :
                        for update in all_updates:
                            try:
                                update.execute()
                            except: 
                                pass
                        curr_db.session.commit()
        except ReadTimeout:
            continue
    return set(strain_ids)

#The url /NServ/initiate.api/%s/init_taxon no longer exists
# """ Create databases from database_info in postgres and Nserv if it does not exist"""
# def sync_databases():
#     app.logger.info('Syncing Databases')
#     # response= requests.get('http://137.205.52.26/NServ/query.api')
#     response= requests.get(app.config['NSERV_ADDRESS'] + 'query.api')
#     nserv_db = json.loads(response.text)
#     keycodes = []
#     try:
#         for keys in app.config['ACTIVE_DATABASES']:
#             engine_string =  'postgresql://%s:%s@%s/%s'%(app.config['USER'], app.config['PASS'], app.config['POSTGRES_SERVER'], keys)
#             engine = create_engine(engine_string)
#             keycodes.append(app.config['ACTIVE_DATABASES'][keys][4])
#             #if not database_exists(engine.url):
#                 #create_database(engine.url)
#                 #dbhandle[keys].create_db()
#                 #app.logger.info('Created DB %s : %s ' % (keys, database_exists(engine.url)) )
#                 ## Import SRA data here.
#             #else:
#                 #app.logger.info('Database exists %s : %s ' % (keys, database_exists(engine.url)) )
#         missing = ','.join(set(keycodes).difference(nserv_db.keys()))
#         app.logger.info('Databases missing from Nserv : %s ' % missing )
#         for missing_db in set(keycodes).difference(nserv_db.keys()):
#             database = None
#             for keys in app.config['ACTIVE_DATABASES']:
#                 if app.config['ACTIVE_DATABASES'][keys][4] == missing_db:
#                     database = keys
#             if database:
#                 values = app.config['ACTIVE_DATABASES'][database]
#                 payload = dict(fullname=values[0], prefix=values[4], curators='Nabil Alikhan', password=app.config['PASS'])
#                 r = requests.post('http://137.205.52.26/NServ/initiate.api/%s/init_taxon' % database, data=payload)
#                 app.logger.info(r.text)
#         missing = ','.join(set(nserv_db.keys()).difference(keycodes))
#         app.logger.info('Databases missing from Enterobase : %s ' % missing )
#
#         # Populate database info
#         db.session.execute("SELECT setval('database_info_id_seq', MAX(id)) FROM database_info;")
#
#         for key in app.config['ACTIVE_DATABASES']:
#             result = db.session.query(DatabaseInfo).filter(DatabaseInfo.name == key).first()
#             if result == None:
#                 result = DatabaseInfo(name=key)
#             values = app.config['ACTIVE_DATABASES'][key]
#             result.description = values[0]
#             result.prefix = values[4]
#             result.val =  '"' + str(app.config['TABLE_CODE']) + '"'
#             result.curator = values[3]
#             result.public = int(values[2])
#             db.session.add(result)
#
#         db.session.commit()
#     except Exception as e:
#         rollback_close_system_db_session()
#         app.logger.exception("sync_databases failed, error message: %s"%str(e))
    
    
""" Create schemes from database_info in postgres and Nserv if it does not exist"""
def sync_schemes(key):
    # Default experimental data is assembly_stats and rMLST
    curr_db =get_database(key)
    if not curr_db:
        app.logger.warning("sync_schemes failed, could not find %s database"%key)
        return

    app.logger.info('Syncing Schemes...')
    
    schemes_config = app.config['DATABASE_SCHEMES'][key]
    try:
        curr_db.session.execute("SELECT setval('schemes_id_seq', MAX(id)) FROM schemes;")
        curr_db.session.commit()
        Schemes = curr_db.models.Schemes
        count =  curr_db.session.query(Schemes).count()
        for scheme_config in schemes_config:
            result = curr_db.session.query(Schemes).filter(Schemes.description == scheme_config['description']).first()
            if result == None:
                result = Schemes(description=scheme_config['description'])
                result.id = count + 1 
            if result.created == '' or result.created == None: result.created = datetime.datetime.utcnow()
            if result.lastmodifiedby == '' or result.lastmodifiedby == None: result.lastmodifiedby = 0
            if result.lastmodifiedby == '' or result.lastmodifiedby == None: result.lastmodifiedby = datetime.datetime.utcnow()
            for att in scheme_config:
                setattr(result, att, scheme_config[att])
            curr_db.session.add(result)
            curr_db.session.commit()
        curr_db.session.close()
    except Exception as e:
        app.logger.exception("sync_schemes failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
'''
""" For each database check strains are unassembled, if so send for assembly """
def check_unassembled(key):
    from entero.cell_app.tasks import send_assembly_job
    import psycopg2
    from psycopg2.extras import RealDictCursor
    
    conn = None
    try:
        #no Assembly
        SQL = "SELECT traces.id as tid FROM strains INNER JOIN traces on traces.strain_id = strains.id  AND traces.seq_platform = 'ILLUMINA' WHERE best_assembly IS NULL"
        #no Scheme info
        #SQL ="SELECT traces.id as tid FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Assembled' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id  LEFT  JOIN st_assembly ON st_assembly.assembly_id= best_assembly WHERE  st_assembly.st_id  IS NULL"
        #Reason failed is null
        #SQL ="SELECT strain,traces.id as tid,other_data,job_id FROM strains INNER JOIN  assemblies on strains.best_assembly = assemblies.id  AND assemblies.status = 'Failed QC' INNER JOIN trace_assembly ON trace_assembly.assembly_id = strains.best_assembly INNER JOIN traces on traces.id = trace_assembly.trace_id WHERE other_data ILIKE '%[null]%'  ORDER BY job_id DESC"
        
        conn = psycopg2.connect(app.config['ACTIVE_DATABASES'][key][1])
        cursor = conn.cursor(cursor_factory=RealDictCursor)    
        cursor.execute(SQL)
        results = cursor.fetchall()
        for res in results:
            send_assembly_job([res['tid']], key)
        conn.close()

    except Exception as e:
        if conn:
            conn.rollback()
            conn.close()
'''
'''   
def clean_assemblies(key):
    app.logger.info('Checking for unassembled records or records requiring reassembly...')
# Check lastmodifiedby, coverage is filled, file_pointer is valid. Pipeline version.

    curr_db = get_database(key)
    if not curr_db:
        app.logger.warning("clean_assemblies failed, could not find %s database"%key)
    Assemblies = curr_db.models.Assemblies
    Strains = curr_db.models.Strains
    try:
        AssemblyLookup = curr_db.models.AssemblyLookup
        curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.st_barcode == 'present').delete()
        curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.st_barcode == 'Present').delete()
        curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.st_barcode == 'unaccepted').delete()
        curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.st_barcode == 'Duplicated').delete()
        assembled = curr_db.session.query(Assemblies).filter(Assemblies.status=='Assembled').filter(Assemblies.other_data == None).order_by(Assemblies.barcode).all()
        count = 0
        for result in assembled:
            if curr_db.session.query(exists().where(Strains.best_assembly==result.id)).scalar():
                if result.created == '' or result.created == None: result.created = datetime.datetime.utcnow()
                if result.lastmodifiedby == '' or result.lastmodifiedby == None: result.lastmodifiedby = 0
                if result.lastmodifiedby == '' or result.lastmodifiedby == None: result.lastmodifiedby = datetime.datetime.utcnow()
                # Validate file_pointer, move if required
                #/share_space/assembly/job_cat/job_id
                valid_file_pointer = '/share_space/assembly/%d/%d/%s.scaffold.fastq'% (result.job_id / 1000, result.job_id, result.barcode)
                if not os.path.exists(result.file_pointer):
                    app.logger.info('ERROR: Assembly file missing %s' % result.barcode)
                    result.other_data = 'DELETE'
                    curr_db.session.add(result)
                elif valid_file_pointer != result.file_pointer:
                    app.logger.info('Assembly is in invalid location %s ' %result.barcode)
                    parent_dir =  '/share_space/assembly/%d' % (result.job_id / 1000)
                    if not os.path.exists(parent_dir): os.mkdir(parent_dir)
                    assembly_dir =  '/share_space/assembly/%d/%d/'% (result.job_id / 1000, result.job_id)
                    if not os.path.exists(assembly_dir): os.mkdir(assembly_dir)
                    shutil.copy2( result.file_pointer, valid_file_pointer)
                    shutil.copy2( result.file_pointer[:-1]+'a', valid_file_pointer[:-1]+'a')
                    shutil.copy2( os.path.dirname(result.file_pointer) +'/err_file', os.path.dirname(valid_file_pointer) +'/err_file')
                    shutil.copy2( os.path.dirname(result.file_pointer) +'/log_file', os.path.dirname(valid_file_pointer) +'/log_file')
                    result.file_pointer = valid_file_pointer
                    result.file_name = os.path.basename(valid_file_pointer)
                    if os.path.exists(result.file_pointer):
                        app.logger.info('Assembly file moved %s' % result.barcode)
                        curr_db.session.add(result)
                        curr_db.session.commit()
                    else:
                        app.logger.info('ERROR: Assembly file not moved  %s' % result.barcode)
            else:
                # Assembly is not in best assembly, remove.
                valid_file_pointer = '/share_space/assembly/%d/%d/%s.scaffold.fastq'% (result.job_id / 1000, result.job_id, result.barcode)
                if os.path.exists(valid_file_pointer):
                    # Remove existing files.
                    shutil.rmtree(os.path.dirname(valid_file_pointer))
                    app.logger.info('ERROR: Assembly file not required, deleting  %s' % result.barcode)
                app.logger.info('ERROR: Assembly record is not a best_assembly, marking for deletion (other_data = DELETE)  %s' % result.barcode)
                result.other_data = 'DELETE'
                # Delete assembly lookup records.
                curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.assembly_id == result.id).delete()
                curr_db.session.add(result)
                count += 1
                if count == 1000:
                    curr_db.session.commit()
                    count = 0
        queued = curr_db.session.query(Assemblies).filter(Assemblies.status=='Queued').order_by(Assemblies.barcode).all()
        jobs = []
        for result in queued:
            jobs.append(result.job_id)
        for job_id in jobs:
            process_job(job_id)
        #curr_db.session.commit()
        failed = curr_db.session.query(Assemblies).filter(Assemblies.status=='Failed QC').order_by(Assemblies.barcode).all()
        for result in failed:
            pass

        from sqlalchemy.sql.expression import func,text
        dbase = dbhandle[key]
        Traces = dbase.models.Traces
        Strains = dbase.models.Strains
        trace_ids = []
        user_trade_ids = []
        recs = dbase.session.query(Traces,Strains).join(Strains,Traces.strain_id==Strains.id).filter(Strains.best_assembly ==None).order_by(func.random())
        for rec in recs:
            if rec.Traces.seq_platform == 'ILLUMINA':
                if len(rec.Traces.assemblylist) > 1:
                    app.logger.info('ERROR: multiple assemblies %s' % rec.Strains.barcode)
                elif len(rec.Traces.assemblylist) > 0:
                    ass =  rec.Traces.assemblylist[0]
                    if not (ass.status == "Assembled" and ass.status == "Failed QC" and ass.status == "Queued"):
                        app.logger.info('Incomplete: Will assembly  %s' % rec.Strains.barcode)
                        if rec.Traces.file_pointer == None:
                            trace_ids.append(rec.Traces.id)
                        else:
                            user_trade_ids.append(rec.Traces.id)
                elif len(rec.Traces.assemblylist) == 0:
                    app.logger.info('No assembly found; Will assembly  %s' % rec.Strains.barcode)
                    if hasattr(rec.Traces, 'file_pointer'):
                        if rec.Traces.file_pointer == None:
                            trace_ids.append(rec.Traces.id)
                        else:
                            user_trade_ids.append(rec.Traces.id)

        for ids in trace_ids:
            send_assembly_job([ids],key)
        for ids in user_trade_ids:
            send_assembly_job([ids],key, user_reads=True)
        #dbase.session.rollback()
       # curr_db.session.commit()
    except Exception as e:
        app.logger.warning("clean_assemblies failed, error message: %s"%str(e))
        curr_db.rollback_close_session()
'''

def create_strain_groups():
    app.logger.info('Build strain groups... Not implemented')

    
def check_missing_metadata():
    # find blank metadata fields and backfill if possible. 
    app.logger.info('Checking for missing metadata... Not implemented')
    # For each database, retrieve metadata fields
    
    # Check if values are missing.
    
    # Check missing  values on metadata server.  and backfill

def update_mcnally():
    direct = '/var/www/entero/McNally_strains.txt' 
    param_file = os.path.join(direct)
    param_handle = open(param_file, 'r')
    dbname = 'yersinia'
    curr_db = get_database(dbname)
    if not curr_db:
        app.logger.warning("update_mcnally failed, no valid database name is provided")
        return
    try:
        Assemblies = curr_db.models.Assemblies
        Traces = curr_db.models.Traces

        AssemblyLookup = curr_db.models.AssemblyLookup

        Strains = curr_db.models.Strains
        for res in curr_db.session.query(Strains, Traces).filter(Strains.comment.like("%McNally%")).filter(Traces.strain_id == Strains.id).all():
            curr_db.session.delete(res[0])
            curr_db.session.delete(res[1])
        for res in curr_db.session.query(Assemblies).filter(Assemblies.other_data.like("%McNally%")).all():
            curr_db.session.delete(res)
        curr_db.session.commit()
        import re
        for insert in param_handle.readlines()[1:]:
        #Add metadata

            insert_array = insert.split('\t')
            new_assembly = Assemblies(user_id =1, status='legacy', other_data='McNally')
            curr_db.session.add(new_assembly)
            curr_db.session.commit()
            new_assembly.barcode = curr_db.encode(new_assembly.id, dbname, 'assemblies')

            curr_db.session.add(new_assembly)
            curr_db.session.commit()

            new_strain = Strains(best_assembly=new_assembly.id,
                                 strain=insert_array[1],
                                 alternative_name=insert_array[2],
                                 species=insert_array[3],
                                 biogroup=insert_array[4],
                                 serotype=insert_array[5],
                                 source_niche=insert_array[7],
                                 source_type=insert_array[8],
                                 source_details=insert_array[9],
                                 country=insert_array[10],
                                 admin1=insert_array[11],
                                 disease=insert_array[12],
                                 created=insert_array[14],
                                 lastmodifiedby=1,
                                 comment='McNally ' + text_type(insert_array[13], errors='replace'),
                                 )
            if isinstance(insert_array[6], int):
                new_strain.collection_year = int(insert_array[6])
            new_strain.sample_accession='NA'
            new_strain.secondary_sample_accession='NA'
            new_strain.study_accession='NA'
            new_strain.secondary_study_accession='NA'
            new_strain.verified= 1

            curr_db.session.add(new_strain)
            curr_db.session.commit()

            new_strain.barcode = curr_db.encode(new_strain.id, dbname, 'strains')
            curr_db.session.add(new_strain)
            curr_db.session.commit()

            new_genome = Traces(strain_id=new_strain.id, status='Legacy',
                                   seq_platform='7GeneMLST', seq_version=1,
                                   seq_library='ND',experiment_accession= 'NA')
            curr_db.session.add(new_genome)
            curr_db.session.commit()

            new_genome.barcode = curr_db.encode(new_genome.id, dbname, 'traces')
            xx = re.match('.+Sender: (.+), (.+)', text_type(insert_array[13], errors='replace'))
            if xx:
                new_strain.contact_name = xx.groups()[0]
                new_strain.contact = xx.groups()[1]

            curr_db.session.add(new_strain)
            curr_db.session.add(new_genome)

        # Find ST barcode for ST number
            response= requests.get('https://137.205.52.26/NServ/search.api/Yersinia/McNally/STs?ST_id=%s' %insert_array[22].strip())
            nserv_db = json.loads(response.text)

            new_lookup = AssemblyLookup(st_barcode=nserv_db[0]['barcode'], assembly_id = new_assembly.id, version = '3.0', scheme_id = 4)
            curr_db.session.add(new_lookup)
            curr_db.session.commit()
    except Exception as e:
        app.logger.exception("update_mcnally failed, error message: %s"%str(e))
        curr_db.rollback_close_session()

#It will fail when called with args why setting the default valuess None
# I have deleted None deafukt values

def add_scheme_info(database, file):
    app.logger.info('Refresh scheme info in data_params...')
    curr_db = get_database(database)
    if not curr_db:
        app.logger.warning("add_scheme_info failed, no valid database name is provided")
    try:
        curr_db.session.execute("SELECT setval('data_param_id_seq', MAX(id)) FROM data_param;")
        DataParam = curr_db.models.DataParam

        with open(file) as f:
            for line in f.readlines()[1:]:
                line = line.strip()
                locus_tag = line.split('\t')[0]
                gene_name = line.split('\t')[4]
                description = line.split('\t')[5]
                # count = curr_db.session.query(DataParam).filter(DataParam.tabname == 'wgMLST').filter(DataParam.group_name == 'Locus').count()
                # results = curr_db.session.query(DataParam).filter(DataParam.name == locus_tag).filter(DataParam.tabname == 'wgMLST').all()
                # # wgMLST may not be in dataParams
                #  # {'default_value': None, 'required': None, 'name': u'SC0831', 'allow_multiple_values': None, 'datatype': u'integer', 'max_value': None, 'display_order': 10, 'min_value': None, 'sra_field': None, 'mlst_field': None, 'param': None, 'label': u'SC0831', 'nested_order': 6, 'tabname': u'cgMLST', 'allow_pattern': None, 'vals': None, 'max_length': None, 'no_duplicates': None, 'group_name': u'Locus', 'id': 239, 'description': None}
                # if len(results) == 0:
                #     result = DataParam(tabname='wgMLST', name=locus_tag, \
                #                        label=locus_tag, \
                #                        datatype='integer', \
                #                        nested_order = int(count+1), \
                #                        display_order=10,
                #                        group_name='Locus')
                #     curr_db.session.add(result)
                #     curr_db.session.commit()
                results = curr_db.session.query(DataParam).filter(DataParam.name == locus_tag).all()
                for result in results:
                    if len(gene_name) > 0 and gene_name != '-' :
                        result.label  = '%s (%s)' %(locus_tag, gene_name)
                    if gene_name == '-':
                        result.label  = '%s' %(locus_tag)
                    if len(description) > 0 :
                        result.description = description.replace('"', '')
                    curr_db.session.add(result)
            curr_db.session.commit()

    except Exception as e:
        app.logger.exception("add_scheme_info failed, error message: %s"%str(e))
        curr_db.rollback_close_session


def refresh_dataparms(key):
    app.logger.info('Refresh data_params table for each database...')
    curr_db = dbhandle[key]
    # Get Schemes
    try:
        Schemes = curr_db.models.Schemes
        scheme_list = curr_db.session.query(Schemes).filter(Schemes.description != 'assembly_stats').filter(Schemes.description != 'CRISPR').filter(Schemes.description != 'CRISPOL').all()
        DataParam = curr_db.models.DataParam
        curr_db.session.execute("SELECT setval('data_param_id_seq', MAX(id)) FROM data_param;")
        for scheme in scheme_list:
            if scheme.param.get('display') == 'none' or scheme.param.get('scheme') is None:
                continue
            address = scheme.param.get('scheme').split('_')
            # Update Loci from NServ API
            
            response = requests.get(app.config['NSERV_ADDRESS']+'query.api/%s/%s/loci' % (address[0], address[1]))
            nserv_db = json.loads(response.text)
            count = 1
            for locus in nserv_db:
                response= requests.get(app.config['NSERV_ADDRESS']+'query.api/%s/%s/loci/%s' % (address[0], address[1], locus['index_id']))
                locus_info = json.loads(response.text)[0]
                result = curr_db.session.query(DataParam).filter(DataParam.tabname == scheme.description).filter(DataParam.name == locus_info['locus']).first()
                if locus_info['info'].has_key('common_name'):
                    label = '%s (%s)' %(locus_info['locus'], locus_info['info']['common_name'])
                else:
                    label =  locus_info['locus']
                if result:
                    result.label = label
                else:
                    result = DataParam(tabname=scheme.description, name=locus_info['locus'], \
                                       label=label, \
                                       datatype='integer', \
                                       nested_order = count, \
                                       display_order=10,
                                       group_name='Locus')
                curr_db.session.add(result)
                count += 1
            curr_db.session.commit()
        
        if curr_db.models.Base.metadata.tables.has_key('data_param'):
            ## first load the generic data params
            all_params = [] 
            
            for param_file in [ '/var/www/entero/entero/databases/%s/data_params.txt' % key]:
                all_params += open(param_file, 'r').readlines()[1:]
                headers = open(param_file, 'r').readlines()[0].split('\t')
            pos_to_header ={}
            pos = 0
            for header in headers:
                pos_to_header[pos] = header
                pos+=1
            for insert in all_params:
                values = insert.split('\t')
                result = curr_db.session.query(DataParam).filter(DataParam.tabname == values[0]).filter(DataParam.name == values[1]).first()
                if result == None:
                    new_param = DataParam()
                    pos = 0 
                    for value in values:
                        value = text_type(value.strip())
                        if not value:
                            pos += 1 
                            continue
                        field = pos_to_header[pos]
                        try:
                            value = int(value)
                        except ValueError:
                            pass
                        setattr(new_param,field,value)
                        pos +=1 
                    curr_db.session.add(new_param)
                    curr_db.session.commit()
            #for insert in param_handle:
                #if  first:
                    #headers = insert.strip().split('\t')
                    #pos=0
                    #for header in headers:
                        #pos_to_header[pos]=header
                        #pos+=1
                    #first=False
                #else:
                    #in_array = insert.split('\t')
                    #new_param = DataParam()
                    #pos=0
                    #for val in in_array:
                        #val=unicode(val.strip())
                        #if not val:
                            #pos+=1
                            #continue
                        #field = pos_to_header[pos]
                        #setattr(new_param, field,val)
                        #pos+=1
                    #self.session.add(new_param)
            #self.session.commit()
            #param_handle.close()
        curr_db.session.close()
    except Exception as e:
        app.logger.exception("refresh_dataparms failed, error message: %s"%str(e))
        curr_db.rollback_close_session()


def add_user_permission_tag(persmission_tag, database, user_name, email=None, value=True):
    '''
    :param persmission_tag: tag to add to UserPermissionTags table
    :param database: databse to apply the persmission to
    :param user_name: user to be given the permission
    :param email: it is required either user name or email
    :param value: default is true
    :return:
    '''
    try:
        if email :
            user = db.session.query(User).filter(User.email == email).first()
        else:
            user = db.session.query(User).filter(User.username == user_name).first()
        if user:
            if persmission_tag=='allowed_schemes':
                # allowed_schemes can be many for the same database, but the value (scheme needs to be diffrent)
                # test the the existing records  to prevent dublications
                is_found=False
                existing_tags = db.session.query(UserPermissionTags).filter_by(user_id=user.id, field=persmission_tag,species=database).all()
                if len(existing_tags)>0:
                    for existing_tag in existing_tags:
                        if existing_tag.value==value:
                            is_found=True
                            print ("Record is already in database %s for  %s : %s for user %s"%(database,existing_tag.field,existing_tag.value, user.username))
                            break
                if not is_found or len(existing_tags)==0:
                    new_tag = UserPermissionTags(user_id=user.id, field=persmission_tag, value=value, species=database)
                    db.session.add(new_tag)
                    db.session.commit()
                    print ("1. Done ...")
            else:
                #test the field, and if it is already in the table, it will update the value
                #otherwise it will isert a new row
                existing_tag = db.session.query(UserPermissionTags).filter_by(user_id=user.id, field=persmission_tag,
                                                                               species=database).first()
                if not existing_tag:
                    new_tag=UserPermissionTags(user_id=user.id, field=persmission_tag, value=value, species=database)
                    db.session.add(new_tag)
                    db.session.commit()
                    print ("2. Done ...")
                else:
                    existing_tag.value=value
                    db.session.commit()
                    print ("3. Done ....")
        else:
            print ("No recotd found for user: %s"%user_name, email)
    except Exception as e:
        app.logger.exception("adding user permission tag is failed, error message: %s" % str(e))
        rollback_close_system_db_session()


def create_curator(key, user_name, api_only, email = None):
    try:
        if email :
            user = db.session.query(User).filter(User.email == email).first()
        else:
            user = db.session.query(User).filter(User.username == user_name).first()
        if user :
            tag_list = [] 
            if not api_only:
                tag_list.append(UserPermissionTags(user_id = user.id, field = 'upload_read_allowed', value = 'Complete Genome', species=key)) 
                tag_list.append(UserPermissionTags(user_id = user.id, field = 'upload_reads', value = 'True', species=key))
                tag_list.append(UserPermissionTags(user_id = user.id, field = 'edit_strain_metadata', value = 'True', species=key))
                tag_list.append(UserPermissionTags(user_id = user.id, field = 'view_species', value = 'True', species=key))
            tag_list.append(UserPermissionTags(user_id = user.id, field = 'api_access', value = 'True', species=key))
            db.session.execute("SELECT setval('user_permission_tags_id_seq', MAX(id)) FROM user_permission_tags;")
            for new_tag in tag_list: 
                try: 
                    existing_tag =  db.session.query(UserPermissionTags).filter_by(user_id = new_tag.user_id, field = new_tag.field, species=key).one()
                    existing_tag.value = new_tag.value 
                    db.session.commit()
                except NoResultFound:
                    #default does not exist yet, so add it...
                    db.session.add(new_tag)
                    db.session.commit()
        else:
            app.logger.exception("No valid user info is provided: user name: %s, email: %s, you need to provide valid one of them"%(user_name, email))
    except Exception as e:
        app.logger.exception("create_curator is failed, error message: %s"%str(e))
        rollback_close_system_db_session()


def remove_curator(key, user_name, email = None):

    try:
        if email:
            user = db.session.query(User).filter(User.email == email).first()
        else:
            user = db.session.query(User).filter(User.username == user_name).first()
        if user:

            try:
                curator_perm = db.session.query(UserPermissionTags).filter_by(user_id=user.id, field='edit_strain_metadata', value='True', species=key).one()
                '''  curator_perm.value = 'False'''
                db.session.delete(curator_perm)
                db.session.commit()

            except NoResultFound:
                app.logger.exception("edit_strain_metadata permission not found")

            try:
                tag_to_delete = db.session.query(UserPermissionTags).filter_by(user_id=user.id, field='upload_read_allowed', value='Complete Genome', species=key).one()
                db.session.delete(tag_to_delete)
                db.session.commit()
            except NoResultFound:
                app.logger.exception("upload_read_allowed permission not found")

        else:
            app.logger.exception("No valid user info is provided: user name: %s, email: %s, you need to provide valid one of them" % (user_name, email))

    except Exception as e:
        app.logger.exception("remove_curator has failed, error message: %s" % str(e))
        rollback_close_system_db_session()



#If Legacy record, accession should match barcode.
'''
def fill_STs(key):
    app.logger.info('Issue STs to assemblies, according to schemes.. ')
    curr_db = dbhandle[key]
    
    # Get current Schemes
    AssemblyLookup = curr_db.models.AssemblyLookup
    Assemblies = curr_db.models.Assemblies
    try:
        Schemes = curr_db.models.Schemes
        scheme_list = curr_db.session.query(Schemes).filter(Schemes.description != 'assembly_stats').filter(Schemes.description != 'T3Effectors').all()
        # DEBUG
        scheme_list = curr_db.session.query(Schemes).filter(Schemes.description == 'CRISPOL').filter(Schemes.description != 'T3Effectors').all()
        all_scheme_ids = {}
        for scheme in scheme_list:
            # Check each assembly has a record for each scheme
            if scheme.pipeline_scheme and scheme.pipeline_scheme != '':
                all_scheme_ids[scheme.id] = scheme.as_dict()
        assembled = curr_db.session.query(Assemblies).filter(Assemblies.status=='Assembled').filter(Assemblies.other_data == None).order_by(Assemblies.barcode).all()
        
        
        # DEBUG
        assembled = curr_db.session.query(Assemblies).filter(Assemblies.id==23213).all()
    
        
        jobs_callback = []
        assemblies = []
        for assembly in assembled:
            assemblies.append([assembly.id, assembly.barcode, assembly.file_pointer])
        no_duplicates = {} 
        for assembly in assemblies:
            for scheme_id in all_scheme_ids:
                assembly_id = assembly[0]
                assembly_barcode = assembly[1]
                assembly_file_pointer= assembly[2]
                try:
                    lookup = curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.assembly_id == assembly_id).filter(AssemblyLookup.scheme_id == scheme_id).one()
                    if lookup.status == None and lookup.st_barcode.endswith('_ST'):
                        lookup.status = 'COMPLETE'
                        curr_db.session.add(lookup)
                        curr_db.session.commit()
                    if lookup.other_data == None:
                        lookup.other_data = {}
                        curr_db.session.add(lookup)
                        curr_db.session.commit()                
                    if isinstance(lookup.other_data, unicode):
                        lookup.other_data = json.loads(lookup.other_data)
    
                    lookup_pipeline = '%s:%s' %(all_scheme_ids[scheme_id]['param']['pipeline'],all_scheme_ids[scheme_id]['pipeline_scheme'])
                    lookup = _update_lookup_job_status(lookup, assembly_barcode, lookup_pipeline)
                    #curr_db.session.add(lookup)
                    #curr_db.session.commit()
                    no_duplicates = {} 
                    
                    if (lookup.status == 'FAILED' and lookup.other_data.get('fail_count') < 3 \
                       and lookup.other_data.get('queued_count') == 0 \
                       and lookup.other_data.get('running_count') == 0 \
                       and not lookup.other_data.has_key('invalid')) \
                       or lookup.other_data.has_key('FORCE'):
                        if not no_duplicates.has_key('%s.%d' %(assembly_barcode,scheme_id)):
                            lookup = _update_lookup_job_status(lookup, assembly_barcode, lookup_pipeline)
                            
                            send_nomenclature_job(key, all_scheme_ids[scheme_id]['description'], assembly_barcode, assembly_file_pointer)
                            no_duplicates['%s.%d' %(assembly_barcode,scheme_id)] = True
                            
                        else:
                            app.logger.error('MULTIPLE JOBS BEING SENT, ERROR ERROR ERROR')
                    elif lookup.status != 'COMPLETE':
                        jobs = db.session.query(UserJobs).filter(UserJobs.accession== assembly_barcode).filter(not_(UserJobs.status.like('KILLED%'))).filter(UserJobs.pipeline==lookup_pipeline).order_by(UserJobs.version.asc(),UserJobs.date_sent.asc()).all()
                        process_job(','.join([str(job.id) for job in jobs ])
                    elif lookup.status == 'COMPLETE' and (lookup.other_data.get('queued_count') > 0 or lookup.other_data.get('running_count') >0):
                        _kill_active_jobs(lookup, assembly_barcode, lookup_pipeline)
                        lookup = _update_lookup_job_status(lookup, assembly_barcode, lookup_pipeline)
                        curr_db.session.add(lookup)
                        curr_db.session.commit()
                    
                except MultipleResultsFound:
                        app.logger.error('ERROR: Multiple records found for  %s (scheme %d). ' % (assembly_barcode, scheme_id) )
                        results = curr_db.session.query(AssemblyLookup).filter(AssemblyLookup.assembly_id == assembly_id).all()
                        exists = [] 
                        for res in results:
                            res_string = '%s.%s' % (res.scheme_id, res.version)
                            if res_string in exists:
                                curr_db.session.delete(res)
                            exists.append(res_string)
                        curr_db.session.commit()
                except NoResultFound:
                    # No result, need to check if job exists
                    jobs = db.session.query(UserJobs).filter(UserJobs.accession== assembly_barcode) \
                        .filter(or_(UserJobs.status.like('%COMPLETE%'),UserJobs.status.like('%QUEUE%'),UserJobs.status.like('%WAIT%'))) \
                        .filter(UserJobs.pipeline=='%s:%s' %(all_scheme_ids[scheme_id]['param']['pipeline'],all_scheme_ids[scheme_id]['pipeline_scheme'])) \
                        .order_by(UserJobs.version.desc(),UserJobs.date_sent.desc()).first()
                    if jobs: 
                        process_job(str(jobs.id))      
    
                        app.logger.info('Sending callback %s' % jobs.id )
    
                    else:
                        # No job found, Issue new job
                        app.logger.info('No Nonmen for %s (scheme %d), will launch new job' % (assembly_barcode, scheme_id) )
                        if not no_duplicates.has_key('%s.%d' %(assembly_barcode,scheme_id)):
                            send_nomenclature_job(key, all_scheme_ids[scheme_id]['description'], assembly_barcode, assembly_file_pointer)
                            no_duplicates['%s.%d' %(assembly_barcode,scheme_id)] = True
                            
                        else:
                            app.logger.error('MULTIPLE JOBS BEING SENT, ERROR ERROR ERROR')
        curr_db.session.close()
    except Exception as e:
        curr_db.session.rollback()
        curr_db.session.close()
'''
def importcgMLST(key='senterica'):
    try:
        f = open('/var/www/entero/Book1-cgmlst.txt')
        curr_db = dbhandle[key]
        Assemblies = dbhandle[key].models.Assemblies
        AssemblyLookup = dbhandle[key].models.AssemblyLookup
        for line in f.readlines()[1:]:
            split = line.split('\t')
            ass = curr_db.session.query(Assemblies).filter(Assemblies.barcode == split[1]).one()
            new_lookup = AssemblyLookup(scheme_id= 6,
                                        assembly_id = ass.id,
                                        st_barcode = split[2],
                                        version = '3.0',
                                        other_data = {},
                                        status = 'COMPLETE')
            curr_db.session.add(new_lookup)
        curr_db.session.commit()
    except Exception as e:
        app.logger.exception("importcgMLST failed, error message")
        
        

def callback_nomen(key):
    pass


def do_it(lookup, key):
    if lookup.has_key(key):
        lookup.other_data[key] += 1
    else:
        lookup.other_data[key] = 1
    return lookup

def fix_legacy():
    pass

# def add_user_schemes(filelist,scheme_name,database,profile_url=None):
#     import re
#     GENES = {}
#     profile=None
#     if profile_url:
#         resp = requests.get(profile_url)
#         profile = resp.text
#     for file_name in filelist:
#         file_handle = open(file_name,'r')
#         for record in file_handle.read().split('>'):
#             record_array = record.split()
#             if len(record_array) > 1:
#                 seq = ''
#                 for rec in record_array[1:]:
#                     if re.match('[ATGCatgc]+', rec):
#                         seq += rec.strip()
#                 if GENES.has_key(record_array[0]):
#                     GENES[record_array[0]].append(seq)
#                 else:
#                     GENES[record_array[0]] = [seq]
#     _send_scheme_request(scheme_name, GENES,database,profile)

# initiale.api is not defined in NServ app.py
# def _send_scheme_request(scheme_name, GENES, database, profile=None):
#     loci = []
#     alleles = []
#     password ='majdvfc1.'
#     fna = ''
#     faa = ''
#     for key in GENES:
#         loci.append(dict(locus = key))
#         count = 1
#         for allele_seq in GENES[key]:
#             if len(key.split('_')) > 1:
#                 alleles.append(dict(locus = key.split('_')[0], accepted = 1, seq = allele_seq, allele_id = int(key.split('_')[1])) )
#             else:
#                 alleles.append(dict(locus = key, accepted = 1, seq = allele_seq, allele_id = count) )
#             count += 1
#             if len(allele_seq) > 10:
#                 fna +='>%s\n' %key.split('_')[0]
#                 fna += allele_seq + '\n'
#             else:
#                 app.logger.info('>%s' %key.split('_')[0])
#                 app.logger.info(allele_seq)
#
#     params = dict(password = password, fna=fna)#, faa=faa)
#     if profile:
#         params['profile'] = profile
#
#     if database == 'listeria': database = 'Listeria'
#     response = requests.post(app.config['NSERV_ADDRESS'] + '/initiate.api/%s/%s/launch_mlst_scheme' %(database,scheme_name), data=params,timeout=app.config['NSERV_TIMEOUT'])
#     app.logger.info(response.text)
   
    
def move_s3_reads():
    from entero.databases.system.models import UserUploads, User
    import boto.s3.connection
    #I have added the following two lines as access_key and secret_key were missing
    access_key = "IBU04651A1ZK483GOF21"
    secret_key = 'vBnNtSUtHkvXay79yuD3GygGLOha4xxGMffG2a45'
    conn = boto.connect_s3(
        aws_access_key_id = access_key,
        aws_secret_access_key = secret_key,
        host = 's3.climb.ac.uk',
        is_secure=False,               # uncomment if you are not using ssl
        calling_format = boto.s3.connection.OrdinaryCallingFormat(),
    )
  
    s3_reads = db.session.query(UserUploads, User).filter(UserUploads.file_location.like('%s3://%')).join(User).all()
    
    for read, user in s3_reads: 
        userid = read.file_location.split('/')[2].split('.')[0]
        database = read.file_location.split('/')[2].split('.')[1]
        user_bucket = conn.create_bucket('%s.%s' %(userid,database))
        s3_file = user_bucket.get_key(read.file_name)
        output_dir = '/share_space/user_reads/%s/%s/' %(user.username ,  database)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        s3_file.get_contents_to_filename( os.path.join(output_dir, read.file_name)  ) 
        read.file_location = output_dir 
        db.session.add(read)
        db.session.commit()
    
                                         
from entero.api_2_0.schemas import StrainDataSchema, AssemblyLookupSchema
import gzip 
from sqlalchemy.orm import Load
import shutil

def strain_dump(database_in =None):
    #for database in ['ecoli']:    
    if not database_in:
        database_in =  dbhandle.keys()
    for database in database_in:        
        schema = StrainDataSchema()
        ass_lookup_schema = AssemblyLookupSchema()
        
        curr_db = dbhandle[database]
        Strains  = curr_db.models.Strains
        Assemblies = curr_db.models.Assemblies
        AssemblyLookup = curr_db.models.AssemblyLookup
        SCHEMES = curr_db.models.Schemes                
        scheme_key = {}
        for res in curr_db.session.query(SCHEMES).filter(SCHEMES.param['pipeline'].astext == 'nomenclature').all():
            scheme_key[res.id] = res.as_dict()
        
        
        output_file = '/share_space/interact/strain_dump/new.%s.strains.gz' %database
        output_handle = gzip.open(output_file, 'w')
        curr_db.update_st_cache(app.config['NSERV_ADDRESS'], lowertime=10, uppertime=60)
        query = curr_db.session.query(AssemblyLookup, Assemblies, Strains)\
            .filter(AssemblyLookup.scheme_id.in_(scheme_key.keys()))\
            .join(Assemblies)\
            .join(Strains)\
            .filter(AssemblyLookup.st_barcode != 'EGG_ST')\
            .filter(AssemblyLookup.st_barcode is not None)\
            .filter(Strains.barcode is not None)        
        for field in schema.fields:
            for table_obj in [Strains , Assemblies]:
                if schema.fields[field].attribute:
                    field_name = schema.fields[field].attribute
                else: 
                    field_name = field
                if hasattr(table_obj, field_name):
                    query = query.options(Load(table_obj).load_only(field_name))        
        query = query.options(Load(AssemblyLookup).load_only('assembly_id', 'st_barcode', 'scheme_id','version','other_data'))
        query = query.order_by(AssemblyLookup.assembly_id)
        data = query.all()
        result = {} 
        strain_barcode = None
        for dat in data:
            # Fetch strain name to collapse st data 
            row = schema.dump(dat[2]).data
            old_barcode = strain_barcode 
            strain_barcode = row['strain_barcode']          
            if not result.get(strain_barcode):  
                if old_barcode:
                    output_handle.write('%s\r\n' %json.dumps(result[old_barcode]))
                    result.pop(old_barcode, None)
                row.update(schema.dump(dat[1]).data)
                if isinstance(row['strain_barcode'],list):
                    row['assembly_barcode'] = row['strain_barcode'][0]
                else:
                    row['assembly_barcode'] = row['strain_barcode']
                row['strain_barcode'] = strain_barcode
                if row.has_key('uberstrain'):
                    row['uberstrain'] = curr_db.encode(int(row['uberstrain']), database, 'strains')           
                result[strain_barcode] = row
            # ST Info                     
            st = dat[0] 
            st_dat = ass_lookup_schema.dump(st).data
            if st.other_data:                       
                st_dat.update(ass_lookup_schema.dump(st.other_data.get('results')).data)                 
            st_dat['scheme_name'] = scheme_key[st.scheme_id]['description']
            if not result.get(strain_barcode).get('sts'):
                result[strain_barcode]['sts'] = []
            result[strain_barcode]['sts'].append(st_dat)        
        if strain_barcode:
            output_handle.write('%s\r\n' %json.dumps(result[strain_barcode]))
        output_handle.close()
        shutil.move(output_file, '/share_space/interact/strain_dump/%s.strains.gz' %database)

