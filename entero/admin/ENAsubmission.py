import xml.etree.cElementTree as ET
import os, subprocess, sys, re
import hashlib, gzip
import glob
import datetime
import ftplib
from dateutil import parser
from subprocess import Popen, PIPE
from entero import app


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem1 in elem:
            indent(elem1, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem


def getTaxonIds(strain_data, database):
    if not os.path.isfile('names.dmp'):
        subprocess.Popen('wget ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz'.split()).wait()
        subprocess.Popen('tar -xzf taxdump.tar.gz'.split()).wait()
        for fname in (
                'taxdump.tar.gz', 'citations.dmp', 'delnodes.dmp', 'division.dmp', 'gencode.dmp', 'merged.dmp',
                'nodes.dmp',
                'gc.prt', 'readme.txt'):
            os.unlink(fname)
    for d, t in strain_data:
        if not d.get('species'):
            print("It is not found", d.get('strain'), d.get('barcode'), d.get('species'))
            if database == 'senterica':
                d['species'] = 'Salmonella enterica'
            elif database == 'ecoli':  # and not d.get('species'):
                d['species'] = 'Escherichia coli'

        if database == 'yersinia' and d['species'] == 'Yersinia pseudotuberculosis Korean Group':
            d['species'] = 'Yersinia pseudotuberculosis'
        elif database == 'ecoli' and d['species'] == 'Escherichia/Shigella sp.':
            d['species'] = 'Escherichia coli'
        elif database == 'ecoli' and d['species'] == 'Escherichia  fergusonii':
            d['species'] = 'Escherichia fergusonii'

    species = {d['species']: '' for d, t in strain_data if d.get('species', None) is not None}
    if len(species):
        with open('names.dmp') as fin:
            for line in fin:
                part = line.split('\t')
                if part[2] in species:
                    species[part[2]] = part[0]
                    if len([v for v in species.values() if len(v) == 0]) == 0:
                        break

    for d, t in strain_data:
        d['TAXON_ID'] = species[d.get('species')]

    return strain_data


def convertData(strain_data, additional_data, database, collected_by,user_name):
    strain_data = getTaxonIds(strain_data, database)
    dataset = {}
    for d, t in strain_data:
        try:

            center = d['contact']
            release_date = d['release_date'].date()
            if release_date <= datetime.datetime.now().date():
                release_date = 'released'
            else:
                release_date = str(release_date)
            nd = {'TAXON_ID': d['TAXON_ID'],
                  'SAMPLE': d['barcode'],
                  'SCIENTIFIC_NAME': d['species'],
                  'DESCRIPTION': 'Whole genome sequencing data uploaded by EnteroBase user',
                  'READ_FILES': t['read_location'].split(','),
                  'broker name': 'EnteroBase',
                  'strain': d['strain'],
                  '__TRACE__': t['barcode']
                  }

            # to be changed to us special field
            # Initial user first name, last nam and user company
            nd["collected by"] = collected_by

            if d.get('serotype', None):
                nd.update({'serovar': d['serotype']})
            if d.get('subspecies', None):
                nd.update({'subspecies': d['subspecies']})
            if d.get('country', None):
                nd.update({'geographic location (country and/or sea)': d['country']})
                location = {v: i for i, v in enumerate([d['city'], d['admin2'], d['admin1']]) if v}
                if len(location)>0:
                    geographic_region = ', '.join(sorted(location.keys(), key=lambda k: -location[k]))
                    nd.update({'Geographic location (region and locality)': geographic_region})
            else:
                app.logger.error("ENA upload for {0} of {1} lacks mandatory country field".format(user_name, d['barcode']))
            if d.get('collection_year', None):
                collection_date = '-'.join(
                    ['{0:02}'.format(v) for v in [d['collection_year'], d['collection_month'], d['collection_date']] if
                     v])
                nd.update({'collection date': collection_date})
            if d.get('source_type', None) or d.get('source_niche', None) or d.get('source_details', None):
                sources = {v: i for i, v in enumerate([d['source_details'], d['source_niche'], d['source_type']]) if v}
                isolation_source = ', '.join(sorted(sources.keys(), key=lambda k: -sources[k]))
                nd.update({'isolation source': isolation_source})
            ad_data = additional_data.get(d['barcode'], {})
            for k, v in ad_data.items():
                if k != 'Barcode' and v:
                    nd[k] = v
            if (center, release_date) in dataset:
                dataset[(center, release_date)].append(nd)
            else:
                dataset[(center, release_date)] = [nd]
        except Exception as e:
            print("Error .................  ")
            print(str(e))

    return dataset


class ENAsubmission(object):
    def __init__(self, local_folder, project_title, bio_project_id, secondary_study_accession, center_name,
                 release_date, shared_ebi_folder, ftp_user_name, ftp_password):
#        study_refname = hashlib.md5(project_title.encode('utf-8')).hexdigest()

        ebi_root_folder = "ebi_working_data"
        if not os.path.exists(ebi_root_folder):
            os.makedirs(ebi_root_folder)
        self.folder = os.path.join(ebi_root_folder, local_folder)
        if not os.path.exists(self.folder):
            os.makedirs(self.folder)
        print('EBI working directory = {0}'.format(self.folder))

        self.ebi_folder = shared_ebi_folder

        self.center_name = center_name
#        self.study_refname = study_refname
        self.primary_study_accession = bio_project_id
        self.secondary_study_accession = secondary_study_accession
        self.refname = str(datetime.datetime.now())

        self.xml_files = {tag: None for tag in ["sample", "experiment", "run"]}
        self.submission_file = "submission.xml"
        self.receipt_file = "receipt.xml"
        self.ftp_user_name = ftp_user_name
        self.ftp_password = ftp_password
        self.release_date = release_date

    @staticmethod
    def check_fix_file(read_file, local_file_fix_folder, test=True):
        """
        Some uploaded gzip files have been gzipped in a way that ENA is not happy with.  This routine 'fixes' the
        files prior to uploading.   The check for a valid format is whether it can be opened by the python gzip module
        If it cant then the file is gunzipped and gzipped using gzip
        """
        st = datetime.datetime.now()
        try:
            if not test:
                raise Exception("Error")
            with gzip.open(read_file, 'rb') as f:
                f.read()
            nd_ = datetime.datetime.now()
            print("Time taken to unzip the file using python gzip: {0} seconds".format((nd_ - st).total_seconds()))
            return True
        except:
            nd = datetime.datetime.now()
            from shutil import copy2, copyfile

            print("copy file to the local folder ....")
            base_name = os.path.basename(read_file)
            local_file_name = os.path.join(local_file_fix_folder, base_name)
            copyfile(read_file, local_file_name)
            print("decompress the file ...")
            process = Popen(['gzip', '-d', local_file_name], stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            print(stdout)
            print(stderr)
            print("compress the file ....")
            process = Popen(['gzip', local_file_name.replace('.gz', '')], stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            print(stdout)
            print(stderr)
            print("copy the fixed file to the original folder ....")
            copyfile(local_file_name, read_file)
            os.remove(local_file_name)
            rd = datetime.datetime.now()
            print("1st check elapsed time: {0} secs".format((nd - st).total_seconds()))
            print("Decompress/compress elapsed time: {0} secs".format((rd - nd).total_seconds()))
            return False

    def run(self, dataset, mode, prod_test=False):
        print(self.secondary_study_accession)
        """ The workhorse for uploading the files to ENA
        """

        old_folder = os.path.abspath(os.curdir)
        os.chdir(self.folder)
        if os.path.exists(self.receipt_file):
            os.remove(self.receipt_file)
        release_date = self.release_date

        #  Create the checksums for each file and then upload files and checksums to ENA
        self.create_checksums_file(dataset)
        self.upload_reads_to_ena_ftp(dataset)

        #  Two stages to submission, a 'test' and then the real thing.  The test phase may find that some files
        #  have already been submitted in which case they are omitted from the list of files being submitted
        #  and the Enterobase data updated with the identity of the associated ENA entry
        self.accessions = {}
        to_submission = False
        # Not sure why the test phase is repeated multiple times.  Twice should be enough to
        # pick up any duplicate files and then test without the duplicates
        for ite in range(1000):
            #  Three xml files need to be created containing information about the samples, expermients and runs
            #  mirroring the data structure on ENA
            self.xml_files['sample'] = self.sample_xml(dataset)
            self.xml_files['experiment'] = self.experiment_xml(dataset, library_strategy='WGS',
                                                               library_source='GENOMIC', library_selection='RANDOM')
            self.xml_files['run'] = self.run_xml(dataset)
            # submission xml is the index to the three xml files
            if not self.submission_xml(release_date=release_date):
                # No files to submit.  May be that there is a receipt file indicating that files have already
                # been submitted, in which case return the receipt file so that data can be updated
                if os.path.exists(self.receipt_file):
                    os.chdir(old_folder)
                    return os.path.join(self.folder, self.receipt_file)
                return False
            if prod_test and mode == 'prod':
                ret = input(
                    'Everything is fine, no test will be performed. Press Y and return for a real submission.'
                    ' Anything else to test.')
                if ret.lower().startswith('y'):
                    print("We are fine to go and submit ...")
                    to_submission = True
                    break

            if self.run_curl_command('test'):
                if mode == 'test':
                    print('Everything is fine. ')
                    os.chdir(old_folder)
                    break
                elif mode == 'force':
                    ret = 'y'
                else:
                    ret = input(
                        'Everything is fine. Press Y and return for a real submission. Anything else to exit.')
                if ret.lower().startswith('y'):
                    to_submission = True
                break
        if not to_submission:
            print('Program ends, with to_submission: ', to_submission)
            return to_submission

        # Should now be ready for the real submission
        self.accessions = {}
        for ite in range(1000):
            # Regenerate the four xml files
            self.xml_files['sample'] = self.sample_xml(dataset)
            self.xml_files['experiment'] = self.experiment_xml(dataset, library_strategy='WGS',
                                                               library_source='GENOMIC', library_selection='RANDOM')
            self.xml_files['run'] = self.run_xml(dataset)
            self.submission_xml(release_date=release_date)
            # and submit....
            if self.run_curl_command('prod'):
                # self.accessions['study'] = self.accessions.pop(self.study_refname, '')
                os.chdir(old_folder)
                # return the receipt filename
                return os.path.join(self.folder, self.receipt_file)

    def create_checksums_file(self, dataset):
        """Creates a checksum for each of the files to be added to ENA.  """
        files_to_be_deleted = os.path.join(self.ebi_folder, "files_to_be_deletd.txt")

        with open(files_to_be_deleted) as f:
            lines_ = f.read()
        already_added_files = lines_.split("\n")
        print(len(already_added_files))

        try:
            counter = 0
            print("creating checksums.....")
            for data in dataset:
                for fname in data['READ_FILES']:
                    print(counter, ", Work for file: %s" % fname)

                    (SeqDir, seqFileName) = os.path.split(fname)
                    md5File = seqFileName + '.md5'

                    if not os.path.isfile(md5File) or os.path.getsize(md5File) < 30:
                        print("Creating file %s, no of created files %s" % (md5File, counter))
                        counter = counter + 1
                        with open(md5File, 'w') as md5out:
                            checksum_value = hashlib.md5(open(fname, 'rb').read()).hexdigest()
                            md5out.write(checksum_value + " " + seqFileName + "\n")
                        if fname not in already_added_files:
                            f = open(files_to_be_deleted, 'a')
                            f.write("\n%s" % fname)
                            f.close()
                            already_added_files.append(fname)
                    else:
                        print("File %s is already created ...%s" % (md5File, counter))
                        counter += 1

            print("created checksums files. ")
        except IOError:
            print("ERROR: Something has gone wrong with creating the checksums file.  Please check and re-submit.")
            sys.exit()

    def upload_reads_to_ena_ftp(self, dataset):
        """No specific ENA requirement as to names of files or positions in subdirectories. Create a subdirectory for
          each study, and put the reads into the subdirectory."""
        refname = str(self.secondary_study_accession)
        try:
            print("\nconnecting to ftp.webin.ebi.ac.uk....")
            ftp = ftplib.FTP("webin.ebi.ac.uk", self.ftp_user_name, self.ftp_password)
        except ftplib.all_errors as e:
            print(str(e))
            print("ERROR: could not connect to the ftp server.  Please check your login details.")
            sys.exit()

        print("Success!")

        try:
            if refname in ftp.nlst():
                print(refname, "directory in ftp already exists....OK no problem...")
            else:
                print("Making new directory in ftp called {0}".format(refname))
                ftp.mkd(refname)
            ftp.cwd(refname)
        except IOError:
            print("ERROR: could not find the checksum file. ")
            sys.exit()

        print("Now uploading all the data to ENA ftp server in the {0} directory".format(refname))
        for i in range(0, 3):
            co = 0
            try:
                for data in dataset:
                    for file in data['READ_FILES']:
                        co = co + 1
                        print(i, ":", co, "/", len(dataset))
                        (SeqDir, seqFileName) = os.path.split(file)
                        for fn, fn2 in ((seqFileName, file), (seqFileName + '.md5', seqFileName + '.md5')):
                            if fn in ftp.nlst():
                                fileSize_in_ftp = ftp.size(fn)
                                fileSize_in_dir = os.path.getsize(str(fn2))
                                if fileSize_in_dir != fileSize_in_ftp:
                                    print(fn + " is uploaded but not all of it so uploading"
                                               " it again now to ENA ftp server.")
                                    ftp.storbinary('STOR ' + fn, open(fn2, 'rb'))
                                else:
                                    print(fn + " has already been uploaded.")
                            else:
                                print("uploading", fn2, "to ENA ftp server.")
                                ftp.storbinary('STOR ' + fn, open(fn2, 'rb'))
                break
            except:
                print("Something went wrong with ftp, lets try again....")
        else:
            print("Something has gone wrong while uploading data to the ENA ftp server!")
            sys.exit()
        ftp.quit()

    def sample_xml(self, dataset):
        sample_set = ET.Element('SAMPLE_SET')

        dataset2 = [data for data in dataset if data['SAMPLE'] not in self.accessions]
        if len(dataset2) == 0:
            return None

        for data in dataset2:
            sample = ET.SubElement(sample_set, 'SAMPLE', alias=data['SAMPLE'], center_name=self.center_name)
            ET.SubElement(sample, "TITLE").text = data['SAMPLE']
            sample_name = ET.SubElement(sample, "SAMPLE_NAME")
            ET.SubElement(sample_name, "TAXON_ID").text = data['TAXON_ID']
            if len(data.get('SCIENTIFIC_NAME', '')):
                ET.SubElement(sample_name, "SCIENTIFIC_NAME").text = data['SCIENTIFIC_NAME']
            if len(data.get('DESCRIPTION', '')):
                ET.SubElement(sample, "DESCRIPTION").text = data['DESCRIPTION']

            metadata_fields = set(
                [k for k, v in data.items() if (not k.startswith('__')) and v and len(str(v)) > 0]) - \
                              {'SAMPLE','TAXON_ID','SCIENTIFIC_NAME','DESCRIPTION','READ_FILES'}
            if len(metadata_fields):
                sample_attributes = ET.SubElement(sample, "SAMPLE_ATTRIBUTES")
                for field in metadata_fields:
                    value = data[field]
                    sample_attribute = ET.SubElement(sample_attributes, "SAMPLE_ATTRIBUTE")
                    ET.SubElement(sample_attribute, "TAG").text = field
                    ET.SubElement(sample_attribute, "VALUE").text = ''.join(value)

        # print sample_id_and_data
        indent(sample_set)
        # create tree
        tree = ET.ElementTree(sample_set)

        # output to outfile
        with open('sample.xml', 'wb') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print("Successfully created sample.xml file")
        return 'sample.xml'

    def experiment_xml(self, dataset, library_strategy, library_source, library_selection):  # , instrument_model):
        # set the root element
        experiment_set = ET.Element('EXPERIMENT_SET')

        dataset2 = [data for data in dataset if data['SAMPLE'] not in self.accessions]
        if len(dataset2) == 0:
            return None
        for data in dataset2:
            experiment = ET.SubElement(experiment_set, 'EXPERIMENT', alias=data['__TRACE__'] + '-EXP',
                                       center_name=self.center_name)
            ET.SubElement(experiment, "STUDY_REF", accession=self.secondary_study_accession, refcenter=self.center_name)
            #  The following is the original STUDY_REF which referenced the alias rather than the secondary study
            #  submission.  This is less clear and had caused confusion.  The advantage of the alias is that
            #  it works immediately after the study has been created, whereas the secondary study accession can only be
            #  referenced about a day later
            # study_ref = ET.SubElement(experiment, "STUDY_REF", refname=self.study_refname, refcenter=self.center_name)

            design = ET.SubElement(experiment, "DESIGN")
            ET.SubElement(design, "DESIGN_DESCRIPTION")
            ET.SubElement(design, 'SAMPLE_DESCRIPTOR', refname=data['SAMPLE'],
                                              refcenter=self.center_name)
            library_descriptor = ET.SubElement(design, "LIBRARY_DESCRIPTOR")
            ET.SubElement(library_descriptor, "LIBRARY_NAME")
            ET.SubElement(library_descriptor, "LIBRARY_STRATEGY").text = str(library_strategy)
            ET.SubElement(library_descriptor, "LIBRARY_SOURCE").text = str(library_source)
            ET.SubElement(library_descriptor, "LIBRARY_SELECTION").text = str(library_selection)
            library_layout_dir = ET.SubElement(library_descriptor, "LIBRARY_LAYOUT")
            # indent
            ET.SubElement(library_layout_dir, "PAIRED")
            # dedent
            platform = ET.SubElement(experiment, "PLATFORM")
            illumina = ET.SubElement(platform, "ILLUMINA")

            # indent
            ET.SubElement(illumina, "INSTRUMENT_MODEL").text = 'unspecified'

            # dedent
            ET.SubElement(experiment, "PROCESSING")

        # use the indent function to indent the xml file
        indent(experiment_set)
        # create tree
        tree = ET.ElementTree(experiment_set)

        # out_dirput to outfile
        with open('experiment.xml', 'wb') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print("Successfully created experiment.xml file")
        return 'experiment.xml'

    def run_xml(self, dataset):
        run_set = ET.Element('RUN_SET')
        checksums = {}
        for fname in glob.glob('*.md5'):
            with open(fname, 'r') as f:
                for line in f:
                    part = line.strip().split()
                    checksums[part[1]] = part[0]

        dataset2 = [data for data in dataset if data['SAMPLE'] not in self.accessions]
        if len(dataset2) == 0:
            return None

        for data in dataset2:
            run = ET.SubElement(run_set, 'RUN', alias=data['__TRACE__'], center_name=self.center_name,
                                run_center=self.center_name)
            ET.SubElement(run, 'EXPERIMENT_REF', refname=data['__TRACE__'] + '-EXP')
            data_block = ET.SubElement(run, 'DATA_BLOCK')
            files = ET.SubElement(data_block, 'FILES')
            try:
                for fname in data['READ_FILES']:
                    (SeqDir, seqFileName) = os.path.split(fname)
                    ET.SubElement(files, 'FILE', checksum=checksums[seqFileName], checksum_method="MD5",
                                         filename=self.secondary_study_accession + "/" + seqFileName, filetype='fastq')
            except StopIteration:
                print("ERROR: no second read found for", data['SAMPLE'])
                sys.exit()

        # create tree
        tree = ET.ElementTree(indent(run_set))
        # out_put to outfile
        with open('run.xml', 'wb') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print("\nSuccessfully created run.xml file")
        return 'run.xml'

    def submission_xml(self, release_date):
        submission_set = ET.Element('SUBMISSION_SET')
        submission = ET.SubElement(submission_set, 'SUBMISSION', alias=self.refname, center_name=self.center_name)
        actions = ET.SubElement(submission, "ACTIONS")
        xml_found = False
        for xml_tag, xml_file in self.xml_files.items():
            if xml_file:
                action = ET.SubElement(actions, "ACTION")
                ET.SubElement(action, "ADD", source=xml_file, schema=xml_tag)
                xml_found = True
        if not xml_found:
            print("No xml files to be submitted")
            return False

        # if a hold date is given until releasing publicly
        action = ET.SubElement(actions, "ACTION")
        if release_date != 'released':
            # change the format as it accepts only date not date time format
            # otherwise it will send back an error
            # K.M 3/6/2019
            dt = parser.parse(release_date)
            ET.SubElement(action, "HOLD", HoldUntilDate=str(dt.date()))  # release_date)
        # else if release is given, i.e. release immediately to the public
        else:
            ET.SubElement(action, "RELEASE")
        # create tree
        tree = ET.ElementTree(indent(submission_set))
        # out_dirput to outfile
        with open(self.submission_file, 'wb') as outfile:
            tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
        print("Successfully created submission.xml file")
        return True

    def run_curl_command(self, mode='test'):
        files = ' '.join(
            ['-F \"{0}=@{1}\"'.format(tag.upper(), fname) for tag, fname in self.xml_files.items() if fname])
        if len(files) == 0:
            print("No new record to submit")
            return True
        # print(files)
        try:
            _file=os.path.join(self.ebi_folder,'files.xml')
            f = open(_file, 'w')
            f.write(files)
            f.close()
        except:
            pass
        test_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
            files) + self.ftp_user_name + "%20" + self.ftp_password
        prod_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
            files) + self.ftp_user_name + "%20" + self.ftp_password
#        test_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
#            files) + self.ftp_user_name + "%20" + self.ftp_password
#        prod_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
#            files) + self.ftp_user_name + "%20" + self.ftp_password
        cmd = prod_cmd if mode == 'prod' else test_cmd

        while True:
            print("Running curl command....")
            self.receipt_file = 'receipt.xml'
            with open(self.receipt_file, 'w') as receipt_xml:
                p = subprocess.Popen(cmd, stdout=receipt_xml, shell=True, stderr=subprocess.PIPE)
                (curl_output, err) = p.communicate()
                print("Outputs: ", curl_output)
                print("error: ", err)

            receipt_xml = open(self.receipt_file, 'r').read()
            #
            try:
                receipt_results_files = os.path.join(self.ebi_folder,'all_sub_EBI_SUB.xml')
                f = open(receipt_results_files, 'a')
                line = ("\n" + receipt_xml + "\n")
                f.write(line)
                f.close()
            except:
                pass
            tree = ET.ElementTree(file=self.receipt_file)
            receipt = tree.getroot()
            # check the success attribute
            print(receipt.get('success'))
            if receipt.get('success') == 'false':
                # if "success=\"false\"" in receipt_xml:
                # I am not sure about it so I leave it
                items = re.findall(
                    '<ERROR>.+alias: "([^"]+)".+The object being added already exists in the submission account with accession: "(.+)"',
                    receipt_xml)
                if len(items):
                    print('WARNING: some items have been registered in ENA before')
                    self.accessions.update(dict(items))
#                    print {"WARNING": "some items have been registered in ENA before'"}
                    return False
                else:
                    # find the real error message from the receipt xml
                    # K.M 03/06/2019
                    errors = receipt.find('MESSAGES').find('ERROR').text
                    print("ERROR: %s" % errors)
                    print(items)
                    print({"error": errors})
                    return False

            elif "success=\"true\"" in receipt_xml:
                print("SUCCESS!")
                return True
            else:
                print("ERROR: unknown")
                return {"Error": "unknown"}


def create_project(project_name, project_title, project_desc, project_centre, mode, ftp_user_name, ftp_password):
    """
    create EBI project.
    :param project_name:
    :param project_title:
    :param project_desc:
    :param mode: either test of prod
    :param ftp_user_name:
    :param ftp_password:
    :return:
    """
    receipt_file = 'receipt.xml'
    study_refname = hashlib.md5(project_title.encode('utf-8')).hexdigest()
    if not os.path.exists(study_refname):
        os.makedirs(study_refname)

    old_folder = os.path.abspath(os.curdir)
    os.chdir(study_refname)

    center_name = project_centre
    study_set = ET.Element('STUDY_SET')
    study = ET.SubElement(study_set, 'STUDY', alias=study_refname, center_name=center_name)
    # indent
    descriptor = ET.SubElement(study, 'DESCRIPTOR')
    # indent
    # center_project_name = ET.SubElement(descriptor, 'CENTER_PROJECT_NAME').text = center_project_name
    ET.SubElement(descriptor, 'CENTER_PROJECT_NAME').text = str(project_name)
    ET.SubElement(descriptor, 'STUDY_TITLE').text = str(project_title)
    ET.SubElement(descriptor, 'STUDY_TYPE', existing_study_type="Whole Genome Sequencing")
    ET.SubElement(descriptor, 'STUDY_ABSTRACT').text = project_desc

    # create tree
    tree = ET.ElementTree(indent(study_set))

    # output to outfile
    with open('study.xml', 'wb') as outfile:
        tree.write(outfile, xml_declaration=True, encoding='utf-8', method="xml")
    print("Successfully created study.xml file")

    xml_files = {"study": "study.xml"}
    submission_set = ET.Element('SUBMISSION_SET')
    submission = ET.SubElement(submission_set, 'SUBMISSION', alias=study_refname, center_name=center_name)
    actions = ET.SubElement(submission, "ACTIONS")
    for xml_tag, xml_file in xml_files.items():
        if xml_file:
            action = ET.SubElement(actions, "ACTION")
            ET.SubElement(action, "ADD", source=xml_file, schema=xml_tag)
    action = ET.SubElement(actions, "ACTION")
    ET.SubElement(action, "RELEASE")
    tree = ET.ElementTree(indent(submission_set))
    with open("submission.xml", 'wb') as submission_out:
        tree.write(submission_out, xml_declaration=True, encoding='utf-8', method="xml")

    files = ' '.join(
        ['-F \"{0}=@{1}\"'.format(tag.upper(), fname) for tag, fname in xml_files.items() if fname])

    test_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
        files) + ftp_user_name + "%20" + ftp_password
    prod_cmd = "curl -k -F \"SUBMISSION=@submission.xml\" {0} \"https://www.ebi.ac.uk/ena/submit/drop-box/submit/?auth=ENA%20\"".format(
        files) + ftp_user_name + "%20" + ftp_password

    cmd = prod_cmd if mode == 'prod' else test_cmd

    print("Running curl command....")
    with open(receipt_file, 'w') as receipt_xml:
        p = subprocess.Popen(cmd, stdout=receipt_xml, shell=True, stderr=subprocess.PIPE)
        (curl_output, err) = p.communicate()
        print("Outputs: ", curl_output)
        print("error: ", err)

    receipt_xml = open(receipt_file, 'r').read()
    tree = ET.ElementTree(file=receipt_file)
    receipt = tree.getroot()
    print(receipt_xml)
    accessions = {}
    secondary_study_accession = None
    bio_project_id = None
    if receipt.get('success') == 'false':
        items = re.findall(
            '<ERROR>.+alias: "([^"]+)".+The object being added already exists in the submission account with accession: "(.+)"',
            receipt_xml)
        if len(items):
            print('WARNING: some items have been registered in ENA before')
            accessions.update(dict(items))
        else:
            errors = receipt.find('MESSAGES').find('ERROR').text
            print("\nERROR: %s" % errors)

    elif "success=\"true\"" in receipt_xml:
        if mode == 'test':
            print("SUCCESS!")
            print("Please note that this is a test mode")
            return None, None
        else:

            receipt_xml = receipt_xml
            items = re.findall(r' accession="(.+)" alias="(.+)" ', receipt_xml)
            accessions = {}
            accessions.update({k: v for v, k in items})
            try:
                bio_project_id = receipt.find('STUDY').find('EXT_ID').get('accession')
                secondary_study_accession = receipt.find('STUDY').attrib['accession']
            except:
                pass

            print("SUCCESS!")
            print(bio_project_id)
            print(secondary_study_accession)
            # copy the file in case of the project is submitted successfully,
            # so we will have a copy which will not be overwritten
            import shutil
            shutil.copy2('receipt.xml', 'receipt_successful.xml')
    else:
        print("ERROR: unknown")
        bio_project_id = None
        secondary_study_accession = None
    os.chdir(old_folder)
    return bio_project_id, secondary_study_accession

