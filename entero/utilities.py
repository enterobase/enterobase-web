from entero import db,app, get_database
from entero.databases.system.models import User,BuddyPermissionTags,UserJobs
from sqlalchemy import func
import datetime,requests,json



#checks the strains database and the users database
#and matches owners to records based on email
def attach_owner_to_strain(dbase):
       database = get_database(dbase)
       if not database:
           app.logger.error("attach_owner_to_strain failed, could not find %s database"%dbase)
           return
       try:
           Strains = database.models.Strains
           orphans = database.session.query(Strains).filter(Strains.owner == None)
           count=0
           for orphan in orphans:
                  if  not orphan.email:
                         continue

                  emailist = orphan.email.split(",")
                  rec_owner=None
                  for email in emailist:
                         owner = User.query.filter(func.lower(User.email)==func.lower(email)).first()
                         if not owner:
                                continue
                         if not rec_owner:
                                orphan.owner = owner.id
                                rec_owner=owner.id
                         #make the owner of subequent email addessess a buddy of the owner with permissions to update
                         else:
                                extra_owner = User.query.filter(func.lower(User.email)==func.lower(email)).first()
                                if extra_owner:
                                       buddy = BuddyPermissionTags.query.filter_by(user_id=rec_owner,buddy_id=extra_owner.id,species= dbase,
                                                                                  field="edit_strain_metadata").first()
                                       if buddy:
                                              buddy.value="True"
                                       else:
                                              buddy  = BuddyPermissionTags(user_id=rec_owner,buddy_id=extra_owner.id,species = dbase,field="edit_strain_metadata",value="True")
                                              db.session.add(buddy)
                                              db.session.flush()


           database.commit()
           db.session.commit()
       except Exception as e:
           app.logger.exception("attach_owner_to_strain faild, error message: %s"%str(e))
           database.rollback_close_session()
           try:
               db.session.rollback()
               db.session.close()
           except:
               pass

'''
This code is moved to databases\database.py

def getStrainInfo(db,field):
       conn = psycopg2.connect(app.config["ACTIVE_DATABASES"][db][1]);
       
       cursor = conn.cursor(cursor_factory=DictCursor) 
       sql = "SELECT %s,COUNT(strains.id) AS num FROM strains GROUP BY %s ORDER BY num DESC" % (field,field)

       cursor.execute(sql) 
       records = cursor.fetchall()
       cursor.close()
       conn.close()
       return records
'''
def send_SRA_assembly(accession,database,username,scheme,user_id=0):
       
       jobs = UserJobs.query.filter_by(accession=accession).all()
       already = False
       for job in jobs:
              if job.status == "QUEUED" or job.status == "RUNNING" or job.status == "COMPLETE":
                     already  = True
                     break
              
       if already:
              return False
       URI = app.config['CROBOT_URI']+"/head/submit"
       params = {
           "source_ip":app.config['CALLBACK_ADDRESS'],
           "usr":username,
           "scheme":scheme,
           "query_time":str(datetime.datetime.now()),
           "reads":{
               "read":accession
           },
           "params":{
               "prefix":"test"
           },
           "pipeline":"QAssembly_ST"
       }
       resp = requests.post(url = URI, data = {"SUBMIT":json.dumps(params)})
       try:
              data = json.loads(resp.text)
       except Exception:
              return False
       job = UserJobs(id=data['tag'],pipeline="QAssembly",date_sent=data["query_time"],database=database,
                      status=data["status"],accession = data["reads"]["read"],user_id=user_id)
       db.session.add(job)
       db.session.commit()



              
              
       