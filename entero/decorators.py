from functools import wraps
from flask import abort,redirect,url_for,request,flash
from flask_login import current_user
from entero.databases.system.models import check_permission
from entero import app
import ujson
import os

#permission required
def permission_required(permission):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can(permission):
                abort(403)
            return f(*args, **kwargs)
        return decorated_function
    return decorator


#used to decorate a method where user login is required
#if not logged in will return an error message. if the json
#parameter is true than the message will be converted to json
def auth_login_method(json):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            #check if the current user is an anonymous user
            if not current_user.is_authenticated():
                msg = "Failed- You must be logged in for this method"
                #just return a string containing the message
                if json:
                    return ujson.dumps(msg)
                return msg
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def active_server():
    return os.path.exists('{0}/activeServer'.format(os.environ['HOME']))

def uses_crobot_flash(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not active_server():
            flash("Function temporarily unavailable during maintenance ")
            return redirect(request.referrer)
        return f(*args, **kwargs)
    return decorated_function


def admin_required(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            try:
                if not current_user.administrator:
                    abort(403)
            except AttributeError:
                abort(403)
            return f(*args, **kwargs)
        return decorated_function

def auth_login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        #check if the current user is an anonymous user
        if not current_user.is_authenticated():
            #redirect to login giving the url of the current page, 
            #which will be returned to after login
            return redirect(url_for('auth.login',next=request.url))
        return f(*args, **kwargs)
    return decorated_function


def view_species_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        #check if the current database in the active databases list
        if kwargs['species'] in app.config['ACTIVE_DATABASES'].keys():
            #check if the current user is an anonymous user
            if  not app.config['ACTIVE_DATABASES'][kwargs['species']][2] :
                if not current_user.is_authenticated():
                    flash("You need to be logged in and have permission to access this database")
                    return redirect(url_for('auth.login',next=request.url))
                else:
                    if not check_permission(current_user.id, 'view_species', kwargs['species']):
                        flash("You do not have access to this page, please contact an administrator.")
                        return redirect(url_for('main.index'))
            return f(*args, **kwargs)
        else:
            flash('%s database is not found' % (kwargs['species']))
            return redirect(url_for('main.index'))
    return decorated_function

#permission required
def user_permission_required(permission):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            database = kwargs['database']
            if not current_user.administrator:
                if not check_permission(current_user.id,permission,database):
                    return ujson.dumps({"status":"failed","msg":"You do not have permission"})
            return f(*args, **kwargs)
        return decorated_function
    return decorator