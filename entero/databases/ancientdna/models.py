from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
#STAssembly = mod.getSTAssemblyTable(metadata)
#STAllele = mod.getSTAlleleTable(metadata)
#SchemeLoci= mod.getSchemeLociTable(metadata)
#AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)

### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass

class Strains(Base,mod.Strains): 
    category = Column("category",Text)
    citation= Column("citation",Text)
    disease= Column("disease",Text)
    host_ethinity= Column("host_ethinity",Text)
    host_sex= Column("host_sex",Text)
    host_age= Column("host_age",Text)
    altern_name = Column("altern_name",Text)
    detailed_address = Column("detailed_address",Text)
    year_range = Column("year_range",Text)
    year_bp = Column("year_bp", Integer)
    project_description = Column("project_description", Text)
    donor = Column("donor",Text)
       
class StrainsArchive(Base,mod.StrainsArchive):
    category = Column("category",Text)
    citation = Column("citation",Text)
    disease= Column("disease",Text)
    host_ethinity= Column("host_ethinity",Text)
    host_sex= Column("host_sex",Text)
    host_age= Column("host_age",Text)
    altern_name = Column("altern_name",Text)
    detailed_address = Column("detailed_address",Text)
    year_range = Column("year_range",Text)
    year_bp = Column("year_bp", Integer)
    project_description = Column("project_description", Text)
    donor = Column("donor",Text)

class DataParam(Base,mod.DataParam): 
    pass

class AssemblyLookup(Base,mod.AssemblyLookup):
    pass




