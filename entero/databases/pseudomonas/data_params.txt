tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_patterdescription	vals	max_lengtno_duplicates	allow_multiple_values	group_name
strains	strain		STRAIN															
strains	contact		SOURCE_LAB															
strains	source_type		ISOLATED_FROM															
strains	collection_year		YEAR															
strains	continent		CONTINENT															
strains	country		COUNTRY															
strains	city		LOCATION															
strains	uberstrain			-1000	0	Uberstrain	integer										
strains	pathovar	Sample,Metadata,Pathovar	8	0	Pathovar	combo			0
strains	race	Sample,Metadata,Race	9	0	Race	combo			0

