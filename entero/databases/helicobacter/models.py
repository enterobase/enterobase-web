from sqlalchemy import Integer, Column, ForeignKey, Table, String, DateTime, Time, Text, MetaData
from sqlalchemy.ext.declarative import declarative_base
from entero.databases import generic_models as mod

metadata = MetaData()
Base = declarative_base(metadata=metadata)

### Lookup tables ###
# N.B Table order matters! defined relationships() in raw data tables
# need lookup, index, and dependent child tables to exist first.
#STAssembly = mod.getSTAssemblyTable(metadata)
#STAllele = mod.getSTAlleleTable(metadata)
#SchemeLoci= mod.getSchemeLociTable(metadata)
#AlleleAssembly = mod.getAlleleAssemblyTable(metadata)
TracesAssembly = mod.getTraceAssemblyTable(metadata)

### Raw data tables ###
class Assemblies(Base, mod.AssembliesRel):
    pass

class AssembliesArchive(Base, mod.AssembliesArchive):
    pass

class Traces(Base, mod.Traces):
    pass

class TracesArchive(Base, mod.TracesArchive):
    pass

class Schemes(Base,mod.Schemes):
    pass

class SchemesArchive(Base,mod.SchemesArchive):
    pass

class Strains(Base,mod.Strains): 
    species = Column("species",String(200))
    contact_2= Column("contact_2",String(200))
    citations= Column("citations",Text)
    disease= Column("disease",Text)
    host_ethinity= Column("host_ethinity",Text)
    host_sex= Column("host_sex",Text)
    host_age= Column("host_age",Text)
    antimicrobial_resistance= Column("antimicrobial_resistance",Text)
    alias= Column("alias",String(200))
       
class StrainsArchive(Base,mod.StrainsArchive):
    species = Column("species",String(200))
    contact_2= Column("contact_2",String(200))
    citations= Column("citations",Text)
    disease= Column("disease",Text)
    host_ethinity= Column("host_ethinity",Text)
    host_sex= Column("host_sex",Text)
    host_age= Column("host_age",Text)
    antimicrobial_resistance= Column("antimicrobial_resistance",Text)
    alias= Column("alias",String(200))    
    
class DataParam(Base,mod.DataParam): 
    pass

class AssemblyLookup(Base,mod.AssemblyLookup):
    pass




