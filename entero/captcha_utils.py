from captcha.image import ImageCaptcha
import random
import string

def generate_captcha_text(length=6):
    # Generate random alphanumeric text
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

def create_captcha_image(text):
    # Create a CAPTCHA generator with distortion
    image = ImageCaptcha(width=280, height=90)
    captcha_image = image.generate_image(text)
    return captcha_image