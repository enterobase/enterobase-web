
function _swatches(swatches, pie_color) {
    return (swatches({color: pie_color, title: "Pathovars", columns: "180px"}))
}


function _chart(d3, width, height, dat, d2, pie, pie_color, format) {
    const svg = d3.create("svg")
        .attr("viewBox", [0, 0, width, height])
        .style("font", "12px sans-serif")
        .attr("text-anchor", "middle")
        .on("click", resetZoom);

    function resetZoom() {
        const transition = svg.transition().duration(750);
        transition.attr("viewBox", [0, 0, width, height]);
    }

    //Attach reset zoom to button:
    const resetButton = document.getElementById('resetButton');

    // Add event listener to the button
    resetButton.addEventListener('click', resetZoom);

    const shadow = d3.create("svg").attr("xmlns", "http://www.w3.org/2000/svg").append("svg:defs").html('<svg:mask></svg:mask>');
    const grayShades = ['#f0f0f0', '#bdbdbd'];

    // Find the maximum layer in the data
    const maxLayer = d3.max(dat, d => d.height);

    // Filter out data for layers with height up to the maximum layer
    const filteredData = dat.filter(d => d.height < maxLayer);

    // Flatten the filtered data to include only leaf nodes
    const flattenedData = filteredData.flatMap(d => d.descendants());


    const Tooltip = d3.select("#chart-container")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "1px")
        .style("border-radius", "5px")
        .style("padding", "5px")
        .style("position", "absolute");

    svg.append("filter")
        .attr("id", shadow.id)
        .append("feDropShadow")
        .attr("flood-opacity", 0.3)
        .attr("dx", 0)
        .attr("dy", 1);

    const node = svg.selectAll("g")
        .style("stroke", "none")
        .data(d3.group(flattenedData, d => d.height))
        .join("g")
        .selectAll("g")
        .data(d => d[1])
        .join("g")
        .attr("transform", d => `translate(${d.x},${d.y})`)
        .on("click", zoomIn)
        .on('mouseenter', function (event, d) {
            Tooltip.style("opacity", 0.8);
            Tooltip.html(`name: ${d.data.name}<br>count: ${d.value}`).style("left", (event.pageX) + 'px').style("top", (event.pageY - 28) + 'px');
            d3.select(this).style("stroke", "black");
        })
        .on('mousemove', function (event, d) {
            Tooltip.style("left", event.pageX + "px");
            Tooltip.style("top", event.pageY + "px");
        })
        .on('mouseleave', function (event, d) {
            Tooltip.style("opacity", 0);
            d3.select(this).style("stroke", "none");
        });

    node.append("circle")
        .attr("r", d => {
            return d.r; // Return the r value
        })
        .attr("fill", d => grayShades[d.height % grayShades.length]) // Assign colors based on height
        .attr("stroke", d => d.height === 0 ? "#000000" : '')
        .attr("flood-opacity", 0.4)
        .attr("pointer-events", "all");


    const leaf = node.filter(d => !d.children);

    leaf.select("circle")
        .attr("id", d => (d.leafUid = d3.create("svg").attr("xmlns", "http://www.w3.org/2000/svg").append("svg:defs").html('<svg:mask></svg:mask>')).id);

    leaf.append("clipPath")
        .append("use")
        .attr("xlink:href", d => d.leafUid.href);

    // Handle click events to zoom in
    function zoomIn(event, d) {
        const [x, y, r] = [d.x, d.y, d.r * 2];
        const transition = svg.transition().duration(750);

        transition.attr("viewBox", [x - r, y - r, r * 2, r * 2]);
        event.stopPropagation();
    }

    const textbutton = document.getElementById('displaytext');
    textbutton.addEventListener("click", function() {
//    var textbutton = d3.select("displaytext");
//    textbutton.on("click", function() {
        console.log('clicked text');
        var displayText = !d3.selectAll(".name-overlay").empty();
        console.log(displayText)
        if (displayText) {
            d3.selectAll(".name-overlay, .label-background").remove();
        } else {
            // Display text for each element
            d2.forEach((dat, index) => {
                const [x, y] = [dat.x, dat.y]; // Get the center coordinates of the pie chart
                // Check for overlap with existing labels
                var overlapping = false;
                d3.selectAll(".name-overlay").each(function() {
                    const labelX = parseFloat(d3.select(this).attr("x"));
                    const labelY = parseFloat(d3.select(this).attr("y"));
                    const distance = Math.sqrt(Math.pow((x - labelX), 2) + Math.pow((y - labelY), 2));
                    if (distance < 40) {
                        overlapping = true;
                    }
                });

                // Add jitter if there's overlap
                const jitterX = overlapping ? Math.random() * 20 - 10 : 0;
                const jitterY = overlapping ? Math.random() * 20 - 10 : 0;

                svg.append("text")
                    .attr("class", "name-overlay")
                    .attr("x", x + jitterX)
                    .attr("y", y + jitterY)
                    .text(dat.data.name)
                    .attr("text-anchor", "middle")
                    .attr("alignment-baseline", "middle")
                    .attr("fill", "black");
            });
        }
    });





    let searchQuery;

    plotchildren(dat)

    function plotchildren(dat) {
        d2.forEach((dat) => {
            var arcs = pie(dat.data.val);
            var ff = dat.data.val.map((d) => d.k);
            var name = dat.data.name;
            var val = dat.data.val.map((d) => d.v);
            var arc = d3
                .arc()
                .innerRadius(0)
                .outerRadius(dat.r);

            var x = [dat.x, dat.y];
            var gg = svg.append("g").attr("transform", `translate(${x})`);

            gg.append("g")
                .selectAll("path")
                .data(arcs)
                .join("path")
                .attr("fill", (d, i) => pie_color(ff[i]))
                .attr("k", (d, i) => ff[i])
                .attr("d", arc)
                .attr("stroke", 'black')
                .each(function (d, i) {
                    d3.select(this).on('mouseenter', function (event, dd) {
                        Tooltip.style("opacity", 0.8);
                        Tooltip.html("name: " + name + "<br>category: " + ff[i] + "<br>count: " + val[i])
                        let border = d3.select(this).style("stroke");
                        let width = d3.select(this).style("stroke-width")

                        d3.select(this).style("left", (event.pageX) + 'px').style("top", (event.pageY - 28) + 'px');
                        d3.select(this).style("stroke", "black");
                        d3.select(this).attr("stroke-colour", border);
                        d3.select(this).attr("stroke-width", 2)
                        d3.select(this).attr("stroke-width-saved", width);
                    });
                    d3.select(this).on('mousemove', function (event, dd) {
                        Tooltip.style("left", event.pageX + "px");
                        Tooltip.style("top", event.pageY + "px");
                    });
                    d3.select(this).on('mouseleave', function (event, dd) {
                        Tooltip.style("opacity", 0);
                        let border = d3.select(this).attr("stroke-colour");
                        let width = d3.select(this).attr("stroke-width-saved")
                        d3.select(this).style("stroke", border);
                        d3.select(this).style("stroke-width", width)

                    });


                })
                .attr("stroke", (d, i) => {
                        let border = (ff[i] === searchQuery )? "yellow" : "black";
                        return border;
                        //return ff[i] === searchQuery ? "yellow" : "none"; // Highlight stroke color
                     })
                     .attr("stroke-width", (d, i) => {
                        let width = (ff[i] === searchQuery) ? 2 : 0;

                        return width;
                        //return ff[i] === searchQuery ? 2 : 0;
                    });

        });
    }

    function resetSearchfunc() {
            searchQuery = null;
            plotchildren(dat)
    }
    const ResetSearch = document.getElementById("resetSearch");
    ResetSearch.addEventListener("click", resetSearchfunc);


    function initializePage(dat) {
        const searchInput = document.getElementById("searchInput");
        const searchButton = document.getElementById("searchButton");

        // Function to handle search
        function performSearch() {
            searchQuery = searchInput.value;
            console.log("Search query:", searchQuery);
            plotchildren(dat)
        }

        searchButton.addEventListener("click", performSearch);

        searchInput.addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();
                performSearch();
            }
        });
    }

    document.addEventListener("DOMContentLoaded", function () {
        initializePage(dat);
    });
    return svg.node();
}


function _pack(d3, width, height, data) {
    const packLayout = d3.pack()
        .size([width, height])  // Set the size of the pack layout
        .padding(5);            // Set padding between circles

    const root = d3.hierarchy(data)
        .sum(d => d.value)     // Set the value accessor
        .sort((a, b) => b.value - a.value); // Optionally, sort nodes by value

    return packLayout(root).descendants(); // Compute positions and sizes of circles, return flattened array of nodes
}

function Swatches(color,Order, {
    columns = null,
    format,
    unknown: formatUnknown,
    swatchSize = 15,
    swatchWidth = swatchSize,
    swatchHeight = swatchSize,
    marginLeft = 0
} = {}) {
    const id = `-swatches-${Math.random().toString(16).slice(2)}`;
    const unknown = formatUnknown == null ? undefined : color.unknown();
    const unknowns = unknown == null || unknown === d3.scaleImplicit ? [] : [unknown];
    const domain = color.domain().concat(unknowns);
    const sortedDomain = domain.sort((a, b) => {
        return Order.indexOf(a) - Order.indexOf(b);
        });


    if (format === undefined) format = x => x === unknown ? formatUnknown : x;


    const div = document.createElement("div");
    div.style.cssText = `min-height: 33px; margin-left: ${+marginLeft}px; font: 10px sans-serif; display: flex; flex-wrap: wrap;`;
    div.innerHTML = `
    <style>
      .${id}-item {
        display: flex;
        align-items: center;
        margin-top: 10px;
        margin-left: 10px;
      }
      .${id}-swatch {
        background: var(--color);
        flex-shrink: 0;
        width: ${+swatchWidth}px;
        height: ${+swatchHeight}px;
        border-color: black;
        border-style: solid;
      }
      .${id}-text {
        margin-left: 5px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    </style>
    ${sortedDomain.map(value => `
      <div class="${id}-item">
        <div class="${id}-swatch swatch" style="--color: ${color(value)}" data-text="${format(value)}"></div>
        <div class="${id}-text">${format(value)}</div>
      </div>
    `).join('')}`;
    return div;
}

function handleSwatchClick(event) {
    const swatch = event.target.closest('.swatch');
    if (swatch) {
        const colorText = swatch.dataset.text; // Get the text associated with the color from the dataset

        // Get the color of the swatch
        const swatchColor = d3.select(swatch).attr("fill");

        // Create a color picker input element
        const colorPicker = document.createElement("input");
        colorPicker.type = "color";
        colorPicker.value = swatchColor;//swatch.dataset.color; // Get the color from the dataset
        colorPicker.style.position = "absolute"; // Position the color picker next to the swatch
        colorPicker.style.left = swatch.offsetLeft + swatch.offsetWidth + "px";
        colorPicker.style.top = swatch.offsetTop + "px";

        // Function to update swatch color
        const updateSwatchColor = (newColor) => {
            swatch.style.backgroundColor = newColor; // Update the background color of the swatch
            swatch.dataset.color = newColor; // Update the color in the dataset
            colorPicker.value = newColor; // Update the color picker value
            let container = d3.select("#chart-container");
            container.selectAll("path").filter(function (d) {
                return d3.select(this).attr("k") === colorText;

            }).style("fill", newColor);

        };

        // Add event listener to handle color changes
        colorPicker.addEventListener("input", function (event) {
            const newColor = event.target.value;
            updateSwatchColor(newColor);
            console.log('New Color:', newColor)
        });
        // Append the color picker next to the swatch
        document.body.appendChild(colorPicker);
        // Function to handle clicks outside of color picker
        const handleClickOutside = (event) => {
            if (!colorPicker.contains(event.target) && !swatch.contains(event.target)) {
                document.body.removeChild(colorPicker); // Remove color picker from DOM
                document.removeEventListener("click", handleClickOutside); // Remove click event listener
            }
        };
        // Add event listener to document body to handle clicks outside of color picker
        document.addEventListener("click", handleClickOutside);
        updateSwatchColor(swatch.dataset.color); // Update color to the stored value
    }
}

// Function to draw swatches
function drawSwatches() {
    // Remove existing swatches if any
    const swatchesContainer = document.getElementById("swatches-container");
    swatchesContainer.innerHTML = '';

    // Append swatches using the Swatches function
    const swatchDiv = Swatches(pie_color,catorder, {format: d => d});
    swatchesContainer.appendChild(swatchDiv);

    // Add event listeners to each swatch for opening color picker
    const swatches = swatchDiv.querySelectorAll(".swatch");
    swatches.forEach((swatch, index) => {
        swatch.addEventListener("click", handleSwatchClick);
    });
}
 const customColors = [
  "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf",
  "#aec7e8", "#ffbb78", "#98df8a", "#ff9896", "#c5b0d5", "#c49c94", "#f7b6d2", "#c7c7c7", "#dbdb8d", "#9edae5",
  "#393b79", "#5254a3", "#6b6ecf", "#9c9ede", "#637939", "#8ca252", "#b5cf6b", "#cedb9c", "#8c6d31", "#bd9e39",
  "#e7ba52", "#e7cb94", "#843c39", "#ad494a", "#d6616b", "#e7969c", "#7b4173", "#a55194", "#ce6dbd", "#de9ed6",
  "#3182bd", "#6baed6", "#9ecae1", "#c6dbef", "#e6550d", "#fd8d3c", "#fdae6b", "#fdd0a2", "#31a354", "#74c476",
  "#a1d99b", "#c7e9c0", "#756bb1", "#9e9ac8", "#bcbddc", "#dadaeb", "#636363", "#969696", "#bdbdbd", "#d9d9d9"
];
