CustomViewGrid.prototype = Object.create(BaseExperimentGrid.prototype);
CustomViewGrid.prototype.constructor= CustomViewGrid;

function CustomViewGrid(name,config,species,scheme,scheme,scheme_name){
    BaseExperimentGrid.call(this,name,config,species,scheme,scheme,scheme_name);
    this.showChanges=true;
    this.custom_columns={};
    this.loci_columns={};
    this.highlight_presence=false;
}


CustomViewGrid.prototype.updateDatabase=function(){
    var changes = {};
     this.waitingDialog = new Enterobase.ModalWaitingDialog(
                        "Uploading Data",
                        function(){
                                waitingDialog.close();
                                waitingDialog = null
                
                        }
                
                ,"/static/img/upload/waiting.gif");
    for (var i=0;i<this.getRowCount();i++){
        var id = this.getRowId(i);
      
        if (!this.rowsWithChanges[id]){
            continue;
        }
        changes[id]={}
        for (var col = 0;col<this.getColumnCount();col++){
            var column = this.col_list[col];
            if (!column['editable']){
                continue;
            }
            changes[id][column['name']]=this.getValueAt(i,col);      
        }      
    }
    to_send={
        database:this.species,
        data:JSON.stringify(changes),
        custom_view_id:current_customViewId
    }
    var self = this;
      Enterobase.call_restful_json("/update_custom_data","POST",to_send,this.showError).done(function(data){
                self.waitingDialog.close();
                if (data['msg']==='OK'){
                        self.clearChanges();
                        Enterobase.modalAlert("Your data has been saved to the database");
        
                }
                else{
                        Enterobase.modalAlert("There was a problem uploading the data")
                }      
     });    
}
CustomViewGrid.prototype.setMetaData = function(data){   
        BaseExperimentGrid.prototype.setMetaData.call(this,data);
        var self = this;
        for (var index in data){
                var col = data[index];
                if (col['description']==="custom_column"){
                        this.custom_columns[col['label']]=col['name']+"";
                }
                else if (col['description']==='locus'){
                        this.loci_columns[col['name']]=col['scheme'];
                        this.columnWidths[col['name']]=70;
                }
        }
        for (var name in this.loci_columns){
                this.addExtraRenderer(name,function(cell,value,rowID){
                        if (self.highlight_presence&& value>0){
                                $(cell).css("background-color","red");
                        
                        }
                        else if (self.highlight_presence&& value<0){
                                 $(cell).css("background-color","orange");
                        }
                       
                });
        
        
        }
        
}

CustomViewGrid.prototype.viewLociDetails= function(row_index,col_index){
        var col = this.col_list[col_index];
        var row_id = this.getRowId(row_index);
        var scheme = this.loci_columns[col['name']];
        var locus = col['name'].replace("_"+scheme,"");
        var database= this.species;
        var strain_name= this.getStrainName(row_index);
        var barcode = this.strainGrid.best_assembly[row_id];
        var allele_id = this.getValueAt(row_index,col_index);
        var other_data={
                "strain_name":strain_name
        
        }
        new AlleleViewDialog(scheme,database,locus,allele_id,barcode,other_data)
        
        


}


CustomViewGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){  
        var self = this;
        var ht ='Highlight Presence';
        var val = this.getValueAt(rowIndex,colIndex);
        if (this.highlight_presence){
                ht = "Don't Highlight Presence";
        }
        var col_name =this.col_list[colIndex]['name'];
        var extramenu=[	{
                         name: 'Save Changes',
                         title: 'Save Changes',
                         fun: function () {
                                 self.updateDatabase()
                        }
                        },
                        {
                                name:"Upload Data",
                                title:"Upload Data",
                                fun:function(){
                                         var i = Enterobase.makeCustomFileInput("fileupload",function(file){  
                                                 self.uploadData(file);
                                         });
                                        i.trigger("click");
                                }
                        },
                         {
                                name:"Download Data",
                                title:"Load Values",
                                fun:function(){
                                       self.downloadTemplate();
                                }
                        },
                        {
                                name:"Paste Here",
                                title:"Paste Here",
                                fun:function(){
                                        self.setCellToPaste(rowIndex,colIndex,target);
                                },
                                disable:!self.editMode
                        
                        
                        },
                        {
                                name:ht,
                                title:ht,
                                fun:function(){
                                        self.highlight_presence = !self.highlight_presence;
                                        self.refreshGrid();
                                        
                                }
                        
                        }
                
                
        ];
        
        if (this.loci_columns[col_name] && !isNaN(val) && val!==0){
                extramenu.push({
                          name:"View Details",
                                title:"View Details",
                                fun:function(){
                                        self.viewLociDetails(rowIndex,colIndex);
                                        
                                }            
                });       
        }
        return extramenu;
}

CustomViewGrid.prototype.getMethodData= function(){
        var self = this;
       return [{
                name:"update_changes",
                label:"Save Changes",
                method:function(){
                        self.updateDatabase;
                }
    
       },
       {
                name:"donload_view",
                label:"Download Data",
                method:function(){
                        self.downloadTemplate();
                }
    
       },
       {
                                name:"upload_view_data",
                                label:"Upload Data",
                                method:function(){
                                         var i = Enterobase.makeCustomFileInput("fileupload",function(file){  
                                                 self.uploadData(file);
                                         });
                                        i.trigger("click");
                                }
        }
       
       
       
       ];
}


CustomViewGrid.prototype.downloadTemplate = function(){
        var bi = this.strainGrid.getColumnIndex("barcode");
        var ni = this.strainGrid.getColumnIndex("strain");
        var bar_name=["Barcode\tName"];
        for (var n=0;n<this.getRowCount();n++){
                var bar = this.strainGrid.getValueAt(n,bi);
                var name = this.strainGrid.getValueAt(n,ni);
                bar_name.push(bar+"\t"+name);
        }
        var data = this.getGridAsText();
        var combined=[]
        for (var n=0;n<data.length;n++){
                combined.push(bar_name[n]+"\t"+data[n]);
        }
        Enterobase.showSaveDialog(combined.join("\n"));

}

CustomViewGrid.prototype.uploadData= function(file){
       var self = this;
        var reader = new FileReader();

        reader.onload = function(progressEvent){
                var metadata={};
                var lines =  this.result.split(/\r\n|\r|\n/g);
                var headers =  lines[0].split("\"").join("").split("\t");
                //map of this file column index to the  grid column index
                var header_index= {};
                var barcode_column= null;
                var is_headers =false;
                for (var index in headers){
                        var header = headers[index]
                        if (header=="Barcode"){
                                barcode_column= parseInt(index);
                                continue;
                        }
                        var col_name= self.custom_columns[header];
                        if (col_name){
                                var col_index=self.getColumnIndex(col_name);
                                if (col_index !==-1){
                                        header_index[index]=col_index;
                                        is_headers=true;
                                }
                        }
                                 
                }
                if (barcode_column === null || ! is_headers){
                        Enterobase.modalAlert("You need a 'Barcode' column plus at least one custom column");
                      
                        return;
                
                }        
                for(var n= 1; n < lines.length; n++){
                        
                        var values =  lines[n].split("\"").join("").split("\t");
                        var barcode= values[barcode_column];
                        if (! barcode){
                                continue;
                        }
                        var id  = Enterobase.decodeBarcode(barcode);
                        var row = self.getRowIndex(id);
                        if (row===-1){
                                continue;                        
                        }
                        for (var i in header_index){
                                var value = values[i];
                                if (value || value === 0){
                                       
                                        var col = header_index[i];
                                        var old_value = self.getValueAt(row,col);
                                        if (value !==old_value){
                                                self.setValueAt(row,col,value,false);
                                                self.modelChanged(row,col,old_value,value,null);
                                        }
                                }			
                        }
                      
               
               
                }
                 self.refreshGrid();
                 Enterobase.modalAlert("You data has been uploaded. Any cells that have changed or have new data will be shown in yellow. To make the changes permanant, right click menu.");
               
        
        }
        reader.readAsText(file);
}