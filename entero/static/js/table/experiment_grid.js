/* global ValidationGrid, Enterobase, SchemeGrid */

ExperimentGrid.prototype = Object.create(BaseExperimentGrid.prototype);
ExperimentGrid.prototype.constructor= ExperimentGrid;


//create new grid giving the name of the species and the scheme
function ExperimentGrid(name,config,species,scheme,scheme_label){
    BaseExperimentGrid.call(this,name,config,species,scheme,scheme_label);
    this.blockColours={};
    this.crisprColumns={};
    this.graphicalView = true;
};



ExperimentGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Experiment_Fields#"+msg,'newwindow','width=1000, height=600');
};



ExperimentGrid.prototype.renderCRISPRCell=function (cell,value){
    c.empty();
    var blocks = value.split('-');
    for (var index in blocks){
        var name = blocks[index];
        var icon = 'glyphicon glyphicon-triangle-right';
        if (! name.startsWith("DR")){
            icon= 'glyphicon glyphicon-stop';               
        }
        
        var colour = this.blockColours[name];
        c.append($("<span class='"+icon+"' style='font-size:+"+self.fontSize+"px;color:"+colour+"' title='"+name+"'></span>"));                            
    }
}

ExperimentGrid.prototype.setMetaData = function(data){
    ValidationGrid.prototype.setMetaData.call(this,data);
    var self =  this;
    for (var index in data){
	 var name = data[index]['name'];
        if (name=== 'CRISPR1' || name ==='CRISPR2' || name ==='spacers'){
           
            this.crisprColumns[name]=true;
            
            this.addExtraRenderer(name,function(cell,value,rowID){
                if (value && self.graphicalView){
                    c = $(cell);
                    self.renderCRISPRCell(c,value);
                }
            });
            self.columnWidths[name]=450;
			
        }
    }

};


ExperimentGrid.prototype.loadNewData= function(data){
      col = this.col_list[0]['name'];
      var r = 255
      var g = 0
      var b = 0
    
      for (index in data){
        for (key in data[index]){
            if (this.crisprColumns[key]){
                var types = data[index][key].split("-");
                for (var ind in types){
                    block = types[ind];
                    if (!this.blockColours[block]){
                        r= Math.floor( Math.random() * ( 256));
                        g= Math.floor( Math.random() * ( 256));
                        b= Math.floor( Math.random() * ( 256));
                        this.blockColours[block] = "rgb("+r+","+g+","+b+")";
                    }
                }
            }
        
        }
      }
          
       ValidationGrid.prototype.loadNewData.call(this,data,false,true);
};

ExperimentGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
       
        var self = this;
        var extramenu=[	{
                         name: 'Text View',
                         title: 'Text View',
                         fun: function () {
                              self.graphicalView = false;
                              self.refreshGrid();
                         }
                 },
                
        
        	{
                         name: 'Graphical View',
                         title: 'Graphical View',
                         fun: function () {
                              self.graphicalView = true;
                              self.refreshGrid();
                         }
                 }
                
        ];   
        return extramenu;
}


