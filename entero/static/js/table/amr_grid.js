AMRGrid.prototype = Object.create(BaseExperimentGrid.prototype);
AMRGrid.prototype.constructor= AMRGrid;

//create a grid for AMR results
function AMRGrid(name,config,species,scheme,scheme_name){
	BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);
};

AMRGrid.prototype.displayAMRDetailsPopup = function(assembly_id){
    var to_send = {
			database:this.species,
			assembly_id:assembly_id,
	}
    Enterobase.call_restful_json("/get_amr_details_by_assembly_id","POST",to_send,this.showMessage).done(function(data){
        // Create pretty-printed json for display
        var header = data.barcode + " AMR data"
        var text = JSON.stringify(data,null,2).replaceAll("\n","<br>")
        Enterobase.modalAlert3('<ul style="white-space: pre;">'+text+'</ul>',header);
    });
};

AMRGrid.prototype.setMetaData= function(data){

    var self =  this;
    var grid_data=[];
    for (var index in data){
          //var val = data[index]['name'];
          grid_data.push(data[index]);
    }
    grid_data.push({
        name:"view_details",
        label:"<span class='glyphicon glyphicon-eye-open'></span>",
        display_order:0,
        not_write:true

    });

    this.addExtraRenderer("view_details",function(cell,value,rowID){
        if (self.extraRowInfo[rowID]){
            $(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")
            .css("cursor","pointer");
        }
        else{
            $(cell).html("&nbsp");
        }
    });

    this.addExtraRenderer("other_classes",function(cell,value,rowID){
        if (!value ){
            $(cell).html('NA');
        }
        else if (!value.includes(':')) {
            $(cell).html(value);
        }
        else{
            // Dont display the extra info inside []
            classes = value.replace(/:\[(.*?)\]/g,"").split(';')
            var output = [];
            for (const class_data of classes) {
                var class_data_split = class_data.split(':');
                var genes = class_data_split[1].split(',');
                var class_output = [];
                for (value of genes) {
                    if (value in this.globalExtraData) {
                     link = "'http://www.ncbi.nlm.nih.gov/protein/?term="+this.globalExtraData[value] + "'";
                        class_output.push("<a style = 'whitespace:nowrap;' target='_blank' href = " + link+">"+value+"</a>");
                     } else {
                        class_output.push(value);
                     }
                }
                output.push(class_data_split[0] + ':' + class_output.join(','));
            }
            $(cell).html(output.join(';'));
        }
    });


    for (var index in data){
        var colname = data[index]['name']
        if (colname.startsWith('AMR') || colname.startsWith('other') ){
            continue;
        }
        this.addExtraRenderer(colname,function(cell,value,rowID){
        if (!value || value == "NA"){
            $(cell).html("NA");
        }
        else{
            values = value.replace(/:\[(.*?)\]/g,"").split(',')
            var output = [];
            for (const value of values) {
                if (value in this.globalExtraData) {
                 link = "'http://www.ncbi.nlm.nih.gov/protein/?term="+this.globalExtraData[value] + "'";
                    output.push("<a style = 'whitespace:nowrap;' target='_blank' href = " + link+">"+value+"</a>");
                 } else {
                    output.push(value);
                 }
            }
            $(cell).html(output.join(','));
        }
        });
    }

   this.addCustomColumnHandler("view_details",function(cell,rowIndex,rowID){
        var assembly_id = self.extraRowInfo[rowID]['assembly_id'];
        self.displayAMRDetailsPopup(assembly_id);
   });

    BaseExperimentGrid.prototype.setMetaData.call(self,grid_data);
    this.columnWidths["view_details"]=30;
};








