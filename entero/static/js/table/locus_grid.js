LocusGrid.prototype = Object.create(ValidationGrid.prototype);
LocusGrid.prototype.constructor = LocusGrid

function LocusGrid(name,config,species,scheme){
 

        var self = this;
        ValidationGrid.call(this,name,config);
        this.lineColors= ["#CCC","#FFF"]
        this.species=species;
        this.scheme=scheme;
        this.row_to_highlight= "";
        this.generic_view=true;
        this.assembly_barcode=null;
        this.windows_opened=1;
        this.fontSize=12;
         this.addExtraRenderer("view_sequence",function(cell,value,rowID){
                if (!self.generic_view){
                        $(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")               //a hack adding function to the cell		   
                        .css("cursor","pointer");
                }
        });
        this.addCustomColumnHandler("view_sequence",function(cell,rowIndex,rowID){ 
                if (!self.generic_view){
                        self.showAllelesInContext(self.extraRowInfo[rowID]);
                }
        });
};


LocusGrid.prototype.showAllelesInContext=function(locus){
        var loci_list=[];
        for(var n=0;n<this.getRowCount();n++){
                var id = this.getRowId(n);
                loci_list.push(this.extraRowInfo[id]);
        }
        var args = "?database="+this.species+"&locus="+locus+"&barcode="+this.assembly_barcode+"&highlight_genes="+loci_list.join(",");
        var win_name = "jbrowse"+this.windows_opened;
        this.windows_opened++;
        window.open("/view_jbrowse_annotation"+args,"ssss",'width=1000, height=600');
}


LocusGrid.prototype.getRowClass= function(rowID){
        
        if (this.highlighted_row==rowID){
                return "highlighted-row";
        }
        
        
}

LocusGrid.prototype.loadNewData = function(data,validate,useIDs){
        this.generic_view=true;
        this.original_data=data;
        ValidationGrid.prototype.loadNewData.call(this,data,validate,useIDs);
        
}

LocusGrid.prototype.restore = function(){
        if (this.original_data){
                this.loadNewData(this.original_data);
                this.refreshGrid();
        }

}

LocusGrid.prototype.getHighlightedRowPosition=function(){
        var index =  this.getRowIndex(this.highlighted_row);
        var row_height = $(this.tBody.rows[0]).height();
        return row_height*index;
}

LocusGrid.prototype.showAlleleDetailsForStrain=function(scheme,barcode,locus_highlight,alleles){
        this.assembly_barcode=barcode;
        this.generic_view=false;
        var loci_to_index={};
        var loci_list=[];
        
        for (var n=0;n<this.getRowCount();n++){
                        var id = this.getRowId(n);
                        var locus = this.extraRowInfo[id];
                        if (locus == locus_highlight){
                                this.highlighted_row = this.getRowId(n);
                        
                        }
                        loci_list.push(locus)
                        loci_to_index[locus]=n;
                 
        }
        var self = this;
        var to_send={
                assembly_barcode:barcode,
                scheme:scheme,
                database:this.species,
                loci:loci_list.join(",")
        
        };
        var contig_i = this.getColumnIndex("contig");
        var start_i = this.getColumnIndex("start");
        var end_i = this.getColumnIndex("end");
        var dir_i = this.getColumnIndex("direction");
        var stat_i = this.getColumnIndex("status");
        var id_i = this.getColumnIndex("allele_id");
        var self = this;
        Enterobase.call_restful_json("/get_allele_details_for_strain","POST",to_send,callFailed).done(function(data){
                        for (var locus in loci_to_index){
                                var info = data[locus];
                                var r = loci_to_index[locus];      
                                if (info){
                                        self.setValueAt(r,contig_i,info[0]);
                                        self.setValueAt(r,start_i,info[1]);
                                        self.setValueAt(r,end_i,info[2]);
                                        self.setValueAt(r,dir_i,info[3]);
                                        self.setValueAt(r,stat_i,"present");
                                        self.setValueAt(r,id_i,alleles[locus]);
                                        
                                }
                                else{
                                        self.setValueAt(r,contig_i,"");
                                        self.setValueAt(r,start_i,"");
                                        self.setValueAt(r,end_i,"");
                                        self.setValueAt(r,dir_i,"");                                               
                                        self.setValueAt(r,stat_i,"absent");
                                        self.setValueAt(r,id_i,"");
                                        
                                
                                }
                        
                        }
                        self.refreshGrid();
                      
        });

}



LocusGrid.prototype.setMetaData= function(){ 
                var self = this;
                self.addCheckSelectColumn();
                var view={
                                name:"view_sequence",
                                label:"<span class='glyphicon glyphicon-eye-open'></span>",
                                display_order:1,
                                not_write:true
                
                }
                self.addNewColumn(view);
             
               
                self.addNewColumn({name:"locus",label:"Locus",display_order:2});
                self.addNewColumn({name:"status",label:"status",display_order:3});
                self.addNewColumn({name:"contig",label:"Contig",display_order:4});
                self.addNewColumn({name:"start",label:"Start",datatype:"integer",display_order:5});
                self.addNewColumn({name:"end",label:"End",datatype:"integer",display_order:6});
                self.addNewColumn({name:"direction",label:"Dir",display_order:7});
                self.addNewColumn({name:"allele_id",label:"ID",datatype:"integer",display_order:8});
                self.addNewColumn({name:"description",label:"description",display_order:9});
                self.columnWidths['direction'] = 25; 
                  self.columnWidths['view_sequence'] = 25;
                self.columnWidths['description'] = 300;
               
}

  












LocusGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Jobs#"+msg,'newwindow','width=1000, height=600');
};



LocusGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){       
        return [];
};

