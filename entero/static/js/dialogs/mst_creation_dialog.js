MSTCreationDialog.prototype = Object.create(null);
MSTCreationDialog.prototype.constructor= MSTCreationDialog;


/**
* Creates a dialog from which the user can create a minimum spanning tree
* The actual dialog is not created, but will be created on the first  call to show
* @constructor
* @param {SRAValidationGrid} strain_gird The table from which the MS Tree will be created from
* @param {function} callback A callback, which is fired when the tree has been sent, which
* should accept the tree name,id and parent workspace if appropriate
*/
function MSTCreationDialog(strain_grid,callback){
        this.strain_grid = strain_grid;
        this.experiment_grid=null;
        this.algorithms={  
                                        "ninja":"NINJA NJ",
					"MSTreeV2":"MSTree V2", 
					"MSTree":"MSTree",
//					"NJ":"Neighbour Joining",
					"RapidNJ":"RapidNJ"
				};
	this.treeSentCallback=callback;
}

/**
* Shows the dialog (if the Experiment Grid supplied supports an MSTree)
* @param {ExperimentGrid} experiment_grid - The table used to get the information for creating the tree
*/
MSTCreationDialog.prototype.show= function(experiment_grid,parent_workspace){
	this.experiment_grid=experiment_grid;
	this.parent_workspace=parent_workspace;
        if (! rightGrid.makeMSTree){
		Enterobase.modalAlert("The Experimental data does not support a minimal spanning tree. Please change it to an MLST scheme.","Warning");
		return;
	}
        if (! this.div){
                this._createDialog();
        }
        else{
		this._update();
                this.div.dialog("open");	
        }
}


//updates the view depending on the number of (selected) strains in the table
MSTCreationDialog.prototype._update = function(){
	this.warning_div.empty();
	this.name_input.focus();
	$("#mst-creation-submit").attr("disabled",false);
	var sel = this.selected_check.prop("checked");
	var info=this.experiment_grid.getSTCount(sel);
	var strains = info['strain_count'];
	var node_number = info['st_count']

	this.strain_number_text.text(strains);
	var suggest_name = this.experiment_grid.scheme_name+"_Tree_"+ new Date().toJSON().slice(0,10);
	if (this.parent_workspace){
		suggest_name=this.parent_workspace[0]+"_"+suggest_name;
	
	}
	this.name_input.val(suggest_name);
	var ws_name = this.parent_workspace?this.parent_workspace[0]:"None";
	this.workspace_name_text.text(ws_name)
	
	this.scheme_text.text(this.experiment_grid.scheme_name);
	
	this.node_number_text.text(node_number);
	if (node_number<2){
		var msg = "<br><b>Error</b><br>You only have "+node_number+ " ST and the GrapeTree Tree will fail ";
				msg += "Please close the dialog and either load more strains or  select a different "+
				"scheme in the experiment drop down (e.g cgMLST or wgMLST)";
		this.warning_div.html(msg);
		$("#mst-creation-submit").attr("disabled",true);
		return;
	
	}
	if (node_number<5){
		var msg = "<br><b>Warning</b><br>You only have "+ node_number+ " different ST(s) in your data "
				msg += "You can make the tree more discriminatory by selecting a different scheme in "+
				"the experiment drop down (e.g cgMLST or wgMLST)"
		this.warning_div.html(msg)
	}

}

MSTCreationDialog.prototype.showError= function(msg){
	Enterobase.modalAlert(msg);
}

MSTCreationDialog.prototype._submitTree=function(){
	var self = this;
	var ST_list = this.experiment_grid.getSTList(this.selected_check.prop("checked"));
	var ws_id = 0;
	if (this.parent_workspace){
		ws_id=this.parent_workspace[3];
	
	}
	
	to_send= {
		ST_list:ST_list,
		scheme:this.experiment_grid.scheme,
		name:this.name_input.val(),
		workspace:ws_id,
		database:this.experiment_grid.species,
		task:this.algorithm_select.val(),
		parameters:{
			strain_number:parseInt(this.strain_number_text.text()),
			scheme:this.experiment_grid.scheme_name,
			algorithm:this.algorithms[this.algorithm_select.val()]	
		}
	}
	Enterobase.call_restful_send_json("/create_ms_tree","POST",to_send,this.showError).done(function(data){
	     self.div.dialog("close");
	      if (data==='Failed'){
		      self.showError("There was a problem creating the MS Tree");
	      }
	      else if (data.startsWith("Error:")){
                      self.showError(data);
          }
	      else{
		      if (self.treeSentCallback){
			      var arr= data.split(":");
			      self.treeSentCallback(arr[0],arr[1],ws_id);
					   
		      }
	      
	      }
	      
      });
	
}




MSTCreationDialog.prototype._createDialog=function(){
        var self = this;
        this.div  = $("<div>");
      
        this.div.css({
                display: "-webkit-flex",
                display: "flex"
        });
        $("body").append(this.div);
        
    
        this.div.dialog({ 
                title: "Create GrapeTree",
                buttons:[
			{
			text:"Submit",
			id:"mst-creation-submit",
			click:function(e){
				self._submitTree();
			
			}
			
			
			
			}	
		]
		
		
		,
                close:function(){
                                    
                },
                autoOpen:true,
                width: 500,
                height:500 ,
              
        });
        var top_div=$("<div>");
        var table = $("<table>").css({"display":"inline-block","flloat":"left"});
	top_div.append(table).appendTo(this.div);
        var row = $("<tr>").appendTo(table).append("<td><label>Name</label></td>");
        this.name_input = $("<input>").css("width","250px");
        $("<td>").append(this.name_input).appendTo(row);
	
	row= $("<tr>").appendTo(table).append("<td><label>Algorithm</label></td>")
	var cell = $("<td>").appendTo(row);
	this.algorithm_select=$("<select>").appendTo(cell);
	for (var val in this.algorithms){
		this.algorithm_select.append($("<option>").attr("value",val).text(this.algorithms[val]));
	}
	
	row= $("<tr>").appendTo(table).append("<td><label>Selected Only</label></td>");
	cell = $("<td>").appendTo(row)
	this.selected_check=$("<input>").attr("type","checkbox").css("margin-left","3px").appendTo(cell);
	this.selected_check.click(function(e){
		self._update();	
	});  
    
        row= $("<tr>").appendTo(table).append("<td><label>Scheme</label></td>")
        this.scheme_text=$("<td>").appendTo(row);
	row= $("<tr>").appendTo(table).append("<td><label>Workspace</label></td>")
        this.workspace_name_text=$("<td>").appendTo(row);
	
	row= $("<tr>").appendTo(table).append("<td><label>No. Strains (With STs)</label></td>")
        this.strain_number_text=$("<td>").appendTo(row);
	
	
        row= $("<tr>").appendTo(table).append("<td><label>Number of Nodes (STs)</label></td>")
        this.node_number_text=$("<td>").appendTo(row);
	this.warning_div= $("<div>").appendTo(top_div);
	table.find("td").css("padding-left","3px");
	this._update();
}
