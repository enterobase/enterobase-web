BPCreationDialog.prototype = Object.create(null);
BPCreationDialog.prototype.constructor= BPCreationDialog;

/**
* Creates a dialog from which the user can create a minimum spanning tree
* The actual dialog is not created, but will be created on the first  call to show
* @constructor
* @param {SRAValidationGrid} strain_gird The table from which the MS Tree will be created from
* @param {function} callback A callback, which is fired when the tree has been sent, which
* should accept the tree name,id and parent workspace if appropriate
*/


function BPCreationDialog(strain_grid, hc_numbers,experiment_details,callback){
        this.strain_grid = strain_grid;
        this.experiment_grid=null;
        this.hc_numbers = hc_numbers;
        this.experiment_details=experiment_details;
    	this.treeSentCallback=callback;
}

/**
* Shows the dialog (if the Experiment Grid supplied supports an MSTree)
* @param {ExperimentGrid} experiment_grid - The table used to get the information for creating the tree
*/
BPCreationDialog.prototype.show= function(experiment_grid,parent_workspace){
	this.experiment_grid=experiment_grid;
	this.parent_workspace=parent_workspace;
	var schemeName = this.experiment_grid.scheme_name;

        if (! schemeName.includes("Hier")){
		Enterobase.modalAlert("The Experimental data does not support a Bubble Plot. Please change it to a Hierarchical MLST scheme","Warning");
		return;
	}

        if (! rightGrid.makeMSTree){
		Enterobase.modalAlert("The Experimental data does not support a Bubble Plot. Please change it to a Hierarchical MLST scheme","Warning");
		return;
	}
        if (! this.div){
                this._createDialog();
        }
        else{
		this._update();
                this.div.dialog("open");	
        }
}


//updates the view depending on the number of (selected) strains in the table
BPCreationDialog.prototype._update = function(){
	this.warning_div.empty();
	this.name_input.focus();
	$("#mst-creation-submit").attr("disabled",false);
	var sel = this.selected_check.prop("checked");
	var info=this.experiment_grid.getSTCount(sel);
	var strains = info['strain_count'];
	var node_number = info['st_count']

	this.strain_number_text.text(strains);
	var suggest_name = this.experiment_grid.scheme_name+"_BubblePlot_"+ new Date().toJSON().slice(0,10);
	if (this.parent_workspace){
		suggest_name=this.parent_workspace[0]+"_"+suggest_name;
	}


	this.name_input.val(suggest_name);
	var ws_name = this.parent_workspace?this.parent_workspace[0]:"None";
	this.workspace_name_text.text(ws_name)
	
	this.scheme_text.text(this.experiment_grid.scheme_name);
	
	this.node_number_text.text(node_number);
	if (node_number<2){
		var msg = "<br><b>Error</b><br>You only have "+node_number+ " ST and the GrapeTree Tree will fail ";
				msg += "Please close the dialog and either load more strains or  select a different "+
				"scheme in the experiment drop down (e.g cgMLST or wgMLST)";
		this.warning_div.html(msg);
		$("#mst-creation-submit").attr("disabled",true);
		return;
	
	}
	if (node_number<5){
		var msg = "<br><b>Warning</b><br>You only have "+ node_number+ " different ST(s) in your data. " 
				msg += "You can make the tree more discriminatory by selecting a different scheme in "+
				"the experiment drop down (e.g cgMLST or wgMLST)"
		this.warning_div.html(msg)
	}

}

BPCreationDialog.prototype.showError= function(msg){
	Enterobase.modalAlert(msg);
}

BPCreationDialog.prototype._submitTree=function(){
	var self = this;
	var ST_list = this.experiment_grid.getSTList(this.selected_check.prop("checked"));
	var ws_id = 0;
	if (this.parent_workspace){
		ws_id=this.parent_workspace[3];
	}

    this._updateHCNumbers();

	to_send= {
		ST_list:ST_list,
		scheme:this.experiment_grid.scheme,
		name:this.name_input.val(),
		workspace:ws_id,
		database:this.experiment_grid.species,
		HCnum:this.selected_hc_numbers,
		exp_details:this.experiment_details,
		strain_number:parseInt(this.strain_number_text.text())
		}

	Enterobase.call_restful_send_json("/create_bubble_plot","POST",to_send,this.showError).done(function(data){
	     self.div.dialog("close");
	      if (data==='Failed'){
		      self.showError("There was a problem creating the Bubble Plot");
	      }
	      else if (data.startsWith("Error:")){
                      self.showError(data);
          }
	      else{
		      if (self.treeSentCallback){
			      var arr= data.split(":");
			      self.treeSentCallback(arr[0],arr[1],ws_id);
					   
		      }
	      
	      }
	      
      });
	
}


BPCreationDialog.prototype._updateHCNumbers = function() {
    var selectedHC = [];
    for (var i = 0; i < this.hc_checkboxes.length; i++) {
        if (this.hc_checkboxes[i].prop("checked")) {
            selectedHC.push(this.hc_numbers[this.experiment_grid.scheme][i]);
        }
    }

    if (selectedHC.length===0){
		Enterobase.modalAlert("Please select at least one HC cluster","Warning");
		throw new Error("Please select at least one HC cluster");
		return;
		}

    this.selected_hc_numbers = selectedHC;

};



BPCreationDialog.prototype._createDialog = function() {
    var self = this;
    this.div = $("<div>");
    this.div.css({
        display: "-webkit-flex",
        display: "flex"
    });
    $("body").append(this.div);

    this.div.dialog({
        title: "Create Bubble Plot",
        buttons: [{
            text: "Submit",
            id: "mst-creation-submit",
            click: function(e) {
                self._submitTree();
            }
        }],
        close: function() {},
        autoOpen: true,
        width: 500,
        height: 500,
    });

    var top_div = $("<div>");
    var table = $("<table>").css({ "display": "inline-block", "flloat": "left" });
    top_div.append(table).appendTo(this.div);

    var row = $("<tr>").appendTo(table).append("<td><label>Name</label></td>");
    this.name_input = $("<input>").css("width", "250px");
    $("<td>").append(this.name_input).appendTo(row);

    $("<tr>").appendTo(table).css("height", "8px");

    // Add Selected Only checkbox
    row = $("<tr>").appendTo(table).append("<td><label>Selected Only</label></td>");
    cell = $("<td>").appendTo(row)
    this.selected_check = $("<input>").attr("type", "checkbox").css("margin-left", "3px").appendTo(cell);
    this.selected_check.click(function(e) {
        self._update();
    });

    $("<tr>").appendTo(table).css("height", "8px");

    // Add HC Number section
    row = $("<tr>").appendTo(table).append("<td><label>Select HC Numbers</label></td>");
    cell = $("<td>").appendTo(row);

    // Inside the BPCreationDialog.prototype._createDialog function where to create checkboxes:
this.hc_checkboxes = [];
var self = this;
var hc_numbers = this.hc_numbers[this.experiment_grid.scheme]
for (var i = 1; i <= hc_numbers.length; i++) {
    var checkbox = $("<input>").attr("type", "checkbox").attr("id", "hc" + i).appendTo(cell);
    var label = $("<label>").attr("for", "hc" + i).text(hc_numbers[i - 1]).appendTo(cell);
    checkbox.css("margin-right", "4px");
    label.css("margin-right", "8px");
    checkbox.on("change", function() {
        var selectedHC = [];
        self.hc_checkboxes.forEach(function(cb, index) {
            if (cb.prop("checked")) {
                selectedHC.push(self.hc_numbers[index]);
            }
        });
        self.selected_hc_numbers = selectedHC;
    });
    this.hc_checkboxes.push(checkbox);
}


    $("<tr>").appendTo(table).css("height", "8px");

    row = $("<tr>").appendTo(table).append("<td><label>Scheme</label></td>")
    this.scheme_text = $("<td>").appendTo(row);
    row = $("<tr>").appendTo(table).append("<td><label>Workspace</label></td>")
    this.workspace_name_text = $("<td>").appendTo(row);
    row = $("<tr>").appendTo(table).append("<td><label>No. Strains (With STs)</label></td>")
    this.strain_number_text = $("<td>").appendTo(row);
    row = $("<tr>").appendTo(table).append("<td><label>Number of Nodes (STs)</label></td>")
    this.node_number_text = $("<td>").appendTo(row);
    this.warning_div = $("<div>").appendTo(top_div);
    table.find("td").css("padding-left", "3px");
    this._update();
}
