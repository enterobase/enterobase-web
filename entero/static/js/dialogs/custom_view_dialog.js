CustomViewDialog.prototype = Object.create(null);
CustomViewDialog.prototype.constructor= CustomViewDialog;


/**
* Creates a dialog from which the user can create a custom view and add custom columns
* The dialog will automatically open.
* @constructor
* @param {list} schemes  The object containing all the schemes. If this is null then the scheme data will be loaded from the server
* @param {string} database The name of the database
* @param {EnteroiWorkspaceDialog} workspace_dialog - The workspace dialog that will be used to save the view and load views for editing.
* Can be null if not required e.g. in get mode
* @param {string} mode If 'default' or no mode is given then users can create/load/edit and save custom views as well as create custom columns. 
* In 'get' mode, users can only choose columns. The chosen columns are accessed by a callback function. See {@link CustomViewDialog#addGetCallback}
*/
function CustomViewDialog(schemes,database,workspace_dialog,mode){
        this.schemes = {};
        var self = this;
        this.workspace_dialog = workspace_dialog
        this.database = database;
        this.columns={};
        this.all_custom_columns = {};
        this.all_custom_views={};
        var self = this;
        this.custom_view_id =0;
        this.mode = mode?mode:"default"
   
        //get all the columns
         if (! schemes){
                  Enterobase.call_restful_json("/get_experiment_details?database="+database,"GET","",this.showErrorMessage).done(function(data){
                        self.schemes=data['experiments'];
                        self._init()
                  });
        
        }
        else{
                for (scheme in schemes){
                        if (schemes[scheme]['custom_view']){
                                continue;
                        }
                        this.schemes[scheme]=schemes[scheme];
                
                }
                this._init();
               
        } 
}
/**
* Adds a callback which is called in get mode when the user has chosen all the columns
* @param {function} callback - A function which should accept a single object which contains
* experiment_columns and custom_columns
*/
CustomViewDialog.prototype.addGetCallback=function(callback){
        this.getCallback=callback;
}


CustomViewDialog.prototype._init= function(){
        var self = this;
         Enterobase.call_restful_json("/get_custom_view_information?database="+this.database,"GET","",this.showErrorMessage).done(function(data){
                self.all_custom_columns = data['custom_column'];
                self.all_custom_views={};//data['custom_views'];
                self._createDialog();
        });

}


/**
* Returns the label of the custom column given its integer id
* @param {integer} id - The id of the custom column
* @retuns {string} The label of the custom column
*/
CustomViewDialog.prototype.getCustomColumnLabel = function(id){

        return this.all_custom_columns[id];
}

CustomViewDialog.prototype.showErrorMessage=function(msg){
        Enterobase.modalAlert(msg);
}

/**
* Shows the dialog
*/
CustomViewDialog.prototype.show=function(){
       this.div.dialog("open");
}

/**
* Closes the dialog
*/
CustomViewDialog.prototype.close=function(){
       this.div.dialog("close");
}


CustomViewDialog.prototype._createDialog=function(){
        var self = this;
        this.div  = $("<div>");
      
        this.locus_suggest= null;
      
        this.div.css({
                display: "-webkit-flex",
                display: "flex"
        });
        $("body").append(this.div);
        
        var buttons = null;
        var title = "Custom View Editor";
        if (this.mode === 'get'){
                title = 'Add Columns';
                buttons= [
                        {
                                id: 'cv-get-button',
                                text:"OK",
                                click: function() {        
                                        self._saveView(null,true);
                                        self.clearForm()
                                        self.close();
                                }                      
                        }     
                ];
        
        }
        else{
                buttons = [
                      
                        {
                                id: 'cv-save-button',
                                text:"Save",
                                click: function() {
                                     
                                        self._saveView(self.custom_view_id)
                                }
                        },
                        {                           
                                id: 'cv-save-as-button',
                                text:"Save As",
                                click: function() {
                                        self._saveView()
                                }
                        }             
                ];
        }
        
        this.div.dialog({ 
                title: title, 
                buttons:buttons,
                close:function(){
                                    
                },
                autoOpen:true,
                width: '650px',
                height:'auto' ,
                position:{my:"center",at:"top",of:window}
        });
        var left_panel = $("<div>").css( { "-webkit-flex": "1", 
                                                                "-ms-flex": "1",      
                                                                "flex": "1",
                                                                "padding":"3px"});
        var right_panel =$ ("<div>").css( { "-webkit-flex": "1", 
                                                                "-ms-flex": "1",      
                                                                    "flex": "1",
                                                                    "padding":"3px"});
        this.div.append(left_panel).append(right_panel);
        //************Add Experiment
        left_panel.append($("<label>").html("Add Experiment Column").css("font-size","14px"));
        var experiment_div = $("<div>").css({"border":"solid #c3bebe","border-width":"1px","padding":"6px","margin-bottom":"6px"});
        left_panel.append(experiment_div);
        
        
        var scheme_chooser = $("<select>").attr("id","scheme-name-select").change(function(e){
                self._setExperimentPanel($(this).val());
        
        });
        for (var scheme in this.schemes){
                scheme_chooser.append($("<option>")
                        .attr("value",scheme)
                        .text(this.schemes[scheme]['label'])
                );
        
        }
        experiment_div.append($("<label>Experiment</label><br>"));
        experiment_div.append(scheme_chooser);
        experiment_div.append($("<br><label>Column</label><br>"));
        experiment_div.append($("<select>").attr("id","scheme-field-select").css("width","160px"));
        var button= $("<button>").html("Add>>").attr("id","experiment-column-add-button").click(function(e){
                self._addExperimentColumn();
        });
        experiment_div.append(button);
        experiment_div.append($("<br><label>Locus</label><br>"));
        experiment_div.append($("<input>").attr({"id":"scheme-locus-select","size":"16"}));
       
       
        this.locus_suggest=new Enterobase.LocusSuggest("scheme-locus-select",this.database,scheme_chooser.val(),
                                                        function(name,label){
                                                                self._addExperimentLocus(name,label)
                                                        });
        this._setExperimentPanel(scheme_chooser.val());
        
        
        //****** Add a custom column
        left_panel.append($("<label>").html("Add Custom Column").css("font-size","14px"));
        var add_column_div = $("<div>").css({"border":"solid #c3bebe","border-width":"1px","padding":"6px","margin-bottom":"6px"});
        add_column_div.append($("<label>Column Name</label><br>"));
        var add_column_select = $("<select>").attr("id","custom-column-select").css("width","120px");
        add_column_div.append(add_column_select);
        
        for (var id in this.all_custom_columns){     
                add_column_select.append($("<option>")
                        .attr("value",id)
                        .text(this.all_custom_columns[id]));
        }
        button= $("<button>").html("Add>>").attr("id","experiment-column-add-button").click(function(e){
                self._addCustomColumn();
        });
                        
       
        add_column_div.append(button)
        left_panel.append(add_column_div);
        
        
        
         //************Create Custom Column
         if (this.mode !== 'get'){
                left_panel.append($("<label>").html("Create Custom Column").css("font-size","14px"));
                var new_column_div = $("<div>").css({"border":"solid #c3bebe","border-width":"1px","padding":"6px","margin-bottom":"6px"});
                new_column_div.append($("<label>Name</label><br>"));        
                new_column_div.append($("<input>").attr("id","new-column-name"));
                new_column_div.append($("<br><label>Datatype</label><br>"));
                var new_column_datatype = $("<select>").attr("id","new-column-datatype");
                new_column_div.append(new_column_datatype);
                var datatypes= [["text","Text"],["integer","Integer"],["double","Double"]];
                for (var index in datatypes){
                        var value = datatypes[index][0];
                        var text = datatypes[index][1];
                        new_column_datatype.append($("<option>")
                                .attr("value",value)
                                .text(text));     
                }
                button= $("<button>").html("Create").attr("id","custom-column-add-button").click(function(e){
                                                                                self._createCustomColumn();
                                                                        }).css("margin-left","6px");
                new_column_div.append(button);
                left_panel.append(new_column_div);
        }
        
        
        
        
        //****************column list
        if (this.mode !== 'get'){
                right_panel.append($("<label>").html("Custom View:").css("font-size","14px"))
                                .append($("<span>").attr("id","cv-name").html("New").css("font-size","14px"));
                right_panel.append("<br>")
                button = $("<button>").html("Load").css("margin-left","3px").click(function(e){
                        self.loadView();
        });
        right_panel.append(button);
        }
        button = $("<button>").html("Clear").css("margin-left","3px").click(function(e){
                self.clearForm()
        });
        right_panel.append(button);        
        
        
        var view_div = $("<div>").css({"padding":"4px","margin-bottom":"6px"});
        
        //view_div.append($("<label>").html("Name:"));
        //var view_name = $("<input>").attr("id","custom-view-name");
        //view_div.append(view_name);
         view_div.append($("<br><label>Columns</label><br>").css("font-size","14px"));
        var column_list = $("<list>").attr("id","custom-column-list").css({"list-style-type": "none","margin": "0","padding": "0", "width": "60%" });
        view_div.append(column_list); 
        right_panel.append(view_div);
        column_list.sortable();
        //A hack to get the buttons to display
      
        //$('#cv-save-button').button('option', 'label', "Save");
       // $('#cv-save-as-button').button('option', 'label', "Save As");
        this.div.css({"font-size":"13px"});
        
       
}



/**
* This method is called after a view is saved/created. Should be provided if you want to do anything with the view once created e.g.
* <pre>
*  customview.viewCreated=function(id,name){
*        //do something with id and name
*}
* </pre>
* @param {integer} id The id of the view
* @param {string} name The name of the view
*/
CustomViewDialog.prototype.viewCreated=function(id,name){};


/**
* Saves the custom view according to the data in the dialog. if an id is given, the view with that id will be overwritten,
* otherwise a new view will be created. To perform any action after creation, use {@link CustomViewDialog#viewCreated}
* @param {integer} id The id of the view or nothing if a new view is to be created
*/
CustomViewDialog.prototype._saveView= function(view_id){
      var ids  = $("#custom-column-list").sortable("toArray");
      var display_order =0;
      var exp_cols = [];
      var cust_cols =[];
      for (var index in ids){
                var id = ids[index];
                var column= this.columns[id];
                if (column['scheme']==="custom_column"){
                        cust_cols.push({id:column['name'],"display_order":display_order});
                        display_order++;             
                }
                else{
                        column['display_order']=display_order;
                        exp_cols.push(column);
                        display_order++;
                
                }
      }
      if (this.mode === 'get'){
                this.getCallback({experiment_columns:exp_cols,custom_columns:cust_cols});
                return;
      }
      var self = this;
      if (view_id){
                var to_send ={
                        database:self.database,
                        data:JSON.stringify({experiment_columns:exp_cols,custom_columns:cust_cols}),
                        id:view_id,
                        name:name
                };
                 Enterobase.call_restful_json("/save_custom_view","POST",to_send,this.showErrorMessage).done(function(data){
                        if (data['msg']==="OK"){
                                 Enterobase.modalAlert("The view "+name+" has been updated");
                        
                        }
                        else
                           if (data['msg']==="Permission Error")
                                {
                                   Enterobase.modalAlert("The view "+name+" has not been updated, "+data['msg']);
                                }
                });
                return;
      }
      this.workspace_dialog.showSaveDialog(function(name){
                var to_send ={
                        database:self.database,
                        data:JSON.stringify({experiment_columns:exp_cols,custom_columns:cust_cols}),
                        id:view_id,
                        name:name
                };
                
                
                Enterobase.call_restful_json("/save_custom_view","POST",to_send,this.showErrorMessage).done(function(data){
                        if (data['msg']==="OK"){
                              
                                        
                                        Enterobase.modalAlert("The view "+name+" has been saved.");
                                        self.workspace_dialog.addNewWorkspace([name,'mine','mine',data['id'],'custom_view']);
                                        self.viewCreated(data['id'],name);
                                        self.clearForm();
                                        self.close();
                                        
                                        
                              
                        }
                        else if (data['msg']==='Exists'){
                                 Enterobase.modalAlert("The view "+name+" already exists. Please choose another");
                        
                        }
                         else if (data['msg']==='Error'){
                                 Enterobase.modalAlert("There was problem. If this persists, please contact an administrator.");
                        }
                });               
      },"Custom View");       
}

/**
* Removes all data from the form, apart from the view name.
*/
CustomViewDialog.prototype.clearForm= function(){
        //$("#custom-view-name").val();
        this.columns={};
        this.custom_view_id=0;
        $("#cv-name").html("New");
        $("#custom-column-list").empty();
        

}

CustomViewDialog.prototype._createCustomColumn= function(){
        var label = $("#new-column-name").val();
        if (!label){
                return;
        }
        var  datatype = $("#new-column-datatype").val();
        to_send={
                label:label,
                datatype:datatype,
                database:this.database
        }
        var self = this;
          Enterobase.call_restful_json("/add_custom_column","POST",to_send,this.showErrorMessage).done(function(data){
                if (data['msg']==='Exists'){
                        Enterobase.modalAlert("The column name already exists");
                        
                }
                else if (data['msg']==='OK'){
                        self.all_custom_columns[data['id]']]=label;
                        $("#custom-column-select").append($("<option>")
                        .attr("value",data['id'])
                        .text(label));
                        Enterobase.modalAlert("A new custom column has been created - You can add it the current view by selecting it in the Add Custom Column panel")
                }
        });    
}



CustomViewDialog.prototype._addExperimentLocus= function(name,label){
       
        if (!name){
                return;
        }
        var scheme = $("#scheme-name-select").val();
       
     
        this._addColumn(name,scheme,label,"integer");
}

CustomViewDialog.prototype._addCustomColumn= function(){
        var column_option =$("option:selected", $('#custom-column-select'));
        var name = $('#custom-column-select').val();
        var scheme ="custom_column";
        var label = column_option.text();
     
        this._addColumn(name,scheme,label);
}

CustomViewDialog.prototype._addExperimentColumn= function(){
        var column_option =$("option:selected", $('#scheme-field-select'));
        var scheme_option = $("option:selected", $('#scheme-name-select'));
        var label = column_option.text()+"("+scheme_option.text()+")";
        var scheme = $("#scheme-name-select").val();
        var name=$("#scheme-field-select").val();
        var datatype = column_option.data("datatype");
        this._addColumn(name,scheme,label,datatype,true);
}



/**
* Loads a view into the dialog, so a user can inspect/modify it
* @param {integer} id The id of the view
*/
CustomViewDialog.prototype.loadView = function(view_id){
        var self =this;
        this.workspace_dialog.getSelectedIDs(function(ids,ws){
                Enterobase.call_restful_json("/get_custom_view?view_id="+ws[0][3],"GET","",this.showErrorMessage).done(function(data){
                        self.clearForm();
                        $("#cv-name").html(ws[0][0]);
                        self.custom_view_id=ws[0][3];
                        data.sort(function(a,b){
                                return a.display_order-b.display_order
                        });
                        for (var index in data){
                                var col = data[index];
                                non_locus=false;
                                if (col['non_locus']==='true'){
                                        non_locus=true;
                                
                                }
                                self._addColumn(col['name'],col['scheme'],col['label'],col['datatype'],non_locus)
                      
                                                   
                        }
        });
        
        },"Load",{types:['custom_view'],owner_only:true})

}

CustomViewDialog.prototype._addColumn= function(name,scheme,label,datatype,non_locus){
        var self = this;
        var id  = name+"-"+scheme;
        if (this.columns[id]){
                return;
        }
          var column= {
                name:name,
                scheme:scheme,
                datatype:datatype,
                label:label
              
        };
        if (non_locus){
                column['non_locus']='true';
        }
        this.columns[id]= column;
        var li = $("<li>")
                                        .css({"margin": "0 2px 2px 2px","padding": "3px 0px 3px 4px", "background-color":"white"})
                                        .attr("id",id);
        li.append($("<span>").html(label).attr("id",id+"-text"));
        $("#custom-column-list").append(li);
        if (scheme !== "custom_column"){
                var edit_icon = $("<span>").attr({"class":"glyphicon glyphicon-edit ws-sum-hide-show","id":id+"-icon"})
                                        .css({"cursor":"pointer","margin-left":"4px"})
                                        .click(function(e){
                                                li.find("span").hide();
                                                var text =$("#"+id+"-text");
                                                var input = $("<input>").val(text.text()).keypress(function(e){
                                                        if (e.which===13){
                                                              
                                                                self.columns[id].label = $(this).val();
                                                                text.html($(this).val())
                                                                $(this).remove();
                                                                li.find("span").show();
                                                        }
                                        });
                       
                     
                           
                        text.html("");
                        $(this).parent().append(input);
                
                });
                li.append(edit_icon);
        }
        var delete_icon = $("<span>").attr("class","glyphicon glyphicon-remove ws-sum-hide-show")
                                                        .css({"cursor":"pointer","margin-left":"4px"});
        delete_icon.click(function(e){
                       delete self.columns[id];
                       $("#"+id).remove();
                
        });
        li.append(delete_icon);
         li.find("span")
}



CustomViewDialog.prototype._setExperimentPanel= function(scheme){
        var has_alleles= false;
        var fields = this.schemes[scheme]['fields'];
        var field_select = $("#scheme-field-select");
        var locus_select = $("#scheme-locus-select")
        field_select.empty();
        locus_select.val("");
        for (var index in fields){
                var field= fields[index];
                if (field['group_name']){
                        continue;
                }
                var  field = fields[index];
                field_select.append($("<option>")
                        .attr("value",field['name'])
                        .text(field['label'])
                        .data("datatype",field['datatype']) 
                        );
        }
        if (this.schemes[scheme]['mlst']){
                this.locus_suggest.setScheme(scheme);
                locus_select.removeAttr("disabled");
        }
        else{
                locus_select.attr("disabled","disabled").val("");
        }
}