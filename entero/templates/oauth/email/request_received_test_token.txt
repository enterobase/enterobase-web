Dear {{ local_enterobase_client.admin.username }},

We have received your request to register your Local Enterobase instance '{{ local_enterobase_client.client_name }}'. 
Please perform the upload test in the Local Enterobase installer using the following test token: {{ local_enterobase_client.test_token }}.

For your reference, here are the credentials that you have provided us with:
Local Enterobase Name: {{ local_enterobase_client.client_name }}
Local Enterobase URL: {{ local_enterobase_client.client_uri }}
Descriptions: {{ local_enterobase_client.description }}
Administrator: {{ user.username }}

Sincerely,

The Enterobase Team

Note: replies to this email address are not monitored.