Dear {{ local_enterobase_client.contact_name }},

We regret to inform you that your request to register your Local Enterobase instance '{{ local_enterobase_client.client_name }}' has been denied.

For your reference, here are the credentials that you provided us with:
Local Enterobase Name: {{ local_enterobase_client.client_name }}
Local Enterobase URL: {{ local_enterobase_client.client_uri }}
Contact Name: {{ local_enterobase_client.contact_name }}
Contact Email: {{ local_enterobase_client.contact_email }}
Descriptions: {{ local_enterobase_client.description }}
Department: {{ local_enterobase_client.contact_email }}
Institution: {{ local_enterobase_client.institution }}
City: {{ local_enterobase_client.city }}
Country: {{ local_enterobase_client.country }}


If you believe your request was wrongfully denied you can reapply via the same form.

Sincerely,

The Enterobase Team

Note: replies to this email address are not monitored.