#!/bin/sh
home_dir=~/enterobase-web/
python_path=~/.pyenv/versions/entero/bin
cd $home_dir
echo $home_dir
dates="+%F %X"
#high priority (0 by default)
#all jobs sent to 'backend' by default, but this can changed
for dbase in senterica ecoli yersinia clostridium streptococcus vibrio
do
      	printf "$(date "$dates") DAEMON - Processing $dbase data from NCBI\n" >> "$home_dir/logs/entero.log"
        $python_path/python $home_dir/manage.py update_assemblies  -l 250 -d $dbase
        $python_path/python $home_dir/manage.py update_all_schemes  -l 120 -d $dbase
        $python_path/python $home_dir/manage.py importSRA -r 10 -d $dbase
#	Skip this because there are problems with it:  Nigel
#	$python_path/python $home_dir/manage.py check_queued_assemblies -d $dbase
    #    /home/admin/venv/bin/python $home_dir/manage.py  generate_strain_table_dump -d $dbase

done

#low priority (-p 9)
for dbase in klebsiella listeria helicobacter  mcatarrhalis porphyromonas treponema
do
      	printf "$(date "$dates") DAEMON - Processing $dbase data from NCBI\n" >> "$home_dir/logs/entero.log"
        $python_path/python $home_dir/manage.py update_assemblies -p 9 -l 150 -d $dbase
        $python_path/python $home_dir/manage.py update_all_schemes -p 9 -l 75 -d $dbase
        $python_path/python $home_dir/manage.py importSRA -r 10 -d $dbase
#       $python_path/python $home_dir/manage.py check_queued_assemblies -d $dbase
done

printf "$(date "$dates") DAEMON - finished\n" >> $home_dir/logs/entero.log
