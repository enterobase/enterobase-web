#!/bin/bash

BACKUP_DIR=/share_space/backups/postgresql/nserv/
POSTGRES_SERVER=hermes.warwick.ac.uk
PORT=6543

FULL_BACKUP_QUERY="select datname from pg_database where not datistemplate and datallowconn $EXCLUDE_SCHEMA_ONLY_CLAUSE order by datname;"

databases_list="$(psql -h $POSTGRES_SERVER -p $PORT -U zhemin -At -c "$FULL_BACKUP_QUERY" postgres)"

for database in $databases_list; 
do 
echo "dumping $database ..."
pg_dump -C -h $POSTGRES_SERVER -p $PORT -U zhemin $database | gzip >/$BACKUP_DIR/$database'.db.gz'
done

