#!/bin/bash
BACKUP_DIR=/share_space/backups/postgresql/crobot/
echo "dumping  CRobotDB..."
pg_dump -C -h enterobase-2.warwick.ac.uk -U admin CRobotDB | gzip >/$BACKUP_DIR/'CRobotDB.db.gz'
