#!/bin/bash
DAYS_TO_KEEP=7
DAY_OF_WEEK_TO_KEEP=6
WEEKS_TO_KEEP=5

BACKUP_DIR=/share_space/backups/$1/enterobase/

POSTGRES_SERVER=$2
POSTGRES_PORT=$3
POSTGRES_USER=$4

if [ $# -gt 4 ]
  then
  WEEKS_TO_KEEP=$5
  fi
if [ $# -gt 5 ]
  then
  DAYS_TO_KEEP=$6
  echo "Days to keep = $DAYS_TO_KEEP"
  fi
if [ $# -gt 6 ]
  then
  DAY_OF_WEEK_TO_KEEP=$7
  fi

function backup()
{
SUFFIX=$1

FINAL_BACKUP_DIR=$BACKUP_DIR"`date +\%Y-\%m-\%d`$SUFFIX/"
if ! mkdir -p $FINAL_BACKUP_DIR; then
		echo "Cannot create backup directory in $FINAL_BACKUP_DIR. Go and fix it!" 1>&2
		exit 1;
	fi;

FULL_BACKUP_QUERY="select datname from pg_database where not datistemplate and datallowconn $EXCLUDE_SCHEMA_ONLY_CLAUSE order by datname;"

databases_list="$(psql -h $POSTGRES_SERVER -p $POSTGRES_PORT -U $POSTGRES_USER -At -c "$FULL_BACKUP_QUERY" postgres)"

for database in $databases_list; 

do 
echo "dumping $database ..."
pg_dump -C -h $POSTGRES_SERVER -p $POSTGRES_PORT -U $POSTGRES_USER $database | gzip >/$FINAL_BACKUP_DIR/$database'.db.gz'
done
}

# Delete all monthly archives as not sensibly supported
find $BACKUP_DIR -maxdepth 1 -name "*-monthly" -exec rm -rf '{}' ';'

if [ $WEEKS_TO_KEEP = 0 ]
then
	find $BACKUP_DIR -maxdepth 1 -name "*-weekly" -exec rm -rf '{}' ';'
else
  DAY_OF_WEEK=`date +%u` #1-7 (Monday-Sunday)
  echo "Day of the week: $DAY_OF_WEEK"
  EXPIRED_DAYS=`expr $((($WEEKS_TO_KEEP * 7) + 1))`
  if [ $DAY_OF_WEEK = $DAY_OF_WEEK_TO_KEEP ]
  then
    # Delete all expired weekly directories
    find $BACKUP_DIR -maxdepth 1 -mtime +$EXPIRED_DAYS -name "*-weekly" -exec rm -rf '{}' ';'

    backup "-weekly"
    exit 0;
  fi
fi


echo "deleting old backup folder in $BACKUP_DIR"

if [ $DAYS_TO_KEEP = 1 ]
then
  find $BACKUP_DIR -maxdepth 1 -name "*-daily" -exec rm -rf '{}' ';'
else
  DAYS_TO_KEEP=`expr $(($DAYS_TO_KEEP -2))`
  find $BACKUP_DIR -maxdepth 1 -mtime +$DAYS_TO_KEEP -name "*-daily" -exec rm -rf '{}' ';'
fi
backup "-daily"

