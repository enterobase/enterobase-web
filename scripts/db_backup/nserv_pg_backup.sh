#!/bin/bash
DAYS_TO_KEEP=7
DAY_OF_WEEK_TO_KEEP=6
WEEKS_TO_KEEP=5

BACKUP_DIR=/share_space/backups/$1/nserv/

POSTGRES_SERVER=$2
POSTGRES_PORT=$3
POSTGRES_USER=$4

function backup()
{
SUFFIX=$1

FINAL_BACKUP_DIR=$BACKUP_DIR"`date +\%Y-\%m-\%d`$SUFFIX/"
if ! mkdir -p $FINAL_BACKUP_DIR; then
		echo "Cannot create backup directory in $FINAL_BACKUP_DIR. Go and fix it!" 1>&2
		exit 1;
	fi;

FULL_BACKUP_QUERY="select datname from pg_database where not datistemplate and datallowconn $EXCLUDE_SCHEMA_ONLY_CLAUSE order by datname;"

databases_list="$(psql  -h $POSTGRES_SERVER -p $POSTGRES_PORT -U $POSTGRES_USER -At -c "$FULL_BACKUP_QUERY" postgres)"

for database in $databases_list; 
#do
#echo "line: $database"
#done


do 
echo "dumping $database ..."
pg_dump -C -h $POSTGRES_SERVER -p $POSTGRES_PORT -U $POSTGRES_USER  $database | gzip >/$FINAL_BACKUP_DIR/$database'.db.gz'
done
}

DAY_OF_MONTH=`date +%d`
echo "DAY_OF_MONTH: $DAY_OF_MONTH"
if [ $DAY_OF_MONTH -eq 1 ];
then
	# Delete all expired monthly directories
	find $BACKUP_DIR -maxdepth 1 -name "*-monthly" -exec rm -rf '{}' ';'
 
	backup "-monthly"
 
	exit 0;
fi


DAY_OF_WEEK=`date +%u` #1-7 (Monday-Sunday)
echo "Day of the week: $DAY_OF_WEEK"
EXPIRED_DAYS=`expr $((($WEEKS_TO_KEEP * 7) + 1))`
if [ $DAY_OF_WEEK = $DAY_OF_WEEK_TO_KEEP ];
then
	# Delete all expired weekly directories
	find $BACKUP_DIR -maxdepth 1 -mtime +$EXPIRED_DAYS -name "*-weekly" -exec rm -rf '{}' ';'
 
	backup "-weekly"
 
	exit 0;
fi


echo "delating old backup folder in $BACKUP_DIR"

find $BACKUP_DIR -maxdepth 1 -mtime +$DAYS_TO_KEEP -name "*-daily" -exec rm -rf '{}' ';'
 
backup "-daily" 

