#!/bin/bash
BACKUPDIR=$(ls -t //share_space/backups/postgresql/enterobase/| head -1)
psotgres_server=localhost
port=5432


#get the latest back folder
main_folder="/share_space/backups/postgresql/enterobase/"

BACKUPDIR=$(ls -t $main_folder | head -1)
echo $BACKUPDIR

for entry in "$main_folder/$BACKUPDIR"/*
do
#get the file name  from the full path
   f="$(basename -- $entry)"
   echo $f
   # get the dbase name from the file by removing the extension
   db=$(basename  $entry .db.gz)
   echo "Restore database $db from file: $entry"
   #delete the database, create a new one, add permsion, then restore the data from a backup file
   psql -h $psotgres_server -p $port  -U postgres -c "drop database $db"
   psql -h $psotgres_server -p $port  -U postgres -c "create DATABASE $db"
   psql -h $psotgres_server -p $port  -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $db to enterobase"
   gunzip -c $entry | psql -h $psotgres_server -p $port -U postgres -d $db

done
# psql -h $psotgres_server -p $port  -U postgres -c "drop database miu"


# psql -h $psotgres_server -p $port  -U postgres -c "create DATABASE miu"
#   psql -h $psotgres_server -p $port  -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE miu to enterobase"
#gunzip -c /share_space/backups/postgresql/enterobase/2019-06-14-daily/miu.db.gz| psql -h $psotgres_server -p $port -U postgres -d miu

echo "Done ... "







