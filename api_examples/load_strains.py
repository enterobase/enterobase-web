from urllib.request import urlopen
from urllib.error import HTTPError
import logging
import urllib
import base64
import ujson as json
import os

# You must have a valid API Token
API_TOKEN = os.getenv('ENTEROBASE_API_TOKEN', None)
# SERVER_ADDRESS = 'http://enterobase-1:8002'
SERVER_ADDRESS = 'https://enterobase.warwick.ac.uk'
SEROTYPE = 'Agona'
DATABASE = 'senterica'
SCHEME = 'cgMLST_v2'

BATCH_SIZE = 5000
BATCH_SIZE_ST = 500

def __create_request(request_str):
    base64string = base64.b64encode('{0}: '.format(API_TOKEN).encode('utf-8'))
    headers = {"Authorization": "Basic {0}".format(base64string.decode())}
    request = urllib.request.Request(request_str, None, headers)
    return request


if not os.path.exists('temp'):
    os.mkdir('temp')

# serotype = "serotype=%s&" % SEROTYPE
serotype = ''
address = SERVER_ADDRESS + '/api/v2.0/%s/straindata?%sassembly_status=Assembled&limit=%d&' \
        % (DATABASE, serotype, BATCH_SIZE)

#  First get the core metadata in batches
try:
    results = {}
    count = 0
    more_data = True
    while more_data:
        response = urlopen(__create_request(address))
        data = json.load(response)
        results.update(data["straindata"])
        count += BATCH_SIZE
        print("Downloaded " + str(count) + " strains")
        if "next" in data["links"]["paging"] and len(data["straindata"]):
            address = data["links"]["paging"]["next"]
        else:
            more_data = False
    print("Number of downloaded strains = " + str(len(results)))

    #  Now get all the cgMLST STs and put them in a dictionary
    cgMLST_list = {}
    for strain in results.items():
        for st in strain[1]["sts"]:
            if st["scheme_name"] == SCHEME:
                st_id = st["st_id"]
                if st_id not in cgMLST_list:
                    cgMLST_list.update({st_id: {}})

    print ("Strains are associated with " + str(len(cgMLST_list)) + " " + SCHEME + " sequence types")

    #  Fetch the HC information for the STs in batches
    ST_list = list(cgMLST_list.keys())
    count = 0
    while count < len(cgMLST_list):
        STs = ','.join(str(st) for st in ST_list[count:count+BATCH_SIZE_ST])
        if not STs:
            break
        address = SERVER_ADDRESS + '/api/v2.0/senterica/{0}/sts?scheme={0}&st_id={1}'.format(SCHEME, STs)
        response = urlopen(__create_request(address))
        data = json.load(response)
        #  Place the returned HC information into a dictionary
        for ST in data['STs']:
            cgMLST_list[int(ST['ST_id'])] = ST["info"]["hierCC"]
        count += BATCH_SIZE_ST
        print(str(count) + " sets of HC data downloaded")

    for strain in results.items():
        #  We get info for all the schemes, so look for cgMLST data
        for st in strain[1]["sts"]:
            if st['scheme_name'] == SCHEME:
                #  Add the HC info for the scheme
                st['HC'] = cgMLST_list[st['st_id']]

    with open(os.path.join('temp', 'json.txt'), 'w') as json_data:
        json_data.write(json.dumps(results, indent=2))

except HTTPError as Response_error:
    logging.error('%d %s. <%s>\n Reason: %s' %(Response_error.code,
                                              Response_error.reason,
                                              Response_error.geturl(),
                                              Response_error.read()))