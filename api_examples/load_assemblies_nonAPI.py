import requests,os

site_url = "https://enterobase.warwick.ac.uk/"
username = os.environ['USERNAME']
password = os.environ['PASSWORD']
database = "senterica"

s = requests.Session()
headers = {'User-Agent': 'Mozilla/5.0'}
payload = {'email': username, 'password': password, 'remember_me': 'y', 'submit': 'Log In'}
s.post('{0}/auth/login'.format(site_url), headers=headers, data=payload)
assemblies_file = open('assemblies.txt', 'r')
assemblies = assemblies_file.readlines()
for assembly in assemblies:
    assembly = assembly.strip()
    fileURL = '{0}upload/download?assembly_barcode={1}&database={2}&file_format=fasta'.format(site_url, assembly,
                                                                                              database)
    webpage = s.get(fileURL).content

    print ('outputting {0}'.format(assembly))
    output_file = open('{0}.fa.gz'.format(assembly), 'wb')
    output_file.write(webpage)
    output_file.close()
